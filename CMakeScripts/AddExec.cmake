#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#
##                     ADD__EXEC MACRO                       ##
#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#
#LINK_DIRECTORIES(  /home/sci/kpotter/Code/kpbr2.0/build/amino/ )
MACRO( ADD_EXEC EXEC LIBS FILES)
  SET( EXEC_EXE ${EXEC} )                                          # Set the executable
  SET( EXEC_SRC ${FILES} )                                         #  Set the sources
  ADD_EXECUTABLE( ${EXEC_EXE} MACOSX_BUNDLE  ${EXEC_SRC} )         # Add srcs to exec
  TARGET_LINK_LIBRARIES( ${EXEC_EXE} ${LIBS} )                     # Link the libs
  IF(APPLE)
    TARGET_LINK_LIBRARIES(${EXEC_EXE} "-framework CoreFoundation") # Add apple frmwrk
  ENDIF(APPLE)
ENDMACRO( ADD_EXEC EXEC FILES )

#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#
#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#
