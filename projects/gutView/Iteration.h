// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// Iteration.h
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#ifndef _ITERATION_H
#define _ITERATION_H
#include "BoundingBox.h"

/*
 * Class to hold each iteration
 */
class Iteration{

 public:

  /// Constructor/Destructor
 Iteration(int iterate, int time, std::string unit) : 
  _iterate(iterate), _time(time), _unit(unit){}
  virtual ~Iteration(){}

  /// Set/get the iteration number
  void setIterate(int iterate){ _iterate = iterate; }
  int getIterate(){ return _iterate; }
  bool isIterate(int iterate, int time, std::string unit){ 
    return (_iterate == iterate) && (_time == time) && (_unit == unit); 
  }

  /// Set/get time
  void setTime(float time){ _time = time;}
  int getTime(){ return _time; }
  
  /// Set/get unit
  void setUnit(std::string unit){ _unit = unit; }
  std::string getUnit(){ return _unit; }

   // Add an agent to the state list
  void addToAgentState(Agent * agent){
    agentStateList.push_back(agent);
  }

  // Add a solute to the state list
  void addToSoluteState(Solute * solute){    
    soluteStateList.push_back(solute);
  }

  // Add a solute sum
  void addToSoluteSum(Solute * solute){
    soluteSumList.push_back(solute);
  }

  /// Set the agent state list
  void setAgentStateList(std::vector<Agent *> agents){ agentStateList = agents; }

  // Return the agent state list
  std::vector<Agent *> getAgentStateList(){ return agentStateList; }

  /// Return the solute state list
  std::vector<Solute *> getSoluteStateList(){ return soluteStateList; }

  /// Print the iteration information
  void print(){
    std::cout << "- - - Iteration - - -" << std::endl;
    std::cout << "iterate: " << _iterate << " time: " << _time << " unit: " << _unit << std::endl;
    std::cout << "num agents: " << agentStateList.size() << " num solutes: " << soluteStateList.size() << " num sums: " << soluteSumList.size() << std::endl; 
  }


 protected:
  int _iterate;
  float _time;
  std::string _unit;

  // A list of agent states
  std::vector<Agent *> agentStateList;
  
  // A list of solute states
  std::vector<Solute *> soluteStateList;
  
  // A list of solute sums
  std::vector<Solute *> soluteSumList;

};

#endif
