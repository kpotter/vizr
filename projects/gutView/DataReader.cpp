// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                       [GUT] * [VIEW]
// Project to explore gut microbe data. 
//  ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// DataReader.cpp
// Class to read in data
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#include "DataReader.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <QFile>
#include <QMap>
#include <QStringList>

/// Main read function, calls read for each folder
Simulation * DataReader::readIDynomicsData(QDir dirName){
 
  std::cout << "reading data from: " << dirName.absolutePath().toAscii().constData();

  // Create the new simulation
  Simulation * simulation = new Simulation();
  float resolution;
  int ni, nj, nk;

  // Set filters to read in only xml files
  QStringList filters;
  filters << "*.xml" ;
  dirName.setNameFilters(filters);

  // Read in the agent states
  dirName.cd("agent_State/");
  QStringList agentStates = dirName.entryList(filters);
  std::cout << std::endl <<  "agent" << agentStates.size() << std::endl;

  // If we have no agent states, return empty simulation
  if(!agentStates.size())
    return simulation;

  // For each file in the agentStates directory
  for(int i = 0; i < agentStates.size(); i++){

    int iterate;
    float time;
    std::string unit;
    //std::string species_name;
    //std::vector
    
    // Create a file from filename, and make sure we can read it
    QString filename = dirName.absolutePath()+"/"+agentStates[i];
    std::cout << "reading: " << filename.toStdString() << std::endl;
    QFile* file = new QFile(filename);
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text)) {
      std::cout << "ERROR in reading " << filename.toStdString() << std::endl;
      continue;
    }
    // Create an xml file reader
    QXmlStreamReader * xml = new QXmlStreamReader(file);
    QXmlStreamAttributes attributes = xml->attributes();

    // While we have xml tokens
    while(!xml->atEnd() && !xml->hasError()) {

      // Read next element.
      QXmlStreamReader::TokenType token = xml->readNext();
      
      // If token is just StartDocument, we'll go to next.
      if(token == QXmlStreamReader::StartDocument)
	continue;

      // If token is a StartElement, we'll see if we can read it.
      if(token == QXmlStreamReader::StartElement) {

	// idynomics element
	if(xml->name() == "idynomics")
	  continue;

	// simulation element ( read iteration information )
	if(xml->name() == "simulation"){
	  if(attributes.hasAttribute("iterate"))    
	    iterate = attributes.value("iterate").toString().toInt();
	  if(attributes.hasAttribute("time"))
	    time = attributes.value("time").toString().toFloat();
	  if(attributes.hasAttribute("unit"))
	    unit = attributes.value("unit").toString().toUtf8().constData();
	}
	
	// grid element
	if(xml->name() == "grid"){
	  if(i == 0){
	    if(attributes.hasAttribute("resolution"))
	      resolution = attributes.value("resolution").toString().toFloat();
	    if(attributes.hasAttribute("nI"))
	      ni = attributes.value("nI").toString().toInt();
	    if(attributes.hasAttribute("nJ")) 
	      nj = attributes.value("nJ").toString().toInt();
	    if(attributes.hasAttribute("nK"))
	      nk = attributes.value("nK").toString().toInt();
	    simulation->setGrid(resolution, ni, nj, nk);
	  }
	}
	
	// species element
	if(xml->name() == "species"){
	

	  
	}
      }
    }
     // readDataFile(dirName.absolutePath()+"/"+agentStates[i], simulation);
  }
  
  /* Read in the environment state */
  /*dirName.cd("../env_State/");
  QStringList envStates = dirName.entryList(filters);
  for(int i = 0; i < envStates.size(); i++)
    readDataFile(dirName.absolutePath()+"/"+envStates[i], simulation);
  if(!envStates.size())
    return NULL;
  */

  std::cout << " done." << std::endl;
  return simulation;

}

/// Read in any passed in data file
Simulation * DataReader::readDataFile(QString filename, Simulation * simulation){

  // Create a new file
  QFile* file = new QFile(filename);
 
  // Make sure we can open the file
  if (!file->open(QIODevice::ReadOnly | QIODevice::Text)) {
    std::cout << "ERROR in reading " << filename.toStdString() << std::endl;
    return  simulation;
  }

  // Create an xml file reader
  QXmlStreamReader * xml = new QXmlStreamReader(file);

  /*printf(filename.toUtf8().constData());
  return simulation;*/

  while(!xml->atEnd() && !xml->hasError()) {

    // Read next element.
    QXmlStreamReader::TokenType token = xml->readNext();

    // If token is just StartDocument, we'll go to next.
    if(token == QXmlStreamReader::StartDocument)
      continue;
    
    // If token is a StartElement, we'll see if we can read it.
    if(token == QXmlStreamReader::StartElement) {
      
      // idynomics element
      if(xml->name() == "idynomics")
	continue;
      
      // simulation element ( read iteration information )
      if(xml->name() == "simulation") 
        readIteration(xml, simulation);
     
      // grid element
      if(xml->name() == "grid" || xml->name() == "solute")
	readGrid(xml, simulation);

      // thickness element
      if(xml->name() == "thickness")
	readThickness(xml, simulation);

      // species element
      if(xml->name() == "species")
      	readAgent(xml, simulation);

      // solute element
      if(xml->name() == "solute")
	readSolute(xml, simulation);     
      
      // bulk element
      if(xml->name() == "bulk")
	readBulk(xml, simulation);
    }
  }
 
  // Error handling. 
  if(xml->hasError()) 
    std::cout << "ERROR IN XML PARSING" << std::endl;
  
  xml->clear();

 // simulation->setCurrentIteration(0);
  return simulation;
}

void DataReader::readBulk(QXmlStreamReader * xml, Simulation * simulation){
  /* std::string bulkName;
  QXmlStreamAttributes attributes = xml->attributes();
  if(attributes.hasAttribute("name"))    
    bulkName = attributes.value("name").toString().toStdString();

  while(!xml->atEnd()){
    xml->readNext();
    if(xml->name() == "solute")
      //  simulation->addSoluteSum(readSolute(xml, simulation));
    // if(xml->
    }*/
}

/// Read in a solute element
void DataReader::readSolute(QXmlStreamReader * xml, Simulation * simulation){
  std::string name, unit;
  std::vector<float> solute;
  QXmlStreamAttributes attributes = xml->attributes();
  if(attributes.hasAttribute("name"))
    name = attributes.value("name").toString().toUtf8().constData();
  if(attributes.hasAttribute("unit"))
    unit = attributes.value("unit").toString().toUtf8().constData();
  xml->readNext();
  QString data = xml->text().toString();
  //  std::cout << "data: " << data.toStdString() << std::endl;
  QStringList lines = data.split(";");
  //std::cout << "line length :" << pow(lines.length(), 1.0/3.0) << std::endl;
  QStringList::const_iterator it;
  for (it = lines.constBegin(); it != lines.constEnd(); ++it)
    if(*it != "")
      solute.push_back((*it).toFloat());  
  simulation->addSoluteState(new Solute(name, unit, solute));
}

void DataReader::readThickness(QXmlStreamReader * xml, Simulation * simulation){
  std::string domain, unit;
  float mean = 0.5, stddev = 0.0, max = 1.0;
  QXmlStreamAttributes attributes = xml->attributes();
  if(attributes.hasAttribute("domain"))    
    domain = attributes.value("domain").toString().toUtf8().constData();
  if(attributes.hasAttribute("unit"))    
    unit = attributes.value("unit").toString().toUtf8().constData();
  xml->readNext();
  if(xml->name() == "mean")
    mean = xml->text().toString().toFloat();
  xml->readNext();
  if(xml->name() == "stddev")
    stddev = xml->text().toString().toFloat();
  xml->readNext();
  if(xml->name() == "max")
    max = xml->text().toString().toFloat();
  simulation->setThickness(domain, unit, mean, stddev, max);
  //  std::cout << "thickness: " << std::endl;
}

void DataReader::readIteration(QXmlStreamReader * xml, Simulation * simulation){
  int iterate;
  float time;
  std::string unit;

  QXmlStreamAttributes attributes = xml->attributes();
  // Get the simulation attributes

  if(attributes.hasAttribute("iterate"))    
    iterate = attributes.value("iterate").toString().toInt();
  if(attributes.hasAttribute("time"))
    time = attributes.value("time").toString().toFloat();
  if(attributes.hasAttribute("unit"))
    unit = attributes.value("unit").toString().toUtf8().constData();

  simulation->setCurrentIteration(iterate, time, unit);
}

void DataReader::readGrid(QXmlStreamReader * xml, Simulation * simulation){
  QXmlStreamAttributes attributes = xml->attributes();
  float resolution;
  int ni, nj, nk;
  if(attributes.hasAttribute("resolution"))
    resolution = attributes.value("resolution").toString().toFloat();
  if(attributes.hasAttribute("nI"))
    ni = attributes.value("nI").toString().toInt();
  if(attributes.hasAttribute("nJ")) 
    nj = attributes.value("nJ").toString().toInt();
  if(attributes.hasAttribute("nK"))
    nk = attributes.value("nK").toString().toInt();  
  simulation->setGrid(resolution, ni, nj, nk);
}

// - - - Read in an agent file - - - //
void DataReader::readAgent(QXmlStreamReader * xml, Simulation * simulation){

  QXmlStreamAttributes attributes = xml->attributes();
 std::string name;
  if(attributes.hasAttribute("name"))
    name = attributes.value("name").toString().toUtf8().constData();
  xml->readNext();
  QString data = xml->text().toString();
  QStringList lines = data.split(";");

  if(lines.length() > 1){
    simulation->addSpecies(name);

    QStringList::const_iterator it;
    for (it = lines.constBegin(); it != lines.constEnd(); ++it){
      simulation->addAgentState(new Agent(name,  (*it).split(",")));
    }
  }

  //std::cout << "jspecies" << simulation->getSpeciesList().size() << std::endl;
 
  //for(int i = 0; i < simulation->getSpeciesList().size(); i++){
  //  std::cout << "species" << simulation->getSpeciesList()[i] << std::endl;
  // }
}
