// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                                                                    [GUT] * [VIEW] 
// Project to explore gut microbe data. 
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// DataReader.h
// Class to read in data
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //

#ifndef _DATA_READER_H
#define _DATA_READER_H
#include "Simulation.h"
#include <QString>
#include <QtXml/QXmlStreamReader>
#include <QDir>


class DataReader{

 public:
  DataReader(){}
  virtual  ~DataReader(){}

  // Function to read in idynomics data
  Simulation * readIDynomicsData(QDir dirName);

  Simulation * readDataFile(QString filename, Simulation * simulation);

  // Read in a simulation element (aka the iteration)
  void  readIteration(QXmlStreamReader * xml, Simulation * simulation);

  // Read in the grid information
  void readGrid(QXmlStreamReader * xml, Simulation * simulation);

  // Read in the agent information
  void readAgent(QXmlStreamReader * xml, Simulation * simulation);
  
  // Read in the thickness information
  void readThickness(QXmlStreamReader * xml, Simulation * simulation);

  // Read in solute information
  void readSolute(QXmlStreamReader * xml, Simulation * simulation);

  // Read in the bulk information
  void readBulk(QXmlStreamReader * xml, Simulation * simulation);

};
#endif
