// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                                                                    [GUT] * [VIEW] 
// Project to explore gut microbe data. 
// Written by Kristi Potter, 2013
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //

#ifndef _SOLUTE_H
#define _SOLUTE_H
#include <vector>
#include <string>

// Solute
class Solute{
 public:
  Solute(){}
  virtual ~Solute(){}
  Solute(std::string name, std::string unit, std::vector<float> concentrate)
    :_name(name), _unit(unit), _concentrate(concentrate){
    _min_concentration = _concentrate[0];
    _max_concentration = _concentrate[0];
    for(int i=0;i<concentrate.size();++i) {
      if(concentrate[i] > _max_concentration)
        _max_concentration = concentrate[i];
    }
    //printSolute();
  }

  void printSolute(){
    std::cout << "Solute: " << _name << std::endl;
    std::cout << "unit: " << _unit << std::endl;
    std::cout << "concentrate: " << std::endl;
    for(int i = 0; i < _concentrate.size(); i++)
      std::cout << _concentrate[i] << " ";
    std::cout << std::endl;
  }

  std::string getName() {
    return _name;
  }

  std::vector<float> getConcentration(){ return _concentrate; }

 private:
  std::string _name, _unit;
  float _max_concentration, _min_concentration;
  std::vector<float> _concentrate;

};

#endif
