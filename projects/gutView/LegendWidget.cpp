// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// LegendWidget.cpp
// Written by Danny Walinsky 2013
// dwalins4@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //

#include "LegendWidget.h"


void LegendWidget::setAgentLegend(std::map<std::string, std::string> colorMap, QWidget *signalReceiver)
{
  QVBoxLayout* layout = new QVBoxLayout;

  this->setLayout(layout);

  std::string styleSheetLines = "";

  std::map<std::string, std::string>::iterator iter;
  for(iter = colorMap.begin(); iter != colorMap.end(); ++iter) {
    // create checkbox line
    QCheckBox *thisCheck = new QCheckBox(QString::fromStdString(iter->first), this);
    thisCheck->setChecked(true);
    thisCheck->setObjectName(QString::fromStdString("agent-" + iter->first));
    styleSheetLines += "QCheckBox::indicator#agent-"+iter->first+":checked {background-color:"+iter->second+";} ";
    layout->addWidget(thisCheck);

    // attach checkboxes to gutpaintwidget
    connect(thisCheck, SIGNAL(stateChanged(int)), signalReceiver, SLOT(legendEvts(int)));
  }

  setStyleSheet(
    QString::fromStdString("QCheckBox::indicator {border-radius:8px;} \
    QCheckBox::indicator:unchecked {background:transparent;border:1px solid gray;}"
    + styleSheetLines)
  );

};


void LegendWidget::setSoluteLegend(std::vector<std::string> soluteVector, QWidget *signalReceiver)
{
  QVBoxLayout* layout = new QVBoxLayout;

  this->setLayout(layout);

  std::string styleSheetLines = "";

  std::vector<std::string>::iterator iter;
  for(iter = soluteVector.begin(); iter != soluteVector.end(); ++iter) {
    // create slider
    QSlider *thisSlide = new QSlider(Qt::Horizontal, this);
    thisSlide->setEnabled(false);
    thisSlide->setObjectName(QString::fromStdString("slider-" + *iter));
    thisSlide->setValue(99);
    layout->addWidget(thisSlide);

    // create checkbox line
    QCheckBox *thisCheck = new QCheckBox(QString::fromStdString(*iter), this);
    thisCheck->setChecked(false);
    thisCheck->setObjectName(QString::fromStdString("solute-" + *iter));
    layout->addWidget(thisCheck);


    // attach checkboxes to gutpaintwidget
    connect(thisCheck, SIGNAL(stateChanged(int)), signalReceiver, SLOT(legendEvts(int)));
    connect(thisSlide, SIGNAL(valueChanged(int)), signalReceiver, SLOT(legendEvts(int)));

  }

  setStyleSheet(
    QString::fromStdString("QCheckBox::indicator:unchecked {background:transparent;border:1px solid gray;}")
  );

};


