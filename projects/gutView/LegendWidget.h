// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// LegendWidget.h
// Written by Danny Walinsky 2013
// dwalins4@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#ifndef LEGEND_WIDGET_H
#define LEGEND_WIDGET_H

#include <iostream>
#include <QWidget>
#include <QtGui>
#include <QSlider>

class LegendWidget : public QWidget
{
  Q_OBJECT


public:
  LegendWidget() {};

  void setAgentLegend(std::map<std::string, std::string> colorMap, QWidget *signalReceiver);

  void setSoluteLegend(std::vector<std::string> soluteVector, QWidget *signalReceiver);

};
#endif // LEGEND_WIDGET_H
