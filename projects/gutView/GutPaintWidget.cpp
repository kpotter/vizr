// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// GutPaintWidget.cpp
// Written by Kristi Potter 2013..
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#include "GutPaintWidget.h"
#include <sstream>

// * * * Constructur * * * //
GutPaintWidget::GutPaintWidget(std::string title, QWidget * parent)
  : PaintWidget(title, parent){
  initializeViewing();
  initializeScene();
  setBackgroundColor(Qt::black);
}

// * * * Initialize the widget * * *  //
void GutPaintWidget::initializeScene(){

  // Create default instances of the drawables
  _simulationDraw  = new SimulationDrawable();
  addDrawable(_simulationDraw);
  update();
}

 // Set up viewing variables 
void GutPaintWidget::initializeViewing(){

  // User-defined transformations
  _xRot = _yRot = _zRot = 0;
  _xTrans = _yTrans = _zTrans = 0;
  
  // Perspective 
  _fovy = 35.0;
  _aspect = width()/height();
  _near = 1.0;
  _far =  1000.0;

  // Camera
  _eye = Vector3d(0.0, 1.0, 0.0);
  _center = Vector3d(0.0, 0.0, 0.0);
  _up = Vector3d(0.0, 0.0, 1.0);
}

// * * *  Set the simulation, update the scene * * * //
void GutPaintWidget::setSimulation(Simulation * simulation){

  if(simulation->getNumIterations() == 0)
    return;
  
  _simulation = simulation;
  _simulation->rescale(_simulationDraw->getBoundingBox());

  // Initialize species colors vector
  std::vector<std::string> speciesList = _simulation->getSpeciesList();
  for(unsigned int i=0;i<speciesList.size();i++) {
    _speciesColors[speciesList[i]] = colorList[i];
  }

  for(int i = 0; i < _simulation->getNumIterations(); i++)
  { 
    _simulationDraw->addAgentList(i, _simulation->getAgentStateList(i), _simulation->getGrid(), _speciesColors);
    _simulationDraw->addSoluteList(i, _simulation->getSoluteStateList(i), _simulation->getGrid());
  }
  _simulationDraw->setSoluteConcentrations();

  update();
}

// Respond to signals from Legend
void GutPaintWidget::legendEvts(int setting) {
    QObject* legendItem = sender();
    std::string name = legendItem->objectName().toAscii().constData();
    _simulationDraw->hideShowDrawables(setting, name, legendItem->parent());
    update();
}

// Get agent type to species mapping
std::map<std::string, std::string> GutPaintWidget::getSpeciesColors() {
  std::map<std::string, std::string> speciesColorsReturnMap;

  std::map<std::string, ColorF>::iterator iter;
  for(iter = _speciesColors.begin(); iter != _speciesColors.end(); ++iter) {
    Color<int> thisSpeciesColor = iter->second.toInt();

    std::stringstream r, g, b, a;
    r << thisSpeciesColor.r();
    g << thisSpeciesColor.g();
    b << thisSpeciesColor.b();
    a << thisSpeciesColor.a(); 
    std::string speciesColorOutput = "rgba("+ r.str()+","+g.str()+","+b.str()+","+a.str()+");";
   
    //    std::string speciesColorOutput = "rgba("+std::to_string(thisSpeciesColor.v[0])+","+std::to_string(thisSpeciesColor.v[1])+","+std::to_string(thisSpeciesColor.v[2])+","+std::to_string(thisSpeciesColor.v[3])+");";
    speciesColorsReturnMap.insert(std::pair<std::string, std::string>(iter->first, speciesColorOutput));
  }
  return speciesColorsReturnMap;
}

// * * * Draw the added drawables. * * * //
void GutPaintWidget::drawScene(){
  glPushMatrix();
  for(unsigned int i = 0; i < _drawables.size(); i++)
    _drawables[i]->draw();
  glPopMatrix();
}

