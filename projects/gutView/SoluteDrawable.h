// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// SoluteDrawable.h
// Written by Kristi Potter 2014
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#ifndef _SOLUTE_DRAWABLE_H
#define _SOLUTE_DRAWABLE_H

#include <vector>
#include <map>
#include "Grid.h"
#include "Solute.h"
#include "Drawable.h"
#include "ColorDefines.h"

class GridPoint {
public:
  GridPoint() {};
  GridPoint (float _center[3]) {
    center[0] = _center[0];
    center[1] = _center[1];
    center[2] = _center[2];
  };
  float center[3];
  float color[4];
  std::vector<float> soluteConcentrations;
};

/*
 * Class to draw the solute amounts
 */
class SoluteDrawable : public Drawable{

  Q_OBJECT
public:

  /// Constructor
  SoluteDrawable(int iteration, std::vector<Solute *> solutes, Grid * grid);

  /// Gets a list of the solutes present in this iteration
  std::vector<std::string> getSoluteTypes() {
    return soluteTypes;
  };

  /// Gets the minimum and maximum concentrations of each solute in the iteration this soluteDrawable represents
  std::map<std::string, std::pair<float, float> > getMaxMinSoluteConcentrations() {
    return _max_min_solute_concentrations;
  };

  /// Rescales solute concentrations to between 0-1 based on maximum and minimum for that solute in the simulation
  void rescaleConcentrations(std::map<std::string, float> maxConcentrations, std::map<std::string, float> minConcentrations);

  /// Preprocesses visualization for the currently displayed solutes and their specified opacities
  void setSolutes(std::map<std::string, int> currentSolutes, std::map<std::string, float> soluteOpacities);

  /// Draw the solutes
  void draw();

private:
  Grid * _grid;
  float gx;
  float gy;
  float gz;
  float unit;

  // Information for drawing solutes
  std::vector<Solute *> _solutes;
  std::vector<std::vector<std::vector<GridPoint> > > Points;
  std::vector<std::vector<std::vector<float> > > _backWall_colors;
  std::vector<std::string> soluteTypes;

  std::map<std::string, std::pair<float, float> > _max_min_solute_concentrations;
  std::map<std::string, Color<float> > soluteColors;

  static const int cube_vertices_count = 8;
};
#endif
