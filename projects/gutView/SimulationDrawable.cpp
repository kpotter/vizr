// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// SimulationDrawable.cpp
// Written by Kristi Potter 2014
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#include "SimulationDrawable.h"

void SimulationDrawable::draw(){

  std::cout << "draw" << _timeStep << std::endl;
  if(_timeStep != -1){ 
    if(!_itSolutesUpdated[_timeStep]) {
      _solutes[_timeStep]->setSolutes(_solutesDisplayed, _solutesOpacity);
      _itSolutesUpdated[_timeStep] = true;
    }

    //   std::cout << _agents.size() << " " << _agents[int(_timeStep)]->getNumAgents() << std::endl;
    _agents[int(_timeStep)]->draw(_displayedSpecies);
    _solutes[int(_timeStep)]->draw();
  }
}

void SimulationDrawable::hideShowDrawables(int setting, std::string name, QObject *legend) {
  if(name.substr(0,5) == "agent")
  {
    // Set species hidden status to new setting
    _displayedSpecies[name.substr(6)] = setting;
  }
  else if(name.substr(0, 6) == "solute") {
    // Get solute's associated bar and enable/disable it
    QSlider *slide = legend->findChild<QSlider *>(QString::fromStdString("slider-"+name.substr(7)));
    slide->setEnabled(setting != 0);

    _solutesDisplayed[name.substr(7)] = setting;
    _solutesOpacity[name.substr(7)] = slide->value();
    updateSolutes();
  }
  else if(name.substr(0, 6) == "slider") {
    // Solute bar movement
    _solutesOpacity[name.substr(7)] = setting;
    updateSolutes();
  }

}

void SimulationDrawable::setSoluteConcentrations() {
  _soluteTypes = _solutes[0]->getSoluteTypes();
  std::vector< std::map< std::string, std::pair<float, float> > > max_min_concentrations_full;
  // Get min/max concentration values from each iteration
  for(int i=0;i<_solutes.size();i++)
  {
    max_min_concentrations_full.push_back(_solutes[i]->getMaxMinSoluteConcentrations());
  }
  // Combine all iterations to get total maximum/minimum values for each solute type
  for(int i=0;i<_soluteTypes.size();i++)
  {
    _solutesDisplayed[_soluteTypes[i]] = 0;
    _solutesOpacity[_soluteTypes[i]] = 99;

    _soluteConcentrationMax[_soluteTypes[i]] = max_min_concentrations_full[0][_soluteTypes[i]].first;
    _soluteConcentrationMin[_soluteTypes[i]] = max_min_concentrations_full[0][_soluteTypes[i]].second;

    for(int j=1;j<max_min_concentrations_full.size();j++)
    {
      if(max_min_concentrations_full[j][_soluteTypes[i]].first > _soluteConcentrationMax[_soluteTypes[i]])
        _soluteConcentrationMax[_soluteTypes[i]] = max_min_concentrations_full[j][_soluteTypes[i]].first;
      if(max_min_concentrations_full[j][_soluteTypes[i]].second < _soluteConcentrationMin[_soluteTypes[i]])
        _soluteConcentrationMin[_soluteTypes[i]] = max_min_concentrations_full[j][_soluteTypes[i]].second;
    }
    //  std::cout << _soluteConcentrationMax[_soluteTypes[i]] << " " << _soluteConcentrationMin[_soluteTypes[i]] << std::endl;
  }
  // Call solute drawables to rescale concentration values based on max/min values
  for(int i=0;i<_solutes.size();i++)
  {
    _solutes[i]->rescaleConcentrations(_soluteConcentrationMax, _soluteConcentrationMin);
  }

}

void SimulationDrawable::updateSolutes() {
  for(int i=0;i<_itSolutesUpdated.size();i++)
  {
    _itSolutesUpdated[i] = false;
  }
  _solutes[_timeStep]->setSolutes(_solutesDisplayed, _solutesOpacity);
  _itSolutesUpdated[_timeStep] = true;
}
