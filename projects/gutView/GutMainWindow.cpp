// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //
// GutMainWindow.h
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#include "GutMainWindow.h"
#include "GutPaintWidget.h"
#include <QFileDialog>

/*
 * Constructor initializes and sets up connections
 */
GutMainWindow::GutMainWindow(QWidget *parent) 
  : MainWindow(parent) {
  initialize(); 
  setupConnections();
}

/*
 * Initialize creates a new Gut Paint Widget and sets it as the main widget
 */
void GutMainWindow::initialize(){

  // glWidget = new GutPaintWidget();
  glWidget = new GLWidget();
  //->setWidget(glWidget);
 
  // Create the control widget
  QDockWidget * animationDock = new QDockWidget();
  _controlWidget = new ControlWidget();
  animationDock->setWidget(_controlWidget);
  addDockWidget(Qt::BottomDockWidgetArea, animationDock);   

  QDockWidget * legendDock = new QDockWidget();
  QWidget * legend = new QWidget();
  QVBoxLayout * legendLayout = new QVBoxLayout;

  _agentLegend = new LegendWidget();
  legendLayout->addWidget(_agentLegend);

  _soluteLegend = new LegendWidget();
  // legendLayout->addWidget(_soluteLegend);

  legendDock->setWidget(legend);
  legend->setLayout(legendLayout);
  legend->show();
  addDockWidget(Qt::RightDockWidgetArea, legendDock);
}

/*
 * Connect the simulation to the gut paint widget
 */
void GutMainWindow::setupConnections(){

  // Connect a simulation update signal with the painted simulation
  connect(this, SIGNAL(simulationUpdate(Simulation *)), 
  	  dynamic_cast<GutPaintWidget *>(glWidget), SLOT(setSimulation(Simulation *)));

  // Connect a simulation update signal with the animation frame rate
  connect(this, SIGNAL(simulationUpdate(Simulation *)), this, SLOT(setNumFrames(Simulation *)));

  // Connect a simulation update signal with the legend
  connect(this, SIGNAL(simulationUpdate(Simulation *)), this, SLOT(setLegendData(Simulation *)));

  // Connect an animation signal with a painted animation signal
  connect(_controlWidget, SIGNAL(animate(float)), glWidget, SLOT(animate(float)));
}

/*
 * Load in the simulation data, signal when its done
 */
void GutMainWindow::loadDataSlot(){

  // Query the user for a data directory
  std::string directory = "/Users/kpotter/Data/Gut Microbes/biofilm_sample/sample_run/";
  if(!QDir(directory.c_str()).exists())
    directory = "C:/users/danny/desktop/school/research/biofilm/sample_run/";

  // Open a file dialog
  QFileDialog * dialog = new QFileDialog(this, tr("Load IDynomics File"),  
					 tr(directory.c_str()), 
					 tr("XML Files (*.xml)"));
  dialog->setFileMode(QFileDialog::Directory);
  
  // If we hit ok on the file dialog
  if(dialog->exec()){
    
    // Get the selected folder
    QDir dirName = dialog->directory();

    // Create a new data reader
    DataReader * dr = new DataReader();
    
    // Create a new simulation by reading the data
    Simulation * simulation = dr->readIDynomicsData(dirName);
    if (!simulation) {
      std::cout << "An error occurred while loading your directory." << std::endl;
    }
    else {
      // Signal that we have an updated simulation
      emit simulationUpdate(simulation);
    }
  }
}

/*
 * Set the number of frames in the animation control
 */
void GutMainWindow::setNumFrames(Simulation * simulation){
  _controlWidget->setTimeInterval(simulation->getTimeInterval());
  _controlWidget->setNumFrames(simulation->getNumIterations()-1);
  _controlWidget->enableAnimation();
}

/*
 * Add data to legends
 */
void GutMainWindow::setLegendData(Simulation * simulation){
  // Add data to legends
  _agentLegend->setAgentLegend(dynamic_cast<GutPaintWidget *>(glWidget)->getSpeciesColors(),
			       dynamic_cast<GutPaintWidget *>(glWidget));
  //_soluteLegend->setSoluteLegend(dynamic_cast<GutPaintWidget *>(glWidget)->getSoluteTypes(),
  //				 dynamic_cast<GutPaintWidget *>(glWidget));
}
