// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// SoluteDrawable.cpp
// Written by Kristi Potter 2014
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#include "SoluteDrawable.h"
#include "Maths.h"

SoluteDrawable::SoluteDrawable(int iteration, std::vector<Solute *> solutes, Grid * grid)  : _solutes(solutes), _grid(grid) {
   //std::cout << "draw solute: " << _solutes.size() << std::endl;

  gx = (_grid->getNI() - 2);
  gy = (_grid->getNJ() - 2);
  gz = (_grid->getNK() - 2);
  unit = ( _boundBox->max.x() - _boundBox->min.x() ) / gx;
  //std::cout << "draw solute: " << _solutes.size() << std::endl;


  std::vector< std::vector<float> > soluteConcentrations;
  std::vector< std::vector<float> > _max_min_solute_concentrations_by_index;

  float soluteSetColors[9][3] = {
    {166,206,227},
    {31,120,180},
    {178,223,138},
    {51,160,44},
    {251,154,153},
    {227,26,28},
    {253,191,111},
    {255,127,0},
    {202,178,214}
  };

  _max_min_solute_concentrations_by_index.resize(_solutes.size());
  for(int i=0;i<_solutes.size();i++)
  {
    soluteTypes.push_back(_solutes[i]->getName());
    soluteConcentrations.push_back( _solutes[i]->getConcentration() );
    _max_min_solute_concentrations_by_index[i].resize(2);
    _max_min_solute_concentrations_by_index[i][0] = soluteConcentrations[i][0];
    _max_min_solute_concentrations_by_index[i][1] = soluteConcentrations[i][0];

    soluteColors[_solutes[i]->getName()];
    soluteColors[_solutes[i]->getName()][0] = soluteSetColors[i][0] / 255.0;
    soluteColors[_solutes[i]->getName()][1] = soluteSetColors[i][1] / 255.0;
    soluteColors[_solutes[i]->getName()][2] = soluteSetColors[i][2] / 255.0;
  }

  Points.resize(gx);
  _backWall_colors.resize(gx);
  for(int x=0;x<gx;x++)
  {
    Points[x].resize(gy);
    _backWall_colors[x].resize(gy);
    for(int y=0;y<gy;y++)
    {
      _backWall_colors[x][y].resize(4);
      Points[x][y].resize(gz);
      for(int z=0;z<gz;z++)
      {
        float center[3] = {unit*x - unit*gx/2 + unit/2, unit*y - unit*gy/2 + unit/2, unit*z - unit*gz/2 + unit/2};
        GridPoint thisPoint(center);
        thisPoint.soluteConcentrations.resize(_solutes.size());

        for(int i=0;i<_solutes.size();i++)
        {
          thisPoint.soluteConcentrations[i] = soluteConcentrations[i][(x+1)*(gy+2)*(gz+2) + (y+1)*(gz+2) + (z+1)];
          if(thisPoint.soluteConcentrations[i] > _max_min_solute_concentrations_by_index[i][0])
            _max_min_solute_concentrations_by_index[i][0] = thisPoint.soluteConcentrations[i];
          if(thisPoint.soluteConcentrations[i] < _max_min_solute_concentrations_by_index[i][1])
            _max_min_solute_concentrations_by_index[i][1] = thisPoint.soluteConcentrations[i];
        }

        Points[x][y][z] = thisPoint;
      }
    }
  }

  // Transfer max/min solute concentrations from temporary vector to map, so solute name is saved
  for(int i=0;i<_solutes.size();i++)
  {
    _max_min_solute_concentrations[_solutes[i]->getName()].first = _max_min_solute_concentrations_by_index[i][0];
    _max_min_solute_concentrations[_solutes[i]->getName()].second = _max_min_solute_concentrations_by_index[i][1];
  }
}

void SoluteDrawable::rescaleConcentrations(std::map<std::string, float> maxConcentrations, std::map<std::string, float> minConcentrations) {
  std::vector<std::vector<float> > _max_min_solute_concentrations_by_index;

  _max_min_solute_concentrations_by_index.resize(soluteTypes.size());
  for(int i=0;i<soluteTypes.size();i++)
  {
    _max_min_solute_concentrations_by_index[i].resize(2);
    _max_min_solute_concentrations_by_index[i][0] = maxConcentrations[soluteTypes[i]];
    _max_min_solute_concentrations_by_index[i][1] = minConcentrations[soluteTypes[i]];
  }

  for(int x=0;x<Points.size();x++)
  {
    for(int y=0;y<Points[x].size();y++)
    {
      for(int z=0;z<Points[x][y].size();z++)
      {
        for(int i=0;i<soluteTypes.size();i++)
        {
          if(_max_min_solute_concentrations_by_index[i][0] - _max_min_solute_concentrations_by_index[i][1] > 0)
            Points[x][y][z].soluteConcentrations[i] = (Points[x][y][z].soluteConcentrations[i] - _max_min_solute_concentrations_by_index[i][1]) / (_max_min_solute_concentrations_by_index[i][0] - _max_min_solute_concentrations_by_index[i][1]);
        }
      }
    }
  }
}

// Called whenever solute display variables are changed to calculate the colors of cubes.
void SoluteDrawable::setSolutes(std::map<std::string, int> displayedSolutes, std::map<std::string, float> soluteOpacities) {
  std::vector<std::string> soluteTypesFiltered;
  std::vector<int> soluteTypesFiltered_indices;
  for(int i=0;i<soluteTypes.size();i++)
  {
    if(displayedSolutes[soluteTypes[i]] != 0) {
      soluteTypesFiltered.push_back(soluteTypes[i]);
      soluteTypesFiltered_indices.push_back(i);
    }
  }
  bool solutesVisible = (soluteTypesFiltered.size() != 0);

  //std::cout << soluteTypesFiltered.size() << std::endl;

  float soluteOpacity;
  float totalOpacity;
  for(int x=0;x<Points.size();x++)
  {
    for(int y=0;y<Points[x].size();y++)
    {
      for(int z=0;z<Points[x][y].size();z++)
      {
        // Clear color of point
        for(int j=0;j<4;j++)
        {
          Points[x][y][z].color[j] = 0;
        }

        if(solutesVisible)
        {
          totalOpacity = 0;
          for(int i=0;i<soluteTypesFiltered.size();i++)
          {
            soluteOpacity = soluteOpacities[soluteTypesFiltered[i]] / 99.0;
            totalOpacity += soluteOpacity;

            for(int j=0;j<3;j++)
            {
              Points[x][y][z].color[j] += soluteOpacity * Points[x][y][z].soluteConcentrations[soluteTypesFiltered_indices[i]] * soluteColors[soluteTypesFiltered[i]][j];
            }
            Points[x][y][z].color[3] += soluteOpacity * Points[x][y][z].soluteConcentrations[soluteTypesFiltered_indices[i]];
          }
          if(totalOpacity > 0) {
            for(int j=0;j<3;j++)
            {
              Points[x][y][z].color[j] /= totalOpacity;
            }
          }
        }

        if(z == 0) {
          _backWall_colors[x][y][0] = 0.0;
          _backWall_colors[x][y][1] = 0.0;
          _backWall_colors[x][y][2] = 0.0;
          _backWall_colors[x][y][3] = 0.0;
        }
        _backWall_colors[x][y][0] += Points[x][y][z].color[0] / Points[x][y].size();
        _backWall_colors[x][y][1] += Points[x][y][z].color[1] / Points[x][y].size();
        _backWall_colors[x][y][2] += Points[x][y][z].color[2] / Points[x][y].size();
        _backWall_colors[x][y][3] += Points[x][y][z].color[3] / Points[x][y].size();

        /*if(z == 0 && y == 0) {
          std::cout << "Colors: " <<  Points[x][y][z].color[0] << " " << Points[x][y][z].color[1] << " " << Points[x][y][z].color[2] << " " << Points[x][y][z].color[3] << std::endl;
        }*/
      }
    }
  }
}

void SoluteDrawable::draw(){
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  for(int x=0;x<Points.size();x++)
  {
    for(int y=0;y<Points[x].size();y++)
    {
      glColor4f(_backWall_colors[x][y][0], _backWall_colors[x][y][1], _backWall_colors[x][y][2], _backWall_colors[x][y][3]);

      glBegin(GL_POLYGON);
        glVertex3f(Points[y][x][0].center[0] - unit/2, Points[y][x][0].center[1] - unit/2, Points[y][x][0].center[2]);
        glVertex3f(Points[y][x][0].center[0] - unit/2, Points[y][x][0].center[1] + unit/2, Points[y][x][0].center[2]);
        glVertex3f(Points[y][x][0].center[0] + unit/2, Points[y][x][0].center[1] + unit/2, Points[y][x][0].center[2]);
        glVertex3f(Points[y][x][0].center[0] + unit/2, Points[y][x][0].center[1] - unit/2, Points[y][x][0].center[2]);
      glEnd();

      glFlush();
    }
  }

  /*for(int x=0;x<Points.size();x++)
  {
    for(int y=0;y<Points[x].size();y++)
    {
      for(int z=0;z<Points[x][y].size();z++)
      {
        glPushMatrix();
        glColor4f(Points[x][y][z].color[0], Points[x][y][z].color[1], Points[x][y][z].color[2], Points[x][y][z].color[3]);
        //glColor4f(0, 0, 0, 0.5);
        glTranslatef(Points[x][y][z].center[0], Points[x][y][z].center[1], Points[x][y][z].center[2]);
        glutSolidCube(unit);
        glPopMatrix();
      }
    }
  }*/

}

