// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// AgentDrawable.cpp
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#include "AgentDrawable.h"
#include "GLIncludes.h"
#include "Maths.h"

/*
 *  Draw the agents
 */
void AgentDrawable::draw(std::map<std::string, int> displayedSpecies){
  Drawable::draw();
  std::string speciesName;

  std::cout << "iteration " << _iteration << std::endl;
  
  for(int i = 0; i < _agents.size(); i++){
    speciesName = _agents[i]->getSpecies();

    // Check if species is hidden
    if(displayedSpecies[speciesName] == 0)
      continue;

    glPushMatrix();
    glColor3fv(_speciesColors[speciesName].to3d().v);
    //glColor3f(0.65, 0.65, 0.65);
    glTranslatef(_agents[i]->getPosition().y(), _agents[i]->getPosition().x(), _agents[i]->getPosition().z());
    glutSolidSphere(_agents[i]->getScaledRadius(), 25, 25);
    glPopMatrix();
  }
}

