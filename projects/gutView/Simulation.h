// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// Simulation.h
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#ifndef _SIMULATION_H
#define _SIMULATION_H

#include "Agent.h"
#include "Solute.h"
#include "Grid.h"
#include "Maths.h"
#include "Iteration.h"
#include <vector>
#include <map>
#include <iostream>

/*
 * Class to hold the simulation data
 */
class Simulation{
 public:

  // Constructor/ Destructor
  Simulation(): _currentIteration(-1) {}
  virtual ~Simulation(){}

  // Set the iteration
  void setCurrentIteration(int iterate, int time, std::string unit){
    _timeUnit = unit;

    // Find the matching iteration if we have one
    int iteration = -1;
    for(int i = 0; i < _iterations.size(); i ++){
      if(_iterations[i]->isIterate(iterate, time, unit)){
	iteration = i;
	break;
      }
    }

    // If we did not find an iteration, make a new one
    if(iteration == -1){
      Iteration * it = new Iteration(iterate, time, unit);
      // _iterations.push_back(it);
      _iterations[iterate] = it;
      _currentIteration = _iterations.size()-1;
    }
    else
      _currentIteration = iteration;
  }

  /// Get the current iteration
  int getCurrentIteration(){ return _currentIteration; }

  /// Get the unit of time per iteration
  std::string getTimeInterval() { return _timeUnit; }

  /// Check if a species is new to the simulation, and add it if it is
  void addSpecies(std::string speciesName) {
    if(std::find(_speciesList.begin(), _speciesList.end(), speciesName) == _speciesList.end()) {
      _speciesList.push_back(speciesName);
    }
  }

  /// Gets all species in simulation
  std::vector<std::string> getSpeciesList() {
    return _speciesList;
  }

  /// Add an agent to the current iteration's agent state list
  void addAgentState(Agent * agent){
    _iterations[_currentIteration]->addToAgentState(agent);
  }

  /// Add a solute to the current iteration's solute state list
  void addSoluteState(Solute * solute){
    _iterations[_currentIteration]->addToSoluteState(solute);
  }
  
  /// Add a solute sum to the current iteration's solute sum list
  void addSoluteSum(Solute * solute){
    _iterations[_currentIteration]->addToSoluteSum(solute);
  }

  /// Get the agent state list for the current iteration
  std::vector<Agent *> getAgentStateList(){
    return getAgentStateList(_currentIteration);
  }

  std::vector<Agent *> getAgentStateList(int iteration){
    return _iterations[iteration]->getAgentStateList();
  }

  /// Get the solute list
  std::vector<Solute *> getSoluteStateList(int iteration){
    return _iterations[iteration]->getSoluteStateList();
  }

  /// Set the grid and resolution
  void setGrid(float resolution, int ni, int nj, int nk){
    _grid = new Grid(resolution, ni, nj, nk);
  }
  
  // Set the thickness
  void setThickness(std::string domain, std::string unit, float mean, float stddev, float max){
    _domain = domain;
    _thickUnit = unit;
    _mean = mean; _stddev = stddev; _max = max;
  }

  // Return the grid
  Grid * getGrid(){ return _grid; }  

  // Print simulation information
  void printSimulation(){
    std::cout << "domain: " << _domain << " thickUnit: " << _thickUnit
	      << " mean: " << _mean << " stddev: " << _stddev << " max: " << _max<< std::endl;
  }

  /// Set the current iteration
  void setCurrentIteration(int i){ _currentIteration = i; }

  /// Get the number of iterations
  int getNumIterations(){ return _iterations.size(); }

  /// Rescale the simulation elements to be within the bounding box
  void rescale(BoundingBox * bb){

    float radiusMin, radiusMax;
    radiusMin = radiusMax = _iterations[0]->getAgentStateList()[0]->getRadius();
    for(int i = 0; i < _iterations.size(); i++){
      std::vector<Agent* > agents = _iterations[i]->getAgentStateList();
      for(int a = 0; a < agents.size(); a++){
	radiusMin = radiusMin < agents[a]->getRadius() ? radiusMin : agents[a]->getRadius();
	radiusMax = radiusMax > agents[a]->getRadius() ? radiusMax : agents[a]->getRadius();	
      }     
    }


    for(int i = 0; i < _iterations.size(); i++){
      std::vector<Agent* > agents = _iterations[i]->getAgentStateList();
      for(int a = 0; a < agents.size(); a++){
	agents[a]->setPosition(affine(0.f, agents[a]->getX(), float(_grid->getXDim()), bb->min.x(), bb->max.x()),
			       affine(0.f, agents[a]->getZ(), float(_grid->getZDim()), bb->min.z(), bb->max.z()),
			       affine(0.f, agents[a]->getY(), float(_grid->getYDim()), bb->min.y(), bb->max.y()));
	agents[a]->setScaledRadius(affine(radiusMin, agents[a]->getRadius(), radiusMax, 0.00001f, 0.05f)); 
      }
      _iterations[i]->setAgentStateList(agents);
    }
  }

 private:
 
  Grid * _grid;
  std::string _domain;
  std::string _thickUnit;
  std::string _timeUnit;
  float _mean, _stddev, _max;
  int _currentIteration;

  // All of the iterations
  std::map<int, Iteration *> _iterations;

  // List of species present in simulation
  std::vector<std::string> _speciesList;
};

#endif
