// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// GutMainWindow.h
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#ifndef _GUT_MAIN_WINDOW_H
#define _GUT_MAIN_WINDOW_H

#include "MainWindow.h"
#include "DataReader.h"
#include "Simulation.h"
#include "ControlWidget.h"
#include "LegendWidget.h"

// The main application window
class GutMainWindow : public MainWindow{
  Q_OBJECT
  public:
  GutMainWindow(QWidget * parent  = 0);
  
 protected:
   
  void initialize();
  void setupConnections();
  ControlWidget * _controlWidget;
  LegendWidget * _agentLegend;
  
  LegendWidget * _soluteLegend;

 signals:
  void simulationUpdate(Simulation *);

public slots:
  virtual void loadDataSlot();
  void setNumFrames(Simulation * simulation);
  void setLegendData(Simulation * simulation);
  
};

#endif
