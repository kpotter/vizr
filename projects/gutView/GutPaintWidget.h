// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// GutPaintWidget.h
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#ifndef _GUT_PAINT_WIDGET_H
#define _GUT_PAINT_WIDGET_H

#include "PaintWidget.h"
#include "Simulation.h"
#include "SimulationDrawable.h"
#include "BoundingBoxDrawable.h"

// Class to handle the drawing of the simulation
class GutPaintWidget : public PaintWidget{
  Q_OBJECT
  
public:
  // Constructor/Destructor
  GutPaintWidget(std::string title = "Gut Microbe Viewer", QWidget * parent = 0);
  virtual ~GutPaintWidget(){}

  // Initialize the viewing
  void initializeViewing();
  
  // Intialize the widget
  void initializeScene(); 
  
  // Draw the scene
  void drawScene();

  // Get mapping of species to printable rgba color
  std::map<std::string, std::string> getSpeciesColors();

  // Get list of solutes in simulation
  std::vector<std::string> getSoluteTypes() {
    return _simulationDraw->getSoluteTypes();
  };

  public slots:
    // Set the simulation
    void setSimulation(Simulation * simulation);
    void legendEvts(int setting);

 protected:
  // The simulation
  Simulation * _simulation;
  
  // A drawable for the simulation
  SimulationDrawable * _simulationDraw;
  
  // Color map of agent species
  std::map<std::string, ColorF> _speciesColors;

};
#endif
