// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// AgentDrawable.h
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#ifndef _SIMULATION_DRAWABLE_H
#define _SIMULATION_DRAWABLE_H

#include "Grid.h"
#include "Drawable.h"
#include "AgentDrawable.h"
#include "SoluteDrawable.h"
#include <QSlider>

class SimulationDrawable : public Drawable{
  Q_OBJECT
  
public:

  /// Constructor
  SimulationDrawable(){}

  /// Draw the simulation
  void draw();

  /// Add an agent list to the simulation drawable
  void addAgentList(int iteration, std::vector<Agent *> agents, Grid * grid, std::map<std::string, ColorF> _speciesColors){
    _agents[iteration] = new AgentDrawable(iteration, agents, grid, _speciesColors);
    //_agents.push_back(new AgentDrawable(iteration, agents, grid, _speciesColors));

    // initialize list of displayed species
    for(std::map<std::string, ColorF>::iterator it = _speciesColors.begin(); it != _speciesColors.end(); ++it) {
      _displayedSpecies[it->first] = 1;
    }
  };

  /// Add a solute list to the simulation drawable
  void addSoluteList(int iteration, std::vector<Solute *> solutes, Grid * grid){
    //_solutes.push_back(new SoluteDrawable(iteration, solutes, grid));
    _solutes[iteration] = new SoluteDrawable(iteration, solutes, grid);
    _itSolutesUpdated.push_back(true);
  };

  /// Returns all solutes present in simulation
  std::vector<std::string> getSoluteTypes() {
    return _soluteTypes;
  };

  /// Called after solute display settings have been modified
  void updateSolutes();

  // Get max and min solute concentrations, and set initially displayed solutes
  void setSoluteConcentrations();

  /// Function called by changes to solute or agent displays by legend
  void hideShowDrawables(int setting, std::string name, QObject *legend);

public slots:

protected:
  //std::vector<AgentDrawable *> _agents;
  //std::vector<SoluteDrawable *> _solutes;

  std::map<int, AgentDrawable *> _agents;
  std::map<int, SoluteDrawable *> _solutes;
  
  // List of hidden species in simulation
  std::map<std::string, int> _displayedSpecies;

  // List of names of solutes in simulation
  std::vector<std::string> _soluteTypes;

  // Solute display information
  std::map<std::string, int> _solutesDisplayed;
  std::map<std::string, float> _solutesOpacity; // 0 - 99
  std::map<std::string, float> _soluteConcentrationMax;
  std::map<std::string, float> _soluteConcentrationMin;

  /// For each iteration, keeps track of whether solute settings are updated
  std::vector<bool> _itSolutesUpdated;

};

#endif
