// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                                                                         //
//                            888  /    e88~-_  888~-_                     //
//                            888 /    d888   \ 888   \                    //
//                            888/\    8888     888    |                   //
//                            888  \   8888     888   /                    //
//                            888   \  Y888   / 888_-/                     //
//                            888    \  "88_-/  888                        //
//                                    NAMESPACE                            //
// Mesh.h                                                                  //
// Written by Kristi Potter                                                //
// February 2013                                                           //
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef CONTOUR_H
#define CONTOUR_H

#include "heartmesh.h"

class Contour
{
public:

    Contour(HeartMesh * mesh, int numContours = 10);

    int getNumContours(){ return _isoContours.size(); }
    std::vector<Vertex> getContour(int i){ return _isoContours[i]; }
    double getIsoVal(int i){ return _isoVals[i]; }
    Vertex getNormal(int i){ return _normals[i]; }

protected:
    HeartMesh * _mesh;
    std::vector<double> _isoVals;
    std::vector<Vertex> _normals;

    void contour(double isoVal);

    std::vector< std::vector<Vertex> > _isoContours;

    std::vector<bool> _aboveTable;

    void createAboveTable(double isoVal);
    void updateAboveTable(double isoVal);

    Vertex getIntersection(Vertex A, Vertex B, double isoA, double isoB, double iso);
    Vertex getMidPoint(Vertex a, Vertex b);
};

#endif // CONTOUR_H
