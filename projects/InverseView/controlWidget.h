#ifndef CONTROLWIDGET_H
#define CONTROLWIDGET_H

#include <QWidget>
#include "animationControl.h"
#include <Q3GroupBox>

class ControlWidget : public QWidget
{
    Q_OBJECT

public:
    ControlWidget(int numFrames);

protected slots:
    void meanSlot(){ emit meanSignal(); _animationControl->setEnabled(false);}
    void stdSlot(){ emit stdSignal();  _animationControl->setEnabled(false); }
    void animateCombinedSlot() { emit animateCombinedSignal(); _animationControl->setEnabled(true); animate(_animationControl->getTimeStep()); }
    void animateMeanSlot() { emit animateMeanSignal(); _animationControl->setEnabled(true); animate(_animationControl->getTimeStep()); }
    void animateStdSlot() { emit animateStdSignal(); _animationControl->setEnabled(true); animate(_animationControl->getTimeStep()); }
    void animateSlot(float timestep){ emit animate(timestep); }

protected:
    QGroupBox * _radioGroup;
    QGroupBox * _animationGroup;
    AnimationControl * _animationControl;

    void createGUI(int numFrames);

signals:
    void stdSignal();
    void meanSignal();
    void animateCombinedSignal();
    void animateMeanSignal();
    void animateStdSignal();
    void animate(float);
};

#endif // CONTROLWIDGET_H
