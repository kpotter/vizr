#ifndef DATAREADER_H
#define DATAREADER_H


#include <vector>
#include "heartmesh.h"

// * * * Class to read in text-based data files * * * //
class DataReader
{    
public:
    DataReader(std::string pointsFile, std::string triFile,
               std::string meanFile, std::string stdFile){
        _hm = new HeartMesh(readPoints(pointsFile), readTriangles(triFile), readData(meanFile), readData(stdFile));
    }

    HeartMesh * getMesh() { return _hm; }

protected:
    HeartMesh * _hm;
    std::vector<Vertex> readPoints(std::string filename);
    std::vector<int> readTriangles(std::string filename);
    std::vector<double> readData(std::string filename);
};

#endif // DATAREADER_H
