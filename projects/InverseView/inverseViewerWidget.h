#ifndef INVERSEVIEWERWIDGET_H
#define INVERSEVIEWERWIDGET_H

#include "PaintWidget.h"
#include "heartMeshDrawable.h"
#include "ContourDrawable.h"
#include "colorbarOverpainter.h"
class BoundingBoxDrawable;

class InverseViewerWidget : public PaintWidget
{
    Q_OBJECT
public:
    InverseViewerWidget();

    // Initialize the widget
    void init();
    void keyPressEvent(QKeyEvent * event);
    int getNumFrames(){ return _heartDraw->getNumFrames(); }
    HeartMeshDrawable * getHeartMesh(){ return _heartDraw; }


  //  void paintEvent(QPaintEvent *event);


public slots:
    void setMesh(HeartMesh * mesh) {
        _heartDraw->setMesh(mesh);
        _contourDraw->setMesh(mesh);
        addDrawable(_heartDraw);
        addDrawable(_contourDraw);
        addOverpainter(_colorbar);
        update();
    }
    virtual void animate(float);

protected:
    HeartMeshDrawable *_heartDraw;
    ColorBarOverpainter *_colorbar;
    ContourDrawable *_contourDraw;
};

#endif // INVERSEVIEWERWIDGET_H
