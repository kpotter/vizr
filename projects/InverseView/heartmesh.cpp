#include "heartmesh.h"

HeartMesh::HeartMesh(std::vector<Vertex> vertices, std::vector<int> indices,
                     std::vector<double> means, std::vector<double> stds,
                     std::vector<Vector3f> normals, std::vector<ColorF> colors)
    : Mesh(vertices, indices, normals, colors), _means(means), _stds(stds)
{
}
