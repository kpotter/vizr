#include "contour.h"
#include "Maths.h"
#include "Vector.h"

Contour::Contour(HeartMesh *mesh, int numContours)
    : _mesh(mesh)
{
    // Find evenly space isovalues
    double min = _mesh->getMeanMin();
    double max = _mesh->getMeanMax();
    double space = (max-min)/((float)numContours+2);

    // Create the contours for each isovalue
    double isoval = min + space;
    for(int i = 0; i < numContours; i++){
        _isoVals.push_back(isoval + i*space);
    }

    for(int i = 0; i < numContours; i++){
       contour(_isoVals[i]);
       updateAboveTable(_isoVals[i]+space-1.0);
    }
}

// - - - Calculate the midpoint between 2 points - - - //
Vertex Contour::getMidPoint(Vertex a, Vertex b){
    return (a+b)/2;
}

// - - - Calculate the intersection point of the isocontour betwen A and B - - - //
Vertex Contour::getIntersection(Vertex A, Vertex B, double isoA, double isoB, double iso){
     return lerp(A,B, affine(isoA, iso, isoB, 0.0, 1.0));
}

// - - - Calculate the contour lines for this isoval - - - //
void Contour::contour(double isoVal){

    // Create or update the below/above table
    if(_aboveTable.size() == 0)
        createAboveTable(isoVal);
    else
       updateAboveTable(isoVal);

    std::vector<Vertex> isoContour;

    // Find all triangles that have an above AND below vertex
    for(int i = 0; i < _mesh->getNumIdx(); i+=3){

        bool a = _aboveTable[_mesh->getIdx(i+0)];
        bool b = _aboveTable[_mesh->getIdx(i+1)];
        bool c = _aboveTable[_mesh->getIdx(i+2)];

        Vertex A = _mesh->getPt(_mesh->getIdx(i+0));
        Vertex B = _mesh->getPt(_mesh->getIdx(i+1));
        Vertex C = _mesh->getPt(_mesh->getIdx(i+2));

        Vertex An = _mesh->getNorm(_mesh->getIdx(i+0));
        Vertex Bn = _mesh->getNorm(_mesh->getIdx(i+1));
        Vertex Cn = _mesh->getNorm(_mesh->getIdx(i+2));

        double aIso = _mesh->getMean(_mesh->getIdx(i+0));
        double bIso = _mesh->getMean(_mesh->getIdx(i+1));
        double cIso = _mesh->getMean(_mesh->getIdx(i+2));

        // If not ALL above or below
        if(!((a==b) && (a==c))){

            if(a==b){

                // Find the location of the isoVal on the line between 2 vertices
                isoContour.push_back(getIntersection(A,C,aIso, cIso, isoVal));
                isoContour.push_back(getIntersection(B,C,bIso, cIso, isoVal));

                _normals.push_back(((An+Cn)/2));
                _normals.push_back(((Bn+Cn)/2));
            }
            else if(b==c){

                // Find the location of the isoVal on the line between 2 vertices
                isoContour.push_back(getIntersection(A,B,aIso, bIso, isoVal));
                isoContour.push_back(getIntersection(A,C,aIso, cIso, isoVal));

                _normals.push_back(((An+Bn)/2));
                _normals.push_back(((An+Cn)/2));
            }
            else if(c==a){

                // Find the location of the isoVal on the line between 2 vertices
                isoContour.push_back(getIntersection(A,B,aIso, bIso, isoVal));
                isoContour.push_back(getIntersection(B,C,bIso, cIso, isoVal));

                _normals.push_back(((An+Bn)/2));
                _normals.push_back(((Bn+Cn)/2));
            }
        }
    }
    _isoContours.push_back(isoContour);
}

// - - - Create the above table - - - //
void Contour::createAboveTable(double isoVal){

    for(int i = 0; i < _mesh->getNumPts(); i++){
        bool x;
        if(_mesh->getMean(i) <= isoVal)
            x = 0;
        else
            x = 1;
        _aboveTable.push_back(x);
    }
}

// - - - Update above table - - - //
void Contour::updateAboveTable(double isoVal){
    for(unsigned int i = 0; i < _aboveTable.size(); i++){
        if(_aboveTable[i]){
            if(_mesh->getMean(i) <= isoVal)
                _aboveTable[i] = 0;
        }
    }
}
