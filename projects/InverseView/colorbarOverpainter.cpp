#include "colorbarOverpainter.h"
#include "ColorMapFactories.h"
#include "Color_Q.h"

ColorBarOverpainter::ColorBarOverpainter()
    : _minValue(0.0), _maxValue(1.0), _title("Set Title")
{
    _colormap = new Colormap(gray);
    _showTime = false;
    _timeStep = 0.0;
}

// - - - Draw the colorbar - - - //
void ColorBarOverpainter::draw(QPainter * painter, QGLWidget * glWidget){

    // The location & size of the bar
    int barStart = 150;
    int barEnd = glWidget->width()-150;
    int barWidth = barEnd-barStart;
    int barHeight = 25;
    int barTop = glWidget->height()-75;

    // Draw the gradient bar
    QRect rectBorder(barStart, barTop, barWidth, barHeight);
    QLinearGradient * gradient = getQLinearGradient(_colormap, rectBorder.topLeft(), rectBorder.bottomRight());
    painter->fillRect(rectBorder, *gradient);
    painter->setPen(Qt::black);
    painter->drawRect(rectBorder);

    // Draw the ticks
    QFontMetrics metrics = QFontMetrics(glWidget->font());
    int numTicks = 5;
    float t = barWidth/(numTicks-1);
    float start = barStart;
//    float min = _timestep + _minValue;
    float min = _minValue;
    float midTickPos = (_maxValue-_minValue)/float(numTicks-1);
    float tickHeight = 0;
    for(int i = 0; i < numTicks; i++){
        QString text = QString::number(min, 'g', 4) + "ms";
        QRect rect = metrics.boundingRect(text);
        tickHeight = rect.height();
        painter->drawText(start-rect.width()*.5, barTop+barHeight+5, rect.width(), rect.height(),  Qt::AlignHCenter | Qt::TextWordWrap, text);
        //painter->drawText(start-rect.width()*.5, barTop+barHeight+5, rect.width(), rect.height(),  Qt::AlignHCenter | Qt::TextWordWrap, text);
        min += midTickPos;
        start += t;
    }

    // Draw the label
    QRect rect = metrics.boundingRect(_title);
   ///painter->drawText(barStart+(barWidth*.5)-(rect.width()*.5), barTop-tickHeight-rect.height(), rect.width(), rect.height(),  Qt::AlignHCenter , _title);
    painter->drawText(barStart+(barWidth*.5)-(rect.width()*.5), barTop-rect.height(), rect.width(), rect.height(),  Qt::AlignHCenter , _title);

    if(_showTime){
        QString timeStepString = " Current Time: ";
        rect = metrics.boundingRect(timeStepString);
        float timeTextStart = barStart + (barWidth*.5) - (rect.width()*.5) - 25;
        painter->drawText(timeTextStart, barTop+barHeight+tickHeight+15, rect.width(), rect.height(),  Qt::AlignRight, timeStepString);
        QString timeString = QString::number(_timeStep, 'g', 3) + "ms";
        painter->drawText(timeTextStart+(rect.width()*.5), barTop+barHeight+tickHeight+15, rect.width(), rect.height(),  Qt::AlignRight , timeString);
    }
}
