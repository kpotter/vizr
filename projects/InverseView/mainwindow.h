#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "heartmesh.h"
//#include "animationControl.h"
#include "controlWidget.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    //AnimationControl * _animationControl;
    ControlWidget * _controlWidget;

public slots:
    void loadConfig();
    void toggleAnimate(){ std::cout << "Toggle" << std::endl; }//_animationControl->setEnabled(!_animationControl->isEnabled()); }

signals:
    void meshUpdate(HeartMesh * mesh);
};

#endif // MAINWINDOW_H
