#include "inverseViewerWidget.h"
#include "BoundingBoxDrawable.h"
#include "colorbarOverpainter.h"
#include <QKeyEvent>

InverseViewerWidget::InverseViewerWidget()
    : PaintWidget()
{
    init();
}

// - - -Initialize the widget - - - //
void InverseViewerWidget::init(){

  //  _bbDraw = new BoundingBoxDrawable();
    _heartDraw = new HeartMeshDrawable();
    _colorbar = new ColorBarOverpainter();
    _contourDraw = new ContourDrawable();

    connect(_heartDraw, SIGNAL(changeColormap(Colormap*,double,double,QString, bool, float)), _colorbar, SLOT(setColormap(Colormap*,double,double,QString, bool, float)));
    connect(_heartDraw, SIGNAL(changeColormap(Colormap*,double,double,QString, bool, float)), this, SLOT(update()));
 //   addDrawable(_bbDraw);
}

// * * * Key press event * * * //
void InverseViewerWidget::keyPressEvent(QKeyEvent * event){
  PaintWidget::keyPressEvent(event);
  update();
}


// * * * Animate * * * //
void InverseViewerWidget::animate(float timestep)
{
    PaintWidget::animate(timestep);
    //_colorbar->updateTimestep(timestep);

}

