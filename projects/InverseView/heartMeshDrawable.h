#ifndef HEARTMESHDRAWABLE_H
#define HEARTMESHDRAWABLE_H
#include "Drawable.h"
#include "heartmesh.h"
#include "colormap.h"

class HeartMeshDrawable : public Drawable
{
    Q_OBJECT
public:
    HeartMeshDrawable();
    void setMesh(HeartMesh * mesh);
    // Draw the mesh
    void draw();
    int getNumFrames(){ return _numFrames; }

public slots:
    void animate(float timestep);

    // Colormap the mesh by data values
    void colormapByIsoValue(double isoVal);
    void colormapByMean();
    void colormapByStd();
    void colormapByCombinedAnimate();
    void colormapByMeanAnimate();
    void colormapByStdAnimate();

signals:
    void changeColormap(Colormap *, double, double, QString, bool, float);

protected:

    bool _animate;
    int _numFrames;
    HeartMesh * _mesh;

    Colormap * _animateCombinedColormap;
    Colormap * _animateMeanColormap;
    Colormap * _animateStdColormap;
    Colormap * _meanColormap;
    Colormap * _stdColormap;

    std::vector< std::vector<ColorF> > _animateCombinedColors;
    std::vector< std::vector<ColorF> > _animateMeanColors;
    std::vector< std::vector<ColorF> > _animateStdColors;
    std::vector<ColorF> _meanColors;
    std::vector<ColorF> _stdColors;

    QString _title;
    Colormap * _colormap;
    std::vector< std::vector<ColorF> > _animateColors;

    // Generate the animated colors
    void generateCombinedAnimateColors();
    void generateMeanAnimateColors();
    void generateStdAnimateColors();
    void generateMeanColors();
    void generateStdColors();

    // Colormapping helper function
    void colormapBy(std::string type);

};

#endif // HEARTMESHDRAWABLE_H
