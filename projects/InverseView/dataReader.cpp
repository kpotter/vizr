#include "datareader.h"
#include <iostream>
#include <fstream>
#include <sstream>

//using namespace kcp;
using namespace std;

// - - - Read a point file - - - //
std::vector<Vertex> DataReader::readPoints(std::string filename){

    // The vector of points
    std::vector<Vertex> points;

    ifstream infile;
    infile.open(filename.c_str());

     while (!infile.eof()) {

         std::string line;
         getline(infile, line);
         if(line.size() != 0){
             stringstream ss(line);
             float x, y, z;
             ss  >> x >> y >> z ;
             Vertex pt(x, y, z);
             points.push_back(pt);
         }
     }
     return points;
}

// - - - Read an indices file - - - //
//std::vector<Vector3i> DataReader::readTriangles(std::string filename){
std::vector<int> DataReader::readTriangles(std::string filename){

    // The vector of triangles
    std::vector<int> tris;

    ifstream infile;
    infile.open(filename.c_str());

    while (!infile.eof()) {

        std::string line;
        getline(infile, line);
        stringstream ss(line);
        float x, y, z;
        ss  >> x >> y >> z ;
        //Vector3i pt(x-1, y-1, z-1); // Matlab index (-1)

        //std::cout << "x: " << x << " y " << y << " z " << z << std::endl;

        tris.push_back(x-1);
        tris.push_back(y-1);
        tris.push_back(z-1);
       // tris.push_back(pt);
    }
    return tris;
}

// - - - Read a data file - - - //
std::vector<double> DataReader::readData(std::string filename){

    // The vector of data
    std::vector<double> data;

    ifstream infile;
    infile.open(filename.c_str());

    while (!infile.eof()) {

        std::string line;
        getline(infile, line);
        stringstream ss(line);
        double d;
        ss >> d;
        data.push_back(d);
    }
    return data;
}


