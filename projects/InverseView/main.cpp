// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//    ____  _  _  _  _  ____  ____  ___  ____  _  _  ____  ____  _    _    //
//   (_  _)( \( )( \/ )( ___)(  _ \/ __)( ___)( \/ )(_  _)( ___)( \/\/ )   //
//    _)(_  )  (  \  /  )__)  )   /\__ \ )__)  \  /  _)(_  )__)  )    (    //
//   (____)(_)\_)  \/  (____)(_)\_)(___/(____)  \/  (____)(____)(__/\__)   //
//                                                                         //
// main.cpp                                                                //
// Written by Kristi Potter                                                //
// February 2013                                                           //
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#include <iostream>
#include <QApplication>
#include "MainWindow.h"

// Application to explore inverse ECG data
int main(int argc, char *argv[])
{
  QApplication a(argc, argv);

  MainWindow w;
  w.show();

  return a.exec();
}
