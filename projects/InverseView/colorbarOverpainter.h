#ifndef COLORBAROVERPAINTER_H
#define COLORBAROVERPAINTER_H

#include "Overpainter.h"
#include "colormap.h"

class ColorBarOverpainter : public Overpainter
{
    Q_OBJECT
public:
    ColorBarOverpainter();
    void draw(QPainter * painter, QGLWidget * glWidget);

public slots:
    void setColormap(Colormap * colormap, double min, double max, QString title, bool showTime, float timestep) {
        _colormap = colormap; _minValue = min; _maxValue = max; _title = title; _showTime = showTime; _timeStep = timestep;
    }

protected:
    Colormap * _colormap;
    float _minValue, _maxValue;
    QString _title;

    bool _showTime;
    float _timeStep;

};

#endif // COLORBAROVERPAINTER_H
