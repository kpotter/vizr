#ifndef HEARTMESH_H
#define HEARTMESH_H
#include "Mesh.h"

class HeartMesh : public Mesh
{
public:
    HeartMesh(std::vector<Vertex> vertices, std::vector<int> indices,
              std::vector<double> means, std::vector<double> stds,
              std::vector<Vector3f> normals = std::vector<Vector3f>(),
              std::vector<ColorF> colors = std::vector<ColorF>());

    double getMean(int i){return _means[i]; }
    double getStd(int i){return _stds[i]; }

    std::vector<double> getMeans() { return _means; }
    std::vector<double> getStds() { return _stds; }

    double getMeanMin(){ return *std::min_element(_means.begin(), _means.end()); }
    double getMeanMax(){ return *std::max_element(_means.begin(), _means.end());}
    double getStdMin(){ return *std::min_element(_stds.begin(), _stds.end()); }
    double getStdMax(){ return *std::max_element(_stds.begin(), _stds.end());}

protected:
    std::vector<double> _means;
    std::vector<double> _stds;
};

#endif // HEARTMESH_H
