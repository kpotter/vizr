#include "controlWidget.h"
#include <QRadioButton>

ControlWidget::ControlWidget(int numFrames)
{
    createGUI(numFrames);
}

// --- Create the GUI --- //
void ControlWidget::createGUI(int numFrames){

    // The radio button group
    _radioGroup = new QGroupBox();
    QRadioButton *radio1 = new QRadioButton(tr("&Mean (Static)"));
    QRadioButton *radio2 = new QRadioButton(tr("&Standard Deviation (Static)"));
    QRadioButton *radio3 = new QRadioButton(tr("Mean (&Animation)"));
    QRadioButton *radio4 = new QRadioButton(tr("Standard &Deviation (Animation)"));
    QRadioButton *radio5 = new QRadioButton(tr("&Combined (Animation)"));

    radio1->setChecked(true);
    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(radio1);
    vbox->addWidget(radio3);
    vbox->addWidget(radio2);  
    vbox->addWidget(radio4);
    vbox->addWidget(radio5);

    vbox->addStretch(1);
    _radioGroup->setLayout(vbox);

    // The animation group
    _animationGroup = new QGroupBox();
    QHBoxLayout *hBox = new QHBoxLayout;
    _animationControl = new AnimationControl;
    _animationControl->setNumberOfFrames(numFrames);
    _animationControl->setEnabled(false);
    hBox->addWidget(_animationControl);
    _animationGroup->setLayout(hBox);

    // The widget layout
    QHBoxLayout *layout = new QHBoxLayout;
    this->setLayout(layout);
    layout->addWidget(_radioGroup);
    layout->addWidget(_animationGroup);

    // Connect the radio buttons
    connect(radio1, SIGNAL(clicked()), this, SLOT(meanSlot()));
    connect(radio2, SIGNAL(clicked()), this, SLOT(stdSlot()));
    connect(radio3, SIGNAL(clicked()), this, SLOT(animateMeanSlot()));
    connect(radio4, SIGNAL(clicked()), this, SLOT(animateStdSlot()));
    connect(radio5, SIGNAL(clicked()), this, SLOT(animateCombinedSlot()));
    connect(_animationControl, SIGNAL(animateSignal(float)), this, SLOT(animateSlot(float)));
}
