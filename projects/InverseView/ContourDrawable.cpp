#include "ContourDrawable.h"
#include "GLIncludes.h"
#include "ColorDefines.h"
#include <QGLContext>
using namespace FTGL;

ContourDrawable::ContourDrawable()
{
    // Create the font
    _font = new FTGLPolygonFont("/Library/Fonts/AmericanTypewriter.ttc");

    if(_font->Error())
        std::cout << "FONT ERROR!" << std::endl;
    _font->FaceSize(4);
}

void ContourDrawable::setMesh(HeartMesh * mesh){
    _contour = new Contour(mesh, 5);
}


// Draw the contour label
void ContourDrawable::makeContourLabel(Vertex A2B, Vertex C2D, Vertex A, Vertex B, Vertex An, Vertex Bn, double isoVal){

    // Normalize the vectors
    A2B.normalize();
    C2D.normalize();

    // Get the Norm
    Vertex norm = (An+Bn)/2;
   // norm.normalize();

    //std::cout << "norm " << norm << std::endl;

    // First get the cosine of the rotation angle
    double toDegree = ((double)180/M_PI);
    Vertex rotAngle =  cross(A2B, C2D);
    double angle = asin(rotAngle.magnitude()) * toDegree;
    //Vertex rotAngle =  cross(norm, C2D);
    //double angle = asin(rotAngle.magnitude()) * toDegree;

    //std::cout << "rotAngle" << rotAngle << std::endl;
    //std::cout << "angle " << angle << std::endl;

   // Vertex normAxis = cross(A2B, norm);
   // double normAngle = asin(normAxis.magnitude()) * toDegree;

    glEnd();
    glPushMatrix();
    norm*=0.0625;


  //  glTranslatef(A[0], A[1], A[2]);
  glTranslatef(A[0]+norm.x(), A[1]+norm.y(), A[2]+norm.z());
    glRotatef(180, 1, 0, 0);
   //
    glRotatef(angle, rotAngle.x(), rotAngle.y(), rotAngle.z());
  //
   // glRotatef(normAngle, normAxis.x(), normAxis.y(), normAxis.z());
    glScalef(.015, .015, .015);

    // Set the font size and render a small text.
    char c[10];
    sprintf(c , "%0.1f" , isoVal);
    _font->Render(c);
    glPopMatrix();
    glBegin(GL_LINES);
/*
    glVertex3fv(A.v);
    Vertex nA = A + An;
    glVertex3fv(nA.v);

    glVertex3fv(B.v);
    Vertex nB = B+norm;
    glVertex3fv(nB.v);
    */
}


// Draw the contour
void ContourDrawable::draw(){
    glLineWidth(2.5);
    //glColor4fv(black.v);

    for(int i = 0; i < _contour->getNumContours(); i++){
   // for(int i = 2; i < 3; i++){
        std::vector<Vertex> isocontour = _contour->getContour(i);


        glBegin(GL_LINES);

        // Draw the isocontour
        for(int j = 0; j < isocontour.size()-1; j+=2){

            bool drawLabel = false;

            glColor3fv(black.v);

            // The contour line
            Vertex A = isocontour[j];
            Vertex B = isocontour[j+1];

            if(j%100 == 0){

                if(A > B){
                    Vertex tmp = A;
                    A = B;
                    B = tmp;
                }

                Vertex A2B = B-A;
                double length = A2B.length();
                if(length > 0.07){

                    makeContourLabel(A2B, Vertex(1,0,0), A, B, _contour->getNormal(j), _contour->getNormal(j+1), _contour->getIsoVal(i));
                  //  glColor3fv(red.v);
                    drawLabel = true;
                }
             }

            if(!drawLabel){
                glVertex3fv(A.v);
                glVertex3fv(B.v);
            }
        }

        glEnd();
    }




}
