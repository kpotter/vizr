#ifndef CONTOURDRAWABLE_H
#define CONTOURDRAWABLE_H

#include "Drawable.h"
#include "contour.h"
#include <QPainter>
#include <QGLWidget>
#include <FTGL/ftgl.h>

class ContourDrawable: public Drawable
{
public:
    ContourDrawable();
    void setMesh(HeartMesh * mesh);
    // Draw the contour
    //void draw(QPainter * painter, QGLWidget * glWidget);

    void draw();
   // }

protected:
     Contour * _contour;
     FTFont * _font;
     void makeContourLabel(Vertex A2B, Vertex C2D, Vertex A, Vertex B, Vertex An, Vertex Bn,double isoVal);
};

#endif // CONTOURDRAWABLE_H
