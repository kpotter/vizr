#include "heartMeshDrawable.h"
#include "GLIncludes.h"
#include "ColorMapFactories.h"
#include "Maths.h"

HeartMeshDrawable::HeartMeshDrawable()
{
    _mesh = 0;
    _animate = false;
    _numFrames = 84;
    _colormap = 0;
}

// - - - Set the mesh - - - //
void HeartMeshDrawable::setMesh(HeartMesh * mesh){
    _mesh = mesh;
    generateStdAnimateColors();
    generateMeanAnimateColors();
    generateCombinedAnimateColors();
    generateMeanColors();
    generateStdColors();
    colormapByMean();
}

// - - - Draw the heart mesh - - - //
void HeartMeshDrawable::draw(){

    if(_mesh){
        glBegin(GL_TRIANGLES);
        for(unsigned int i = 0; i < _mesh->getNumIdx(); i++){
            int idx = _mesh->getIdx(i);
            glColor4fv(_mesh->getColor(idx).v);
            glNormal3fv(_mesh->getNorm(idx).v);
            glVertex3fv(_mesh->getPt(idx).v);
        }
        glEnd();
    }

}

// - - - Colormap by isovalue - - - //
void HeartMeshDrawable::colormapByIsoValue(double isoVal){
    std::vector<ColorF> isoColors;

    for(int i = 0; i < _mesh->getNumPts(); i++){
        if(_mesh->getMean(i) < isoVal)
            isoColors.push_back(red);
        else
            isoColors.push_back(gray);
    }
    _mesh->setColors(isoColors);
}

// - - - Change the colormap - - - //
void HeartMeshDrawable::colormapBy(std::string type){

    if(!_mesh)
        return;

    bool showTime;
    std::string title = "Colorbar Title";
    if(type == "meanAnimate"){
        _animate = true;
        _colormap = _animateMeanColormap;
        _animateColors = _animateMeanColors;
        _title = QString("Mean Activation Time");
        showTime = true;
    }
    else if(type == "stdAnimate"){
        _animate = true;
        _colormap = _animateStdColormap;
        _animateColors = _animateStdColors;
        _title = "Standard Deviation of Activation Time";
        showTime = true;
    }
    else if(type == "combinedAnimate"){
        _animate = true;
        _colormap = _animateCombinedColormap;
        _animateColors = _animateCombinedColors;
        _title = "Deviation of Mean Activation Time from Current Time";
         showTime = true;
    }
    else if(type == "mean"){
        _animate = false;
        _colormap = _meanColormap;
        _mesh->setColors(_meanColors);
        _title = "Mean Activation Time";
        showTime = false;
    }
    else if(type == "std"){
        _animate = false;
        _colormap = _stdColormap;
        _mesh->setColors(_stdColors);
        _title = "Standard Deviation of Activation Time";
        showTime = false;
    }
    emit changeColormap(_colormap, _colormap->getRangeMin(), _colormap->getRangeMax(), _title, showTime, _timeStep);
}

// - - - Colormap by mean and animate - - - //
void HeartMeshDrawable::colormapByMeanAnimate(){ colormapBy("meanAnimate"); }
// - - - Colormap by std and animate - - - //
void HeartMeshDrawable::colormapByStdAnimate(){ colormapBy("stdAnimate"); }
// - - - Colormap by mean & std combined and animate - - - //
void HeartMeshDrawable::colormapByCombinedAnimate(){ colormapBy("combinedAnimate"); }
// - - - Colormap by mean - - - //
void HeartMeshDrawable::colormapByMean(){ colormapBy("mean"); }
// - - - Colormap by std - - - //
void HeartMeshDrawable::colormapByStd(){ colormapBy("std"); }


// - - - Update colors based on animation timestep - - - //
void HeartMeshDrawable::animate(float timestep){

    if(!_mesh)
        return;

    if(_animate){
     int timeIdx = (timestep/100.0)*_numFrames;
     _mesh->setColors(_animateColors[timeIdx]);
     emit changeColormap(_colormap, _colormap->getRangeMin(), _colormap->getRangeMax(), _title, true, timestep);
    }
}

// - - - Generate the mean colors - - - //
void HeartMeshDrawable::generateMeanColors(){

    std::vector<double> means = _mesh->getMeans();
    double min = *std::min_element(means.begin(), means.end());
    double max = *std::max_element(means.begin(), means.end());

    ColormapFactory * factory = new BlueYellowColormapFactory(min, max);
    _meanColormap = factory->getColormap();
    _meanColors.resize(_mesh->getNumPts());
    for(unsigned int i = 0; i < _mesh->getNumPts(); i++){
        _meanColors[i] = _meanColormap->getColor(means[i]);
    }
}

// - - - Generate the std colors - - - //
void HeartMeshDrawable::generateStdColors(){

    std::vector<double> stds = _mesh->getStds();
    double min = *std::min_element(stds.begin(), stds.end());
    double max = *std::max_element(stds.begin(), stds.end());

    //ColormapFactory * factory = new PurpleYellowColormapFactory(min, max);
    ColormapFactory * factory = new TealTangerineColormapFactory(min, max);
    _stdColormap = factory->getColormap();
    _stdColors.resize(_mesh->getNumPts());
    for(unsigned int i = 0; i < _mesh->getNumPts(); i++){
        _stdColors[i] = _stdColormap->getColor(stds[i]);
    }   
}

// - - - Generate the std animated colors - - - //
void HeartMeshDrawable::generateStdAnimateColors(){

    std::vector<double> stds = _mesh->getStds();
    double min = *std::min_element(stds.begin(), stds.end());
    double max = *std::max_element(stds.begin(), stds.end());

    ColormapFactory * cmf = new TealTangerineColormapFactory(min, max);
    _animateStdColormap = cmf->getColormap();

    int timestep = 0;
    while(timestep < _numFrames){

        // The colors for this time step
        std::vector<ColorF> stepColors;

        for(unsigned int i = 0; i < stds.size(); i++){

            // If the mean falls within this range
            if(timestep <= stds[i] && stds[i] <= timestep+1)
                stepColors.push_back(_animateStdColormap->getColor(stds[i]));
            else
                stepColors.push_back(gray);

        }
        _animateStdColors.push_back(stepColors);
        timestep ++;
    }
}

// - - - Generate the mean animated colors - - - //
void HeartMeshDrawable::generateMeanAnimateColors(){

    std::vector<double> means = _mesh->getMeans();
    double min = *std::min_element(means.begin(), means.end());
    double max = *std::max_element(means.begin(), means.end());


    ColormapFactory * cmf = new BlueYellowColormapFactory(min, max);
    _animateMeanColormap = cmf->getColormap();

    int timestep = 0;
    while(timestep < _numFrames){

        // The colors for this time step
        std::vector<ColorF> stepColors;

        for(unsigned int i = 0; i < means.size(); i++){

            // If the mean falls within this range, color it base on the mean
            if(timestep <= means[i] && means[i] <= timestep+1)
                stepColors.push_back(_animateMeanColormap->getColor(means[i]));
            else
                stepColors.push_back(gray);
        }
        _animateMeanColors.push_back(stepColors);
        timestep ++;
    }
}


// - - - Generate the combined animated colors - - - //
void HeartMeshDrawable::generateCombinedAnimateColors(){

    std::vector<double> means = _mesh->getMeans();
    std::vector<double> stds = _mesh->getStds();
    double min = *std::min_element(means.begin(), means.end());
    double max = *std::max_element(means.begin(), means.end());
    double stdMin = *std::min_element(stds.begin(), stds.end());
    double stdMax = *std::max_element(stds.begin(), stds.end());


    ColormapFactory * cmf =  new MinskTwilightColormapFactory(-stdMax, stdMax);
    _animateCombinedColormap = cmf->getColormap();

    int timestep = 0;
    while(timestep < _numFrames){

        // The colors for this time step
        std::vector<ColorF> stepColors;

        for(unsigned int i = 0; i < means.size(); i++){

            int rangeL = floor(means[i]-stds[i]);
            int rangeH = ceil(means[i]+stds[i]);

            // If the mean falls within this range
            if(rangeL <= timestep && timestep <=rangeH){

                // Color it by how far the mean is from the timestep
                double val = timestep - means[i];
                stepColors.push_back(_animateCombinedColormap->getColor(val));
            }
            else
                stepColors.push_back(gray);

        }
        _animateCombinedColors.push_back(stepColors);
        timestep ++;
    }
}

