#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <fstream>
#include <QFileDialog>
#include <QDockWidget>
#include "datareader.h"
#include "inverseViewerWidget.h"
#include "BoundingBoxDrawable.h"

using namespace std;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    // Set up the ui created in designer
    ui->setupUi(this);

    // Create the central widget
    QDockWidget * dock = new QDockWidget ();
    InverseViewerWidget * glWidget = new InverseViewerWidget();
    //glWidget->addDrawable(new BoundingBoxDrawable());
    dock->setWidget(glWidget);
    setCentralWidget(dock);

    // Create the control widget
    dock = new QDockWidget ();
    _controlWidget = new ControlWidget(glWidget->getNumFrames());
    dock->setWidget(_controlWidget);
    addDockWidget (Qt::BottomDockWidgetArea, dock);    

    // Connect the main window's action signals
    connect(this->ui->actionLoadConfig, SIGNAL(triggered()), this, SLOT(loadConfig()));
    connect(this, SIGNAL(meshUpdate(HeartMesh*)), glWidget, SLOT(setMesh(HeartMesh*)));
    connect(_controlWidget, SIGNAL(meanSignal()), glWidget->getHeartMesh(), SLOT(colormapByMean()));
    connect(_controlWidget, SIGNAL(stdSignal()), glWidget->getHeartMesh(), SLOT(colormapByStd()));
    connect(_controlWidget, SIGNAL(animateMeanSignal()), glWidget->getHeartMesh(), SLOT(colormapByMeanAnimate()));
    connect(_controlWidget, SIGNAL(animateStdSignal()), glWidget->getHeartMesh(), SLOT(colormapByStdAnimate()));
    connect(_controlWidget, SIGNAL(animateCombinedSignal()), glWidget->getHeartMesh(), SLOT(colormapByCombinedAnimate()));
    connect(_controlWidget, SIGNAL(animate(float)), glWidget, SLOT(animate(float)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadConfig(){

    // Query the user for a config file
    QString fileName = QFileDialog::getOpenFileName(this, tr("Load Config File"), "/Users/kpotter/Data/InverseHeart/", tr("Text Files (*.txt)"));

    std::cout << "Reading Config File." << std::endl;

    // Read the config file
    if (fileName.length() != 0) {
        ifstream infile;
        infile.open(fileName.toStdString().c_str());
        std::string baseDir, nodeFile, faceFile, meanFile, stdFile;
        while (!infile.eof()) {
            std::string line;
            getline(infile, line);
            size_t found = line.find_first_of(":");
            if(line.find("BASE_DIR") != string::npos)
                baseDir =  QString(line.substr(found + 1).c_str()).trimmed().toStdString();
            if (line.find("NODES") != string::npos)
                nodeFile = QString(line.substr(found + 1).c_str()).trimmed().toStdString();
            if (line.find("FACES") != string::npos)
                faceFile = QString(line.substr(found + 1).c_str()).trimmed().toStdString();
            if (line.find("MEAN") != string::npos)
                meanFile = QString(line.substr(found + 1).c_str()).trimmed().toStdString();
            if (line.find("STD") != string::npos)
                stdFile = QString(line.substr(found + 1).c_str()).trimmed().toStdString();
           }
           infile.close();     

           DataReader * dr = new DataReader(baseDir+nodeFile, baseDir+faceFile, baseDir+meanFile, baseDir+stdFile);
           emit meshUpdate(dr->getMesh());
    }
}

