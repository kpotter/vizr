// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                                                           [Text]*[Projection] 
// Project to play with text projection
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// main.cpp
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#include <iostream>
#include <QApplication>
#include "mainwindow.h"

// Application to explore inverse ECG data
int main(int argc, char *argv[])
{
  QApplication a(argc, argv);

  MainWindow w;
  w.show();

  return a.exec();
}
