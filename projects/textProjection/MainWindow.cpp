#include "MainWindow.h"
#include "ProjectionWidget.h"
#include "ProjectionDrawable.h"
#include <QGridLayout>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    // Create the central widget
    QWidget * centralWidget = new QWidget;

    // Create the layout
    QGridLayout * layout = new QGridLayout();
    centralWidget->setLayout(layout);

    // Set the central widget
    setCentralWidget(centralWidget);

    // Create the gl window widget
    ProjectionWidget *glWidget = new ProjectionWidget();
    glWidget->addDrawable(new ProjectionDrawable());
    layout->addWidget(glWidget, 0, 0);
}
