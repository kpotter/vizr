#ifndef PROJECTIONDRAWABLE_H
#define PROJECTIONDRAWABLE_H
#include "Drawable.h"
#include "ColorDefines.h"
#include "GLIncludes.h"

class ProjectionDrawable : public Drawable
{
    Q_OBJECT
public:
    explicit ProjectionDrawable();
    void draw();
signals:
    
public slots:
    
};

#endif // PROJECTIONDRAWABLE_H
