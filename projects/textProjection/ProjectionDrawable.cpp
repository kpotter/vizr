#include "ProjectionDrawable.h"

#include <FTGL/ftgl.h>
using namespace FTGL;

ProjectionDrawable::ProjectionDrawable() :
    Drawable()
{
}

void ProjectionDrawable::draw(){
    // Create the font
    FTFont* font = new FTGLPolygonFont("/Library/Fonts/AmericanTypewriter.ttc");

    if(font->Error())
        return;
    font->FaceSize(4);

    glBegin(GL_LINES);

    glColor3fv(black.v);
    glVertex3f(0, -1, 0);
    glVertex3f(0, 1, 0);

    glColor3fv(red.v);
    glVertex3f(-1, 0, 0);
    glVertex3f(1, 0, 0);

   // Vertex A(3, 0, 0);
   // Vertex B(3.75, .75, .75);

  //  Vertex A( 0.281436, 0.240134, 0.670673);
   // Vertex B(0.202437, 0.202915, 0.640155);

 //   Vertex A(0.790893, 0.222008, 0.0296141);
  //  Vertex B(0.83953, 0.149241, -0.00121942);
    Vertex A(0.281436, 0.240134, 0.670673);
    Vertex B(0.202437, 0.202915, 0.640155);

    // The vector to transform to
    glColor3fv(blue.v);

    glVertex3fv(A.v);
    glVertex3fv(B.v);

    // The text vector
    glColor3fv(purple.v);
    Vertex C(0,0,0);
    Vertex D(1,0,0);
    glVertex3fv(C.v);
    glVertex3fv(D.v);

    glEnd();

    // Find the vector we want to align this text label to
    Vertex A2B;
    if(A > B)
        A2B = A-B;
    else
        A2B = B-A;


   // Vertex A2B = A - B;
   // std::cout << "A2B " << A2B << std::endl;
   // std::cout << "A2B" << A2B.normalize() << std::endl;
   // std::cout << "(B-A)" << (B-A).normalize() << std::endl;

    A2B.normalize();

   // std::cout << "A2B " << A2B << std::endl;

    // Find the vector of the text
    Vertex C2D = D - C;
    C2D.normalize();
 //   std::cout << "C2D " << C2D << std::endl;

    // First get the cosine of the rotation angle
    double toDegree = ((double)180/M_PI);
    Vertex rotAngle =  cross(A2B, C2D);
    double angle = asin(rotAngle.magnitude()) * toDegree;

    std::cout << "rotAngle" << rotAngle << std::endl;
    std::cout << "angle " << angle << std::endl;

    glPushMatrix();
    glTranslatef(A[0], A[1], A[2]);
    glRotatef(180, 1, 0, 0);
    glRotatef(angle, rotAngle.x(), rotAngle.y(), rotAngle.z());
    glScalef(.025, .025, .025);


    glColor3fv(black.v);
    std::string text = "Hiya!";
    font->Render(text.c_str());

    glColor3fv(purple.v);
    glBegin(GL_LINES);
    glVertex3fv(C.v);
    glVertex3fv(D.v);
    glEnd();

    glPopMatrix();

}
