#ifndef PROJECTIONWIDGET_H
#define PROJECTIONWIDGET_H

#include "PaintWidget.h"

class ProjectionWidget : public PaintWidget
{
    Q_OBJECT
public:
    ProjectionWidget();

    // Initialize the widget
    void init();
    void keyPressEvent(QKeyEvent * event);
};

#endif // PROJECTIONWIDGET_H
