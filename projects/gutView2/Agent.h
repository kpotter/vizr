// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                                                                    [GUT] * [VIEW] 
// Project to explore gut microbe data. 
// Written by Kristi Potter, 2013
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //

#include <QStringList>
#include <iostream>
#include "Vector.h"
#ifndef _AGENT_H
#define _AGENT_H


// Agent
class Agent{

 public:
  Agent(){}
  virtual ~Agent(){} 
 Agent(std::string name, QStringList agentParams): _name(name), _death(-1.0){
    QStringList::const_iterator it = agentParams.constBegin();
    if(agentParams.size() >= 14){
      _family         = (*it).toInt();
      _genealogy      = (*++it).toInt();
      _generation     = (*++it).toInt();
      _birthday       = (*++it).toFloat();
      _biomass        = (*++it).toFloat();
      _inert          = (*++it).toFloat();
      _capsule        = (*++it).toFloat();
      _growthRate     = (*++it).toFloat();
      _volumeRate     = (*++it).toFloat();
      _locationX      = (*++it).toFloat();
      _locationY      = (*++it).toFloat();
      _locationZ      = (*++it).toFloat();
      _radius         = (*++it).toFloat();
      _totalRadius    = (*++it).toFloat();
      setPosition(_locationX, _locationY, _locationZ);
    }
    if(agentParams.size() >= 15 ){
      _death  = (*++it).toFloat();
    }
  }

  // Set the position of the agent
  void setPosition(float x, float y, float z){
     _position = Vector3f(x, y, z);
  }

  // Set the radius of the agent
  void setScaledRadius(float radius){
    _radiusScaled = radius;
  }
 
  // Print the agent information
  void printAgent(){
    std::cout << "Agent: " << _name << std::endl;
    std::cout << "family: " << _family << " genealogy: " << _genealogy << " generation: "
	      << _generation << " birthday: " << _birthday << std::endl;
    std::cout << "biomass: " << _biomass << " inert: " << _inert << " capsule: "
	      << _capsule << " growthRate: " << _growthRate << " volumeRate: " << _volumeRate << std::endl;
    std::cout << "locationX: "<< _locationX << " locationY: "<< _locationY << " locationZ: " <<_locationZ << std::endl;
    std::cout << "radius: " << _radius << " totalRadius: " << _totalRadius << std::endl;
    std::cout << "death: " << _death << std::endl;
  } 

  // Print the agent family information
  void printFamily(){
    std::cout << "Agent: " << _name << "family: " << _family << " generation: " << _generation
	      << " birthday: " << _birthday << std::endl;
  }
  
  float getX(){ return _locationX; }
  float getY(){ return _locationY; }
  float getZ(){ return _locationZ; }
  float getRadius(){ return _radius; }
  float getScaledRadius(){ return _radiusScaled; }
  Vector3f getPosition(){ return _position; }
  std::string getSpecies(){ return _name; }
  int getFamily(){ return  _family; }
  int getGeneration(){ return _generation; }
  float getBirthday(){ return _birthday; }
  int getGenealogy(){ return _genealogy; }


private:
  std::string _name;
  int _family, _genealogy, _generation;
  float _birthday, _biomass, _inert, _capsule, _growthRate, _volumeRate;
  float _locationX, _locationY, _locationZ, _radius, _totalRadius;
  float _death;
  Vector3f _position; // The scaled location
  float _radiusScaled; // The scaled radius

};

#endif
