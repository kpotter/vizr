// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// Grid.h
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#ifndef _GRID_H
#define _GRID_H
#include <iostream>

// Class to hold the grid information
class Grid{
  
public:
  
  Grid(float resolution, int ni, int nj, int nk) : _resolution(resolution), _ni(ni), _nj(nj), _nk(nk){}
  ~Grid(){}
  
  void print(){ 
    std::cout << "Grid" << std::endl;
    std::cout << "resolution: " << _resolution << std::endl;
    std::cout << " ni: " << _ni << " nj: " << _nj << " nk: " << _nk << std::endl; 
  }
  
  float getResolution(){ return _resolution; }
  int getNI(){ return _ni; }
  int getNJ(){ return _nj; }
  int getNK(){ return _nk; }

  float getXDim(){ return _ni*_resolution; }
  float getYDim(){ return _nj*_resolution; }
  float getZDim(){ return _nk*_resolution; }

protected:
  float _resolution;
  int _ni, _nj, _nk;
};

#endif
