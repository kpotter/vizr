// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// GutPaintWidget.h
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#ifndef _GUT_PAINT_WIDGET_H
#define _GUT_PAINT_WIDGET_H

#include "GLWidget.h"
#include "Simulation.h"
#include "SimulationDrawable.h"
#include "colormap.h"

// Class to handle the drawing of the simulation
class GutPaintWidget : public GLWidget{
  Q_OBJECT
  
public:
  // Constructor/Destructor
  GutPaintWidget(std::string title = "Gut Microbe Viewer", QWidget * parent = 0);
  virtual ~GutPaintWidget(){}

  // Create the scene to draw
  void createScene();

  // Set the color map for the species
  void setSpeciesColors(Simulation * simulation);

  // Get mapping of species to printable rgba color
  std::map<std::string, std::string> getSpeciesColors();
  
  // Get the species colormaps
  std::map<std::string, Colormap*> getSpeciesColormaps(){ return _speciesColorMaps; }						       
						       

			       
public slots:
  // Set the simulation
  void setSimulation(Simulation * simulation);
  void legendEvts(int setting);
  void setBoxBottom(int value){
    _bottom = value;
    _simulationDraw->setBoxBottom(_bottom);
  }
  void setBoxTop(int value) {
    _top = value;
    _simulationDraw->setBoxTop(_top);
  }
  void setBlackAndWhite(bool blackAndWhite){
    _simulationDraw->setBlackAndWhite(blackAndWhite);
    update();
  }
  void setBoundingBox(bool boundingBox){
    _simulationDraw->setBoundingBox(boundingBox);
    update();
  }
  void updateBox(int bottom, int top){
    setBoxBottom(bottom);
    setBoxTop(top);
    update();
  }

  
 protected:

  // The top and bottom cutting planes
  int _top, _bottom;
  
  // The min and max data values to colormap by
  int _minData, _maxData;

  // The simulation
  Simulation * _simulation;
  
  // A drawable for the simulation
  SimulationDrawable * _simulationDraw;
  
  // Color map of agent species
  std::map<std::string, ColorF> _speciesColors;
  std::map<std::string, ColorF> _speciesColorsLight;
  std::map<std::string, Colormap *> _speciesColorMaps;


};
#endif
