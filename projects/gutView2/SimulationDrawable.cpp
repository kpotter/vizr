// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// SimulationDrawable.cpp
// Written by Kristi Potter 2014
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#include "SimulationDrawable.h"

// Draw all of the agents
void SimulationDrawable::draw(){
  if(_drawBox)
    drawBox();
  
  /* 
  // Testing Spheres
  glPushMatrix();
  glColor3f(1.0, 0.0, 0.0); 
  glutSolidSphere(.05, 25, 25);
  glTranslatef(.1, -.1, 0);
  glutSolidSphere(.05, 25, 25);
  glPopMatrix();
  */
  
  if(_timeStep != -1 && _agents.find(_timeStep) != _agents.end()){
    _agents[int(_timeStep)]->draw(_displayedSpecies, _top, _bottom);
   }
}

// Set the hide or show state of each agent
void SimulationDrawable::hideShowDrawables(int setting, std::string name, QObject *legend) {
  if(name.substr(0,5) == "agent"){
    // Set species hidden status to new setting
    _displayedSpecies[name.substr(6)] = setting;
  }
}

/// Add an agent list to the simulation drawable
void SimulationDrawable::addAgentList(int iteration, std::vector<Agent *> agents,
				      std::map<std::string, Colormap *> speciesColorMaps){
  // Add in the iteration's agents
  _agents[iteration] = new AgentDrawable(iteration, agents, speciesColorMaps);
  
  // initialize list of displayed species
  for(std::map<std::string, Colormap *>::iterator it = speciesColorMaps.begin(); it != speciesColorMaps.end(); it++)
    _displayedSpecies[it->first] = 1;
}

