// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// GutPaintWidget.cpp
// Written by Kristi Potter 2013..
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#include "GutPaintWidget.h"
#include <sstream>
#include <math.h>

// * * * Constructur * * * //
GutPaintWidget::GutPaintWidget(std::string title, QWidget * parent)
  : GLWidget(title, parent), _top(0), _bottom(0){
  //setBackgroundColor(Qt::black);
  createScene();
  connect(this, SIGNAL(changeBlackAndWhite(bool)), this, SLOT(setBlackAndWhite(bool)));
  connect(this, SIGNAL(changeBoundingBox(bool)), this, SLOT(setBoundingBox(bool)));
}

// * * * Create the scene * * *  //
void GutPaintWidget::createScene(){

  // Create default instances of the drawables
  _simulationDraw  = new SimulationDrawable();
  addDrawable(_simulationDraw);
}

// * * *  Set the simulation, update the scene * * * //
void GutPaintWidget::setSimulation(Simulation * simulation){
  
  // If we have no iterations, return
  if(simulation->getNumIterations() == 0)
    return;
  
  // Set the simulation
  _simulation = simulation;

  // Rescale the bounding box
  _simulation->rescale(_simulationDraw->getBoundingBox());
  
  // Set the simulation colors
  setSpeciesColors(_simulation);

  // Add agents to the simulation drawable
  std::map<int, Iteration*> iterations = _simulation->getIterations();  
  for(std::map<int, Iteration*>::iterator it = iterations.begin(); it != iterations.end(); it++)
    _simulationDraw->addAgentList(it->first, _simulation->getAgentList(it->first),
    				  _speciesColorMaps);
  update();
}

// Set the species mapping
void GutPaintWidget::setSpeciesColors(Simulation * simulation){
  std::cout << "Gut Paint set species colors" << std::endl;
  
  // Get the species names from the simulation
  std::vector<std::string> speciesNames = simulation->getSpeciesNames();

  // Initialize species colors vectors
  for(unsigned int i=0; i < speciesNames.size(); i++) {
    _speciesColors[speciesNames[i]] = colorList[i];
    _speciesColorsLight[speciesNames[i]] = colorListLight[i];
  }
  
  // Create colormaps for each species name
  for(unsigned int i=0; i < speciesNames.size(); i++) {
    
    if(speciesNames[i] == "WT"){
      std::pair<int, int> minMax = simulation->getMinMaxSpeciesData(speciesNames[i]);
      TwoColormapFactory * tmp = new TwoColormapFactory( _speciesColors[speciesNames[i]], _speciesColorsLight[speciesNames[i]]);
      // TwoColormapFactory * tmp = new TwoColormapFactory( black, white );
      if(minMax.first != minMax.second){
	tmp->setRange(minMax.first, minMax.second);
	// std::cout << "min max: " << minMax.first << ", " << minMax.second << std::endl;
      }
      _speciesColorMaps[speciesNames[i]] = tmp->getColormap();
    }
    else{
      std::pair<int, int> minMax = simulation->getMinMaxSpeciesData(speciesNames[i]);
      //std::cout << "min max: " << minMax.first << ", " << minMax.second << std::endl;    
      TwoColormapFactory * tmp = new TwoColormapFactory( _speciesColors[speciesNames[i]],
							 _speciesColorsLight[speciesNames[i]]);

      TwoColormap *tcm = dynamic_cast<TwoColormap * >(tmp->getColormap());
      tcm->setColorByHisto(true);
      tcm->setNumBins(100);
      
      if(minMax.first != minMax.second){
	tcm->setRange(minMax.first, minMax.second);
      }
      _speciesColorMaps[speciesNames[i]] = tcm;
    }    
  }
}

// Get agent type to species mapping
std::map<std::string, std::string> GutPaintWidget::getSpeciesColors() {
  std::map<std::string, std::string> speciesColorsReturnMap;

  std::map<std::string, ColorF>::iterator iter;
  for(iter = _speciesColors.begin(); iter != _speciesColors.end(); ++iter) {
    Color<int> thisSpeciesColor = iter->second.toInt();

    std::stringstream r, g, b, a;
    r << thisSpeciesColor.r();
    g << thisSpeciesColor.g();
    b << thisSpeciesColor.b();
    a << thisSpeciesColor.a(); 
    std::string speciesColorOutput = "rgba("+ r.str()+","+g.str()+","+b.str()+","+a.str()+");";
    speciesColorsReturnMap.insert(std::pair<std::string, std::string>(iter->first, speciesColorOutput));
  }
  return speciesColorsReturnMap;
}

// Respond to signals from Legend
void GutPaintWidget::legendEvts(int setting) {
    QObject* legendItem = sender();
    std::string name = legendItem->objectName().toAscii().constData();
    _simulationDraw->hideShowDrawables(setting, name, legendItem->parent());
    update();
}
