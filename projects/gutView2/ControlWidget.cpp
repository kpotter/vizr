// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// ControlWidget.cpp
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#include "ControlWidget.h"
#include <QRadioButton>

// --- Create the GUI --- //
void ControlWidget::createGUI(){

    // The animation group
    _animationGroup = new QGroupBox();
    QHBoxLayout *hBox = new QHBoxLayout;
    _animationControl = new AnimationControl;
    _animationControl->setEnabled(false);
    hBox->addWidget(_animationControl);
    _animationGroup->setLayout(hBox);

    // The widget layout
    QHBoxLayout *layout = new QHBoxLayout;
    this->setLayout(layout);
    layout->addWidget(_animationGroup);

    // Connect the radio buttons
    connect(_animationControl, SIGNAL(animateSignal(float)), this, SLOT(animateSlot(float)));
}

// ---- Set the time information from the simulation --- //
void ControlWidget::setTimeInformation(int minTime, int maxTime, int numFrames,
				       std::string timeUnit, int timeout){
  _animationControl->setTimeInformation(minTime, maxTime, numFrames, timeUnit, timeout);
  if(minTime != maxTime)
    _animationControl->setEnabled(true);
}
