// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// AgentDrawable.h
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#ifndef _AGENTDRAWABLE_H
#define _AGENTDRAWABLE_H

#include <vector>
#include <map>
#include "Grid.h"
#include "Agent.h"
#include "Drawable.h"
#include "colormap.h"

/*
 * Class to draw the agents as sphere
 */
class AgentDrawable : public Drawable{

  Q_OBJECT
public:
  
  // Constructor
  AgentDrawable(int iteration, std::vector<Agent * > agents,
		std::map<std::string, Colormap *> speciesColorMaps);
  
  // Draw the agents
  void draw(std::map<std::string, int> displayedSpecies, int, int);
  
  // Set the agents 
  void setAgents(std::vector<Agent * > agents){ _agents = agents; }

  // Get the number of agents
  int getNumAgents(){ return _agents.size(); }

  // Set black and white
  void setBlackAndWhite(bool blackAndWhite){ _blackAndWhite = blackAndWhite; }
  
protected:
  
  int _iteration;
  bool _blackAndWhite;
  std::vector<Agent* > _agents;
  //std::map<std::string, ColorF> _speciesColors;
  //std::map<std::string, ColorF> _speciesColorsLight;
  std::map<std::string, Colormap *> _speciesColorMaps;

  TwoColormapFactory * depthFactory;
  Colormap * depthColormap;

};
#endif
