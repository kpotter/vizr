// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// AgentDrawable.cpp
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#include "AgentDrawable.h"
#include "GLIncludes.h"
#include "Maths.h"
#include "ColorDefines.h"

// Constructor
AgentDrawable::AgentDrawable(int iteration, std::vector<Agent * > agents, std::map<std::string, Colormap *> speciesColorMaps)
  : Drawable(), _iteration(iteration), _agents(agents), _speciesColorMaps(speciesColorMaps),
    _blackAndWhite(false) {

  depthFactory = new TwoColormapFactory(black, ltgray);//dkgrey, ltgray);//dkgray, ltgray);//
  depthFactory->setRange(0.0, 150.0);
  depthColormap = depthFactory->getColormap();
}

/*
 *  Draw the agents
 */
void AgentDrawable::draw(std::map<std::string, int> displayedSpecies, int top, int bottom){


  std::string speciesName;
  for(int i = 0; i < _agents.size(); i++){
    speciesName = _agents[i]->getSpecies();

    // std::cout << "agents: " << _agents[i]->getPosition().x() << std::endl;

    
    // Check if species is hidden
    if(displayedSpecies[speciesName] == 0)
      continue;

    if(_agents[i]->getX() > top || _agents[i]->getX() < bottom)
      continue;
    
    else{    
      glPushMatrix();
      if(_blackAndWhite){

	// Depth-based coloring
	glColor3fv(depthColormap->getColor(_agents[i]->getX()).v);

	// Simple grayscale
	//glColor4fv(ltgray.v);
	//glColor4fv(dkgrey.v);
	
      }
      else{
	Colormap * cmap = _speciesColorMaps[speciesName];
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Change color variable HERE
	//ColorF color = _speciesColorMaps[speciesName]->getColor(_agents[i]->getGeneration()); // Generation
	ColorF color = _speciesColorMaps[speciesName]->getColor(_agents[i]->getFamily());     // Family
	//ColorF color = _speciesColorMaps[speciesName]->getColor(_agents[i]->getBirthday());   // Birthday
	//ColorF color = _speciesColorMaps[speciesName]->getColor(_agents[i]->getGenealogy());	// Genealogy
	//ColorF color = _speciesColorMaps[speciesName]->getColor(0.0);                           // Solely on species name
    
	glColor3fv(color.v);
	//	glColor3fv(_speciesColorMaps[speciesName]->getColor(0.0).v);
      }      
      glTranslatef(_agents[i]->getPosition().x(),
		   _agents[i]->getPosition().y(),
		   _agents[i]->getPosition().z());
      glutSolidSphere(_agents[i]->getScaledRadius(), 25, 25);
      glPopMatrix();
    }
  }
}

