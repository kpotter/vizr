// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// BoxSliderWidget.cpp
// Written by Kristi Potter 2015
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#include <QtGui>
#include <iostream>
#include "BoxSliderWidget.h"

BoxSliderWidget::BoxSliderWidget(Qt::Orientation orientation, const QString &title,
			   QWidget *parent)
  : QGroupBox(title, parent)
{
  
  bottomLabel = new QLabel(tr("Bottom: 0"));
  bottomLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  bottomLabel->setFixedSize(75, 25);
  bottom_slider = new QSlider(orientation);
  bottom_slider->setFocusPolicy(Qt::StrongFocus);
  bottom_slider->setTickPosition(QSlider::TicksBothSides);
  bottom_slider->setTickInterval(10);
  bottom_slider->setSingleStep(1);
  
  topLabel = new QLabel(tr("Top: 0"));
  topLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  topLabel->setFixedSize(50, 25);
  top_slider = new QSlider(orientation);
  top_slider->setFocusPolicy(Qt::StrongFocus);
  top_slider->setTickPosition(QSlider::TicksBothSides);
  top_slider->setTickInterval(10);
  top_slider->setSingleStep(1);

  updateButton = new QPushButton(tr("Update"));
  
  connect(bottom_slider, SIGNAL(valueChanged(int)), this, SLOT(setBottomValue(int)));
  connect(top_slider, SIGNAL(valueChanged(int)), this, SLOT(setTopValue(int)));
  connect(updateButton, SIGNAL(clicked()), this, SLOT(updateWindow()));
  
  QGridLayout *slidersLayout = new QGridLayout;
  slidersLayout->addWidget(bottomLabel,0,0);
  slidersLayout->addWidget(bottom_slider,1,0);
  slidersLayout->addWidget(topLabel,0,1);
  slidersLayout->addWidget(top_slider,1,1);
  slidersLayout->addWidget(updateButton, 2,0, 1, 2);
  setLayout(slidersLayout);
 }

 void BoxSliderWidget::setBottomValue(int value)
 {
   
   bottom_slider->setValue(value);  
   bottomLabel->setText("Bottom: " + QString::number(value, 'g', 3));
   
   int topVal = top_slider->value();
   if(topVal < value)
     setTopValue(value);
 
   // emit bottomValueChanged(value);
 }

void BoxSliderWidget::setTopValue(int value)
 {
   top_slider->setValue(value);
   topLabel->setText("Top: " + QString::number(value, 'g', 3));
   int bottomVal = bottom_slider->value();
   if(bottomVal > value)
     setBottomValue(value);
     // emit topValueChanged(value);
 }

 void BoxSliderWidget::setMinimum(int value)
 {
     bottom_slider->setMinimum(value);
     top_slider->setMinimum(value);
 }

 void BoxSliderWidget::setMaximum(int value)
 {
     bottom_slider->setMaximum(value);
     top_slider->setMaximum(value);
 }


