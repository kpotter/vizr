// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// Simulation.cpp
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#include "Simulation.h"
#include <math.h>


/// Rescale the simulation elements to be within the bounding box
void Simulation::rescale(BoundingBox * bb){

  std::map<int, Iteration*>::iterator it;

  // Scale the radius based on the bounding box
  float radiusScale = (bb->max.x() - bb->min.x())/_grid->getXDim();
  float radiusMin, radiusMax;
  radiusMin = radiusMax = _iterations.begin()->second->getAgentList()[0]->getRadius();  
  for(it = _iterations.begin(); it != _iterations.end(); it++){
    std::vector<Agent* > agents = it->second->getAgentList();
    for(int a = 0; a < agents.size(); a++){
      radiusMin = radiusMin < agents[a]->getRadius() ? radiusMin : agents[a]->getRadius();
      radiusMax = radiusMax > agents[a]->getRadius() ? radiusMax : agents[a]->getRadius();	
    }     
  }

  // Rescale the agent radii and the positions
  for(it = _iterations.begin(); it != _iterations.end(); it++){
    std::vector<Agent* > agents = it->second->getAgentList();
    for(int a = 0; a < agents.size(); a++){
      agents[a]->setPosition(affine(0.f, agents[a]->getX(), float(_grid->getXDim()), bb->min.x(), bb->max.x()),
			     affine(0.f, agents[a]->getZ(), float(_grid->getZDim()), bb->min.z(), bb->max.z()),
			     affine(0.f, agents[a]->getY(), float(_grid->getYDim()), bb->min.y(), bb->max.y()));


      // -- Scale agents based on simulation size and drawing bounding box -- //
       agents[a]->setScaledRadius(agents[a]->getRadius()*radiusScale);
    }
    it->second->setAgentList(agents);
  }


  // Find the min/max of each species
  for(it = _iterations.begin(); it != _iterations.end(); it++){
    std::map<std::string, std::pair<int, int> > speciesMinMax = it->second->getMinMaxData();
    for(std::map<std::string, std::pair<int, int> >::iterator x = speciesMinMax.begin(); x != speciesMinMax.end(); ++x){

      std::string species = x->first;
      int min = x->second.first;
      int max = x->second.second;

      if(_minMaxSpeciesData.find(species) == _minMaxSpeciesData.end()){
	std::pair<int, int> minMax(min, max);
	_minMaxSpeciesData[species] = minMax;
      }
      else{
	if(min < _minMaxSpeciesData[species].first)
	  _minMaxSpeciesData[species].first = min;
	if(max > _minMaxSpeciesData[species].second)
	  _minMaxSpeciesData[species].second = max;
      }
    }
  }

  // Create a histogram for the "Joiners"
  /* float numBins = 100;
  int histogram[100] = {0};
  int interval = ceil((_minMaxSpeciesData["Joiner"].second-_minMaxSpeciesData["Joiner"].first)/numBins);
  
  //  std::cout << "mini: " << _minMaxSpeciesData["Joiner"].first << " " << _minMaxSpeciesData["Joiner"].second << std::endl;
  //  std::cout << "internal: " << interval << std::endl;
  
      
  for(it = _iterations.begin(); it != _iterations.end(); it++){
    std::cout << "iteratuib: " << it->first << std::endl;
    std::vector<Agent* > agents = it->second->getAgentList();
    for(int a = 0; a < agents.size(); a++){
      if(agents[a]->getSpecies() == "Joiner"){

	int bin = (agents[a]->getFamily()-_minMaxSpeciesData["Joiner"].first)/interval;
	//std::cout << "family: " << agents[a]->getFamily() <<  "bin:  " << bin << std::endl;
	histogram[bin] ++;
      }
    }
  }

  for(int i = 0; i< 100; i++)
    std::cout << histogram[i] << std::endl;
  std::cout << std::endl;
  */
  
  
  // Print out the mins/maxes
  /*std::map<std::string, std::pair<int, int> >::iterator p;
  for(p = _minMaxSpeciesData.begin(); p != _minMaxSpeciesData.end(); ++p){
    std::cout << "species, min, max: " << p->first << ", " << p->second.first << ", " << p->second.second << std::endl;
    }*/
  

}

