// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ // 
 // BoxControlWidget.h
 // Written by Kristi Potter 2015
 // kpotter@uoregon.edu
 // =====-----=====------=====------=====------=====------=====------=====------=====------===== //
 #ifndef BOX_SLIDER_WIDGET_H
 #define BOX_SLIDER_WIDGET_H
 
 #include <QWidget>
 #include <QGroupBox>
 #include <QSlider>
 #include <QPushButton>
 #include <QVBoxLayout>
 #include <QLabel>
#include <iostream>

  class QDial;
  class QScrollBar;
  class QSlider;
 
  class BoxSliderWidget : public QGroupBox
  {
      Q_OBJECT
 
  public:
      BoxSliderWidget(Qt::Orientation orientation, const QString &title,
 		     QWidget *parent = 0);
 
  signals:
      void bottomValueChanged(int value);
      void topValueChanged(int value);
      void update(int bottom, int top);
 
  public slots:
      void setBottomValue(int value);
      void setTopValue(int value);
      
      void setMinimum(int value);
      void setMaximum(int value);
 
      void updateWindow(){ emit update(bottom_slider->value(), top_slider->value()); }
      
  private:
      QSlider *bottom_slider;
      QSlider *top_slider;
      QLabel *bottomLabel;
      QLabel *topLabel;
      QPushButton * updateButton;
  };
 #endif
