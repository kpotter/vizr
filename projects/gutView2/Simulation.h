// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// Simulation.h
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#ifndef _SIMULATION_H
#define _SIMULATION_H

#include <vector>
#include <map>
#include <iostream>
#include "Grid.h"
#include "Maths.h"
#include "Iteration.h"

/*
 * Class to hold the simulation data
 */
class Simulation{
 public:

  // Constructor/ Destructor
  Simulation(){}
  virtual ~Simulation(){}

  /// Set the grid and resolution
  void setGrid(Grid * grid){ _grid = grid; }
  
  /// Return the grid
  Grid * getGrid(){ return _grid; }

  // Set the vector of species names
  void setSpeciesNames(std::vector<std::string> speciesNames){ _speciesNames = speciesNames; }

  /// Gets all species names in simulation
  std::vector<std::string> getSpeciesNames(){ return _speciesNames; }

  /// Set the iterations
  void setIterations(std::map<int, Iteration *> iterations){ _iterations = iterations; }

  /// Get the number of iterations
  int getNumIterations(){ return _iterations.size(); }

  /// Get the iterations
  std::map<int, Iteration *> &getIterations(){ return _iterations; }
  
  /// Rescale the simulation elements to be within the bounding box
  void rescale(BoundingBox * bb);

  /// Set the time unit
  void setTimeUnit(std::string timeUnit) { _timeUnit = timeUnit; }

  /// Get the unit of time per iteration
  std::string getTimeUnit() { return _timeUnit; }

  /// Get the agent state list for the current iteration
  std::vector<Agent *> getAgentList(){ return getAgentList(_currentIteration); }
  std::vector<Agent *> getAgentList(int iteration){ return _iterations[iteration]->getAgentList(); }

  /// Get maximum bounding box height
  float getMaxHeight() { return _grid->getXDim(); }

  // Get the min/maxes for the species data for colormapping
  std::pair<int, int> getMinMaxSpeciesData(std::string species){ return _minMaxSpeciesData[species]; }
  
 private:
  Grid * _grid;
  int _currentIteration;
  std::string _timeUnit;
  std::map<int, Iteration *> _iterations;
  std::vector<std::string> _speciesNames;
  std::map<std::string, std::pair<int, int> > _minMaxSpeciesData;

};

#endif
