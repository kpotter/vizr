// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// AgentDrawable.h
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#ifndef _SIMULATION_DRAWABLE_H
#define _SIMULATION_DRAWABLE_H

#include "Grid.h"
#include "Drawable.h"
#include "AgentDrawable.h"
#include <QSlider>

class SimulationDrawable : public Drawable{
  Q_OBJECT
  
public:

  /// Constructor
  SimulationDrawable() : Drawable() { _agents.clear(); }

  /// Draw the simulation
  void draw();

  /// Add an agent list to the simulation drawable
  void addAgentList(int iteration, std::vector<Agent *> agents,
  		    std::map<std::string, Colormap *> speciesColorMaps);

  /// Function called by changes to solute or agent displays by legend
  void hideShowDrawables(int setting, std::string name, QObject *legend);

public slots:

  // Set the top clipping plane
  void setBoxTop(int value){ _top = value; }
  
  // Set the bottom clipping plane
  void setBoxBottom(int value){ _bottom = value; }

  // Set black and white drawing
  void setBlackAndWhite(bool value){
     std::map<int, AgentDrawable*>::iterator it; 
     for(it=_agents.begin(); it != _agents.end(); it++){
     it->second->setBlackAndWhite(value);
    }
  }

  // Set the drawing of the boudning box
  void setBoundingBox(bool value){
    _drawBox = value;
  }

protected:
 
  // The top and bottom of the bounding box
  int _top, _bottom;
  
  std::map<int, AgentDrawable *> _agents;
  
  // List of hidden species in simulation
  std::map<std::string, int> _displayedSpecies;
};

#endif
