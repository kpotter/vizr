// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// Iteration.h
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#ifndef _ITERATION_H
#define _ITERATION_H
#include "BoundingBox.h"
#include "Agent.h"

/*
 * Class to hold each iteration
 */
class Iteration{
  
public:
  
  /// Constructor/Destructor
  Iteration(int iteration, std::string unit) : _iteration(iteration), _unit(unit){}
  virtual ~Iteration(){}

  // Add an agent to the list of agents
  void addAgent(Agent * agent){ agentList.push_back(agent); }

  // Get the iteration
  int getIteration(){ return _iteration; }
  
  // Get the time unit
  std::string getTimeUnit() { return _unit; }
  
  // Return the agent list
  std::vector<Agent *> getAgentList(){ return agentList; }

  // Set the agent list
  void setAgentList(std::vector<Agent *> agents){ agentList = agents; }

  // Save min/maxes for each species
  void addMinMaxData(std::string species, int min, int max){
    if(minMaxSpeciesData.find(species) == minMaxSpeciesData.end()){
      std::pair<int, int> minMax(min, max);
      minMaxSpeciesData[species] = minMax;
    }
    else{
      if(min < minMaxSpeciesData[species].first)
	minMaxSpeciesData[species].first = min;
      if(max > minMaxSpeciesData[species].second)
	minMaxSpeciesData[species].second = max;
    }
  } 

  std::map<std::string, std::pair<int, int> > getMinMaxData(){ return minMaxSpeciesData; }

protected:

  // The iteration number
  int _iteration;

  // The unit of time
  std::string _unit;

  // A list of agent states
  std::vector<Agent *> agentList;

  std::map<std::string, std::pair<int, int> > minMaxSpeciesData;
  
};

#endif
