// -*- C++ -*-
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                                                                    [GUT] * [VIEW] 
// Project to explore gut microbe data. 
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// DataReader.h
// Class to read in data
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //

#ifndef _DATA_READER_H
#define _DATA_READER_H
#include "Simulation.h"
#include <QString>
#include <QtXml/QXmlStreamReader>
#include <QDir>

class DataReader{

 public:
  DataReader(){}
  virtual  ~DataReader(){}

  // Read in the agent states, return an iteration map
  std::map<int, Iteration *> readAgentStateData(QStringList agentStateFiles);

  Grid * getGrid(){ return _grid; }
  std::vector<std::string> getSpeciesNames() { return _speciesNames; }
  
protected:
  Grid * _grid;
  std::vector<std::string> _speciesNames;
};

#endif
