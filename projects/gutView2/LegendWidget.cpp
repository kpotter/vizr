// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// LegendWidget.cpp
// Written by Danny Walinsky 2013
// dwalins4@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //

#include "LegendWidget.h"

/// Constructor creates a layout
LegendWidget::LegendWidget(const QString &title, QWidget *parent)
  : QGroupBox(title, parent)
{
  layout = new QVBoxLayout;
  this->setLayout(layout); 
}

/// Set the legend's color map and connect signals
void LegendWidget::setAgentLegend(std::map<std::string, std::string> colorMap, QWidget *signalReceiver)
{  
  std::string styleSheetLines = "";

  std::map<std::string, std::string>::iterator iter;
  for(iter = colorMap.begin(); iter != colorMap.end(); ++iter) {
    // create checkbox line
    QCheckBox *thisCheck = new QCheckBox(QString::fromStdString(iter->first), this);
    thisCheck->setChecked(true);
    thisCheck->setObjectName(QString::fromStdString("agent-" + iter->first));
    styleSheetLines += "QCheckBox::indicator#agent-"+iter->first+":checked {background-color:"+iter->second+";} ";
    layout->addWidget(thisCheck);

    // attach checkboxes to gutpaintwidget
    connect(thisCheck, SIGNAL(stateChanged(int)), signalReceiver, SLOT(legendEvts(int)));
  }
  setStyleSheet(
    QString::fromStdString("QCheckBox::indicator {border-radius:8px;} \
    QCheckBox::indicator:unchecked {background:transparent;border:1px solid gray;}"
    + styleSheetLines)
  );
};



