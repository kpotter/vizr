// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                       [GUT] * [VIEW]
// Project to explore gut microbe data. 
//  ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// DataReader.cpp
// Class to read in data
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#include "DataReader.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <QFile>
#include <QMap>
#include <QStringList>
#include "Agent.h"

/// Read in the agent states data
std::map<int, Iteration*> DataReader::readAgentStateData(QStringList agentStateFiles){

  std::map<int, Iteration*> iterationMap;
  
  // For each file in the list
  for(int i = 0; i < agentStateFiles.size(); i++){

    std::cout << "Reading: " << agentStateFiles[i].toStdString() << std::endl;
    
    // Open the file
    QFile* file = new QFile(agentStateFiles[i]);
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text)) {
      std::cout << "ERROR in reading " << agentStateFiles[i].toStdString() << std::endl;
      continue;
    }

    // Create an xml reader
    QXmlStreamReader * xml = new QXmlStreamReader(file);

    // Create a new iteration
    Iteration * iteration;
    
    // While we have xml tokens
    while(!xml->atEnd() && !xml->hasError()) { 
      
      // Read next element and get its attributes
      QXmlStreamReader::TokenType token = xml->readNext();
      QXmlStreamAttributes attributes = xml->attributes();
      
      // If token is just StartDocument, we'll go to next.
      if(token == QXmlStreamReader::StartDocument)
	continue;

      // If token is a StartElement, we'll see if we can read it.
      if(token == QXmlStreamReader::StartElement) {
	// idynomics element
	if(xml->name() == "idynomics")
	  continue;
	
	// simulation element ( read iteration information )
	if(xml->name() == "simulation"){
	  
	  if(attributes.hasAttribute("iterate") && attributes.hasAttribute("time") && 
	     attributes.hasAttribute("unit"))
	    
	    // Create a new Iteration
	    iteration = new Iteration(attributes.value("iterate").toString().toInt(),
				      // attributes.value("time").toString().toFloat(),
				      attributes.value("unit").toString().toUtf8().constData());
	}

	// species element
	if(xml->name() == "species"){

	  // Get the species name and add it to the simulation
	  std::string speciesName;
	  if(attributes.hasAttribute("name")){
	    speciesName = attributes.value("name").toString().toUtf8().constData();
	    if(std::find(_speciesNames.begin(), _speciesNames.end(), speciesName) == _speciesNames.end())
	      _speciesNames.push_back(speciesName);
    	  }
	  
	  int min = 2147483647;
	  int max = -1;

	  // Get the next element, parse the data
	  xml->readNext();
	  QString data = xml->text().toString();  
	  QStringList lines = data.split(";");

	  // If we have data..
	  if(lines.length() > 1){
	    QStringList::const_iterator it;
	    for (it = lines.constBegin(); it != lines.constEnd(); ++it){
	      QStringList commaSep =  (*it).split(",");
	      if(commaSep.size() != 1){
		Agent * agent = new Agent(speciesName,  (*it).split(","));
		iteration->addAgent(agent);
	      
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Change color variable HERE
		//int data = agent->getGeneration();   
		int data = agent->getFamily(); 
		//int data = agent->getBirthday(); 
		//int data = agent->getGenealogy(); 
		if(data < min)
		  min = data;
		if(data > max)
		  max = data;
	      }
	    }
	  }
	  else{
	    // Else we dont have any of these species, set the min and max to 0
	    min = max = 0;
	  }
	  iteration->addMinMaxData(speciesName, min, max);
	}

	// Only read in the grid on the first file
	if(i == 0){
	  // grid element
	  if(xml->name() == "grid"){
	    
	    // If this is the first file, read in the grid (assuming they are all the same)
	    if(i == 0){
	      if(attributes.hasAttribute("resolution") && attributes.hasAttribute("nI") && 
		 attributes.hasAttribute("nJ") && attributes.hasAttribute("nK")){
		_grid = new Grid(attributes.value("resolution").toString().toFloat(),
				 attributes.value("nI").toString().toInt(),
				 attributes.value("nJ").toString().toInt(),
				 attributes.value("nK").toString().toInt());
	      }
	    }
	  }
	}
      }
    }
    iterationMap[iteration->getIteration()] = iteration;
  }
  return iterationMap;
}
