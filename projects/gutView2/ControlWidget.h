// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //     
// ControlWidget.h
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#ifndef CONTROLWIDGET_H
#define CONTROLWIDGET_H

#include <QWidget>
#include "animationControl.h"
#include <QGroupBox>

class ControlWidget : public QWidget
{
    Q_OBJECT

public:
    ControlWidget() : QWidget() { createGUI(); }
    void setTimeInformation(int minTime, int maxTime, int numFrames,
			    std::string timeUnit = "hour", int timeout = 750);

protected slots:
    void animateSlot(float timestep){ emit animate(timestep); }
    
protected:
    QGroupBox * _radioGroup;
    QGroupBox * _animationGroup;
    AnimationControl * _animationControl;

    void createGUI();

signals:
    void animate(float);
};

#endif // CONTROLWIDGET_H
