// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
//                        ___  __  __  ____    _  _  ____  ____  _    _                         //
//                       / __)(  )(  )(_  _)  ( \/ )(_  _)( ___)( \/\/ )                        //
//                      ( (_-. )(__)(   )(     \  /  _)(_  )__)  )    (                         //
//                       \___/(______) (__)     \/  (____)(____)(__/\__)                        //
//                                    Project to explore gut microbe data                       //
// ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  ~   ~   ~   ~   ~   ~   ~   ~   ~ //
// GutMainWindow.h
// Written by Kristi Potter 2013
// kpotter@uoregon.edu
// =====-----=====------=====------=====------=====------=====------=====------=====------===== //
#include "GutMainWindow.h"
#include "GutPaintWidget.h"
#include <QFileDialog>
#include "DataReader.h"

/*
 * Constructor initializes and sets up connections
 */
GutMainWindow::GutMainWindow(QWidget *parent) 
  : MainWindow(parent) {
  initialize();
}

/*
 * Create the drawing scene
 */
void GutMainWindow::createScene(){
  
  // The GL widget
  glWidget = new GutPaintWidget;
  glWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  // The control widget
  _controlWidget = new ControlWidget;
  _controlWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

  // The legend widget
  _agentLegend = new LegendWidget(tr("Agent Legend"));
  _agentLegend->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

  // The slider widget
  _boxSliderWidget = new BoxSliderWidget(Qt::Vertical, tr("Bounding Box"));
  _boxSliderWidget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
  
  // Add widgets to layout
  left_layout->addWidget(glWidget);
  left_layout->addWidget(_controlWidget);
  right_layout->addWidget(_agentLegend);
  right_layout->addWidget(_boxSliderWidget);
}

/*
 * Connect the simulation to the gut paint widget
 */
void GutMainWindow::setupConnections(){

  MainWindow::setupConnections();
  
  // Connect an animation signal with a painted animation signal
  connect(_controlWidget, SIGNAL(animate(float)), glWidget, SLOT(animate(float)));

  // Connect the slider update signal with an update
  connect(_boxSliderWidget, SIGNAL(update(int, int)), glWidget, SLOT(updateBox(int, int)));
}

/*
 * Load in the simulation data, signal when its done
 */
void GutMainWindow::loadDataSlot(){

  // The list of agent state files
  QStringList agentStateFiles;
  
  // Some default data directory
  std::string directory = "/Users/kpotter/Data/Gut Microbes/biofilm/";

  // Open a file dialog
  QFileDialog * dialog = new QFileDialog(this, tr("Load IDynomics File"),  
					 tr(directory.c_str()), 
					 tr("XML Files (*.xml)"));
  dialog->setFileMode(QFileDialog::AnyFile);
  
  // If we hit ok on the file dialog
  if(dialog->exec()){

    // Get the files from the dialog
    agentStateFiles = dialog->selectedFiles();

    // If we selected a file or directory
    if(agentStateFiles.size() > 0){
    
      // Get the file info for the file
      QFileInfo fileInfo(agentStateFiles[0]);

      // Create a new data reader
      DataReader * dr = new DataReader();
      
      // Switch
      if(fileInfo.isDir()){
	
	// Get the selected folder
	QDir dirName = dialog->directory();

	// Move into agentState/ if we are not already there
	if(dirName.dirName() != tr("agentState/")){	
	  dirName.cd("agent_State/");
	}
	
	// Set filters to read in only xml files
	QStringList filters;
	filters << "*.xml" ;
	dirName.setNameFilters(filters);

	// Get the agent state files
	agentStateFiles = dirName.entryList(filters);

	// Add the directory name to the path of the agent state files
	for(int i = 0; i < agentStateFiles.size(); i++)
	  agentStateFiles[i] = dirName.absolutePath() + "/" + agentStateFiles[i];	
      }

      // Get the map of iterations
      std::map<int, Iteration*> iterationsMap = dr->readAgentStateData(agentStateFiles);
      
      // Create the simulation
      Simulation * simulation = new Simulation();
      simulation->setIterations(iterationsMap);
      simulation->setGrid(dr->getGrid());
      simulation->setSpeciesNames(dr->getSpeciesNames());
      simulation->setTimeUnit(iterationsMap.begin()->second->getTimeUnit());
      
      // Update the control, slider, and legend widgets
      updateControlWidget(simulation);
      updateSliderWidget(simulation);
      	
      // Update the drawing of the simulation
      dynamic_cast<GutPaintWidget *>(glWidget)->setSimulation(simulation);

      // Add data to agent legend
      _agentLegend->setAgentLegend(dynamic_cast<GutPaintWidget *>(glWidget)->getSpeciesColors(),
				   dynamic_cast<GutPaintWidget *>(glWidget));


    } 
  }
}

/*
 * Set the variables in the control widget
 */
void GutMainWindow::updateControlWidget(Simulation * simulation){

  // Get the iterations and create an iterator
  std::map<int, Iteration*> itrs = simulation->getIterations(); 
  std::map<int, Iteration*>::iterator it = itrs.begin();
  
  // Find the min and max iteration number
  int min, max;
  min = max = it->first;
  for( it = itrs.begin(); it != itrs.end(); it ++){
    if(it->first < min)
      min = it->first;
    if(it->first > max)
    max = it->first; 
  }
  
  // Set the control widget variables
  _controlWidget->setTimeInformation(min, max, itrs.size(), simulation->getTimeUnit());
}

/*
 * Update the size of the sliders
 */
void GutMainWindow::updateSliderWidget(Simulation * simulation){

  // Set the max values for the box sliders
  _boxSliderWidget->setMinimum(0);
  _boxSliderWidget->setMaximum(simulation->getMaxHeight());
  _boxSliderWidget->setBottomValue(0);
  _boxSliderWidget->setTopValue(simulation->getMaxHeight());
  dynamic_cast<GutPaintWidget *>(glWidget)->setBoxTop(simulation->getMaxHeight());
  dynamic_cast<GutPaintWidget *>(glWidget)->setBoxBottom(0);
  
}

