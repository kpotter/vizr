// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                                 ___        ___         __         ___       __      __     ___                                                         //    
//                               /   __)    (   ,   )      /  O \     (   ,   \   (    )  /   _ ) /   __)                                                       //    
//                              (  (      >   )      \    /  (  )  \     )    _/   )  (  (   ( _   \__  \                                                       //    
//                               \___/   (_)  (_) (__) (__)  (__)     (__)   \___) (___/                                                      //    
//                                                                                                         Widgets                                                        //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  //
// mainQtGLWindow.cpp
// Written by Kristi Potter, 2010
// Qt main graphics window
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#include "mainQtGLWindow.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <QImage>
#include <GLIncludes.h>
#include <KPKeys.h>

// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
// Default constructor initalizes the format of the QtGL widget
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
MainQtGLWindow::MainQtGLWindow(QWidget *parent, const QGLWidget *shareWidget, Qt::WindowFlags f)
  : QGLWidget( QGLFormat(QGL::AlphaChannel | QGL::DoubleBuffer | QGL::DepthBuffer | QGL::Rgba),
	       parent, shareWidget, f)
{ initializeMainWindow(); }

// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
// Constructor takes in a GLContext
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
MainQtGLWindow::MainQtGLWindow(QGLContext *context, QWidget *parent, 
			       const QGLWidget * shareWidget, Qt::WindowFlags f)
  : QGLWidget( context, parent, shareWidget, f )
{ initializeMainWindow(); }
 
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
//  Constructor takes in a format
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
MainQtGLWindow::MainQtGLWindow(const QGLFormat & format, QWidget *parent, 
			       const QGLWidget *shareWidget, Qt::WindowFlags f)
  : QGLWidget( format, parent, shareWidget, f )
{ initializeMainWindow(); }

// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
// Initialize the mainQtGLWindow
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
void MainQtGLWindow::initializeMainWindow()
{
#ifdef _MQGW_DEBUG
  std::cerr << "MainQtGLWindow::InitializeMainWindow" << std::endl;
#endif

  // Initialize the cameras and manipulators
  _camera   = new Camera();
  _manip = new Manipulator();
  _homeCamera = new Camera( *_camera );
  _homeManip = new Manipulator( *_manip );
  _hideCurs = false;
  _curMouse = 0;
  _curMove = 0;
  _manip->setCamera(_camera);
  setHome();

  // Set the background color
  _bgColor = charcoal;

  // Set some Qt stuff
  setFocusPolicy(Qt::StrongFocus);
}

// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
// Resize the GL window
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
void MainQtGLWindow::resizeGL ( int w, int h )
{
#ifdef _MQGW_DEBUG
  std::cerr << "MainQtGLWindow::resizeGL" << std::endl;
#endif

  _camera->resizeCamera( w, h);
  _homeCamera->resizeCamera( w, h);
  setCameraGL( _camera );
  glViewport(0, 0, w, h);
}


// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
// Set the camera in GL - sets the perspective and view matricies
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
void MainQtGLWindow::setCameraGL( CameraSP cam )
{
#ifdef _MQGW_DEBUG
  std::cerr << "MainQtGLWindow::setCameraGL" << std::endl;
#endif

  glMatrixMode(GL_PROJECTION);
  glLoadMatrixf(cam->getProjection().m);
  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixf(cam->getModelView().m);
}

// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
// Initialize the GL state
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
void MainQtGLWindow::initializeGL ()
{
#ifdef _MQGW_DEBUG
  std::cerr << "MainQtGLWindow::initializeGL" << std::endl;
#endif

  // Setup GL state
  qglClearColor(Color2QColor(_bgColor.toInt()));
  glShadeModel(GL_SMOOTH);
  glEnable( GL_RESCALE_NORMAL );
  glEnable( GL_COLOR_MATERIAL );
  glEnable( GL_CULL_FACE );
  glCullFace( GL_BACK );
  glEnable( GL_MULTISAMPLE );

  glEnable( GL_DEPTH_TEST );
  glDepthFunc(GL_LEQUAL);

  glEnable(GL_LINE_SMOOTH);
  glLineWidth(2);

  /* Set up lighting. mostly for widgets */
  static float ambient[]             = {0.4, 0.4, 0.4, 10};
  static float diffuse[]             = {0.5, 1.0, 1.0, 1.0};
  static float front_mat_shininess[] = {60.0};
  static float front_mat_specular[]  = {0.2, 0.2,  0.2,  1.0};
  static float front_mat_diffuse[]   = {0.5, 0.28, 0.38, 1.0};
  static float lmodel_ambient[]      = {0.2, 0.2,  0.2,  1.0};
  static float ltpos[]               = {25,25,25,1};

  glEnable(GL_LIGHTING);
  glLightfv(GL_LIGHT0, GL_POSITION, ltpos);
  glLightfv(GL_LIGHT0, GL_AMBIENT,  ambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE,  diffuse);
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT,  lmodel_ambient);
  glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);  
  glEnable(GL_LIGHT0);
 
  glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, front_mat_shininess);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,  front_mat_specular);
  glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE,   front_mat_diffuse);
  
  GLfloat fogColor[4] = {1,1,1,1};
  glFogi( GL_FOG_MODE,GL_LINEAR );
  glFogfv( GL_FOG_COLOR, _bgColor.v );
  glFogf( GL_FOG_DENSITY,0.35 );
  glHint( GL_FOG_HINT,GL_DONT_CARE );
  glFogf( GL_FOG_START, _camera->getClips()[0]);
  glFogf( GL_FOG_END, _camera->getClips()[1] );
  glEnable( GL_FOG );
}


// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
// Qt calls this to paint the gl window
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
void MainQtGLWindow::paintGL()
{
#ifdef _MQGW_DEBUG
  std::cerr << "MainQtGLWindow::paintGL" << std::endl;
#endif

  // Clear the buffers
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  // Set up the viewing
  setCameraGL( _camera );
  
  this->drawScene();
  
  // Signal that the drawing is done
  drawDone();
}

// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
//                          DRAW
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
void MainQtGLWindow::drawScene()
{
#ifdef _MQGW_DEBUG
  std::cerr << "MainQtGLWindow::drawScene" << std::endl;
#endif

  // Draw the default scene
  glPushMatrix();
   {
     // Cube from -.25 to .25 in each axis 
     glColor4f(.25,0,.25,0);
     glutSolidCube(0.5);
     
     // Axes
     glBegin(GL_LINES);
     {
       // X Axis is red
       glColor4f(1,0,0,0);
       glVertex3f(0, 0, 0);
       glVertex3f(1, 0, 0);
       // Y Axis is green
       glColor4f(0,1,0,0);
       glVertex3f(0, 0, 0);
       glVertex3f(0, 1, 0);
       // Z axis is blue
       glColor4f(0,0,1,0);
       glVertex3f(0, 0, 0);
       glVertex3f(0, 0, -1);
     }
     glEnd();
     
     // X Axis arrow
     glPushMatrix();
     glColor4f(1,0,0,0); 
     glTranslatef(1, 0, 0);
     glScalef(.0625, .0625, .0625);
     glutSolidOctahedron();
     glPopMatrix();
     
     // Y Axis arrow
     glPushMatrix();
     glColor4f(0,1,0,0); 
     glTranslatef(0, 1, 0);
     glScalef(.0625, .0625, .0625);
     glutSolidCube(1.0);
     glPopMatrix();  
     
     // Z Axis arrow
     glPushMatrix();
     glColor4f(0,0,1,0);
     glTranslatef(0, 0, -1);
     glScalef(.0625, .0625, .0625);
     glutSolidSphere(0.75, 20, 20);
     glPopMatrix();
   }
   glPopMatrix();
}

// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
// Mouse press event
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
void MainQtGLWindow::mousePressEvent( QMouseEvent * e )
{
  if(_hideCurs) setCursor( QCursor( Qt::BlankCursor ) ) ;
  _curMouse = new Mousing(e,_camera,_manip);
  _curMove = new MouseMove(*_curMouse, _curMouse->getPos());
  _camera->mouse(*_curMouse);
  _manip->mouse(*_curMouse);
  update();

#ifdef _MQGW_DEBUG
  std::cerr << "The mouse was pressed at " << e->x() << " , " << e->y() 
  << " Global : " << e->globalX() << " , " << e->globalY() << std::endl;
#endif
}

// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
// Mouse Release
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
void MainQtGLWindow::mouseReleaseEvent ( QMouseEvent * e )
{
   if(_hideCurs)             setCursor( QCursor( Qt::ArrowCursor ) );
   if(_curMouse)             _curMouse->setButtonDown(false);
   if(_camera && _curMouse)  _camera->mouse(*_curMouse);
   if(_manip && _curMouse)   _manip->mouse(*_curMouse);
   if(_curMouse)             _curMouse->setButton(KP_BUTTON_NONE);
   update();

#ifdef _MQGW_DEBUG
  std::cerr << "The mouse was released" << std::endl;
#endif
}

// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
// Mouse Move
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
void MainQtGLWindow::mouseMoveEvent( QMouseEvent * e )
{
  if(!_curMove)
    {
      if(!_curMouse)  
	_curMouse = new Mousing(e,_camera,_manip);
      _curMove = new MouseMove(*_curMouse, Vector3f(e->x(), e->y(),_curMouse->z()));
    }
   // update the current move event's  position
  _curMove->setPos(Vector3f(e->x(), e->y(), _curMouse->z()));
  update();

#ifdef _MQGW_DEBUG
   std::cerr << "The mouse is moving " << e->x() << " , " << e->y() 
	     << " Global : " << e->globalX() << " , " << e->globalY() << std::endl;
#endif
}

// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
// Key press
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
void MainQtGLWindow::keyPressEvent ( QKeyEvent * e )
{
  // only use the key if we want it
  e->ignore();
  if(e->isAccepted()) return;
  switch(e->key())
    {
    case Qt::Key_Escape:
      // Close window
      e->accept();
      close();
      break;

    case Qt::Key_Home: 
      // Save the home location via sft-, ctl- or alt-home
      if( (e->modifiers() & Qt::ShiftModifier) &&
	  !(e->modifiers() & Qt::ControlModifier) && 
          !(e->modifiers() & Qt::AltModifier) )  
	{
	  setHome();
	  e->accept();
	}
      // Set camera to home
      else 
	{
	  goHome();
	  e->accept();
	}
      break;
    case 'S':
      // Save the current frame buffer into an image
      writeToImage();
      break;
    default:
      if(! e->text().isEmpty())
	{
	  // send ascii key signal
	  keyPressSignal( e->key() );
	  // set qt-key signnal
	  qtKeyPressSignal( e );
	}
      break;
    }

  //std::cerr << "keyPressEvent done" << std::endl;
  update();
}

// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
// Key release
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
void MainQtGLWindow::keyReleaseEvent ( QKeyEvent * e )
{
  e->ignore();
  return;
}

// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
// Read Frame buffer
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
bool MainQtGLWindow::readFrameBuffer(void *dataPtr, int size, 
			       GLenum format, 
			       GLenum type)
{
  // Set the current GL context to this one (used when there are multiple QGLWidgets)
  QGLWidget::makeCurrent();

  switch(format)
    {
    case GL_RGBA:
    case GL_BGRA_EXT:
      if(size < int(_camera->getScreen().x()) * int(_camera->getScreen().y()) * 4)
	return true;
      break;
    case GL_RGB:
    case GL_BGR_EXT:
      if(size < int(_camera->getScreen().x()) * int(_camera->getScreen().y()) * 3)
	return true;
      break;
    default:
      std::cerr << "MainQtGLWindow::readFrameBuffer(), format not supported " << std::endl 
	   << " - possibly not implemented " << std::endl;
      return true;
    }
  glReadPixels(0,0,_camera->getScreen().x(),_camera->getScreen().y(),
	       format, type, dataPtr);
  return true;
}

// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
// Write Frame buffer
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
bool MainQtGLWindow::writeFrameBuffer(void *dataPtr, int screenx, int screeny,       
                               GLenum format, GLenum type)
{
  // Set the current GL context to this one (used when there are multiple QGLWidgets)
  QGLWidget::makeCurrent();
  glDrawPixels(screenx, screeny, format, type, dataPtr);
  return true;
}

// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
// Write the frame buffer to a file
// <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> <~-~> //
void MainQtGLWindow::writeToImage(std::string name)
{
  // Variables to write an image
  QImage image;
  int imageNum = 1;

  if(name == "")
    name = "screenshot";

  std::string imageFilename = name;
  std::fstream imagefile;
  std::stringstream num;
  num <<  std::setw(4) << std::setfill('0') << imageNum;	
  imageFilename += num.str()+".png";
         
  imagefile.open(imageFilename.c_str(), std::fstream::in);
  
  // Check if the file open worked, in which case we need to increment our image count
  while(imagefile.is_open())
    {
      imagefile.close();
      imageNum ++;
      num.str("");
      num <<  std::setw(4) << std::setfill('0') << imageNum;
      imageFilename = name+num.str()+".png";
      imagefile.open(imageFilename.c_str(), std::fstream::in);
    }
  imagefile.close();
  
  // Write the image file
  std::cout << "Write Image:" << imageFilename << std::endl;
  image = this->grabFrameBuffer();
  image.save(QString(imageFilename.c_str()));
}

