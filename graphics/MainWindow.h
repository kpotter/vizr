// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//               ___  ____    __    ____  _   _  ____  ___  ___            //
//              / __)(  _ \  /__\  (  _ \( )_( )(_  _)/ __)/ __)           //
//             ( (_-. )   / /(__)\  )___/ ) _ (  _)(_( (__ \__ \           //
//              \___/(_)\_)(__)(__)(__)  (_) (_)(____)\___)(___/           //
//                                             Drawing & Widgets           //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// MainWindow.h 
// Written by Kristi Potter December 2013 
// A generic main window widget that hold a point widget
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QColor>
#include <QDockWidget>
#include <QVBoxLayout>
#include "GLWidget.h"

namespace Ui {
    class MainWindow;
}

// MainWindow class that inherits a QMainWindow and has a PaintWidget
class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    
  // Contructor/Destructor
  MainWindow(QWidget *parent = 0);
  virtual ~MainWindow();
  void initialize();
 
protected:
 
  GLWidget * glWidget;    
  QWidget * centralWidget;
  QWidget * rightWidget;
  QWidget * leftWidget;
  QHBoxLayout * central_layout;
  QVBoxLayout * left_layout;
  QVBoxLayout * right_layout;
  
  Ui::MainWindow *ui; 
  
  void setupGUI();
  virtual void createScene();
  virtual void setupConnections();
  
signals:
    
public slots:
  virtual void loadDataSlot();
  virtual void grabImageSlot();
  void changeBackgroundColorSlot();
  void changeProjectionSlot();
  void topViewSlot();
  void sideViewSlot();
  void blackWhiteSlot();
  void boundingBoxSlot();
  
};

#endif // MAINWINDOW_H
