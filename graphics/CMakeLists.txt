## ======================================================================= ##
##               ___  ____    __    ____  _   _  ____  ___  ___            ##
##              / __)(  _ \  /__\  (  _ \( )_( )(_  _)/ __)/ __)           ##
##             ( (_-. )   / /(__)\  )___/ ) _ (  _)(_( (__ \__ \           ##
##              \___/(_)\_)(__)(__)(__)  (_) (_)(____)\___)(___/           ##
##                                             Drawing & Widgets           ##
## ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ##
## Qt Graphics implementations for VIZR 
## Written by Kristi Potter February 2013 
##  
## This file finds the amino library and defines the following vars: 
##      GRAPHICS_INC  
##      GRAPHICS_SRC 
##      graphicslib 
## ======================================================================= ##
PROJECT(GRAPHICS)

## Set the include path
INCLUDE_DIRECTORIES( ${GRAPHICS_PATH} ${VIZR_INC_DIRS} )

## Add the Qt definitions
ADD_DEFINITIONS( ${VIZR_DEFS} )

## Set the header files
SET(GRAPHICS_INC Graphics.h
                 PaintWidget.h
		 Drawable.h
		 Overpainter.h
		 BoundingBoxDrawable.h
		 InstructionsOverpainter.h
		 animationControl.h
		 MainWindow.h
		 GLWidget.h

) 

## Set the source files
SET(GRAPHICS_SRC PaintWidget.cpp
                 InstructionsOverpainter.cpp
		 animationControl.cpp
		 MainWindow.cpp
		 GLWidget.cpp
)

## Set the header MOC files
SET(GRAPHICS_MOC_FILES PaintWidget.h
                       Drawable.h
		       BoundingBoxDrawable.h
		       Overpainter.h
		       animationControl.h
		       MainWindow.h
		       GLWidget.h
		      
)

## Set the UI files
SET(GRAPHICS_UI_FILES MainWindow.ui )

## Set the RCC files
#SET(VIZR_RCC_FILES ${CMAKE_SOURCE_DIR}/resources/resources.qrc)

## Generate rules for building source files from the resources
#QT4_ADD_RESOURCES(VIZR_RCC_SRC ${VIZR_RCC_FILES})
#SET(VIZR_RCC_SRC ${VIZR_RCC_SRC} PARENT_SCOPE)

#MESSAGE("grahics" ${VIZR_RCC_SRC})

## Generate the ui .h's
QT4_WRAP_UI(GRAPHICS_UI_SRC ${GRAPHICS_UI_FILES})

## Generate the MOC sources
QT4_WRAP_CPP(GRAPHICS_MOC_SRC ${GRAPHICS_MOC_FILES})

## Include the current binary directory
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR} )

## Add the Qt compiled stuff to the library
ADD_LIBRARY( graphicslib  ${GRAPHICS_INC}
                          ${GRAPHICS_SRC}
			  ${GRAPHICS_MOC_SRC}
			  ${GRAPHICS_UI_SRC}
)

