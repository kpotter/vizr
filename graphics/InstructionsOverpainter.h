// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                                 ___        ___         __         ___       __      __     ___                                                         //    
//                               /   __)    (   ,   )      /  O \     (   ,   \   (    )  /   _ ) /   __)                                                       //    
//                              (  (      >   )      \    /  (  )  \     )    _/   )  (  (   ( _   \__  \                                                       //    
//                               \___/   (_)  (_) (__) (__)  (__)     (__)   \___) (___/                                                      //    
//                                                                                                         Widgets                                                        //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  //
// InstructionsOverpainter.h  
// Written by Kristi Potter September 2010   
// Draws instructions over the 3D GL.
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#ifndef INSTRUCTIONSOVERPAINTER_H
#define INSTRUCTIONSOVERPAINTER_H

#include "Overpainter.h"

class InstructionsOverpainter : public Overpainter{
public:
    InstructionsOverpainter() : Overpainter() {}
    InstructionsOverpainter(const InstructionsOverpainter & ip) {}
    virtual ~InstructionsOverpainter(){}

    void draw(QPainter * painter, QGLWidget * glWidget);
};

#endif // INSTRUCTIONSOVERPAINTER_H
