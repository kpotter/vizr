
// . . . get the angle of rotation . . . //
static void normalizeAngle(int &angle)
{
    while (angle < 0)
        angle += 360 * 16;
    while (angle > 360 * 16)
        angle -= 360 * 16;
}

// . . . get the new rotation . . . //
static int getRotation(int angle){
    normalizeAngle(angle);
    return angle;
}
