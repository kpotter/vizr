// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                                 ___        ___         __         ___       __      __     ___                                                         //    
//                               /   __)    (   ,   )      /  O \     (   ,   \   (    )  /   _ ) /   __)                                                       //    
//                              (  (      >   )      \    /  (  )  \     )    _/   )  (  (   ( _   \__  \                                                       //    
//                               \___/   (_)  (_) (__) (__)  (__)     (__)   \___) (___/                                                      //    
//                                                                                                         Widgets                                                        //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  //
// mainQtGLWidget.h 
// Written by Kristi Potter, 2010
// Qt main graphics window
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#ifndef _MAIN_QT_GL_WINDOW_H
#define _MAIN_QT_GL_WINDOW_H

#include <QtCore>
#include <QtGui>
#include <QGLWidget>
#include <Amino.h>
//#include <Transformers.h>

//#include <ManipulatorEvents.h>
//#include <Mousing.h>
//#include <MouseMove.h>

#define _MQGW_DEBUG

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
// Convert from kpbr colors to Qt colors
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
inline QColor Color2QColor(ColorI color)
{
  QColor qcolor(color.r(), color.g(), color.b(), color.a());
  return qcolor;
}

// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Base class for a QGLWidget - implements a simple Qt-based GL window
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
class MainQtGLWindow : public QGLWidget
{
public:

  // Declarations for Qt and kpbr signals/slots
  Q_OBJECT;
public:
  HAS_SLOTS;
  
  //  - + - + - + - + - + - + - + Constructors + - + - + - + - + - + - + - + - //
  MainQtGLWindow(QWidget *parent=0, const QGLWidget *shareWidget=0, Qt::WindowFlags f=0 );
  MainQtGLWindow(QGLContext *context, QWidget *parent=0, 
		 const QGLWidget * shareWidget=0, Qt::WindowFlags f=0);
  MainQtGLWindow(const QGLFormat & format, QWidget *parent=0, 
		 const QGLWidget *shareWidget=0, Qt::WindowFlags f=0); 
  virtual ~MainQtGLWindow() { }
   
  //  - + - + - + - + - + - + - Set some GL State  - + - + - + - + - + - + - //
  QSize sizeHint() const { return QSize(512, 512); }
  virtual QSizePolicy sizePolicy() const { return QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding); }
  Vector2ui getScreenDims() const { return _camera->getScreen(); }
  void setBackground(const ColorF color)  { _bgColor = color;  qglClearColor(Color2QColor(_bgColor.toInt())); }
  
  //  - + - + - + - + Camera & Manipulator get & set + - + - + - + - + - + - //
  virtual CameraSP        getCamera() const          { return _camera; }
  virtual void            setCamera(CameraSP c)      { _camera = c;   update(); }
  virtual ManipSP         getManip() const           { return _manip; }
  virtual void            setManip(ManipSP m)        { _manip = m; update(); }

  //  - + - + - + - + - + - + - + Mouse Cursor + - + - + - + - + - + - + - + - //    
  void hideCursorMouseDown( bool yes = true ) { _hideCurs = yes; }
  bool getCursorHide() const { return _hideCurs; }

  //  - + - + - + - + Read & Write Frame Buffer + - + - + - + - + - + - + - //
  bool readFrameBuffer(void *dataPtr, int size, GLenum format, GLenum type);
  bool writeFrameBuffer(void *dataPtr, int screenx, int screeny, GLenum format, GLenum type);
  void writeToImage(std::string name= "");

  //  - + - + - + - + - + - + Public Signals + _ + - + - + - + - + - + - //
  // Key press signal, use it to capture an unused (ascii) key event
  Signal< int > keyPressSignal; 

  // key press using the QTKey object, be sure to check isAccepted and accept() if you took the event
  Signal<QKeyEvent*> qtKeyPressSignal;

  // Called after the draw event completes.
  Signal<> drawDone;
		   
public slots:
  virtual void update() { QGLWidget::makeCurrent(); updateGL(); }
  virtual void goHome() { (*_camera) = *_homeCamera; (*_manip) = *_homeManip; }
  virtual void setHome(){ (*_homeCamera) = *_camera; (*_homeManip) = *_manip; }

protected:

  //  - + - + - + - + - + - Initialize the GL State  - + - + - + - + - + - + - //
  virtual void initializeMainWindow();
  virtual void initializeGL();

  // void setupViewport(int width, int height);
  void resizeGL( int width, int height );
  // virtual void resizeCamera(int w, int h, CameraSP cam );
  virtual void setCameraGL( CameraSP cam );  

  void paintGL();  
  virtual void drawScene();

  //  - + - + - + - + - + - Mouse Callbacks + - + - + - + - + - + - + - + - + - //
  virtual void mouseMoveEvent( QMouseEvent * e);
  virtual void mousePressEvent( QMouseEvent * e);
  virtual void mouseReleaseEvent ( QMouseEvent * e );
  
  //  - + - + - + - + - + Keyboard Callbacks + - + - + - + - + - + - + - + - + - //
  virtual void keyPressEvent ( QKeyEvent * e );
  virtual void keyReleaseEvent ( QKeyEvent * e );
  
protected:

  // ~~~ Cameras and Manipulators ~~~
  CameraSP  _camera;
  CameraSP  _homeCamera;
  ManipSP   _manip;
  ManipSP   _homeManip;
  
  // ~~~ GL State ~~~
  ColorF  _bgColor;
  bool    _hideCurs;

private:
   MousingSP    _curMouse;
   MouseMoveSP  _curMove;
};

#endif
