/*
* $Id: glUtil.h,v 1.1.1.1 2008/06/09 20:01:42 kpotter Exp $
*/

/*
**  Joe Michael Kniss (c) 2002
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This library is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
**  Lesser General Public License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// glUtils.h

#ifndef __GL_UTILS_DOT_H
#define __GL_UTILS_DOT_H

#include "GLIncludes.h"
#include <iostream>
//#include <Light.h>

///////////////////////////////////////////////////////////////////////////
/// General GL/WGL error reporting
///////////////////////////////////////////////////////////////////////////

bool glErr(std::ostream &os, const char *where = 0, const char *when= 0);

inline bool glErr(const char* where=0, const char* when=0)
{ return glErr(std::cerr, where, when); }

void wglGetLastError(std::ostream &os, const char *where = 0);


///////////////////////////////////////////////////////////////////////////
/// GL Window state
///////////////////////////////////////////////////////////////////////////
struct WinStatsGL 
{
   int            colorBits[4];
   int            accumBits[4];
   int            depthBits;
   int            stencilBits;
   unsigned char  doubleBuffer;
   unsigned char  stereoBuffer;
   unsigned char  auxBuffer;
};

WinStatsGL getGLWinStats(bool print = false);


///////////////////////////////////////////////////////////////////////////
/// GL Limits
///////////////////////////////////////////////////////////////////////////
struct LimitsGL
{
   int maxTexUnits;
   int maxTexCoords;
};

// you must call this function before you use the gl-limits structure
const LimitsGL getLimitsGL();

///////////////////////////////////////////////////////////////////////////
/// GL Color & Texture Utils
///////////////////////////////////////////////////////////////////////////
void	setColorMask( GLenum channel );
GLenum	validateSingleRGBChannel( GLenum channel  );
bool	isSingleChannel( GLenum channel ); /// Return true if 'channel' is single-channel GLenum
int		getNumChannels( GLenum format );	/// Get the number of color channels represented by 'format'
int		getTexDim( GLenum texType );		/// Get the dimensionality of the texture type

///////////////////////////////////////////////////////////////////////////
/// Light stuff  GL-->GUTZ
///////////////////////////////////////////////////////////////////////////

//int getLightGL( int lightNum, gutz::Light *light );

/// Glut has some issues with vc7
#if 0
///////////////////////////////////////////////////////////////////////////
/// GL Text Utils
///////////////////////////////////////////////////////////////////////////
void renderBitmapString( float x, float y, int spacing, void *font, char *string);


///////////////////////////////////////////////////////////////////////////
/// GLUT main window context switcher, should be called the first time
///   when the main window context is current
///////////////////////////////////////////////////////////////////////////
void MakeGlutWindowCurrent();
#endif

#endif
