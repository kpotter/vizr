// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//               ___  ____    __    ____  _   _  ____  ___  ___            //
//              / __)(  _ \  /__\  (  _ \( )_( )(_  _)/ __)/ __)           //
//             ( (_-. )   / /(__)\  )___/ ) _ (  _)(_( (__ \__ \           //
//              \___/(_)\_)(__)(__)(__)  (_) (_)(____)\___)(___/           //
//                                             Drawing & Widgets           //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// MainWindow.cpp 
// Written by Kristi Potter December 2013 
// A generic main window widget that hold a point widget
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <iostream>
#include <QFileDialog>
#include <QColorDialog>
#include <QSizePolicy>
#include "animationControl.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
  // Set up the ui created in designer
  ui->setupUi(this);
  setupGUI();
}

// Delete the ui
MainWindow::~MainWindow(){
  delete ui;
}

/// Initialize the main window
void MainWindow::initialize(){
  createScene();
  setupConnections();
}

void MainWindow::setupGUI(){

  // Create the widgets and the layouts
  centralWidget = new QWidget;
  central_layout = new QHBoxLayout;
  leftWidget = new QWidget;
  left_layout = new QVBoxLayout;
  rightWidget = new QWidget;
  right_layout = new QVBoxLayout;
  
  // Set the layouts
  centralWidget->setLayout(central_layout);
  leftWidget->setLayout(left_layout);
  rightWidget->setLayout(right_layout);
  leftWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  rightWidget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
  
  // Add the widgets
  central_layout->addWidget(leftWidget);
  central_layout->addWidget(rightWidget);
  
  // Set the central widget
  this->setCentralWidget(centralWidget);
}

/// Create the scene
void MainWindow::createScene(){
  
  // The GL  widget
  glWidget = new GLWidget();
  
  // The animation control
  AnimationControl *animationControl = new AnimationControl;

  left_layout->addWidget(glWidget);
  left_layout->addWidget(animationControl);
  glWidget->createScene();
}

// Connect the main window's action signals
void MainWindow::setupConnections(){
  connect(this->ui->actionLoad_Data, SIGNAL(triggered()), this, SLOT(loadDataSlot()));
  connect(this->ui->actionGrab_Image, SIGNAL(triggered()), this, SLOT(grabImageSlot()));
  connect(this->ui->actionReset_Camera, SIGNAL(triggered()), glWidget, SLOT(resetCamera()));
  connect(this->ui->actionChange_Background_Color, SIGNAL(triggered()), this, SLOT(changeBackgroundColorSlot()));
  connect(this->ui->actionChange_Projection, SIGNAL(triggered()), this, SLOT(changeProjectionSlot()));
  connect(this->ui->actionTop_View, SIGNAL(triggered()), this, SLOT(topViewSlot()));
  connect(this->ui->actionSide_View, SIGNAL(triggered()), this, SLOT(sideViewSlot()));
  connect(this->ui->actionToggle_BlackAndWhite, SIGNAL(triggered()), this, SLOT(blackWhiteSlot()));
  connect(this->ui->actionToggle_BoundingBox, SIGNAL(triggered()), this, SLOT(boundingBoxSlot()));
}


// Load the data
void MainWindow::loadDataSlot(){
 QString fileName = QFileDialog::getOpenFileName(this, tr("Load Data"),  "./", tr("All Files (*.*)"));
 std::cout << "You selected file: " << fileName.toStdString() << std::endl;
}

// Grab an image
void MainWindow::grabImageSlot(){
  glWidget->grabImage();
}

// Toggle black&white
void MainWindow::blackWhiteSlot(){
  glWidget->toggleBlackAndWhite();
}

// Toggle bounding box
void MainWindow::boundingBoxSlot(){
  glWidget->toggleBoundingBox();
}

// Slot to create a color dialog and pass back the color
void MainWindow::changeBackgroundColorSlot(){
  
  // Create a color dialog
  QColorDialog * colorDialog = new QColorDialog();
  colorDialog->setCurrentColor(glWidget->getBackgroundColor());
  colorDialog->open(glWidget, SLOT(setBackgroundColor(QColor)));
}

// Slot to change the viewing projection
void MainWindow::changeProjectionSlot(){
  glWidget->switchProjection();  
}

void MainWindow::topViewSlot(){
  glWidget->cameraTopDown();
}

void MainWindow::sideViewSlot(){
  glWidget->cameraSideView();
}
