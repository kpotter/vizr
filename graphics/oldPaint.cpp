// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//               ___  ____    __    ____  _   _  ____  ___  ___            //
//              / __)(  _ \  /__\  (  _ \( )_( )(_  _)/ __)/ __)           //
//             ( (_-. )   / /(__)\  )___/ ) _ (  _)(_( (__ \__ \           //
//              \___/(_)\_)(__)(__)(__)  (_) (_)(____)\___)(___/           //
//                                             Drawing & Widgets           //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// PaintWidget.cpp
// Written by Kristi Potter, 2013
// Widget that paints over the 3D GL widget
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#include "PaintWidget.h"
#include "GLIncludes.h"
#include <QMouseEvent>
#include "BoundingBoxDrawable.h"

// . . . get the angle of rotation . . . //
static void normalizeAngle(int &angle)
{
    while (angle < 0)
        angle += 360 * 16;
    while (angle > 360 * 16)
        angle -= 360 * 16;
}

// . . . get the new rotation . . . //
static int getRotation(int angle){
    normalizeAngle(angle);
    return angle;
}

// * * * Constructor * * * //
PaintWidget::PaintWidget(std::string title, QWidget *parent)
  : QGLWidget(QGLFormat(QGL::SampleBuffers), parent), _ctl_down(false), _shift_down(false)
{  
  // Initialize the window
  setTitle(title);
  setFocusPolicy(Qt::StrongFocus);
  setAutoFillBackground(false);
  setMinimumSize(500, 500);
  
  // Initialize the GL context
  // initializeGL();

  // Initialize the camera variables
  //intializeCamera();

  // Initialize the viewing variables
  //initializeViewing();

  // Initialize the lighting
  //intializeLighting();

  // Set the background color
  setBackgroundColor(Qt::white);
  
  // Initialize the timestep
  _timeStep = 0;
}


// * * * Initialize GL stuff, called once * * * //
void PaintWidget::initializeGL()
{
    //glEnable( GL_MULTISAMPLE );
    glEnable( GL_LINE_SMOOTH );
    glEnable( GL_POLYGON_SMOOTH );
    glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
    glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
    glCullFace(GL_BACK);
}


/*
 // Set up viewing variables 
void PaintWidget::initializeViewing(){
  
  std::cout << "viewing " << std::endl;
  
  // Perspective 
  _fovy = 50.0;
  _aspect = width()/height();
  _near = 1.0;
  _far =  1000.0;

  // Camera
  _eye = Vector3d(0.0, 0.0, 5.0);
  _center = Vector3d(0.0, 0.0, 0.0);
  _up = Vector3d(0.0, 1.0, 0.0);
}

// * * * Initialize the camera * * * //
void PaintWidget::intializeCamera(){
  
  // User-defined transformations
  _xRot = _yRot = _zRot = 0;
  _xTrans = _yTrans = _zTrans = 0;

  std::cout << "init" <<  _xTrans << std::endl;
  
}
*/
// * * * Set up the viewing * * * //
void PaintWidget::setupViewport(int width, int height)
{
  // Define the viewport
  int side = qMin(width, height);
  glViewport(0, 0, width, height);

  // Define the projection matrix
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(_fovy, _aspect, _near, _far);
  glMatrixMode(GL_MODELVIEW);
  
  // Define the camera position
  glLoadIdentity();
  gluLookAt(_eye.x(), _eye.y(), _eye.z(),
	            _center.x(), _center.y(), _center.z(),
	             _up.x(), _up.y(), _up.z());
  
 
}
/*

// * * * Set up the lighting * * * //
void PaintWidget::intializeLighting(){
  static GLfloat lightPosition[4] = { 0.5, 5.0, 7.0, 1.0 };
  glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
}

*/
// * * * Handle resize call * * * //
void PaintWidget::resizeGL(int width, int height)
{
  // Change the viewing aspect ratio to keep the frustum a box
  _aspect = float(width)/height;
  update();
}

// * * * Paint the GL scene * * * //
void PaintWidget::paintGL(){

  std::cout << "paint GL" << _backgroundColor.name().toStdString() << std::endl;
  glTranslatef(_xTrans, _yTrans, _zTrans);
  glRotatef(_xRot / 16.0, 1.0, 0.0, 0.0);
  glRotatef(_yRot / 16.0, 0.0, 1.0, 0.0);
  glRotatef(_zRot / 16.0, 0.0, 0.0, 1.0);

  glPushMatrix();
  
  // Clear the scene
  qglClearColor(_backgroundColor);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
 
  // Enable depth test & lighting for 3D scene
  glShadeModel(GL_SMOOTH);
  glEnable(GL_DEPTH_TEST);
  glColorMaterial ( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE ) ;
  glEnable(GL_COLOR_MATERIAL);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  // static GLfloat lightPosition[4] = { 0.5, 5.0, 7.0, 1.0 };
  //glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

  // Draw the scene
  drawScene();
  
}

// * * * Draw the added drawables. * * * //
void PaintWidget::drawScene(){
  std::cout << "Draw dscene " << _drawables.size() << std::endl;
  for(unsigned int i = 0; i < _drawables.size(); i++)
    _drawables[i]->draw();
}


// * * * Draw the overpaint * * * //
/*void PaintWidget::drawOverpainters(QPainter * painter){
  std::cout << "odraw overpainters " << _overpainters.size() << std::endl;
    for(unsigned int i = 0; i < _overpainters.size(); i++)
        _overpainters[i]->draw(painter, this);
	}*/


// * * * Animate * * * //
/*void PaintWidget::animate(float timestep)
{
  _timeStep = timestep;
  for(unsigned int i = 0; i < _drawables.size(); i++)
    _drawables[i]->animate(timestep);
  
  update();
}

// * * * Event on mouse press * * * //
void PaintWidget::mousePressEvent(QMouseEvent *event)
{
  // Update the last mouse position
  _lastPos = event->pos();
  }

// * * * Event on mouse move * * * //
void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
  // Difference between this & last mouse position
    int dx = event->x() - _lastPos.x();
    int dy = event->y() -_lastPos.y();
    
    // Rotate
    if ((event->buttons() & Qt::LeftButton) && !_ctl_down && !_shift_down) {
      _xRot = getRotation(_xRot + 8 * dy);
      _yRot = getRotation(_yRot + 8 * dx);
    } 
    else if ((event->buttons() & Qt::RightButton) || ((event->buttons() & Qt::LeftButton) && !_shift_down)) {
      _xTrans = _xTrans + dx*.015;
      _yTrans = _yTrans - dy*.015;
      std::cout << "mouse move " << _xTrans << std::endl;
    }
    else if((event->buttons() & Qt::MiddleButton) || (event->buttons() & Qt::LeftButton)){
      _zTrans = _zTrans + dy*.5;
    }
    _lastPos = event->pos();
    update();
}

// * * * Key press event * * * //
void PaintWidget::keyPressEvent(QKeyEvent * event){
  
  // Change background color
  if(event->key() == Qt::Key_B){
    if (_backgroundColor == Qt::white)
      setBackgroundColor(Qt::darkGray);  
    else
      setBackgroundColor(Qt::white);
  }

  if(event->key() == Qt::Key_Control){
    _ctl_down = true;
  }
  if(event->key() == Qt::Key_Shift){
    _shift_down = true;
  }
}
void PaintWidget::keyReleaseEvent(QKeyEvent * event){
  if(event->key() == Qt::Key_Control){
    _ctl_down = false;
  }
  if(event->key() == Qt::Key_Shift){
    _shift_down = false;
  }
}
*/
void PaintWidget::resetCamera(){

  // std::cout << "Reset" << std::endl;
  
  _xRot = _yRot = _zRot = 0;
  _xTrans = _yTrans = _zTrans = 0;
  std::cout << " resent _xTRans" << _xTrans << std::endl;
  update();
}


// Set the background color and pass it to Qt
void PaintWidget::setBackgroundColor(QColor background){
  std::cout << "-- paint widget set background -- " << background.name().toStdString() << std::endl;
  _backgroundColor = background; 
  qglClearColor(_backgroundColor);
  update();
  //repaint();
}
