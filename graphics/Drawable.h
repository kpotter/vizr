// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//               ___  ____    __    ____  _   _  ____  ___  ___            //
//              / __)(  _ \  /__\  (  _ \( )_( )(_  _)/ __)/ __)           //
//             ( (_-. )   / /(__)\  )___/ ) _ (  _)(_( (__ \__ \           //
//              \___/(_)\_)(__)(__)(__)  (_) (_)(____)\___)(___/           //
//                                             Drawing & Widgets           //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// Drawable.h     
// Written by Kristi Potter, 2013
// Basic drawable object
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _DRAWABLE_H
#define _DRAWABLE_H

#include <QObject>
#include "BoundingBox.h"
#include "ColorDefines.h"
#include "GLIncludes.h"



// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
//!
//! Base class for anything drawn in an openGL scene (3D). 
//! By extending drawable you get a bounding box, as
//! well as an animation slot.  You can then use addDrawable() in any class
//!
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
class Drawable : public QObject{
  Q_OBJECT
  
public:
  
  // Constructor (default)
  Drawable() : _drawBox(true), _timeStep(-1), _boxColor(black), _boxColorFront(gray), _boxColorBack(ltgrey)
  { _boundBox  = new BoundingBox(); }
  
  // Constructor (take in bounding box)
  Drawable(BoundingBox * box) : _boundBox(box), _timeStep(-1), _drawBox(true) {}
  
  // Copy Constructor
  Drawable(const Drawable &d) { _boundBox = d._boundBox; _timeStep = d._timeStep; _drawBox = d._drawBox; }
  
  // Destructor
  virtual ~Drawable(){}

  // OVERWRITE !!
  //Draw function; gets called automatically when a drawable is added to an  OverpaintWidget 
  virtual void draw(){
    if(_drawBox)
      drawBox();
  }

  // Set the bounding box
  void setBox(BoundingBox * box){ _boundBox = box; }

  // Get the bounding box
  BoundingBox * getBoundingBox() { return _boundBox; } 

  // Draw the bounding box
  void drawBox(){
    
    glDisable(GL_LIGHTING);
    glColor3fv(_boxColor.v);
    glLineWidth(0.5);

    glBegin(GL_LINE_LOOP);
    glColor3fv(_boxColorBack.v);
    glVertex3f(_boundBox->max.x(), _boundBox->max.y(), _boundBox->min.z());
    glVertex3f(_boundBox->min.x(), _boundBox->max.y(), _boundBox->min.z());
    glVertex3f(_boundBox->min.x(), _boundBox->min.y(), _boundBox->min.z());
    glVertex3f(_boundBox->max.x(), _boundBox->min.y(), _boundBox->min.z());
    glEnd();
    
    /* glBegin(GL_LINE_LOOP);
    glColor3fv(_boxColorFront.v);
    glVertex3f(_boundBox->max.x(), _boundBox->max.y(), _boundBox->max.z());
    glVertex3f(_boundBox->min.x(), _boundBox->max.y(), _boundBox->max.z());
    glVertex3f(_boundBox->min.x(), _boundBox->min.y(), _boundBox->max.z());
    glVertex3f(_boundBox->max.x(), _boundBox->min.y(), _boundBox->max.z());
    glEnd();
    */
    
    glBegin(GL_LINES);
    glColor3fv(_boxColorFront.v);

    // Bottom front
    glVertex3f(_boundBox->max.x(), _boundBox->max.y(), _boundBox->max.z());
    glVertex3f(_boundBox->min.x(), _boundBox->max.y(), _boundBox->max.z());

    // Bottom side
    glVertex3f(_boundBox->min.x(), _boundBox->max.y(), _boundBox->max.z());
    glVertex3f(_boundBox->min.x(), _boundBox->min.y(), _boundBox->max.z());

    // Top front
     glVertex3f(_boundBox->max.x(), _boundBox->min.y(), _boundBox->max.z()); 
     glVertex3f(_boundBox->max.x(), _boundBox->max.y(), _boundBox->max.z());
    
     glVertex3f(_boundBox->min.x(), _boundBox->min.y(), _boundBox->max.z());
     glVertex3f(_boundBox->max.x(), _boundBox->min.y(), _boundBox->max.z());
    
    glColor3fv(_boxColorBack.v);
    glVertex3f(_boundBox->max.x(), _boundBox->max.y(), _boundBox->min.z());
    glColor3fv(_boxColorFront.v);
    glVertex3f(_boundBox->max.x(), _boundBox->max.y(), _boundBox->max.z());
        

    glColor3fv(_boxColorBack.v);
    glVertex3f(_boundBox->min.x(), _boundBox->max.y(), _boundBox->min.z());
    glColor3fv(_boxColorFront.v);
    glVertex3f(_boundBox->min.x(), _boundBox->max.y(), _boundBox->max.z());
    
    
    glColor3fv(_boxColorBack.v);
    glVertex3f(_boundBox->min.x(), _boundBox->min.y(), _boundBox->min.z());
    glColor3fv(_boxColorFront.v);
    glVertex3f(_boundBox->min.x(), _boundBox->min.y(), _boundBox->max.z());
    
    glColor3fv(_boxColorBack.v);
    glVertex3f(_boundBox->max.x(), _boundBox->min.y(), _boundBox->min.z());
    glColor3fv(_boxColorFront.v);
    glVertex3f(_boundBox->max.x(), _boundBox->min.y(), _boundBox->max.z());
    

    glEnd();
    
    glEnable(GL_LIGHTING);
  }

  // Toggle the bounding box on or off
  void toggleBox(){ _drawBox = !_drawBox;}

public slots:
 
  // Slot to increment the animation time step
  virtual void animate(float timeStep){ _timeStep = timeStep; }

protected:
  // The current animation timestep
  float _timeStep;

  // The bounding box of this drawable
  BoundingBox * _boundBox;

  // The bounding box color
  ColorF _boxColor;
  ColorF _boxColorFront;
  ColorF _boxColorBack;

  // Flag to draw bounding box
  bool _drawBox;

};

#endif
