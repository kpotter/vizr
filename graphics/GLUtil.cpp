/*
* $Id: glUtil.cpp,v 1.1.1.1 2008/06/09 20:01:42 kpotter Exp $
*/

/*
**  Joe Michael Kniss (c) 2002
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This library is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
**  Lesser General Public License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// glUtils.cpp

//#ifdef WIN32
//#  include <windows.h>
//#endif

#include "GLUtil.h"
#include <iostream>
using namespace std;

//=========================================================================
// Report GL Errors
//=========================================================================

#ifndef NDEBUG // DEBUG mode
bool glErr(std::ostream &os, const char *where, const char *when)
{
   GLuint errnum;
   const char *errstr;
   bool state = false;
   int n = 0;
   while ( (errnum = glGetError()) && n < 10 ) 
   {
       ++n;

      errstr = reinterpret_cast<const char *>(gluErrorString(errnum));
      os << errstr; 

      if(where) os << " at " << where;
      if(when) os << "::" << when;

      os << "\n";
      state = true;
   }
   return state;
}
#else /// no-op
bool glErr(std::ostream &os, const char *where, const char *when)
{
   return false;
}
#endif


//=========================================================================
// Get Last Windows GL (WGL) Error
//=========================================================================
#ifndef NDEBUG // DEBUG mode
#if WIN32
void wglGetLastError(std::ostream &os, const char *where)  //from Sebastian
{
   DWORD err = GetLastError();
   switch(err)
   {
   case ERROR_INVALID_PIXEL_FORMAT:
      os << "Win32 Error:  ERROR_INVALID_PIXEL_FORMAT ";
      break;
   case ERROR_NO_SYSTEM_RESOURCES:
      os << "Win32 Error:  ERROR_NO_SYSTEM_RESOURCES ";
      break;
   case ERROR_INVALID_DATA:
      os << "Win32 Error:  ERROR_INVALID_DATA ";
      break;
   case ERROR_INVALID_WINDOW_HANDLE:
      os << "Win32 Error:  ERROR_INVALID_WINDOW_HANDLE ";
      break;
   case ERROR_RESOURCE_TYPE_NOT_FOUND:
      os << "Win32 Error:  ERROR_RESOURCE_TYPE_NOT_FOUND ";
      break;
   case ERROR_SUCCESS:
      // no error
      break;
   default:
      os << "Win32 Error:  " << err;
      break;
   }

   if(err>0) {
      if(where) os << " at " << where << "\n";
      else os << "\n";
   }	
   SetLastError(0);
}
#endif
#else
void wglGetLastError(std::ostream &os, const char *where)  //from Sebastian
{}
#endif

//=========================================================================
// Get GL Window Statistics
//=========================================================================
WinStatsGL getGLWinStats(bool print)
{
   WinStatsGL gws;

   glGetIntegerv(GL_RED_BITS,           (GLint*)&gws.colorBits[0]);
   glGetIntegerv(GL_GREEN_BITS,         (GLint*)&gws.colorBits[1]);
   glGetIntegerv(GL_BLUE_BITS,          (GLint*)&gws.colorBits[2]);
   glGetIntegerv(GL_ALPHA_BITS,         (GLint*)&gws.colorBits[3]);
   glGetIntegerv(GL_ACCUM_RED_BITS,     (GLint*)&gws.accumBits[0]);
   glGetIntegerv(GL_ACCUM_GREEN_BITS,   (GLint*)&gws.accumBits[1]);
   glGetIntegerv(GL_ACCUM_BLUE_BITS,    (GLint*)&gws.accumBits[2]);
   glGetIntegerv(GL_ACCUM_ALPHA_BITS,   (GLint*)&gws.accumBits[3]);
   glGetIntegerv(GL_DEPTH_BITS,         (GLint*)&gws.depthBits);
   glGetIntegerv(GL_STENCIL_BITS,       (GLint*)&gws.stencilBits);
   glGetBooleanv(GL_DOUBLEBUFFER,       &gws.doubleBuffer);
   glGetBooleanv(GL_STEREO,             &gws.stereoBuffer);
   glGetBooleanv(GL_AUX_BUFFERS,        &gws.auxBuffer);

   if(print)
   {
      cerr << "Window Attributes" << endl;
      cerr << "-----------------" << endl;
      cerr << "color:    " 
	   << gws.colorBits[0] + gws.colorBits[1] + 
	      gws.colorBits[2] + gws.colorBits[3] << " bits";
      cerr << " (" 
           << gws.colorBits[0] << "," << gws.colorBits[1] << "," 
           << gws.colorBits[2] << "," << gws.colorBits[3] << ")" << endl;
      cerr << "accum:    " 
           << gws.accumBits[0] + gws.accumBits[1] + 
              gws.accumBits[2] + gws.accumBits[3]   << " bits";
      cerr << " (" << gws.accumBits[0] << "," << gws.accumBits[1] 
           << "," << gws.accumBits[2] << "," << gws.accumBits[3] << ")" << endl;
      cerr << "depth:    " << gws.depthBits << " bits" << endl;
      cerr << "stencil:  " << gws.stencilBits << " bits" << endl;
      cerr << "double:   "; 
      if(gws.doubleBuffer) cerr << "YES" << endl;
      else   cerr << "NO" << endl;
      cerr << "aux:      ";
      if(gws.auxBuffer) cerr << "YES" << endl;
      else    cerr << "NO" << endl;
      cerr << "stereo :  ";
      if(gws.stereoBuffer) cerr << "YES" << endl;
      else   cerr << "NO" << endl;
      cerr << "-----------------" << endl;
   }

   return gws;
}

//=========================================================================
// Get GL Limits
//=========================================================================

const LimitsGL getLimitsGL()
{
   static LimitsGL glim = {0,0};

   if( glim.maxTexUnits == 0 ) {
     glGetIntegerv(GL_MAX_TEXTURE_UNITS_ARB, (GLint*)&glim.maxTexUnits);
      //TODO: This is not actually querying ogl for the number of texcoords,
      //needs to be fixed
      glim.maxTexCoords = glim.maxTexUnits > 8 ? 8 : glim.maxTexUnits;
   }

   return glim;
}

//=========================================================================
// GL Color & Texture Utils
//=========================================================================
/// Set color mask: Use GL_NONE to turn all off and GL_RGBA to turn all on
void setColorMask( GLenum channel )
{
   switch(channel){
   case GL_NONE:  glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); break;
   case GL_RED:   glColorMask(GL_TRUE,  GL_FALSE, GL_FALSE, GL_FALSE); break;
   case GL_GREEN: glColorMask(GL_FALSE, GL_TRUE,  GL_FALSE, GL_FALSE); break;
   case GL_BLUE:  glColorMask(GL_FALSE, GL_FALSE, GL_TRUE,  GL_FALSE); break;
   case GL_ALPHA: glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_TRUE ); break;
   case GL_RGB:   glColorMask(GL_TRUE,  GL_TRUE,  GL_TRUE,  GL_FALSE ); break;
   case GL_RGBA:  glColorMask(GL_TRUE,  GL_TRUE,  GL_TRUE,  GL_TRUE ); break;
   default: cerr << "setColorMask(...):\n\tUnsupported channel\n";
      //exit(1);
   }
}

// Returns 'channel' if is GL_RED, GL_GREEN, or GL_BLUE, otherwise
// gives error message and dies.
GLenum validateSingleRGBChannel( GLenum channel  )
{
   if( channel != GL_RED &&
      channel != GL_GREEN &&
      channel != GL_BLUE ) {
         cerr << "validateSingleChannel(...) Error :\n"
            << "\t'channel' (" << channel << ") is not a single-channel R, G, or B.\n";
         //exit(1);
      }

      return channel;
}

/// Return true if 'channel' is single-channel GLenum
bool isSingleChannel( GLenum channel )
{
   bool okay=false;

   switch( channel ) {
   case GL_RED:
   case GL_GREEN:
   case GL_BLUE:
   case GL_ALPHA:
   case GL_LUMINANCE:
      okay = true; 
      break;
   default:
      okay = false;
   }

   return okay;
}

int getNumChannels( GLenum format )
{
   int numChannels;
   switch(format) {
      case GL_COLOR_INDEX:
      case GL_STENCIL_INDEX:
      case GL_DEPTH_COMPONENT:
      case GL_RED:		   
      case GL_GREEN:
      case GL_BLUE:	   
      case GL_ALPHA:
      case GL_LUMINANCE: numChannels = 1; break;

      case GL_RGB:	   numChannels = 3; break;

      case GL_RGBA:	   numChannels = 4; break;

      default: cerr << "getNumChannels Error: Unknown format " << format << endl;
         //exit(1);
   }

   return numChannels;
}

/// Get the dimensionality of the texture type
int getTexDim( GLenum texType )
{
   int texDim;

   switch(texType) {
   case GL_TEXTURE_1D: 		  texDim = 1; break;
   case GL_TEXTURE_2D: 		  texDim = 2; break;
   case GL_TEXTURE_3D: 		  texDim = 3; break;
   case GL_TEXTURE_CUBE_MAP_ARB: texDim = 2; break;
   default:
      cerr << "getTexDim(" << texType << ") Error:\n\tUnrecognized texture target name.\n";
      //exit(1);
   }

   return texDim;
}

//=========================================================================
// Light Stuff
//=========================================================================

int getLightGL( int lightNum, gutz::Light *light )
{
    if( !light ) return 1;
    vec4f tmp;

    glGetLightfv( GL_LIGHT0 + lightNum, GL_POSITION, tmp.v );
    
    light->setEyePos( tmp );

    return 0;

}

#if 0
//=========================================================================
// GL Text Utils
//=========================================================================
void renderBitmapString( float x, float y, int spacing, void *font, char *string) 
{
   char *c;
   int x1=x;
   for (c=string; *c != '\0'; c++) {
      glRasterPos2f(x1,y);
      glutBitmapCharacter(font, *c);
      x1 = x1 + glutBitmapWidth(font,*c) + spacing;
   }
}

//=========================================================================
// Make Glut Window Current - first call initiallizes this
//=========================================================================
void MakeGlutWindowCurrent()
{
  //   static int glutWinId = glutGetWindow();
  //   glutSetWindow( glutWinId );
}
#endif



//------------------------------------------------------------------------
//C++ 
//                   ________    ____   ___ 
//                  |        \  /    | /  /
//                  +---+     \/     |/  /
//                  +--+|  |\    /|     < 
//                  |  ||  | \  / |  |\  \               .
//                  |      |  \/  |  | \  \               .
//                   \_____|      |__|  \__\               .
//                       Copyright  2001 
//                      Joe Michael Kniss
//               "All Your Base are Belong to Us"
//-------------------------------------------------------------------------
