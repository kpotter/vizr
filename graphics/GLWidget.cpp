// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//               ___  ____    __    ____  _   _  ____  ___  ___            //
//              / __)(  _ \  /__\  (  _ \( )_( )(_  _)/ __)/ __)           //
//             ( (_-. )   / /(__)\  )___/ ) _ (  _)(_( (__ \__ \           //
//              \___/(_)\_)(__)(__)(__)  (_) (_)(____)\___)(___/           //
//                                             Drawing & Widgets           //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// GLWidget.cpp
// Written by Kristi Potter, 2015
// Widget that paints a 3D GL widget
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#include "GLWidget.h"
#include "GLIncludes.h"
#include "helperFunctions.h"
#include <QMouseEvent>
#include "BoundingBoxDrawable.h"


// * * * Constructor * * * //
GLWidget::GLWidget(std::string title, QWidget *parent)
  : QGLWidget(QGLFormat(QGL::SampleBuffers), parent), 
    _ctl_down(false), _shift_down(false), _blackAndWhite(true), _timeStep(-1), _perspective(true)
{

  // Initialize the window
  setTitle(title);
  setFocusPolicy(Qt::StrongFocus);
  setAutoFillBackground(false);
  setMinimumSize(500, 500);
  
  // Initialize the viewing variables
  initializeViewing();
  
  // Set the background color
  setBackgroundColor(Qt::white);
}

// * * * Set the background color * * * //
void GLWidget::setBackgroundColor(QColor background){
  _backgroundColor = background; 
  qglClearColor(_backgroundColor);
  update();
}

// * * * Set up viewing variables * * * //
void GLWidget::initializeViewing(){
  
  // User-defined transformations
  _xRot = 4313;
  _yRot = _zRot = 0; 
  _xTrans = _yTrans = _zTrans = 0;
  
  // Perspective 
  _fovy = 25.0;
  _aspect = width()/height();
  _near = 1.0;
  _far =  1000.0;

  // Camera
  _eye = Vector3d(0.0, 5.0, 0.0);
  _center = Vector3d(0.0, 0.0, 0.0);
  _up = Vector3d(1.0, 0.0, 0.0);
}

// * * * Initialize GL stuff, called once * * * //
void GLWidget::initializeGL()
{ 
  glEnable( GL_MULTISAMPLE );
  glEnable( GL_LINE_SMOOTH );
  glEnable( GL_POLYGON_SMOOTH );
  glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
  glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
  
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
  glEnable( GL_RESCALE_NORMAL );

  glEnable( GL_CULL_FACE );
  glCullFace( GL_BACK );
  
  initializeLighting();
}


// * * * Set up the lighting * * * //
void GLWidget::initializeLighting(){

  
  glEnable(GL_LIGHTING);
  
  glShadeModel(GL_SMOOTH);
  glColorMaterial ( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE ) ;
  glEnable(GL_COLOR_MATERIAL);
  
  glEnable(GL_LIGHT0);
  static GLfloat light0Position[4] = { 0.0, 20.0, 20.0, 1.0 };
  static GLfloat light0Ambient[4] = {0.05, 0.05, 0.05, 1.0};
  static GLfloat light0Diffuse[4] = {0.25, 0.25, 0.25, 1.0};
  static GLfloat light0Specular[4] = {.75, .75, .75, 1.0}; 
  glLightfv(GL_LIGHT0, GL_POSITION, light0Position);
  glLightfv(GL_LIGHT0, GL_AMBIENT, light0Ambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, light0Diffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR, light0Specular);
  //  glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, lightPosition3);
  
  // glEnable(GL_LIGHT1);
  static GLfloat light1Position[4] = { 0.0, 2.0, 2.0, 0.0 };
  static GLfloat light1Ambient[4] = {0.0, 0.0, 0.0, 1.0};
  static GLfloat light1Diffuse[4] = {0.5, 0.5, 0.5, 1.0};
  static GLfloat light1Specular[4] = {0.25, 0.25, 0.25, 1.0}; 
  //glLightfv(GL_LIGHT1, GL_POSITION, light1Position);
  //glLightfv(GL_LIGHT1, GL_AMBIENT, light1Ambient);
  //glLightfv(GL_LIGHT1, GL_DIFFUSE, light0Diffuse);
  //glLightfv(GL_LIGHT1, GL_SPECULAR, light0Specular);
 

  /* Set up lighting. mostly for widgets */
  static float ambient[]             = {0.25, 0.25, 0.25, 1};
  static float diffuse[]             = {.5, .5, .5, 1.0};
  static float front_mat_shininess[] = {64.0};
  static float front_mat_specular[]  = {0.5, 0.5, 0.5, 1.0};
  static float front_mat_diffuse[]   = {0.5, 0.5, 0.5, 1.0};
  static float front_mat_emission[] = {0.1, 0.1, 0.1, 1.0};
  static float lmodel_ambient[]      = {0.5, 0.5,  0.5,  1.0};
  
//  glLightfv(GL_LIGHT0, GL_POSITION, ltpos);
  //glLightfv(GL_LIGHT1, GL_AMBIENT,  ambient);
  //glLightfv(GL_LIGHT1, GL_DIFFUSE,  diffuse);
  
  glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);  
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT,  lmodel_ambient);   
  glMaterialfv(GL_FRONT, GL_SHININESS, front_mat_shininess);
  glMaterialfv(GL_FRONT, GL_SPECULAR,  front_mat_specular);
  glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE,   front_mat_diffuse);
  glMaterialfv(GL_FRONT, GL_EMISSION,   front_mat_emission);
  
 
  //  glEnable(GL_LIGHT1);
  //glEnable(GL_LIGHT2);
  
  //static GLfloat lightPosition2[4] = { 0.5, 2.0, 0.5, 1.0 };
  //glLightfv(GL_LIGHT1, GL_POSITION, lightPosition2);

  //static GLfloat lightPosition3[4] = { 0.0, 2.5, 2.5, 1.0 };
  //glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, lightPosition3);
  
}

// * * * Set up the viewing * * * //
void GLWidget::setViewing()
{
  
  
  // Define the projection matrix
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  
  // Define the viewport
  glViewport(0, 0, width(), height());
  
  // Do orthographic or projection
  if(_perspective)
    gluPerspective(_fovy, _aspect, _near, _far);
  else
    glOrtho(-1.05, 1.05,-1.05,1.05, -10, 100);
  glMatrixMode(GL_MODELVIEW);

  // Define the camera position
  glLoadIdentity();
  gluLookAt(_eye.x(), _eye.y(), _eye.z(),
	    _center.x(), _center.y(), _center.z(),
	    _up.x(), _up.y(), _up.z());
  
  // Define the user-controlled viewing movement
  glTranslatef(_yTrans, _zTrans, _xTrans);
  glRotatef(_xRot / 16.0, 1.0, 0.0, 0.0);
  glRotatef(_yRot / 16.0, 0.0, 1.0, 0.0);
  glRotatef(_zRot / 16.0, 0.0, 0.0, 1.0);
  glPushMatrix();

}

// * * * Reset the camera * * * //
void GLWidget::resetCamera(){
  _xRot = 4313;
  _yRot = _zRot = 0;
  _xTrans = _yTrans = _zTrans = 0;
  update();
}

// * * * Handle resize call * * * //
void GLWidget::resizeGL(int width, int height)
{
  // Change the viewing aspect ratio to keep the frustum a box
  _aspect = float(width)/height;
}

// * * * Create the scene to draw * * * //
void GLWidget::createScene(){  
  // Add a bounding box to the scene
  //BoundingBoxDrawable *bbDraw = new BoundingBoxDrawable();
  //addDrawable(bbDraw);
}

// * * * Key press/release events * * * //
void GLWidget::keyPressEvent(QKeyEvent * event){
  if(event->key() == Qt::Key_Control){
    _ctl_down = true;
  }
  if(event->key() == Qt::Key_Shift){
    _shift_down = true;
  }
  if(event->key() == Qt::Key_X){
    _x_down = true;
  }
  if(event->key() == Qt::Key_Y){
    _y_down = true;
  }
  if(event->key() == Qt::Key_Z){
    _z_down = true;
  }
}
void GLWidget::keyReleaseEvent(QKeyEvent * event){
  if(event->key() == Qt::Key_Control){
    _ctl_down = false;
  }
  if(event->key() == Qt::Key_Shift){
    _shift_down = false;
  }
 if(event->key() == Qt::Key_X){
    _x_down = false;
  }
  if(event->key() == Qt::Key_Y){
    _y_down = false;
  }
  if(event->key() == Qt::Key_Z){
    _z_down = false;
  }
}

// * * * Event on mouse press * * * //
void GLWidget::mousePressEvent(QMouseEvent *event)
{
  // Update the last mouse position
  _lastPos = event->pos();
}

// * * * Event on mouse move * * * //
void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
  // Difference between this & last mouse position
  int dx = event->x() - _lastPos.x();
  int dy = event->y() -_lastPos.y();

  // Left Button
  if(event->buttons() && Qt::LeftButton){

  
    // Translate in Z
    if(_shift_down){
       _zTrans = _zTrans + dy*.015;
    }
    // Rotate
    else if(_x_down){
      _xRot = getRotation(_xRot + 8 * dx);
      //_yRot = getRotation(_yRot - 8 * dy);
      // _zRot = getRotation(_yRot + 8 * dy);
    }
    // Rotate
    else if(_y_down){
      // _xRot = getRotation(_xRot + 8 * dx);
      _yRot = getRotation(_yRot - 8 * dy);
      // _zRot = getRotation(_yRot + 8 * dy);
    }
 // Rotate
    else if(_z_down){
      // _xRot = getRotation(_xRot + 8 * dx);
      //_yRot = getRotation(_yRot - 8 * dy);
       _zRot = getRotation(_zRot + 8 * dy);
    }

    // Translate in XY
    else{
      _xTrans = _xTrans + dx*.015;
      _yTrans = _yTrans - dy*.015;
    }

    // Save this position for the next movement
    _lastPos = event->pos(); 
    update();
  }

  //std::cout << "rots: " << _xRot <<  " " << _yRot << " " << _zRot << std::endl;
  //std::cout << "trans: " << _xTrans << " " << _yTrans <<  " " << _zTrans << std::endl;

  // Save this position for the next movement
  _lastPos = event->pos(); 
  update();
}


// * * * Animate * * * //
void GLWidget::animate(float timestep)
{  
  _timeStep = timestep;
  for(unsigned int i = 0; i < _drawables.size(); i++)
    _drawables[i]->animate(timestep);
  
  update();
}

// * * * Draw the added drawables. * * * //
void GLWidget::drawScene(){
  
  for(unsigned int i = 0; i < _drawables.size(); i++)
    _drawables[i]->draw();
}

// * * * Paint GL * * * //
void GLWidget::paintGL(){
 

  // Set the viewing
  setViewing();
  
  // Clear the scene
  qglClearColor(_backgroundColor);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  /*
  glBegin(GL_QUADS);  
  glColor3f(0.0,0.0,0.0);
  glVertex3f(-1.0, -10.0, 10.0);
  glVertex3f( -1.0, 10.0, 10.0);
  glColor3f(1.0,1.0,1.0);
  glVertex3f(-1.0, 10.0, -5.0);
  glVertex3f(-1.0, -10.0, -5.0);
  glEnd();
  */

  drawScene();
}

// * * * Paint event * * * //
void GLWidget::paintEvent(QPaintEvent *event){
  QGLWidget::paintEvent(event);
}


// * * * Toggle black and white * * * //
void GLWidget::toggleBlackAndWhite(){  
  _blackAndWhite = !_blackAndWhite;
  emit changeBlackAndWhite(_blackAndWhite);
}

// * * * Toggle bounding box * * * //
void GLWidget::toggleBoundingBox(){
  _boundingBox = !_boundingBox;
  emit changeBoundingBox(_boundingBox);
}

void GLWidget::grabImage(){

  paintGL();
  glFlush();
  QImage image = grabFrameBuffer();
  
  QString name = "tmp.png";
  QFile f(name);
  
  int index = 0;
  while(f.exists()){
    name = "tmp" + QString::number(index) + ".png";
    f.setFileName(name);
    index ++;
  }
  
  image.save(name);
  
}
