// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                  ___  ____    __    ____  _   _  ____  ___  ___         //
//                 / __)(  _ \  /__\  (  _ \( )_( )(_  _)/ __)/ __)        //
//                ( (_-. )   / /(__)\  )___/ ) _ (  _)(_( (__ \__\         //
//                 \___/(_)\_)(__)(__)(__)  (_) (_)(____)\___)(___/        //
//                                                          Widgets        //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// AnimationControl.cpp
// Written by Kristi Potter, 2013
// Widget that controls animation playback
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#include "AnimationControl.h"
#include <QLabel>
#include <QSpacerItem>
#include <QDir>

/// Constructor
AnimationControl::AnimationControl(QWidget *parent) :
  QWidget(parent), _loop(false), _timeStep(-1)
{
  // Create the GUI
  createControls();
  
  // Initialize time information
  setTimeInformation(0, 100, 100);
 
  // Connect the animation timer
  _animationTimer.setSingleShot(false);
  connect(&_animationTimer, SIGNAL(timeout()), this, SLOT(animate()));
}

/// Set up the time for the data
void AnimationControl::setTimeInformation(int minTime, int maxTime, int numFrames,
					  std::string timeUnit, int timeout){  
  _minTime = minTime;
  _maxTime = maxTime;
  _numFrames = numFrames;
  _timeUnit = timeUnit;
  _timeout = timeout;
  _timeStep = _minTime;
  _timeIncrement = (_maxTime-_minTime+1)/_numFrames;
  _timestepSlider->setRange(_minTime, _maxTime);
  moveAnimation(_timeStep);
}
      
/// Animate moves the timestep by one time increment automatically
void AnimationControl::animate(){
  
  _timeStep += _timeIncrement;
  updateControls();
}

/// Move the animation to a certain value
void AnimationControl::moveAnimation(int value){
  _timeStep = value;
  stopAnimation();
}

/// Move to the end of the animation
void AnimationControl::endOfAnimation(){
  _timeStep = _maxTime;
  stopAnimation();
}

/// Move to the start of the animation
void AnimationControl::startOfAnimation(){
  _timeStep = _minTime;
  stopAnimation();
}

/// Move the animation forward one
void AnimationControl::forwardOneAnimation(){
  _timeStep += _timeIncrement;
  stopAnimation();
}

/// Move the animation back one
void AnimationControl::reverseOneAnimation(){
  _timeStep -= _timeIncrement;
  stopAnimation();
}

/// Start the animation
void AnimationControl::startAnimation(){
  startOrStopAnimation(true);
}
  
/// Stop the animation
void AnimationControl::stopAnimation(){
  startOrStopAnimation(false);
}

/// Start or stop the animation
void AnimationControl::startOrStopAnimation(bool clicked){
  if(clicked){
    _animationTimer.start(_timeout);
    playButton->setChecked(true);
  }
  else{
    _animationTimer.stop();
    playButton->setChecked(false);
  }
    
    updateControls();
}

/// Update the GUI controls
void AnimationControl::updateControls(){
  
  if(_timeStep > _maxTime){
    if(_loop)
      _timeStep = _minTime;
    else
      _timeStep = _maxTime;
  }
  if(_timeStep < _minTime){
    if(_loop)
      _timeStep = _maxTime;
    else
      _timeStep = _minTime;
  } 
  // Set the GUI
  _timestepSlider->setValue(_timeStep);
  _time->setText(QString::number(_timeStep, 'g', 3)+ " "+ QString::fromStdString(_timeUnit));
  
  // Emit animate signal
  emit animateSignal(_timeStep);
}

/// Create the GUI controls
void AnimationControl::createControls(){
  
  setMaximumSize(300, 100);
  QSize iconSize(36, 36);
  
  backButton = new QPushButton;
  backButton->setIcon(QIcon(":/back2.png"));
  backButton->setIconSize(iconSize);
  backButton->setToolTip(tr("Back"));
  backButton->setFlat(true);
  backButton->setStyleSheet(" QPushButton:flat {   border: none; } QPushButton:pressed{ border: 2px solid #8f8f91; border-radius: 6px;}");
  backButton->setMinimumWidth(50);
  backButton->setMaximumWidth(50);
  
  reverseButton = new QPushButton;
  reverseButton->setIcon(QIcon(":/reverse.png"));
  reverseButton->setIconSize(iconSize);
  reverseButton->setToolTip(tr("Reverse"));
  reverseButton->setFlat(true);
  reverseButton->setStyleSheet(" QPushButton:flat {   border: none; } QPushButton:pressed{ border: 2px solid #8f8f91; border-radius: 6px;}");
  reverseButton->setMaximumWidth(50);
  reverseButton->setMinimumWidth(50);
  
  playButton = new QPushButton;
  QIcon * playIcon = new QIcon;
  playIcon->addPixmap(QPixmap(":/play2.png"),QIcon::Normal,QIcon::Off);
  playIcon->addPixmap(QPixmap(":/pause2.png"),QIcon::Normal,QIcon::On);
  playButton->setIcon(*playIcon);
  playButton->setIconSize(iconSize);
  playButton->setToolTip(tr("Play"));
  playButton->setCheckable(true);
  playButton->setFlat(true);
  playButton->setStyleSheet(" QPushButton:flat {   border: none; } QPushButton:checked{ border:none; }");
  playButton->setMaximumWidth(50);
  playButton->setMinimumWidth(50);
  
  forwardButton = new QPushButton;
  forwardButton->setIcon(QIcon(":/forward.png"));
  forwardButton->setIconSize(iconSize);
  forwardButton->setToolTip(tr("Forward"));
  forwardButton->setFlat(true);
  forwardButton->setStyleSheet(" QPushButton:flat { border: none; } QPushButton:pressed{ border: 2px solid #8f8f91; border-radius: 6px;}");
  forwardButton->setMinimumWidth(50);
  forwardButton->setMaximumWidth(50);
  
  frontButton = new QPushButton;
  frontButton->setIcon(QIcon(":/front2.png"));
  frontButton->setIconSize(iconSize);
  frontButton->setToolTip(tr("Front"));
  frontButton->setFlat(true);
  frontButton->setStyleSheet(" QPushButton:flat { border: none; } QPushButton:pressed{ border: 2px solid #8f8f91; border-radius: 6px;}");
  frontButton->setMinimumWidth(50);
  frontButton->setMaximumWidth(50);
  
  connect(playButton, SIGNAL(clicked(bool)), this, SLOT(startOrStopAnimation(bool)));
  connect(reverseButton, SIGNAL(clicked()), this, SLOT(reverseOneAnimation()));
  connect(backButton, SIGNAL(clicked()), this, SLOT(startOfAnimation()));
  connect(forwardButton, SIGNAL(clicked()), this, SLOT(forwardOneAnimation()));
  connect(frontButton, SIGNAL(clicked()), this, SLOT(endOfAnimation()));
  
  _timestepSlider = new QSlider(Qt::Horizontal);
  _timestepSlider->setRange(0, _numFrames);
  _time = new QLabel;
  _time->setMinimumWidth(50);
  connect(_timestepSlider, SIGNAL(sliderMoved(int)), this, SLOT(moveAnimation(int)));  
  
  QLabel * tLabel = new QLabel("Timestep: ");
  tLabel->setMinimumWidth(250);
  
  layout = new QGridLayout;  
  layout->addWidget(tLabel, 0, 0);
  layout->addWidget(_timestepSlider, 0, 2, 1, 3);
  layout->addWidget(_time, 0, 5);
  layout->addWidget(backButton, 1, 0);
  layout->addWidget(reverseButton, 1, 1);
  layout->addWidget(playButton, 1, 2);
  layout->addWidget(forwardButton, 1, 3);
  layout->addWidget(frontButton, 1, 4);
  setLayout(layout);
}
