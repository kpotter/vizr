// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                                 ___        ___         __         ___       __      __     ___                                                         //    
//                               /   __)    (   ,   )      /  O \     (   ,   \   (    )  /   _ ) /   __)                                                       //    
//                              (  (      >   )      \    /  (  )  \     )    _/   )  (  (   ( _   \__  \                                                       //    
//                               \___/   (_)  (_) (__) (__)  (__)     (__)   \___) (___/                                                      //    
//                                                                                                         Widgets                                                        //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  //
// Overpainter.h 
// Written by Kristi Potter, 2013
// Base class to paint over the 3D GL scene.
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _OVERPAINTER_H
#define _OVERPAINTER_H

#include <QObject>
#include <QGLWidget>

// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Base class for anything painted over the gl scene
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
class Overpainter : public QObject{
  Q_OBJECT
  
public:
  Overpainter(){}
  virtual ~Overpainter(){}
  virtual void draw(QPainter * painter, QGLWidget * glWidget) = 0;
};

#endif
