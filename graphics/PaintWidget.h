// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//               ___  ____    __    ____  _   _  ____  ___  ___            //
//              / __)(  _ \  /__\  (  _ \( )_( )(_  _)/ __)/ __)           //
//             ( (_-. )   / /(__)\  )___/ ) _ (  _)(_( (__ \__ \           //
//              \___/(_)\_)(__)(__)(__)  (_) (_)(____)\___)(___/           //
//                                             Drawing & Widgets           //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// PaintWidget.h
// Written by Kristi Potter, 2013
// Widget that paints over the 3D GL widget
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _PAINT_WIDGET_H
#define _PAINT_WIDGET_H

#include <iostream>
#include <QGLWidget>
#include <QTimer>

#include "Drawable.h"
#include "Overpainter.h"
#include "Vector.h"


// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Base class for painting gl widgets that allow overpainting (optional)
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
class PaintWidget : public QGLWidget {
  
  Q_OBJECT
  
public:
  
  ///
  /// Constructor takes in a QWidget parent (optional).
  PaintWidget(std::string title = "VIZR", QWidget *parent = 0);

  // Destructor
  virtual ~PaintWidget(){}
  
  ///
  /// Overwrite this to draw the scene
  virtual void drawScene();

  /// Overwrite this to overpaint
  void drawOverpainters(QPainter * painter);

  // Add a drawable to the widget
   void addDrawable(Drawable * drawable) { 
    _drawables.push_back(drawable); 
    }

  // Add an overpainter to the widget
  void addOverpainter(Overpainter * overpainter) { _overpainters.push_back(overpainter); }

  // Set the title of the window
  void setTitle(std::string title){ _title = title; setWindowTitle(tr(_title.c_str())); }

  // Return the background color
   QColor getBackgroundColor(){ return _backgroundColor; }

public slots:
  virtual void animate(float);
  void resetCamera();
  void changeProjection();
    
  // Set the background color and pass it to Qt
  void setBackgroundColor(QColor background){
    _backgroundColor = background;
    qglClearColor(_backgroundColor);
  }
  
protected:
  
  // - - - Member Variables - - - //

  // Window title
  std::string _title;

  // Viewing variables
  double _fovy, _aspect, _near, _far;
  Vector3d _eye, _center, _up;
  
  // User-controlled transformations
  int _xRot, _yRot, _zRot;
  float _xTrans, _yTrans, _zTrans;

  // The last mouse position
  QPoint _lastPos; 

  // Trackers for key press
  bool _ctl_down;
  bool _shift_down;

  // Display objects
  std::vector<Drawable *> _drawables;
  std::vector<Overpainter *> _overpainters;

  // Background color
  QColor _backgroundColor;

  // Animation timer
  //QTimer _animationTimer;
  float _timeStep;

  void initializeGL();
  virtual void initializeViewing();
  void intializeLighting();
  void setViewing();

  void resizeGL(int width, int height);
 
  void mousePressEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);
  void keyPressEvent(QKeyEvent * event);
  void keyReleaseEvent(QKeyEvent * event);
  void setupViewport(int width, int height);
  
 virtual void paintEvent(QPaintEvent *event);
};
#endif
