// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                                 ___        ___         __         ___       __      __     ___                                                         //    
//                               /   __)    (   ,   )      /  O \     (   ,   \   (    )  /   _ ) /   __)                                                       //    
//                              (  (      >   )      \    /  (  )  \     )    _/   )  (  (   ( _   \__  \                                                       //    
//                               \___/   (_)  (_) (__) (__)  (__)     (__)   \___) (___/                                                      //    
//                                                                                                         Widgets                                                        //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  //
// InstructionsOverpainter.cpp 
// Written by Kristi Potter September 2010   
// Draws instructions over the 3D GL.
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#include "InstructionsOverpainter.h"

void InstructionsOverpainter::draw(QPainter * painter, QGLWidget * glWidget){

    QString text = tr("Click and drag with the left mouse button "
              "to rotate.");
    QFontMetrics metrics = QFontMetrics(glWidget->font());
    int border = qMax(4, metrics.leading());

    QRect rect = metrics.boundingRect(0, 0, glWidget->width() - 2*border, int(glWidget->height()*0.125),
                      Qt::AlignCenter | Qt::TextWordWrap, text);
    painter->setRenderHint(QPainter::TextAntialiasing);
    painter->fillRect(QRect(0, 0, glWidget->width(), rect.height() + 2*border),
              QColor(0, 0, 0, 127));
    painter->setPen(Qt::white);
    painter->fillRect(QRect(0, 0, glWidget->width(), rect.height() + 2*border),
              QColor(0, 0, 0, 127));
    painter->drawText((glWidget->width() - rect.width())/2, border,
              rect.width(), rect.height(),
              Qt::AlignCenter | Qt::TextWordWrap, text);

}
