// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//               ___  ____    __    ____  _   _  ____  ___  ___            //
//              / __)(  _ \  /__\  (  _ \( )_( )(_  _)/ __)/ __)           //
//             ( (_-. )   / /(__)\  )___/ ) _ (  _)(_( (__ \__ \           //
//              \___/(_)\_)(__)(__)(__)  (_) (_)(____)\___)(___/           //
//                                             Drawing & Widgets           //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// BoundingBoxDrawable.h  
// Written by Kristi Potter September 2010   
// A drawable that renders its bounding box.
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#ifndef BOUNDINGBOXDRAWABLE_H
#define BOUNDINGBOXDRAWABLE_H

#include "BoundingBox.h"
#include "Drawable.h"
#include "ColorDefines.h"
#include "GLIncludes.h"

// Class that will draw a bounding box
class BoundingBoxDrawable : public Drawable{
    Q_OBJECT
public:

  // Default constructor
  BoundingBoxDrawable(): Drawable(), _boxColor(black){}
  
  // Copy constructor takes in a constant bounding box
  BoundingBoxDrawable(const BoundingBoxDrawable &bb): Drawable(bb) { _boxColor = bb._boxColor; }
  
  // Destructor
  virtual ~BoundingBoxDrawable(){}

  // Set the color to draw the box
  void setBoxColor(ColorF color) { _boxColor = color; }

  // Draw the bounding box
  void draw(){
    std::cout << "Bb drablable" << std::endl;
    glColor3fv(_boxColor.v);
    glBegin(GL_LINE_LOOP);
    glColor3f(0.75, 0.75, 0.75);
    glVertex3f(_boundBox->max.x(), _boundBox->max.y(), _boundBox->min.z());
    glVertex3f(_boundBox->min.x(), _boundBox->max.y(), _boundBox->min.z());
    glVertex3f(_boundBox->min.x(), _boundBox->min.y(), _boundBox->min.z());
    glVertex3f(_boundBox->max.x(), _boundBox->min.y(), _boundBox->min.z());
    glEnd();
    
    glBegin(GL_LINE_LOOP);
    glColor3f(0.15, 0.15, 0.15);
    glVertex3f(_boundBox->max.x(), _boundBox->max.y(), _boundBox->max.z());
    glVertex3f(_boundBox->min.x(), _boundBox->max.y(), _boundBox->max.z());
    glVertex3f(_boundBox->min.x(), _boundBox->min.y(), _boundBox->max.z());
    glVertex3f(_boundBox->max.x(), _boundBox->min.y(), _boundBox->max.z());
    glEnd();
    
    
    glBegin(GL_LINES);

    glColor3f(0.75, 0.75, 0.75);
    glVertex3f(1, 1, -1);
    
    glColor3f(0.15, 0.15, 0.15);
    glVertex3f(1, 1, 1);

    glColor3f(0.75, 0.75, 0.75);
    glVertex3f(-1, 1, -1);

    glColor3f(0.15, 0.15, 0.15);
    glVertex3f(-1, 1, 1);

    glColor3f(0.75, 0.75, 0.75);
    glVertex3f(-1, -1, -1);

    glColor3f(0.15, 0.15, 0.15);
    glVertex3f(-1, -1, 1);

    glColor3f(0.75, 0.75, 0.75);
    glVertex3f(1, -1, -1);
    
    glColor3f(0.15, 0.15, 0.15);
    glVertex3f(1, -1, 1);
    glEnd();
  }
  
protected:
  ColorF _boxColor;
};

#endif // BOUNDINGBOXDRAWABLE_H
