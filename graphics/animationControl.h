// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                  ___  ____    __    ____  _   _  ____  ___  ___         //
//                 / __)(  _ \  /__\  (  _ \( )_( )(_  _)/ __)/ __)        //
//                ( (_-. )   / /(__)\  )___/ ) _ (  _)(_( (__ \__\         //
//                 \___/(_)\_)(__)(__)(__)  (_) (_)(____)\___)(___/        //
//                                                          Widgets        //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// AnimationControl.h
// Written by Kristi Potter, 2013
// Widget that controls animation playback
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#ifndef ANIMATIONCONTROL_H
#define ANIMATIONCONTROL_H

#include <iostream>
#include <QWidget>
#include <QToolButton>
#include <QPushButton>
#include <QHBoxLayout>
#include <QSlider>
#include <QTimer>
#include <QGridLayout>
#include <QLabel>


class AnimationControl : public QWidget
{
  Q_OBJECT
  
public:
  explicit AnimationControl(QWidget *parent = 0);
  ~AnimationControl(){}
  
  int getCurrentTimeStep(){ return _timeStep; }
  void setTimeInformation(int minTime, int maxTime, int numFrames,
			  std::string timeUnit = "hour", int timeout = 750);
  void setLoop(bool loop){ _loop = loop; }
  
signals:

  void animateSignal(float);
  void goToEndAnimation();
  void goToFrontAnimation();
			   
public slots:
  
  void animate();
  void startOrStopAnimation(bool clicked);
  void endOfAnimation();
  void startOfAnimation();
  void forwardOneAnimation();
  void reverseOneAnimation();
  void moveAnimation(int value);
  void startAnimation();
  void stopAnimation();
  
private:

  // Create and update the gui 
  void createControls();
  void updateControls();
  
  // GUI items
  QPushButton *backButton;
  QPushButton *playButton;
  QPushButton *frontButton;
  QPushButton *forwardButton;
  QPushButton *reverseButton;
  QSlider * _timestepSlider;
  QGridLayout * layout;
  QHBoxLayout * playBackLayout;
  QLabel * _time;
  
  // Animation timer variables
  QTimer _animationTimer;
  int _timeout;
  bool _loop;

  // Time variables
  float _timeStep;
  int _numFrames;
  int _minTime, _maxTime;
  float _timeIncrement;
  std::string _timeUnit;

};

#endif // ANIMATIONCONTROL_H
