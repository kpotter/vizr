// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//               ___  ____    __    ____  _   _  ____  ___  ___            //
//              / __)(  _ \  /__\  (  _ \( )_( )(_  _)/ __)/ __)           //
//             ( (_-. )   / /(__)\  )___/ ) _ (  _)(_( (__ \__ \           //
//              \___/(_)\_)(__)(__)(__)  (_) (_)(____)\___)(___/           //
//                                             Drawing & Widgets           //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// GLWidget.h
// Written by Kristi Potter, 2015
// Widget that paints a 3D GL widget
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _GL_WIDGET_H
#define _GL_WIDGET_H

#include <QGLWidget>
#include <QTimer>
#include <iostream>
#include "Drawable.h"


// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Base class for a gl widget
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
class GLWidget : public QGLWidget {
Q_OBJECT

public:

  /// Constructor takes in an optional parent and a title
  GLWidget(std::string title = "VIZR", QWidget *parent = 0);

  /// Destructor
  virtual ~GLWidget(){}

  // Set the title of the window
  void setTitle(std::string title){ _title = title; setWindowTitle(tr(_title.c_str())); }

  // Get the title of the window
  std::string getTitle(){ return _title; }
  
  // Return the background color
  QColor getBackgroundColor(){ return _backgroundColor; }

  // Add a drawable to the widget
  void addDrawable(Drawable * drawable) { _drawables.push_back(drawable); }

  // Draw the scene
  virtual void drawScene();

  // Set perpective viewing
  void setPerspective(bool p){ _perspective = p; }
			  
public slots:

  // Animate the scene
  virtual void animate(float);

  // Reset the camera position
  void resetCamera();
    
  // Set the background color and pass it to Qt
  void setBackgroundColor(QColor background);

  // Change the projection
  void switchProjection(){ _perspective = !_perspective; update(); }

  // Create the scene to draw
  virtual void createScene();

  virtual void cameraTopDown(){
    _eye = Vector3d(5.0, 0.0, 0.0);
    _up = Vector3d(0.0, 0.0, 1.0);
    update();
  }
  
  virtual void cameraSideView(){
    _eye = Vector3d(0.0, 5.0, 0.0);
    _up = Vector3d(1.0, 0.0, 0.0);

    /*
    _xRot = 5216;
    _yRot = 8;
    _xTrans = -0.135;
    _yTrans = 1.065;
    */

    // 4297 5232 0
    //trans: 0.015 -0.03 1.275

    /*
    _xRot =  4297;
    _yRot = 5232;//5384;
    _zRot = 0;
    
    _xTrans = 0.015;
    _yTrans = 0.0;
    _zTrans = -2;//1.275;
    */

    // Latest version of figure 2
    _xRot = 4297;
    _yRot = 5440;
    _zRot = 0;
    _xTrans = 0.03;
    _yTrans = 0.12;
    _zTrans = -1.085;


    update();
  }

  void toggleBlackAndWhite();
  void toggleBoundingBox();
  
  void grabImage();

signals:
  void changeBlackAndWhite(bool);
  void changeBoundingBox(bool);
  
  
protected:

  // Window title
  std::string _title;

  // Background color
  QColor _backgroundColor;

  // Black and white
  bool _blackAndWhite;

  // Bounding box on
  bool _boundingBox;
  
  // Viewing variables
  bool _perspective;
  double _fovy, _aspect, _near, _far;
  Vector3d _eye, _center, _up;
  
  // User-controlled transformations
  int _xRot, _yRot, _zRot;
  float _xTrans, _yTrans, _zTrans;

  // The last mouse position
  QPoint _lastPos; 

  // Trackers for key press
  bool _ctl_down;
  bool _shift_down;
  bool _x_down;
  bool _y_down;
  bool _z_down;

  // The light position
  float _ltpos[4];

  // Animation timer
  //QTimer _animationTimer;
  float _timeStep;
  
  // Display objects
  std::vector<Drawable *> _drawables;

  // Functions that can be overwritten
  virtual void initializeGL();
  virtual void initializeViewing();
  virtual void setViewing();
  virtual void initializeLighting();
  virtual void resizeGL(int w, int h);
  virtual void paintGL();
  virtual void paintEvent(QPaintEvent * event);				      
  virtual void mousePressEvent(QMouseEvent *event);
  virtual void mouseMoveEvent(QMouseEvent *event);
  virtual void keyPressEvent(QKeyEvent * event);
  virtual void keyReleaseEvent(QKeyEvent * event); 
};

#endif
