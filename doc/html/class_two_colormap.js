var class_two_colormap =
[
    [ "TwoColormap", "class_two_colormap.html#a8eeee285fc8ae4d2c04ffcf2a4eb6db3", null ],
    [ "TwoColormap", "class_two_colormap.html#a7080c1b833102891a5b0bfcc923d3bbd", null ],
    [ "~TwoColormap", "class_two_colormap.html#a48a68ab0f7cd60368424e954fc47d953", null ],
    [ "getColorDef", "class_two_colormap.html#a8e26a140b73f1f9b009f44323a8939fc", null ],
    [ "getHigh", "class_two_colormap.html#a71584cbcc3c60ebb348e38bafe1b8cf3", null ],
    [ "getLow", "class_two_colormap.html#a16ff35638e640195194d0bd4889495bf", null ],
    [ "getMid", "class_two_colormap.html#a2e2538955ac9a30d3c5fc1bd67aad248", null ],
    [ "_high", "class_two_colormap.html#abe0958baf899280cbd04b7a115e240b7", null ],
    [ "_low", "class_two_colormap.html#a5fd0b13cd9ea1af4dcb40a68c28da76b", null ]
];