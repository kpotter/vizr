var class_mesh =
[
    [ "Mesh", "class_mesh.html#af131f88f3a965db5e628c2fa34437aae", null ],
    [ "calculateNormals", "class_mesh.html#a701a467a8d313be322e2fdee653a0516", null ],
    [ "findBoundingBox", "class_mesh.html#a1decd0d1caf6d83b3cf6c716e0fd8be2", null ],
    [ "getColor", "class_mesh.html#a7206438334106368c422a1091a55bd18", null ],
    [ "getIdx", "class_mesh.html#ab07626dca971622a2b4224c1f0dc154e", null ],
    [ "getNorm", "class_mesh.html#ab0390c04fdd507211965be50735afa82", null ],
    [ "getNumIdx", "class_mesh.html#a105f4b4a56ebf4e1a179c75f6206b76e", null ],
    [ "getNumPts", "class_mesh.html#ae5ee2cb4dd7ba7f2555898734a697688", null ],
    [ "getPt", "class_mesh.html#a1d121ae9684d6dcd99a2e703f6f37264", null ],
    [ "rescaleMesh", "class_mesh.html#aef632cd3b8efa183c25bfebd475a6027", null ],
    [ "setColor", "class_mesh.html#a1be402d126c5cbaf46afbdbe2751ed9c", null ],
    [ "setColors", "class_mesh.html#a46d7f32f85e5bcafd0aef6a541e9d0f7", null ],
    [ "_bBox", "class_mesh.html#ac25011bf285ddf9a90948992378c49c0", null ],
    [ "_colors", "class_mesh.html#a8de564f57f7656b9f585e10b6b1f1e35", null ],
    [ "_indices", "class_mesh.html#a4828a6b071404cd085225f3d8f248f15", null ],
    [ "_normals", "class_mesh.html#a7f612e20af50c3bbc17a5b93bff8ecb1", null ],
    [ "_vertices", "class_mesh.html#ac8bc82604ad2b353e0f3f22edb166960", null ]
];