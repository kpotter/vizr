var class_simulation =
[
    [ "Simulation", "class_simulation.html#a5b224cc5b36bcc8eb29689aff223de41", null ],
    [ "~Simulation", "class_simulation.html#a701bd94edc5cf562b5c4e66ae43400bb", null ],
    [ "addToAgentState", "class_simulation.html#af67ebf019b975e9162916134166ee6bf", null ],
    [ "addToSoluteState", "class_simulation.html#ac62e87b044d415dd8d8ed67cf7d82f31", null ],
    [ "addToSoluteSum", "class_simulation.html#a9d615bf40263f5cd5e20a461011a29eb", null ],
    [ "getAgentStateList", "class_simulation.html#a386db44d8fdb77a50d630c0dd154dab2", null ],
    [ "printSimulation", "class_simulation.html#a156ed1e8508b7d1b28bacdaf4098bf35", null ],
    [ "setGrid", "class_simulation.html#a67f7bcb6a3d12572278db497b5996c86", null ],
    [ "setIteration", "class_simulation.html#a41cb80c4c69ae4665e04e4688963be18", null ],
    [ "setThickness", "class_simulation.html#a2a7d95c086075f38a90d8c8f64769fff", null ]
];