var class_quaternion =
[
    [ "Quaternion", "class_quaternion.html#a8c62ba9feeccf2b5e7a7939baeb191fc", null ],
    [ "Quaternion", "class_quaternion.html#a557e906d7b997899f52107683c5479bc", null ],
    [ "Quaternion", "class_quaternion.html#a080eab231d01d3470533005cbeb1d52c", null ],
    [ "Quaternion", "class_quaternion.html#ad5a27203102664359161a8a60b9fedec", null ],
    [ "Quaternion", "class_quaternion.html#a84426d1cd3f7dffeeccd47c6d35bdfd2", null ],
    [ "Quaternion", "class_quaternion.html#a8039f6181238c481ad602491063940e8", null ],
    [ "Quaternion", "class_quaternion.html#aa9426450365744c2bf1b5dd67ee2a6ed", null ],
    [ "~Quaternion", "class_quaternion.html#a388100cf7e0fc78d7a4db2a29f3be17a", null ],
    [ "conj", "class_quaternion.html#aa40d801e5e8763a513a927ff4a8a893b", null ],
    [ "getAngleAxis", "class_quaternion.html#a0e7c563003e1f68a8758b46faa16f805", null ],
    [ "inv", "class_quaternion.html#a72ddb0761ebaa63a4d39a659a8cf140b", null ],
    [ "mult", "class_quaternion.html#a9a9961ea89733e270143431ac775e6e9", null ],
    [ "rot", "class_quaternion.html#a3cfe36cf50193c464d40dec3e412539c", null ],
    [ "rot", "class_quaternion.html#a41d28cfebe6b878f3d3f88ec9118dc24", null ],
    [ "set", "class_quaternion.html#a60c483253c3e97537285f38e3c97292a", null ],
    [ "set", "class_quaternion.html#a8c0b5018536a0daa8f6c0509eb44c160", null ]
];