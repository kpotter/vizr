var dir_ae03ab6f89d78897aa79711e24feeba2 =
[
    [ "aminoTests", "dir_38fa650e26c3b6f5c3d563afad4356c1.html", "dir_38fa650e26c3b6f5c3d563afad4356c1" ],
    [ "beacon", "dir_0822da72a4c534593c767a6475c00f7c.html", "dir_0822da72a4c534593c767a6475c00f7c" ],
    [ "Amino.h", "_amino_8h.html", null ],
    [ "BoundingBox.h", "_bounding_box_8h.html", [
      [ "BoundingBox", "class_bounding_box.html", "class_bounding_box" ]
    ] ],
    [ "Color.h", "_color_8h.html", "_color_8h" ],
    [ "Color_Q.h", "_color___q_8h.html", "_color___q_8h" ],
    [ "ColorDefines.h", "_color_defines_8h.html", "_color_defines_8h" ],
    [ "ColorFunctions.h", "_color_functions_8h.html", "_color_functions_8h" ],
    [ "colormap.h", "colormap_8h.html", "colormap_8h" ],
    [ "ColorMapFactories.h", "_color_map_factories_8h.html", [
      [ "GreyscaleColormapFactory", "class_greyscale_colormap_factory.html", "class_greyscale_colormap_factory" ],
      [ "YellowBlueColormapFactory", "class_yellow_blue_colormap_factory.html", "class_yellow_blue_colormap_factory" ],
      [ "BlueYellowColormapFactory", "class_blue_yellow_colormap_factory.html", "class_blue_yellow_colormap_factory" ],
      [ "BlackYellowColormapFactory", "class_black_yellow_colormap_factory.html", "class_black_yellow_colormap_factory" ],
      [ "PurpleYellowColormapFactory", "class_purple_yellow_colormap_factory.html", "class_purple_yellow_colormap_factory" ],
      [ "MagentaLimeColormapFactory", "class_magenta_lime_colormap_factory.html", "class_magenta_lime_colormap_factory" ],
      [ "TealTangerineColormapFactory", "class_teal_tangerine_colormap_factory.html", "class_teal_tangerine_colormap_factory" ],
      [ "RedGreenColormapFactory", "class_red_green_colormap_factory.html", "class_red_green_colormap_factory" ],
      [ "RainbowColormapFactory", "class_rainbow_colormap_factory.html", "class_rainbow_colormap_factory" ],
      [ "GreenYellowOrangeColormapFactory", "class_green_yellow_orange_colormap_factory.html", "class_green_yellow_orange_colormap_factory" ],
      [ "MinskTwilightColormapFactory", "class_minsk_twilight_colormap_factory.html", "class_minsk_twilight_colormap_factory" ]
    ] ],
    [ "Maths.h", "_maths_8h.html", "_maths_8h" ],
    [ "Matrix.h", "_matrix_8h.html", "_matrix_8h" ],
    [ "MatrixMaths.h", "_matrix_maths_8h.html", "_matrix_maths_8h" ],
    [ "Mesh.cpp", "_mesh_8cpp.html", null ],
    [ "Mesh.h", "_mesh_8h.html", [
      [ "Mesh", "class_mesh.html", "class_mesh" ]
    ] ],
    [ "Plane.h", "_plane_8h.html", "_plane_8h" ],
    [ "Quaternion.h", "_quaternion_8h.html", "_quaternion_8h" ],
    [ "SmartPointer.h", "_smart_pointer_8h.html", "_smart_pointer_8h" ],
    [ "Vector.h", "_vector_8h.html", "_vector_8h" ]
];