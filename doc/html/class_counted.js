var class_counted =
[
    [ "~Counted", "class_counted.html#ab860ab037b9d280fd67bf302b0a642a8", null ],
    [ "Counted", "class_counted.html#a97a8e60834458460f72f017627e707b5", null ],
    [ "Counted", "class_counted.html#a2d9683c30b70763171607302c657dded", null ],
    [ "_decCount", "class_counted.html#ae1cdddac35129c785f935a575b2e924f", null ],
    [ "_getCount", "class_counted.html#a0d2029612e192531f4061f22aec75442", null ],
    [ "_incCount", "class_counted.html#a860dd85e19c6391585042ae9e7354389", null ],
    [ "operator=", "class_counted.html#a0aa4dd309362feaa17c9edec707ca954", null ],
    [ "SmartPointer", "class_counted.html#a8ba7fcba280c8327ab50de1f45a1aa3a", null ],
    [ "SmartPointerRef", "class_counted.html#a546039590c50da310d65950b18619559", null ]
];