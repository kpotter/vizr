var hierarchy =
[
    [ "_CallIF< A1, A2, A3, A4, A5, A6, A7, A8 >", "class___call_i_f.html", [
      [ "_Call< CE, F, A1, A2, A3, A4, A5, A6, A7, A8 >", "class___call.html", [
        [ "Call< CE, F, A1, A2, A3, A4, A5, A6, A7, A8 >", "class_call.html", null ]
      ] ],
      [ "_Call< CE, F, A1, A2, A3, A4, A5, A6, NA, NA >", "class___call.html", [
        [ "Call< CE, F, A1, A2, A3, A4, A5, A6, NA, NA >", "class_call_3_01_c_e_00_01_f_00_01_a1_00_01_a2_00_01_a3_00_01_a4_00_01_a5_00_01_a6_00_01_n_a_00_01_n_a_01_4.html", null ]
      ] ],
      [ "_Call< CE, F, A1, A2, A3, A4, A5, NA, NA, NA >", "class___call.html", [
        [ "Call< CE, F, A1, A2, A3, A4, A5, NA, NA, NA >", "class_call_3_01_c_e_00_01_f_00_01_a1_00_01_a2_00_01_a3_00_01_a4_00_01_a5_00_01_n_a_00_01_n_a_00_01_n_a_01_4.html", null ]
      ] ],
      [ "_Call< CE, F, A1, A2, A3, A4, NA, NA, NA, NA >", "class___call.html", [
        [ "Call< CE, F, A1, A2, A3, A4, NA, NA, NA, NA >", "class_call_3_01_c_e_00_01_f_00_01_a1_00_01_a2_00_01_a3_00_01_a4_00_01_n_a_00_01_n_a_00_01_n_a_00_01_n_a_01_4.html", null ]
      ] ],
      [ "_Call< CE, F, A1, A2, A3, NA, NA, NA, NA, NA >", "class___call.html", [
        [ "Call< CE, F, A1, A2, A3, NA, NA, NA, NA, NA >", "class_call_3_01_c_e_00_01_f_00_01_a1_00_01_a2_00_01_a3_00_01_n_a_00_01_n_a_00_01_n_a_00_01_n_a_00_01_n_a_01_4.html", null ]
      ] ],
      [ "_Call< CE, F, A1, A2, NA, NA, NA, NA, NA, NA >", "class___call.html", [
        [ "Call< CE, F, A1, A2, NA, NA, NA, NA, NA, NA >", "class_call_3_01_c_e_00_01_f_00_01_a1_00_01_a2_00_01_n_a_00_01_n_a_00_01_n_a_00_01_n_a_00_01_n_a_00_01_n_a_01_4.html", null ]
      ] ],
      [ "_Call< CE, F, A1, NA, NA, NA, NA, NA, NA, NA >", "class___call.html", [
        [ "Call< CE, F, A1, NA, NA, NA, NA, NA, NA, NA >", "class_call_3_01_c_e_00_01_f_00_01_a1_00_01_n_a_00_01_n_a_00_01_n_a_00_01_n_a_00_01_n_a_00_01_n_a_00_01_n_a_01_4.html", null ]
      ] ],
      [ "_Call< CE, F, NA, NA, NA, NA, NA, NA, NA, NA >", "class___call.html", [
        [ "Call< CE, F, NA, NA, NA, NA, NA, NA, NA, NA >", "class_call_3_01_c_e_00_01_f_00_01_n_a_00_01_n_a_00_01_n_a_00_01_n_a_00_01_n_a_00_01_n_a_00_01_n_a_00_01_n_a_01_4.html", null ]
      ] ]
    ] ],
    [ "_CallIF< A1, A2, A3, A4, A5, A6, A7, NA >", "class___call_i_f.html", [
      [ "_Call< CE, F, A1, A2, A3, A4, A5, A6, A7, NA >", "class___call.html", [
        [ "Call< CE, F, A1, A2, A3, A4, A5, A6, A7, NA >", "class_call_3_01_c_e_00_01_f_00_01_a1_00_01_a2_00_01_a3_00_01_a4_00_01_a5_00_01_a6_00_01_a7_00_01_n_a_01_4.html", null ]
      ] ]
    ] ],
    [ "_CallIF< A1, A2, A3, A4, A5, A6, NA, NA >", "class___call_i_f.html", null ],
    [ "_CallIF< A1, A2, A3, A4, A5, NA, NA, NA >", "class___call_i_f.html", null ],
    [ "_CallIF< A1, A2, A3, A4, NA, NA, NA, NA >", "class___call_i_f.html", null ],
    [ "_CallIF< A1, A2, A3, NA, NA, NA, NA, NA >", "class___call_i_f.html", null ],
    [ "_CallIF< A1, A2, NA, NA, NA, NA, NA, NA >", "class___call_i_f.html", null ],
    [ "_CallIF< A1, NA, NA, NA, NA, NA, NA, NA >", "class___call_i_f.html", null ],
    [ "_CallIF< NA, NA, NA, NA, NA, NA, NA, NA >", "class___call_i_f.html", null ],
    [ "_NA", "class___n_a.html", null ],
    [ "Agent", "class_agent.html", null ],
    [ "BaseDraw", "class_base_draw.html", null ],
    [ "BaseWindow", "class_base_window.html", null ],
    [ "BoundingBox", "class_bounding_box.html", null ],
    [ "Bubble", "class_bubble.html", null ],
    [ "Colormap", "class_colormap.html", [
      [ "TwoColormap", "class_two_colormap.html", [
        [ "ThreeColormap", "class_three_colormap.html", null ]
      ] ]
    ] ],
    [ "ColormapFactory", "class_colormap_factory.html", [
      [ "ThreeColormapFactory", "class_three_colormap_factory.html", [
        [ "GreenYellowOrangeColormapFactory", "class_green_yellow_orange_colormap_factory.html", null ],
        [ "MinskTwilightColormapFactory", "class_minsk_twilight_colormap_factory.html", null ],
        [ "RainbowColormapFactory", "class_rainbow_colormap_factory.html", null ]
      ] ],
      [ "TwoColormapFactory", "class_two_colormap_factory.html", [
        [ "BlackYellowColormapFactory", "class_black_yellow_colormap_factory.html", null ],
        [ "BlueYellowColormapFactory", "class_blue_yellow_colormap_factory.html", null ],
        [ "GreyscaleColormapFactory", "class_greyscale_colormap_factory.html", null ],
        [ "MagentaLimeColormapFactory", "class_magenta_lime_colormap_factory.html", null ],
        [ "PurpleYellowColormapFactory", "class_purple_yellow_colormap_factory.html", null ],
        [ "RedGreenColormapFactory", "class_red_green_colormap_factory.html", null ],
        [ "TealTangerineColormapFactory", "class_teal_tangerine_colormap_factory.html", null ],
        [ "YellowBlueColormapFactory", "class_yellow_blue_colormap_factory.html", null ]
      ] ]
    ] ],
    [ "Contour", "class_contour.html", null ],
    [ "Counted", "class_counted.html", [
      [ "MySmarty", "class_my_smarty.html", null ],
      [ "MySmarty", "class_my_smarty.html", null ],
      [ "SmartPointer< T >", "class_smart_pointer.html", null ]
    ] ],
    [ "DataReader", "class_data_reader.html", null ],
    [ "Geometry", "struct_geometry.html", null ],
    [ "iHasSlots", "classi_has_slots.html", null ],
    [ "iSignals", "classi_signals.html", null ],
    [ "Matrix< Type, Dim >", "class_matrix.html", null ],
    [ "Mesh", "class_mesh.html", [
      [ "HeartMesh", "class_heart_mesh.html", null ]
    ] ],
    [ "Patch", "class_patch.html", null ],
    [ "Plane< Type >", "class_plane.html", null ],
    [ "plusminus< Dim >", "classplusminus.html", null ],
    [ "plusminus< 1 >", "classplusminus_3_011_01_4.html", null ],
    [ "QGLWidget", null, [
      [ "GLWidget", "class_g_l_widget.html", null ],
      [ "MainQtGLWindow", "class_main_qt_g_l_window.html", null ],
      [ "PaintWidget", "class_paint_widget.html", [
        [ "GutPaintWidget", "class_gut_paint_widget.html", null ],
        [ "InverseViewerWidget", "class_inverse_viewer_widget.html", null ],
        [ "ProjectionWidget", "class_projection_widget.html", null ]
      ] ],
      [ "PaintWidget", "class_paint_widget.html", null ]
    ] ],
    [ "QMainWindow", null, [
      [ "MainWindow", "class_main_window.html", [
        [ "GutMainWindow", "class_gut_main_window.html", null ]
      ] ],
      [ "MainWindow", "class_main_window.html", null ],
      [ "MainWindow", "class_main_window.html", null ]
    ] ],
    [ "QObject", null, [
      [ "Drawable", "class_drawable.html", [
        [ "AgentDrawable", "class_agent_drawable.html", null ],
        [ "BoundingBoxDrawable", "class_bounding_box_drawable.html", null ],
        [ "ContourDrawable", "class_contour_drawable.html", null ],
        [ "HeartMeshDrawable", "class_heart_mesh_drawable.html", null ],
        [ "ProjectionDrawable", "class_projection_drawable.html", null ]
      ] ],
      [ "Overpainter", "class_overpainter.html", [
        [ "ColorBarOverpainter", "class_color_bar_overpainter.html", null ],
        [ "InstructionsOverpainter", "class_instructions_overpainter.html", null ]
      ] ],
      [ "QtLogo", "class_qt_logo.html", null ]
    ] ],
    [ "QWidget", null, [
      [ "AnimationControl", "class_animation_control.html", null ],
      [ "ControlWidget", "class_control_widget.html", null ],
      [ "PerspectiveDialog", "class_perspective_dialog.html", null ]
    ] ],
    [ "Rectoid", "class_rectoid.html", [
      [ "RectPrism", "class_rect_prism.html", null ],
      [ "RectTorus", "class_rect_torus.html", null ]
    ] ],
    [ "SignalInterface", "class_signal_interface.html", [
      [ "Signal< A1, A2, A3, A4, A5, A6, A7, A8 >", "class_signal.html", null ],
      [ "Signal< int >", "class_signal.html", null ],
      [ "Signal< int, std::string >", "class_signal.html", null ],
      [ "Signal< QKeyEvent * >", "class_signal.html", null ]
    ] ],
    [ "SignalTracker", "class_signal_tracker.html", null ],
    [ "Simulation", "class_simulation.html", null ],
    [ "SmartPointerAttrib< T >", "class_smart_pointer_attrib.html", null ],
    [ "SmartPointerRef< T >", "class_smart_pointer_ref.html", null ],
    [ "SmartPointerRefAttrib< T >", "class_smart_pointer_ref_attrib.html", null ],
    [ "Solute", "class_solute.html", null ],
    [ "StdPtrAttrib< T >", "class_std_ptr_attrib.html", null ],
    [ "Ui_MainWindow", "class_ui___main_window.html", [
      [ "Ui::MainWindow", "class_ui_1_1_main_window.html", null ],
      [ "Ui::MainWindow", "class_ui_1_1_main_window.html", null ]
    ] ],
    [ "Vec< Type, Dim >", "class_vec.html", null ],
    [ "Vec< double, 3 >", "class_vec.html", null ],
    [ "Vec< float, 4 >", "class_vec.html", [
      [ "Color< float >", "class_color.html", null ]
    ] ],
    [ "Vec< Type, 3 >", "class_vec.html", null ],
    [ "Vec< Type, 4 >", "class_vec.html", [
      [ "Color< Type >", "class_color.html", null ],
      [ "Quaternion< Type >", "class_quaternion.html", null ]
    ] ],
    [ "Vector< Type, Dim >", "class_vector.html", null ]
];