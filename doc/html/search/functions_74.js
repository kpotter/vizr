var searchData=
[
  ['t',['T',['../class_matrix.html#a99b819c28eddeedfa55a03c18f3620ec',1,'Matrix']]],
  ['tand',['tanD',['../_maths_8h.html#abfb915802b5eba9528748b8fb6b89c07',1,'Maths.h']]],
  ['tb_5fproject_5fto_5fsphere',['tb_project_to_sphere',['../_matrix_maths_8h.html#a9a4cf1e1c6cfc82874025dda3e2c94e7',1,'MatrixMaths.h']]],
  ['tdir',['tdir',['../_matrix_8h.html#ac7e445bd2a86beb7d1f721bfc83d6a87',1,'Matrix.h']]],
  ['tealtangerinecolormapfactory',['TealTangerineColormapFactory',['../class_teal_tangerine_colormap_factory.html#afcce34dc95f2283330d2a89992eb47be',1,'TealTangerineColormapFactory']]],
  ['threecolormap',['ThreeColormap',['../class_three_colormap.html#a21be9a5167a3be69df71168ed9c0b650',1,'ThreeColormap::ThreeColormap(ColorF low, ColorF mid, ColorF high, float min=0.0, float max=1.0)'],['../class_three_colormap.html#aa9fddee764ea9e212476f1daab6214f1',1,'ThreeColormap::ThreeColormap(const ThreeColormap &amp;tcm)']]],
  ['threecolormapfactory',['ThreeColormapFactory',['../class_three_colormap_factory.html#a07925850a521a16475597b5086e5853c',1,'ThreeColormapFactory']]],
  ['to3d',['to3d',['../class_color.html#a061197d99ad66656bba12fe8da588385',1,'Color']]],
  ['tofloat',['toFloat',['../class_color.html#a01270e4782608ed5b67afb74d7ebcc68',1,'Color']]],
  ['toggleanimate',['toggleAnimate',['../class_main_window.html#a20125e31d2425de3ccba1b5681085a0b',1,'MainWindow']]],
  ['toint',['toInt',['../class_color.html#ad230d93fc3ad588f3cf2a6b960da5017',1,'Color']]],
  ['toqcolor',['toQColor',['../_color___q_8h.html#a27b335ee8199de1c249bcb6dd1e774b3',1,'Color_Q.h']]],
  ['tpoint',['tpoint',['../_matrix_8h.html#ac7401c6007ad09d81073e6b1dc1e68c1',1,'Matrix.h']]],
  ['trackball',['trackball',['../_matrix_maths_8h.html#a60b55d6f8c6d368e4394c197080eef63',1,'MatrixMaths.h']]],
  ['trans',['trans',['../_matrix_8h.html#a9a38c25449a411d35de4e891a18875a8',1,'Matrix.h']]],
  ['translate',['translate',['../class_patch.html#a1444e077999f3cce570c6ec7fdaa4f98',1,'Patch::translate()'],['../class_rectoid.html#ac2dd2fcef5af97e46b575e1587dbbace',1,'Rectoid::translate()']]],
  ['twocolormap',['TwoColormap',['../class_two_colormap.html#a8eeee285fc8ae4d2c04ffcf2a4eb6db3',1,'TwoColormap::TwoColormap(ColorF low=black, ColorF high=white, float min=0.0, float max=1.0)'],['../class_two_colormap.html#a7080c1b833102891a5b0bfcc923d3bbd',1,'TwoColormap::TwoColormap(const TwoColormap &amp;tcm)']]],
  ['twocolormapfactory',['TwoColormapFactory',['../class_two_colormap_factory.html#a79480c4d3fb6b4644527aa63d718a183',1,'TwoColormapFactory']]]
];
