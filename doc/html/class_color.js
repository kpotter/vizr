var class_color =
[
    [ "Color", "class_color.html#a1b7d6c0aff4313dae33eef1aa4cf781f", null ],
    [ "Color", "class_color.html#af60fe167a74f81b698187df3c5c84f2a", null ],
    [ "Color", "class_color.html#afe7d9f7e938644a4fb48e8fb2a1047ec", null ],
    [ "Color", "class_color.html#a35080461048c53e862eef3f9bfc640d1", null ],
    [ "~Color", "class_color.html#ab886b140ef68eddfc128b3be31d374bc", null ],
    [ "a", "class_color.html#a6c2a47a53641c743202adc172d57037d", null ],
    [ "a", "class_color.html#aed5aa9fb0deaa1b8c60528470c5520fc", null ],
    [ "b", "class_color.html#a69474773f8fdb4a96d50556d9f062c5e", null ],
    [ "b", "class_color.html#a73c9f174908c36138222b9890066cb7a", null ],
    [ "detectType", "class_color.html#a386a1e1fa4f5db46a7671b3a3e04c46c", null ],
    [ "g", "class_color.html#ab07facefa6c29b21f26f41c5090cfed1", null ],
    [ "g", "class_color.html#a4c639bccc91fc3478c50544ebd6505e2", null ],
    [ "r", "class_color.html#acc12e0df66592a366cb6406c0b6776d2", null ],
    [ "r", "class_color.html#a93b257bc74795070b23308b0a9d0a755", null ],
    [ "to3d", "class_color.html#a061197d99ad66656bba12fe8da588385", null ],
    [ "toFloat", "class_color.html#a01270e4782608ed5b67afb74d7ebcc68", null ],
    [ "toInt", "class_color.html#ad230d93fc3ad588f3cf2a6b960da5017", null ]
];