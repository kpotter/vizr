## ======================================================================= ##
##               ___  ____    __    ____  _   _  ____  ___  ___            ##
##              / __)(  _ \  /__\  (  _ \( )_( )(_  _)/ __)/ __)           ##
##             ( (_-. )   / /(__)\  )___/ ) _ (  _)(_( (__ \__ \           ##
##              \___/(_)\_)(__)(__)(__)  (_) (_)(____)\___)(___/           ##
#                                                          Test            ##
## ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ##
## Written by Kristi Potter 2010
## Tests for general graphics
## ======================================================================= ##
PROJECT(GRAPHICS_TEST)

## Set where to find the files
INCLUDE_DIRECTORIES( ${GRAPHICS_PATH} ${VIZR_INC_DIRS}  )

## Add the executables
ADD_EXEC( testGraphics "${VIZR_LIBS}" testGraphics.cpp )


#ADD_EXEC( testUI  "${VIZR_LIBS}" testUIWindows.cpp )