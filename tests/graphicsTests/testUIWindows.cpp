// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __ ___   __  ___ _  _ __ __ ___                     //
//                    / _)  ,) (  )(  ,\ )( )  ) _) __)                    //
//                   ( (/\)  \ /__\ ) _/)__( )( (_\__ \                    //
//                    \__/_)\_)_)(_)_) (_)(_)__)__)___/                    //
// testGraphics.cpp                                                        //
// Written by Kristi Potter                                                //
// September 2010                                                          //
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//#include <Graphics.h>
#include <iostream>
//#include <QMainWindow>
#include <QApplication>
//#include <QVBoxLayout>
#include "MainWindow.h"
#include "PaintWidget.h"
#include "BoundingBoxDrawable.h"
/*
#include "PaintWidget.h"
#include "BoundingBoxDrawable.h"
#include "InstructionsOverpainter.h"
#include "animationControl.h"
*/

int main(int argc, char *argv[])
{
  std::cout << "~~~~~ Graphics Test ~~~~~~" << std::endl;
 
  QApplication a(argc, argv);

  //QMainWindow * mainWindow = new QMainWindow;

  MainWindow *mainWindow = new MainWindow();
  PaintWidget *paint = new PaintWidget();
  //mainWindow->setGLWidget(paint);
  BoundingBoxDrawable *bbDraw = new BoundingBoxDrawable();
  //  paint->addDrawable(bbDraw);
  
  //  mainWindow

  
  /*

  
  QWidget * centralWidget = new QWidget;
  QVBoxLayout * layout = new QVBoxLayout;
  centralWidget->setLayout(layout);
  PaintWidget * window = new PaintWidget;

  BoundingBoxDrawable *bbDraw = new BoundingBoxDrawable();
  window->addDrawable(bbDraw);
  InstructionsOverpainter *ip = new InstructionsOverpainter();
  window->addOverpainter(ip);
  layout->addWidget(window);

  AnimationControl *animationControl = new AnimationControl;
  layout->addWidget(animationControl);

  mainWindow->setCentralWidget(centralWidget);
  //


  window->show();
  */
  mainWindow->show();
  
  return a.exec();
}
