// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __ ___   __  ___ _  _ __ __ ___                     //
//                    / _)  ,) (  )(  ,\ )( )  ) _) __)                    //
//                   ( (/\)  \ /__\ ) _/)__( )( (_\__ \                    //
//                    \__/_)\_)_)(_)_) (_)(_)__)__)___/                    //
// testGraphics.cpp                                                        //
// Written by Kristi Potter                                                //
// September 2010                                                          //
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#include <Graphics.h>
#include <iostream>
#include <QMainWindow>
#include "MainWindow.h"
#include <QApplication>
#include <QVBoxLayout>
//#include "PaintWidget.h"
#include "BoundingBoxDrawable.h"
#include "InstructionsOverpainter.h"
#include "animationControl.h"
#include "GLWidget.h"



int main(int argc, char *argv[])
{
  std::cout << "~~~~~ Graphics Test ~~~~~~" << std::endl;
 
  QApplication a(argc, argv);

  MainWindow * mainWindow = new MainWindow;
  
  //QWidget * centralWidget = new QWidget;
  //QVBoxLayout * layout = new QVBoxLayout;
  //centralWidget->setLayout(layout);

  //QGLWidget * paint = new QGLWidget;
  //layout->addWidget(paint);
  //PaintWidget * paint = new PaintWidget;
  //layout->addWidget(paint);

  // PaintWidget * window = new PaintWidget;
  //layout->addWidget(window);
  
  //BoundingBoxDrawable *bbDraw = new BoundingBoxDrawable();
  //paint->addDrawable(bbDraw);
  //InstructionsOverpainter *ip = new InstructionsOverpainter();
  //window->addOverpainter(ip);
  //
  
  //AnimationControl *animationControl = new AnimationControl;
  //layout->addWidget(animationControl);

  //mainWindow->setCentralWidget(centralWidget);
  mainWindow->initialize();
  mainWindow->show();
  mainWindow->raise();
  return a.exec();
}

