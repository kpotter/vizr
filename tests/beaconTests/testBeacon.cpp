// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                          ___ ___  __  __  __ _  _                       //
//                         (  ,)  _)(  )/ _)/  \ \( )                      //
//                          ) ,\) _)/__\ (_( () )  (                       //
//                         (___/___)_)(_)__)\__/_)\_)                      //
//                                                  TEST                   //
// testBeacon.cpp                                                          //
// Written by Kristi Potter                                                //
// September 2010                                                          //
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#include "Beacon.h"
#include <iostream>
#include <string>

// Class with slots
class iHasSlots{
public:
  HAS_SLOTS;
  
  void myVoidSlot(){ std::cout << "Got your void signal!" << std::endl; }
  void myIntSlot(int i) { std::cout << "You signaled me with: " << i << std::endl; }
  void myDoubleSlot(int i, std::string s) { std::cout << "Ok, now you've signaled me with: " << i << " and: " << s << std::endl; } 
};

// Class that signals
class iSignals{
public :
  
  Signal<> myVoidSignal;
  Signal<int> myIntSignal;
  Signal<int, std::string> myDoubleSignal;
};

// Test out and demonstrate the signals and slots
int main(int argc, char * argv[])
{
  std::cout << "~~~~~ Beacon Test ~~~~~~" << std::endl;
  
  iHasSlots *slotted = new iHasSlots();
  iSignals *signaler = new iSignals();

  // Connect the void signal/slot
  connect(signaler->myVoidSignal, slotted, &iHasSlots::myVoidSlot);

  // Connect the int signal/slot
  connect(signaler->myIntSignal, slotted, &iHasSlots::myIntSlot);

  // Connect the double signal/slot
  connect(signaler->myDoubleSignal, slotted, &iHasSlots::myDoubleSlot);

  // Send out signals
  signaler->myVoidSignal();
  signaler->myIntSignal(100);
  signaler->myDoubleSignal(400, "You have amazing signals");
}

