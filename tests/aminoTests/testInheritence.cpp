#include <iostream>
#include <vector>


class BaseDraw{
public:
  BaseDraw(): title("moo"){}
  std::string getTitle(){return title; }

protected:
  std::string title;
};



class BaseWindow{

public:
  BaseWindow(){ 

  }

  virtual ~BaseWindow(){}
  void draw(){
    for(int i = 0; i < drawers.size(); i++)
      std::cout << drawers[i]->getTitle() << std::endl;
    
  }
    
protected:
  
  std::vector<BaseDraw *> drawers;
};


int main(){
  std::cout << "Test inheritence" << std::endl;

  BaseWindow * bw  = new BaseWindow();
  bw->draw();

}
