// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                                                     __          _      _       __      _    _      ___                                                          //
//                                                   /  O \      (   \ /   )     (   )   (   \(   )   (      )                                                         //
//                                                 /   (   ) \     )        (       )(      )       (     ) O (                                                         //
//                                                (__) (__)  (_/\/\_)   (__)   (_) \_)   (___)                                                        //
//                                                                                                                  TESTS                                                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  //
// testMatrix.cpp  
// Written by Kristi Potter, 2010
// Program to test matrix functions of amino
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#include <Matrix.h>
#include <MatrixMaths.h>
#include <iostream>
#include <Quaternion.h>
#include <Vector.h>


int main(int argc, char * argv[])
{
  std::cout << "~~~~~ Matrix Test ~~~~~~" << std::endl;
 
  Matrix<float, 4> mat4;
  std::cout << mat4 << std::endl;

  Matrix<float, 3> mat3;
  std::cout << mat3 << std::endl;
 
  std::cout << "mat4[7]: " << mat4[7] << " mat3(1, 1): " << mat3(1,1) << std::endl;
  float a[] = {1,2,3,4};
  float b[] = {0,1,5,6};

  Matrix<float, 2> mat2a(a);
  Matrix<float, 2> mat2b(b);
  
  std::cout << "Twos: " <<std::endl;
  std::cout << mat2a; 
  std::cout<< mat2b;

  Matrix<float,2> m = mat2a*mat2b;
  std::cout << m;
  
  std::cout << mat2b.T();
  Matrix<float, 4> scl = scale<float, 4>(45, 15, 2);
  std::cout << "scale: " << std::endl << scl;
  Matrix<float, 4> rottn = rot<float>();
  std::cout << "rot: " << std::endl << rottn;
  Matrix<float, 4> transl = trans<float, 4>(10, 11, 12);
  std::cout << "trans: " << std::endl << transl;

  Matrix<float,4> m4(0,1,2,3, 0,1,0,0, 0,0,1,0, 0,0,0,1);
  Vec<float,4> v4(3,2,1,0);
  std::cout << "matrix*vec: " << m4*v4 << std::endl;
  std::cout << "vec*matrix: " << v4*m4 << std::endl;

  std::cout <<"inverse: " << inverse(m4);

  Quatf * quat = new Quatf();

}

