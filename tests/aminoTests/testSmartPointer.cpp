// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                                                     __          _      _       __      _    _      ___                                                          //
//                                                   /  O \      (   \ /   )     (   )   (   \(   )   (      )                                                         //
//                                                 /   (   ) \     )        (       )(      )       (     ) O (                                                         //
//                                                (__) (__)  (_/\/\_)   (__)   (_) \_)   (___)                                                        //
//                                                                                                                  TESTS                                                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  //
// testSmartPointer.cpp
// Written by Kristi Potter, 2010
// Program to test smart pointers
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#include <SmartPointer.h>
#include <iostream>

// For your class to have a smart pointer to it, it needs to inherit from Counted
class MySmarty : public Counted
{
public:
  MySmarty() { std::cout << "Constructor" << std::endl; }
  ~MySmarty() { std::cout << "Destructor (automatically called)" << std::endl; }
  
  void someFunction() { std::cout << "MySmarty::someFunction()" << std::endl; }
};

typedef SmartPointer< MySmarty > MySmartySP;


int main(int argc, char * argv[])
{
  std::cout << "~~~~~ Test SmartPointer ~~~~~~" << std::endl;

  // Construct just like normal
  MySmartySP smart = new MySmarty();

  // Use the smart pointer just like a regular pointer
  smart->someFunction();

  // Get the original pointer
  smart.getPtr()->someFunction();
}
