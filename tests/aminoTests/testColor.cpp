// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                                                     __          _      _       __      _    _      ___                                                          //
//                                                   /  O \      (   \ /   )     (   )   (   \(   )   (      )                                                         //
//                                                 /   (   ) \     )        (       )(      )       (     ) O (                                                         //
//                                                (__) (__)  (_/\/\_)   (__)   (_) \_)   (___)                                                        //
//                                                                                                                  TESTS                                                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  //
// testColor.cpp
// Written by Kristi Potter, 2010
// Program to test the color functions of amino
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#include <Color.h>
#include <ColorDefines.h>
#include <vector>
#include <iostream>

int main(int argc, char * argv[])
{
  std::cout << "~~~~~ Test Color ~~~~~~" << std::endl;
  ColorF color(1, 0, 0);
  std::cout << "color: " << color << std::endl;
  std::cout << "color + color: " << color+color << std::endl;
 
  std::cout << "black: "       << black << std::endl;
  std::cout << "blue: "        << blue  << std::endl;
  std::cout << "green: "       << green << std::endl;
  std::cout << "lime: "        << lime << std::endl;
  std::cout << "seagreen: "    << seagreen << std::endl;
  std::cout << "red: "         << red << std::endl;
  std::cout << "white: "       << white << std::endl;
  std::cout << "yellow: "      << yellow << std::endl;
  std::cout << "olivegreen: "  << olivegreen << std::endl;
  std::cout << "cornflower: "  << cornflower << std::endl;
  std::cout << "purple: "      << purple << std::endl;
  std::cout << "purple2: "     << purple2 << std::endl;
  std::cout << "dkpurple: "    << dkpurple << std::endl;
  std::cout << "seafoam: "     << seafoam << std::endl;
  std::cout << "spring: "      << spring << std::endl;
  std::cout << "lemon: "       << lemon << std::endl;
  std::cout << "royalblue: "   << royalblue << std::endl;
  std::cout << "gray: "        << gray << std::endl;
  std::cout << "grey: "        << grey << std::endl;
  std::cout << "ltgray: "      << ltgray << std::endl;
  std::cout << "dkgray: "      << dkgray << std::endl;
  std::cout << "dkgreengray: " << dkgreengray << std::endl;
  std::cout << "dkblue: "      << dkblue << std::endl;
  std::cout << "ltsalmon: "    << ltsalmon << std::endl;
  std::cout << "orange: "      << orange << std::endl;
  std::cout << "maroon: "      << maroon << std::endl;
  std::cout << "cyan: "        << cyan << std::endl;
  std::cout << "dkbluegray: "  << dkbluegray << std::endl;
  std::cout << "honeydew: "    << honeydew << std::endl;
  std::cout << "slateblue: "   << slateblue << std::endl;
  std::cout << "opaque:"       << opaque << std::endl;
  std::cout << "transp:"       << transp << std::endl;

  // HSV colors
  std::cout << "green_hsv: "   << green_hsv << std::endl;  
  std::cout << "blue_hsv: "    << blue_hsv << std::endl;    
  std::cout << "red_hsv: "     << red_hsv << std::endl;      
  std::cout << "purple_hsv: "  << purple_hsv << std::endl;
  
  ColorF floatColor(0.5, 0.75, .96, .02);
  std::cout << "float: " << floatColor << " int: " << floatColor.toInt() << std::endl;
  ColorI intColor(255, 156, 78, 25);
  std::cout << "int: " << intColor << " float: " << intColor.toFloat() << std::endl;

  ColorF * col = new ColorF();
}
