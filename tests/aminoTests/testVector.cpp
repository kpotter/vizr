// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                                                     __          _      _       __      _    _      ___                                                          //
//                                                   /  O \      (   \ /   )     (   )   (   \(   )   (      )                                                         //
//                                                 /   (   ) \     )        (       )(      )       (     ) O (                                                         //
//                                                (__) (__)  (_/\/\_)   (__)   (_) \_)   (___)                                                        //
//                                                                                                                  TESTS                                                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  //
// testVector.cpp
// Written by Kristi Potter, 2010
//  Program to test vector functions of amino
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#include <Vector.h>
#include <iostream>

int main(int argc, char * argv[])
{
  std::cout << "~~~~~ Vector Test ~~~~~~" << std::endl;
  
  Vec<float, 3> v;
  v[0] = 10;  v[1] = 20;   v[2] = 30;
  Vec<float, 3> v2(100, 200, 300);
  Vec<double, 3> v3;
  v3 = convertVectorTypes<float, double, 3>(v2);
  std::cout << "v3: " << v3 << std::endl;

  std::cout << "Size Accessors: .len():" << v.len() << " .dim():" << v.dim() << " .size():" << v.size() << std::endl;

  std::cout << "Vector v: " << v << " v2: " << v2 << std::endl;

  // - + - + - + - + - + - + - Math Operators - + - + - + - + - + - + - + -//
  std::cout << "-v: " << -v << std::endl;

  std::cout << "v+v: " << v+v << std::endl;
  std::cout << "v+32: " << v+32 << std::endl;
  
  std::cout << "v-v: " << v-v << std::endl;
  std::cout << "v-32: " << v-32 << std::endl;

  std::cout << "v*v: " << v*v << std::endl;
  std::cout << "v*32: " << v*32 << std::endl;

  std::cout << "v/v: " << v/v << std::endl;
  std::cout << "v/32: " << v/32 << std::endl;

  std::cout << "v+=v2: " << (v+=v2) << std::endl;
  std::cout << "v-=v2: " << (v-=v2) << std::endl;
  
  std::cout << "v+=36: " << (v+=36) << std::endl;
  std::cout << "v-=36: " << (v-=36) << std::endl;

  std::cout << "v*=v2: " << (v*=v2) << std::endl;
  std::cout << "v/=v2: " << (v/=v2) << std::endl;
  
  std::cout << "v*=36: " << (v*=36) << std::endl;
  std::cout << "v/=36: " << (v/=36) << std::endl;

  // - + - + - + - + - + Comparison Operators + - + - + - + - + - + - //  
  std::cout << "v==v2: " << (v==v2) << std::endl;
  std::cout << "v!=v2: " << (v!=v2) << std::endl;

  std::cout << "v<v2: " << (v<v2) << std::endl;
  std::cout << "v<=v2: " << (v<=v2) << std::endl;

  std::cout << "v>v2: " << (v>v2) << std::endl;
  std::cout << "v>=v2: " << (v>=v2) << std::endl;

  // Operators external to the class
  std::cout << "v: " << v << std::endl;
  std::cout << "37+v: " << (37.f+v) << std::endl;
  std::cout << "37*v: " << (37.f*v) << std::endl;
  
  std::cout << "v2: " << v2 << std::endl;
  std::cout << "v2.norm(): " << v2.norm() << std::endl;
  std::cout << "v2.length(): " << v2.length() << std::endl;
  std::cout << "v2.normalize(): " << v2.normalize() << " " << v2 << std::endl;
  std::cout << "v2.norm(): " << v2.norm() << std::endl;

   //Vec3f inVec;
  //std::cout << "Input the vector" << std::endl;
  //std::cin >> inVec;
  //std::cout << "InVec: " << inVec << std::endl;
  //std::cout << "InVec Addy: " << inVec.v << std::endl;
  

  Vec<float, 3> v3f(10, 20, 30);
  Vec<float, 4> v4f(v3f);

  std::cout << "v4f: " << v4f;

  
  

}
