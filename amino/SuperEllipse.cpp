//<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>//
// <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> //
//                        A SuperEllipsoid Implementation                   //
//                               supeEllipse.cpp                            //
//                                                                          //
//                                                                          //
//                   $$$    $$$    $$$$$$$    $$$$$$$$                      //
//                   $$$   $$$    $$$$$$$$$   $$$$$$$$$                     //
//                   $$$  $$$    $$$     $$$  $$$    $$$                    //
//                   $$$ $$$    $$$           $$$     $$                    // 
//                   $$$$$$     $$$           $$$    $$$                    //
//                   $$$$$$$    $$$           $$$$$$$$$                     //
//                   $$$  $$$   $$$           $$$$$$$$                      //
//                   $$$   $$$   $$$     $$$  $$$                           //
//                   $$$    $$$   $$$$$$$$$   $$$                           //
//                   $$$     $$$   $$$$$$$    $$$                           //
//                                                                          //
//                        Written by Kristi Potter                          //
//                            October 24, 2006                              //
// <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> //
//<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>//

#include "superEllipse.h"
#include "GLIncludes.h"
#include <iostream>
//#include <gutzAttrib.h>

using namespace std;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
inline Vector2d toSpherical(const Vector3d &A)
{
  return  Vector2d(asin(A[2]), atan2(A[1], A[0]));
}

template <class T>
inline int sign(T num)
{
  if(num < 0)
    return -1;
  else return 1;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
SuperEllipse::SuperEllipse(double x, double y, double z, 
			   double e1, double e2, int depth)
  : GenPrimGL(GL_TRIANGLES, 3, 0, 0, 0, true, false, false, false),
    _a1(x), _a2(y), _a3(z), 
    _e1(e1), _e2(e2), 
    _maxSubdivide(depth),
    _position(Vector3f_zero), 
    _calculated(false)
{
  calculateApprox();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
SuperEllipse::SuperEllipse(int slice,int stack)
  : GenPrimGL(GL_TRIANGLES, 3, 0, 0, 0, true, false, false, false),
    _a1(1), _a2(1), _a3(1), 
    _e1(1), _e2(1), 
    _maxSubdivide(2),
    _position(Vector3f_zero),
    _calculated(false)
{
  calculateApprox();
}


//%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-//
/// Calculate Approx creates an icoshedron as the rudimentary
/// approximation to the superQuad.  The triangles of the 
/// icosahedron are then subdivided until MAX_DEPTH is reached,
/// and the final points and normals are found.
//%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-//
void SuperEllipse::calculateApprox()
{
  // Create an icosahedron that approximates the unit sphere
  const double X = .525731112119133606;
  const double Z = .850650808352039932;

  // Create the icosahedron
  std::vector<Vector3d> icosPoints;

  // Add the icosahedron point data
  icosPoints.push_back(Vector3d( -X, 0.0,   Z));
  icosPoints.push_back(Vector3d(  X, 0.0,   Z));
  icosPoints.push_back(Vector3d( -X, 0.0,  -Z));
  icosPoints.push_back(Vector3d(  X, 0.0,  -Z));
  icosPoints.push_back(Vector3d(0.0,   Z,   X));
  icosPoints.push_back(Vector3d(0.0,   Z,  -X));
  icosPoints.push_back(Vector3d(0.0,  -Z,   X));
  icosPoints.push_back(Vector3d(0.0,  -Z,  -X));
  icosPoints.push_back(Vector3d(  Z,   X, 0.0));
  icosPoints.push_back(Vector3d( -Z,   X, 0.0));
  icosPoints.push_back(Vector3d(  Z,  -X, 0.0));
  icosPoints.push_back(Vector3d( -Z,  -X, 0.0));

  std::vector<Vector3i> icosTris;

  // Add the triangles of the icosahedron
  icosTris.push_back(Vector3i( 1, 4, 0));
  icosTris.push_back(Vector3i( 4, 9, 0));
  icosTris.push_back(Vector3i( 4, 5, 9));
  icosTris.push_back(Vector3i( 8, 5, 4));
  icosTris.push_back(Vector3i( 1, 8, 4));
  icosTris.push_back(Vector3i( 1,10, 8));
  icosTris.push_back(Vector3i(10, 3, 8));
  icosTris.push_back(Vector3i( 8, 3, 5));
  icosTris.push_back(Vector3i( 3, 2, 5));
  icosTris.push_back(Vector3i( 3, 7, 2));
  icosTris.push_back(Vector3i( 3,10, 7));
  icosTris.push_back(Vector3i(10, 6, 7));
  icosTris.push_back(Vector3i( 6,11, 7));
  icosTris.push_back(Vector3i( 6, 0,11));
  icosTris.push_back(Vector3i( 6, 1, 0));
  icosTris.push_back(Vector3i(10, 1, 6));
  icosTris.push_back(Vector3i(11, 0, 9));
  icosTris.push_back(Vector3i( 2,11, 9));
  icosTris.push_back(Vector3i( 5, 2, 9));
  icosTris.push_back(Vector3i(11, 2, 7));


  // For each triangle of the icosahedron, find 
  // the theta,phi for each vertex, 
  // then find the superquadric point from this uv
  for(int i = 0; i < static_cast<int>(icosTris.size()); i++)
    {
      // Get the icosahedron pts
      Vector3d A = icosPoints[icosTris[i][0]];
      Vector3d B = icosPoints[icosTris[i][1]];
      Vector3d C = icosPoints[icosTris[i][2]];
       
      subdivide(A, B, C, 0);
    }

  // Create the vertex array
  _vertPoints = ArrayWrap1<Vector3d>(static_cast<int>(_points.size()), 
					      &_points[0]);
  for(int i = 0; i < static_cast<int>(_points.size()); ++i)
    _vertPoints[i] = _points[i];
  _verts.setArray(_vertPoints);

  // Create the normals array
  _vertNorms = ArrayWrap1<Vector3d>(static_cast<int>(_normals.size()), 
					     &_normals[0]);
  for(int i = 0; i < static_cast<int>(_normals.size()); ++i)
	  _vertNorms[i] = _normals[i];
  _norms.setArray(_vertNorms);
  _norms.setValidSize(_vertNorms.size());

  _calculated = true;
}

// Given a a triangle in sphereical coordinates, subdivide until MAX_DEPTH
void SuperEllipse::subdivide(Vector3d A, Vector3d B, Vector3d C, int depth)
{
  Vector3d a,b,c;
  Vector3d an,bn,cn;
  
  if(depth == _maxSubdivide)
    {
      Vector2d sphereA = toSpherical(A);
      Vector2d sphereB = toSpherical(B);
      Vector2d sphereC = toSpherical(C);
      
      a = getPoint(sphereA[0], sphereA[1]);
      an = getNormal(sphereA[0], sphereA[1]);
      
      b = getPoint(sphereB[0], sphereB[1]);
      bn = getNormal(sphereB[0], sphereB[1]);
      
      c = getPoint(sphereC[0], sphereC[1]);
      cn = getNormal(sphereC[0], sphereC[1]);

      _points.push_back(a);
      _points.push_back(b);
      _points.push_back(c);
      
      _normals.push_back(an);
      _normals.push_back(bn);
      _normals.push_back(cn);
      
      return;
    }
  
  Vector3d AB, BC, CA;
  
  AB = (A+B)/2.0;
  BC = (B+C)/2.0;
  CA = (C+A)/2.0;
  
  AB.normalize();
  BC.normalize();
  CA.normalize();
  
  subdivide(A, AB, CA, depth+1);
  subdivide(B, BC, AB, depth+1);
  subdivide(C, CA, BC, depth+1);
  subdivide(AB, BC, CA, depth+1);
}

// Given and phi(n) and a theta (w) return the (x,y,z) point
Vector3d SuperEllipse::getPoint(double n, double w)
{
  // Find the point on the super ellipse
  double cos_n = cos(n);
  double sin_n = sin(n);

  double cos_w = cos(w);
  double sin_w = sin(w);

  double cos_e1_n = sign(cos_n) * pow(fabs(cos_n), _e1);
  double sin_e1_n = sign(sin_n) * pow(fabs(sin_n), _e1);

  double cos_e2_w = sign(cos_w) * pow(fabs(cos_w), _e2);
  double sin_e2_w = sign(sin_w) * pow(fabs(sin_w), _e2);
	     
  double x =  _a1 * cos_e1_n * cos_e2_w;
  double y =  _a2 * cos_e1_n * sin_e2_w;
  double z =  _a3 * sin_e1_n;
  
  Vector3d pt = Vector3d(x,y,z);

  return pt;
}

// Given and phi(n) and a theta (w) return the normal of the (x,y,z) point
Vector3d SuperEllipse::getNormal(double n, double w)
{
  // Find the point on the super ellipse
  double cos_n = cos(n);
  double sin_n = sin(n);
  
  double cos_w = cos(w);
  double sin_w = sin(w);

  double cos_2_e1_n = sign(cos_n) * pow(fabs(cos_n), 2-_e1);
  double cos_2_e2_w = sign(cos_w) * pow(fabs(cos_w), 2-_e2);
  double sin_2_e1_n = sign(sin_n) * pow(fabs(sin_n), 2-_e1);
  double sin_2_e2_w = sign(sin_w) * pow(fabs(sin_w), 2-_e2);
  
  double nx = 0;
  if(_a1 != 0)
    nx = 1/_a1*cos_2_e1_n*cos_2_e2_w;
	  
  double ny = 0;
  if(_a2 != 0)
    ny = 1/_a2*cos_2_e1_n*sin_2_e2_w;
  
  double nz = 0;
  if(_a3 != 0)
    nz = 1/_a3*sin_2_e1_n;
  
  Vector3d normal = Vector3d(nx,ny,nz);
  return normal;
}

// Return the bounding box of the super ellipse
ArrayOwn1< Vector3d > SuperEllipse::getBoundingBox()
{
  double minX = _points[0][0];
  double minY = _points[0][1];
  double minZ = _points[0][2];

  double maxX = _points[0][0];
  double maxY = _points[0][1];
  double maxZ = _points[0][2];

  for(int i = 0; i < static_cast<int>(_points.size()); ++i)
  {
    minX = min2(_points[i][0], minX);
    minY = min2(_points[i][1], minY);
    minZ = min2(_points[i][2], minZ);

    maxX = max2(_points[i][0], maxX);
    maxY = max2(_points[i][1], maxY);
    maxZ = max2(_points[i][2], maxZ);
  }

  ArrayOwn1< Vector3d > boundingBox(2, Vector3d_zero);
  boundingBox[0] = Vector3d(minX, minY, minZ);
  boundingBox[1] = Vector3d(maxX, maxY, maxZ);

  return boundingBox;
}

// Draw the super ellipse
void SuperEllipse::draw()
{
  glPushMatrix();
  {
    glTranslatef(_position[0], _position[1], _position[2]);
    GenSuperPrim::draw();
  }
  glPopMatrix();
}

void SuperEllipse::drawPoint(const Vector3f &pos, float rad)
{
  glPushMatrix();
  {
    glTranslatef(pos.x(), pos.y(), pos.z());
    glScalef( rad, rad, rad );
    draw();
  }
  glPopMatrix();
}
