// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// TexCoordArray.h
// Written by Kristi Potter, April 2009           
// A texture coordinates array implementation
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _TEXCOORD_ARRAY_H
#define _TEXCOORD_ARRAY_H

#include "GLArrayAttribs.h"

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
/// A GL texCoord array
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template< class StorageType, int Tuple, int GLType >
class TexCoordArray : public GLArrayAttribs< StorageType >
{
public:

  /// Make the storage type of the array accessible 
  typedef StorageType TStorageType;

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Constructor initially sets the array to active
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  TexCoordArray(ArrayOwn1< StorageType > array = ArrayOwn1< StorageType >(),
		int stride = 0, int start = 0, unsigned int texCoordNum = 0) 
    : GLArrayAttribs< StorageType >(array, stride, start),
      _texNum(texCoordNum)
  {} 
  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Copy constructor
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  TexCoordArray(const TexCoordArray &va) 
    :  GLArrayAttribs< StorageType > (va),
       _texNum(va._texNum)
  {}

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  // Equals operator
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  TexCoordArray &operator=(const TexCoordArray & va)
  {
    GLArrayAttribs< StorageType >::operator=(va);
    _texNum = va._texNum;
    return *this;
  }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Destructor
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual ~TexCoordArray() {}

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  // Get and set the texture number
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  int getTexCoordNum() const { return _texNum; }
  void setTexCoordNum( unsigned int texCoordNum) { _texNum = texCoordNum; }

 protected:

  /// The texture number
  unsigned int _texNum;

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Enable the texCoord array and set the pointer
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual void enableDef()
  { 
    glClientActiveTextureARB(GL_TEXTURE0_ARB + _texNum);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glTexCoordPointer(Tuple, GLType, this->getStride(), this->getStartPointer());
   glClientActiveTextureARB(GL_TEXTURE0_ARB);
    glErr("enableDef()");
  }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Disable the texCoord array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual void disableDef()
  {
    glClientActiveTextureARB(GL_TEXTURE0_ARB + _texNum);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glClientActiveTextureARB(GL_TEXTURE0_ARB);
    glErr("disbleDef()");
  }



}; 

// typedef normally used texCoord arrays
typedef TexCoordArray< short,       1, GL_SHORT > TexCoordArray1S;
typedef TexCoordArray< Vector2s, 2, GL_SHORT > TexCoordArray2S;
typedef TexCoordArray< Vector3s, 3, GL_SHORT > TexCoordArray3S;
typedef TexCoordArray< Vector4s, 4, GL_SHORT > TexCoordArray4S;

typedef TexCoordArray< int,         1, GL_INT > TexCoordArray1I;
typedef TexCoordArray< Vector2i, 2, GL_INT > TexCoordArray2I;
typedef TexCoordArray< Vector3i, 3, GL_INT > TexCoordArray3I;
typedef TexCoordArray< Vector4i, 4, GL_INT > TexCoordArray4I;

typedef TexCoordArray< float,       1, GL_FLOAT > TexCoordArray1F;
typedef TexCoordArray< Vector2f, 2, GL_FLOAT > TexCoordArray2F;
typedef TexCoordArray< Vector3f, 3, GL_FLOAT > TexCoordArray3F;
typedef TexCoordArray< Vector4f, 4, GL_FLOAT > TexCoordArray4F;

typedef TexCoordArray< double,      1, GL_DOUBLE > TexCoordArray1D;
typedef TexCoordArray< Vector2d, 2, GL_DOUBLE > TexCoordArray2D;
typedef TexCoordArray< Vector3d, 3, GL_DOUBLE > TexCoordArray3D;
typedef TexCoordArray< Vector4d, 4, GL_DOUBLE > TexCoordArray4D;

#endif
