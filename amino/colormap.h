// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// Colormap.h 
// Written by Kristi Potter, 2013
// Class to define a colormap
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#ifndef COLORMAP_H
#define COLORMAP_H
#include "Color.h"
#include "ColorDefines.h"
#include "Maths.h"

// - - -
// Linearly interpolate between colors
inline ColorF lerpColor(ColorF a, ColorF b, float val){

    ColorF newColor;
    newColor.x(affine(0.f, val, 1.f, a.x(), b.x()));
    newColor.y(affine(0.f, val, 1.f, a.y(), b.y()));
    newColor.z(affine(0.f, val, 1.f, a.z(), b.z()));
    return newColor;
}

//-----------------------------------------------//
// Colormap (generic)
//-----------------------------------------------//
class Colormap
{
public:

  Colormap(float min = 0.f, float max = 1.f): _min(min), _max(max), _defColor(blue) {}
  Colormap(ColorF color) : _min(0.0), _max(1.0), _defColor(color){}
  Colormap(const Colormap &cm) { _min = cm._min; _max = cm._max; _defColor = cm._defColor; }
  virtual~Colormap(){}
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  // Getters/Setters
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  void   setRange(float min, float max){ _min = min; _max = max; }
  float  getRangeMin()                 { return _min; }
  float  getRangeMax()                 { return _max; }
  void   printRange()                  { std::cout << "[" << _min << ", " << _max << "]" << std::endl; }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  // Get the color
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  ColorF getColor(float value){
    return getColorDef(value);
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  // Define colormap (override this function) 0<=value<=1
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  virtual ColorF getColorDef(float value) {

    std::cout << "getColor: " << value << std::endl;
    std::cout << "min/max: " << _min << " " << _max << std::endl;
    
    // Clamp the value to be in the range of the colormap
    if(value < _min)
      value = _min;
    if(value > _max)
      value = _max;
    
    // Make the range of the data between 0 and 1
    float newVal = affine(_min, value, _max, 0.0, 1.0);
    return _defColor;
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  // Get the gradient colors
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  virtual ColorF getLow()  { return _defColor; }
  virtual ColorF getMid()  { return _defColor; }
  virtual ColorF getHigh() { return _defColor; }


protected:
  float _min, _max;
  ColorF _defColor;
};

//-----------------------------------------------//
// Colormapping factory
//-----------------------------------------------//
class ColormapFactory{
public:
  Colormap * cmap;
  ColormapFactory(){ cmap = new Colormap(); }
  Colormap * getColormap(){  return cmap; }
  virtual void setRange(float min, float max){ cmap->setRange(min, max); }
};

//-----------------------------------------------//
// A two-color colormap
//-----------------------------------------------//
class TwoColormap : public Colormap{

protected:

  ColorF _low, _high; // The colors to map through
  bool _histo;
  int _numBins;
  int _interval;
  std::pair<float, float> minMax;
 
public:

    // - - - - - - - - - - - - - - - - - - - - - - - - - - //
    // Constructor
    // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  TwoColormap(ColorF low = black, ColorF high = white, float min = 0.0, float max = 1.0, bool histo=false)
    : Colormap(min, max), _low(low), _high(high), _histo(false), _numBins(100){}
    TwoColormap(const TwoColormap &tcm){}
    virtual ~TwoColormap(){}
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  // Get the colors
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  ColorF getLow()  { return _low; }
  ColorF getMid()   { return (_low+_high)/2.0; }
  ColorF getHigh() { return _high; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  // Set the histogram options
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  void setColorByHisto(bool val){ _histo = val; }
  void setNumBins(int val){ _numBins = val; }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  // Get the color
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  ColorF getColorDef(float value){

    if(_histo){
      int val = (val-minMax.first)/_interval;
      float newVal = affine(_min, float(val), _max, 0.0, 1.0);
      return lerpColor(_low, _high, newVal);
    }
    else{
      // Clamp the value to be in the range of the colormap
      if(value < _min)
	value = _min;
      if(value > _max)
	value = _max;
    
      // Make the range of the data between 0 and 1
      float newVal = affine(_min, value, _max, 0.0, 1.0);
    
      return lerpColor(_low, _high, newVal);
    }
  }  

  void setRange(float min, float max){
    
    if(_histo){
      minMax.first = min;
      minMax.second = max;
      _interval = ceil((minMax.second-minMax.first)/_numBins);
      Colormap::setRange(0, _numBins);
    }
    else
      setRange(min, max);
  }
  
  
};


//-----------------------------------------------//
// Two colormap factory
//-----------------------------------------------//
class TwoColormapFactory : public ColormapFactory{

public:
  TwoColormapFactory(ColorF low, ColorF high, float min=0.0, float max=1.0){
    cmap = new TwoColormap(low, high, min, max);  }
};

//-----------------------------------------------//
// A three-color colormap
//-----------------------------------------------//
class ThreeColormap : public TwoColormap{

protected:

  ColorF _mid; // The midpoint color to map through
  
public:
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  // Constructor
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  ThreeColormap(ColorF low, ColorF mid, ColorF high, float min = 0.0, float max = 1.0)
    : TwoColormap(low, high, min, max), _mid(mid){}
  ThreeColormap(const ThreeColormap &tcm){}
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  // Get the mid color
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  ColorF getLow()  { return _low; }
  ColorF getMid() { return _mid; }
  ColorF getHigh() { return _high; }
  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  // Get the color
  // - - - - - - - - - - - - - - - - - - - - - - - - - - //
  ColorF getColorDef(float value){
    ColorF col;
    if(value < 0.5)	col = lerpColor(_low, _mid, value*2.0);
    else                      col = lerpColor(_mid, _high, (value-0.5)*2.0);
    return col;
  }
};

//-----------------------------------------------//
// Theww colormap factory
//-----------------------------------------------//
class ThreeColormapFactory : public ColormapFactory{
  
public:
  ThreeColormapFactory(ColorF low, ColorF mid, ColorF high, float min=0.0, float max=1.0){
    cmap = new ThreeColormap(low, mid, high, min, max);  }
};

#endif // COLORMAP_H
