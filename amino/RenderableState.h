// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// RenderableState.h
// Written by Kristi Potter, October 2006           
// Base class for renderable states
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _RENDERABLE_STATE_H
#define _RENDERABLE_STATE_H

#include "beacon.h"
#include "SmartPointer.h"

//----------------------------------------------------------------------
/// Base class for renderable states
///
/// The following functions are pure virtual and MUST be implemented:
///      virtual RenderableState *clone() = 0;
///      virutal void printState() = 0;
///      virtual void bindDef() = 0;
///      virtual void releaseDef() = 0;
//----------------------------------------------------------------------
class RenderableState : public Counted
{
 public:
  HAS_SLOTS;
  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Pure virtual clone MUST be implemented by inherited class
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual RenderableState *clone() = 0;

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Destructor
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual ~RenderableState() {}
 
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  ///@name Bind/Release 
  /// Bind should enable any state that is applicable, release should undo 
  ///  that, back to defaults.  A Concrete Class should implement 
  ///  the bindDef() and releaseDef() for the specific platform (if applicable). 
  ///@{ 
  inline void bind()    { bindDef(); _bound = true; } 
  inline void release() { releaseDef(); _bound =false; } 
  ///@} 
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Return if this state is bound
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  bool isBound() const { return _bound; } 

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Prints out the current state, MUST be implemented by inherited class
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~// 
  virtual void printState() = 0;

protected:

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Pure virutal bind and release def MUST be implemented by inherited class
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual void bindDef() = 0;
  virtual void releaseDef() = 0;

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Constructor sets _bound flag to false (default)
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  RenderableState() 
    : _bound(false) 
    {}

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Copy constructor
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  RenderableState(const RenderableState &rs) 
    : _bound(rs._bound) 
    {}

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Assignment opertator
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  RenderableState &operator=(const RenderableState &rs) 
    {return *this;}

  // Member variable bound flag
  bool _bound; 

};

/// SmartPtr and vector typedefs
typedef SmartPointer<RenderableState> RenderableStateSP;
typedef std::vector<RenderableStateSP>  RenderableStateSPVec;

#endif
