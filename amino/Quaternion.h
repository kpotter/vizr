// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// Quaternion.h
// Written by Kristi Potter, 2010
// Class to define quaternion rotations
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _QUATERNION_H_KP
#define _QUATERNION_H_KP

#include "Matrix.h"
#include "Vector.h"

// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Definition of a variable type quaternion class
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
template<class Type>
class Quaternion : public Vec<Type, 4>
{
public:
  using Vec<Type,4>::v;
  using Vec<Type,4>::x;
  using Vec<Type,4>::y;
  using Vec<Type,4>::z;
  using Vec<Type,4>::w;

  //  - + - + - + - + - + - + Constructors  + - + - + - + - + - + - + - //
  Quaternion();
  Quaternion(const Type x, const Type y, const Type z, const Type w);
  Quaternion(const Vec<Type, 4> &v);
  Quaternion(const Quaternion<Type> &q);
  Quaternion(const Vec<Type, 3> &v);
  Quaternion(const Type phi, const Vec<Type, 3> &k);
  Quaternion(const Matrix<Type, 3> &m);
  virtual ~Quaternion(){}

  //  - + - + - + - + - + - + Functions on Quats  + - + - + - + - + - + - + - //
  void set(const Type phi, const Vec<Type,3>& k);
  void set(const Matrix<Type, 3>& m);
  
  Quaternion mult(const Quaternion &q) const;
  Quaternion conj() const;
  Quaternion inv() const;
  Vec<Type,3> rot(const Vec<Type,3> &v) const;
  Quaternion rot(const Quaternion &q) const;

  Vec<Type,4> getAngleAxis() const;
};

// -<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>- //
// Define some common Quaternion Types  
// -<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>- //
typedef Quaternion<float> Quatf;
typedef Quaternion<double> Quatd;

// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Implementation of Quaternion
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
template<class Type> 
Quaternion<Type>::Quaternion(): Vec<Type, 4>(0,0,0,1){ }
template<class Type>
Quaternion<Type>::Quaternion(const Type x, const Type y, const Type z, const Type w)
  : Vec<Type, 4>(x, y, z, w) { }
template<class Type>
Quaternion<Type>::Quaternion(const Vec<Type, 4> &v)
  : Vec<Type, 4>(v) { }
template<class Type>
Quaternion<Type>::Quaternion(const Quaternion<Type> &q)
  : Vec<Type,4>(q) { }
template<class Type>
Quaternion<Type>::Quaternion(const Vec<Type, 3> &v)
  : Vec<Type, 4>(v.x(), v.y(), v.z(), 0) { }
template<class Type>
Quaternion<Type>::Quaternion(const Type phi, const Vec<Type, 3> &v)
{ set(phi, v); }
template<class Type>
Quaternion<Type>:: Quaternion(const Matrix<Type, 3> &m)
{ set(m); }
template <class Type>
void Quaternion<Type>::set(const Type phi, const Vec<Type,3>& k)
{ Vec<Type,3> axis = k;  axis.normalize();
  Type sphi = static_cast<Type>(sin(phi*0.5));
  x() = axis.x()*sphi; y() = axis.y()*sphi; z() = axis.z()*sphi; w() = static_cast<Type>(cos(phi*0.5));
}
template <class Type>
void Quaternion<Type>::set(const Matrix<Type,3>& m)
{ Type M11 = m.m[0]; Type M22 = m.m[4]; Type M33 = m.m[8]; Type M00 = M11 + M22 + M33;
  switch (maxIndexOf4(M00, M11, M22, M33))
    {
    case 0: w() = sqrt(1 + M00); x() = (m.m[5] - m.m[7])/w(); y() = (m.m[6] - m.m[2])/w(); z() = (m.m[1] - m.m[3])/w(); break;
    case 1: x() = sqrt(1 + 2*M11 - M00); w() = (m.m[5] - m.m[7])/x(); y() = (m.m[1] + m.m[3])/x(); z() = (m.m[2] + m.m[6])/x(); break;
    case 2: y() = sqrt(1 + 2*M22 - M00); w() = (m.m[6] - m.m[2])/y(); x() = (m.m[1] + m.m[3])/y(); z() = (m.m[5] + m.m[7])/y(); break;
    case 3: z() = sqrt(1 + 2*M33 - M00); w() = (m.m[1] - m.m[3])/z(); x() = (m.m[6] + m.m[2])/z(); y() = (m.m[5] + m.m[7])/z(); break;
    }
  x() *= 0.5; y() *= 0.5; z() *= 0.5; w() *= 0.5;
}

template <class Type>
Quaternion<Type> Quaternion<Type>::mult(const Quaternion<Type>& q) const
{

  //std::cout << "q: " << q << std::endl;
  // std::cout << "this->x(): " << this->x() << std::endl;
  //std::cout << "this->y(): " << this->y() << std::endl;
  // //std::cout << "this->z(): " << this->z() << std::endl;
  //std::cout << "this->w(): " << this->w() << std::endl;
  
  Quaternion<Type> tmp= Quaternion<Type>( this->w()*q.x() + q.w()*this->x() + this->y()*q.z() - q.y()*this->z(),
					  this->w()*q.y() + q.w()*this->y() + this->z()*q.x() - q.z()*this->x(),
					  this->w()*q.z() + q.w()*this->z() + this->x()*q.y() - q.x()*this->y(),
					  this->w()*q.w() - q.x()*this->x() - this->y()*q.y() - q.z()*this->z());

  // std::cout << "tmp: " << tmp << std::endl;
  return tmp;
 }


template <class Type>        
Quaternion<Type> Quaternion<Type>::conj() const
{ return Quaternion(-this->x(), -this->y(), -this->z(), this->w()); }
template <class Type>        
Quaternion<Type> Quaternion<Type>::inv() const
{ Type n = 1/(w()*w() + x()*x() + y()*y() + z()*z()); return Quaternion<Type>(-x()*n, -y()*n, -z()*n, w()*n); }
template <class Type>
Vec<Type,3> Quaternion<Type>::rot(const Vec<Type,3> &v) const
{  // calculate tmp = q*v
  Quaternion<Type> tmp(w()*v.x() + y()*v.z() - v.y()*z(),
		       w()*v.y() + z()*v.x() - v.z()*x(),
		       w()*v.z() + x()*v.y() - v.x()*y(),
		       - x()*v.x() - y()*v.y() - z()*v.z());
  // final result is (q*v)*conj(q)
  return Vec<Type,3>(- tmp.w()*x() + w()*tmp.x() - tmp.y()*z() + y()*tmp.z(),
			- tmp.w()*y() + w()*tmp.y() - tmp.z()*x() + z()*tmp.x(),
			- tmp.w()*z() + w()*tmp.z() - tmp.x()*y() + x()*tmp.y());
}
template <class Type>
Quaternion<Type> Quaternion<Type>::rot(const Quaternion<Type> &r) const
{  // calculate tmp = q*r
  Quaternion<Type> tmp(w()*r.x() + r.w()*x() + y()*r.z() - r.y()*z(),
		       w()*r.y() + r.w()*y() + z()*r.x() - r.z()*x(),
		       w()*r.z() + r.w()*z() + x()*r.y() - r.x()*y(),
		       w()*r.w() - x()*r.x() - y()*r.y() - z()*r.z());
  // final result is (q*r)*conj(q)
  return Quaternion<Type>(- tmp.w()*x() + w()*tmp.x() - tmp.y()*z() + y()*tmp.z(),
			  - tmp.w()*y() + w()*tmp.y() - tmp.z()*x() + z()*tmp.x(),
			  - tmp.w()*z() + w()*tmp.z() - tmp.x()*y() + x()*tmp.y(),
			  tmp.w()*w() + tmp.x()*x() + tmp.y()*y() + tmp.z()*z());
}
template <class Type>
Vec<Type,4> Quaternion<Type>::getAngleAxis() const 
{ Vec<Type,4> ret;
  ret[3] = 2 * acos( w() );
  Type s = sqrt(1 - w()*w() );
  if( s < .00001 ) { ret[0] = x(); ret[1] = y(); ret[2] = z(); }
  else { ret[0] = x()/s; ret[1] = y()/s; ret[2] = z()/s; }
  return ret;
}

// -:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:- //
// Quaternion Constants
// -:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:- //
const Quatf Quatf_id = Quatf();
const Quatd Quatd_id = Quatd();

#endif
