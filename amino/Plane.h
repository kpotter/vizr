// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// Plane.h
// Written by Kristi Potter, 2010
// Class to define a plane
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _PLANE_H
#define _PLANE_H

#include "Vector.h"

// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Definition of a variable type plane class.
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
template<class Type>
class Plane
{
public:
  // Default plane pos = (0,0,0), normal = z
  Plane() : p(0,0,0), n(0,0,1) { }
  // Construct a plane from a point and normal
  Plane(const Vec<Type,3> &pos, const Vec<Type,3> &norm) : p(pos), n(norm) { }

  // Construct a plane given 3 points on it.
  // pos = p1, normal = (p2 - p1) x (p3 - p1) \n
  // for a correct x,y,z frame, p1 = ll, p2 = lr, p3 = ul
  Plane(const Vec<Type,3> &p1, const Vec<Type,3> &p2, const Vec<Type,3> &p3) : p(p1), n(cross((p2-p1),(p3-p1))) {}
  // Copy constructor
  Plane(const Plane &pl) : p(pl.p), n(pl.n) {}
  // Assignment operator 
  Plane &operator=(const Plane &pl) {  p = pl.p; n = pl.n; return *this; }
public:
   Vec<Type,3> p;
   Vec<Type,3> n;
};

// -<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>- //
// Define some common Plane Types  
// -<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>- //
typedef Plane<float>  Planef;
typedef Plane<double> Planed;

#endif
