// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// BoundingBox.h
// Written by Kristi Potter, 2013
// Bounding box centered at zero and extends -1 to 1 in all dimensions
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _BOUNDING_BOX_H
#define _BOUNDING_BOX_H

#include "Vector.h"
/*
 * Default bounding box centered at zero and extends -1 to 1 in all dimensions
 */
class BoundingBox{
 public:
  
  // The min and max corners
  Vertex min, max;

  // * * * Constructors * * * //
  BoundingBox (float xMin = -1, float xMax = 1, float yMin = -1, float yMax = 1, float zMin = -1, float zMax = 1)
  { min = Vertex(xMin, yMin, zMin); max = Vertex(xMax, yMax, zMax); }
  BoundingBox (Vertex minC, Vertex maxC)
  { min = minC; max = maxC; }
  BoundingBox (const BoundingBox &bb){ min = bb.min; max = bb.max; }

  // * * * Print the bounding box * * * //
  void print(){ std::cout << "Bounding Box: " << min << " " << max << std::endl; }
};

#endif
