# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/Users/kpotter/Code/vizr/amino/Mesh.cpp" "/Users/kpotter/Code/vizr/amino/CMakeFiles/aminolib.dir/Mesh.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "amino"
  "/System/Library/Frameworks/OpenGL.framework"
  "/System/Library/Frameworks/GLUT.framework/Headers"
  "/Library/Frameworks/QtDesigner.framework/Headers"
  "/Library/Frameworks/QtDeclarative.framework/Headers"
  "/Library/Frameworks/QtScriptTools.framework/Headers"
  "/Library/Frameworks/QtDBus.framework/Headers"
  "/Library/Frameworks/QtXml.framework/Headers"
  "/Library/Frameworks/QtSql.framework/Headers"
  "/Library/Frameworks/QtOpenGL.framework/Headers"
  "/Library/Frameworks/QtMultimedia.framework/Headers"
  "/Library/Frameworks/QtNetwork.framework/Headers"
  "/Library/Frameworks/phonon.framework/Headers"
  "/Library/Frameworks/QtXmlPatterns.framework/Headers"
  "/Library/Frameworks/QtWebKit.framework/Headers"
  "/Library/Frameworks/QtHelp.framework/Headers"
  "/usr/include/QtUiTools"
  "/Library/Frameworks/QtTest.framework/Headers"
  "/Library/Frameworks/QtScript.framework/Headers"
  "/Library/Frameworks/QtSvg.framework/Headers"
  "/Library/Frameworks/Qt3Support.framework/Headers"
  "/Library/Frameworks/QtGui.framework/Headers"
  "/Library/Frameworks/QtCore.framework/Headers"
  "/usr/local/Qt4.8/mkspecs/default"
  "/Library/Frameworks/QtCore.framework"
  "amino/beacon"
  "amino/arrays"
  "graphics"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
