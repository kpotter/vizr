// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// ColorFunctions.h
// Written by Kristi Potter, 2013
// Define color functions like conversions
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _COLOR_FUNCTIONS_H
#define _COLOR_FUNCTIONS_H
#include "Color.h"
#include <math.h>

// -(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)- //
// Operators external to the class
// -(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)- //

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Convert RGB to HSV
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class Type>
inline ColorF<Type> rgb2hsv(ColorF<Type> rgb)
{
  Type r = rgb.r(); Type g = rgb.g(); Type b = rgb.b();
    
  // find the min and max of rgb
  Type maxColor = r;
  maxColor = (g > maxColor ? g : maxColor);
  maxColor = (b > maxColor ? b : maxColor);
    
  Type minColor = r;
  minColor = (g < minColor ? g : minColor);
  minColor = (b < minColor ? b : minColor);
    
  Type delta = maxColor - minColor;
  Type V = maxColor;
  Type S = 0;
  Type H = 0;
    
  if (delta == 0)
    {	H = 0;S = 0; }
  else
    {
      S = delta / maxColor;
      Type dR = 60.f*(maxColor - r)/delta + 180.0;
      Type dG = 60.f*(maxColor - g)/delta + 180.0;
      Type dB = 60.f*(maxColor - b)/delta + 180.0;
      if (r == maxColor)
	H = dB - dG;
      else if (g == maxColor)
	H = 120 + dR - dB;
      else
	H = 240 + dG - dR;
    }
    
  if (H<0)    H+=360;
  if (H>=360) H-=360;
    
  return ColorF<Type>(H, S, V, rgb.a());
}
  
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Convert HSV to RGB
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class Type>
inline ColorF<Type> hsv2rgb(ColorF<Type> hsv)
{
  int i;
  Type f, p, q, t;
  ColorF<Type> rgb; 
  Type h = hsv.r(); Type s = hsv.g(); Type v = hsv.b();
     
  // achromatic (grey)
  if( s == 0 ) 
    { rgb = ColorF<Type>(v, v, v); return rgb; }
    
  h /= 60.f;			// sector 0 to 5
  i = floor( h );
  f = h - i;			// factorial part of h
  p = v * ( 1 - s);
  q = v * ( 1 - s * f );
  t = v * ( 1 - s * ( 1 - f ) );
    
  switch( i ) {
  case 0:
    rgb = ColorF<Type>(v, t, p, hsv.a());
    break;
  case 1:
    rgb = ColorF<Type>(q, v, p, hsv.a());
    break;
  case 2:
    rgb = ColorF<Type>(p, v, t, hsv.a());
    break;
  case 3:
    rgb = ColorF<Type>(p, q, v, hsv.a());
    break;
  case 4:
    rgb = ColorF<Type>(t, p, v, hsv.a());
    break;
  default:		// case 5:
    rgb = ColorF<Type>(v, p, q, hsv.a());
    break;
  }
  return rgb;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/// Convert wavelenth color to rgb
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class Type>
inline ColorF<Type> wavelength2RGB(Type wl, int maxIntensity=100)
{  
  Type r, g, b;
    
  //- Set RGB values (normalized in range 0 to 1) depending on
  //- heuristic wavelength intervals:
  if((wl >= 380.0) && (wl < 440.0))
    {	r = -(wl - 440.) / (440. - 380.);  g = 0.0;  b  = 1.0; }
  else if ((wl >= 440.0) && (wl < 490.0))
    { r = 0.0;  g = (wl - 440.0) / (490.0 - 440.0);  b  = 1.0; }
  else if ((wl >= 490.0) && (wl < 510.0))
    {	r = 0.0;  g = 1.0;  b  = -(wl - 510.0) / (510.0 - 490.0); }
  else if ((wl >= 510.0) && (wl < 580.0))
    {	r  = (wl - 510.0) / (580.0 - 510.0);  g = 1.0; 	b  = 0.0; }
  else if ((wl >= 580.0) && (wl < 645.0))
    { r = 1.0;  g = -(wl - 645.0) / (645.0 - 580.0);  b  = 0.0; }    
  else if ((wl >= 645.0) && (wl <= 780.0))
    { r = 1.0;  g = 0.0; b  = 0.0; }
  else
    {	r = 0.0; g = 0.0; b  = 0.0; }
    
  //- Let the intensity fall off near the vision limits:
  Type fact;
  if ((wl >= 380.0) && (wl < 420.0))
    fact = 0.3 + 0.7*(wl - 380.) / (420. - 380.);  
  else if ((wl >= 420.0) && (wl < 701.0))
    fact = 1.0;
  else if ((wl >= 701.0) && (wl <= 780.0))
    fact = 0.3 + 0.7*(780. - wl) / (780. - 700.);
  else
    fact = 0.0;
    
  //- Adjust and scale RGB values to 0 to MaxIntensity integer range:
  r = adjustAndScale(r, fact, maxIntensity);
  g = adjustAndScale(g, fact, maxIntensity);
  b = adjustAndScale(b, fact, maxIntensity);
    
  //- Return color
  return ColorF<Type>(r, g, b);
}
  
template<class Type>
inline Type adjustAndScale(Type c, Type fact, int maxIntensity=100)
{
  // Gamma adjustment.
  // Arguments:
  //      * c:  Value of R, G, or B, on a scale from 0 to 1, inclusive,
  //        with 0 being lowest intensity and 1 being highest.  Typeing
  //        point value.
  //      * Factor:  Factor obtained to have intensity fall off at limits 
  //        of human vision.  Typeing point value.
  //      * Highest:  Maximum intensity of output, scaled value.  The 
  //        lowest intensity is 0.  Scalar integer.
  //
  //        Returns an adjusted and scaled value of R, G, or B, on a scale 
  //      from 0 to Highest, inclusive, as an integer, with 0 as the lowest 
  //      and Highest as highest intensity.
  //
  //     Since this is a helper function I keep its existence hidden.
  //     See http://www.efg2.com/Lab/ScienceAndEngineering/Spectra.htm and
  //     http://www.physics.sfasu.edu/astro/color/spectra.html for details.
  Type gamma = 0.80;  Type result;
  if (c == 0.0) result = 0.0;
  else
    {	result = int( round(pow(c * fact, gamma) * round(maxIntensity)) );
      if(result < 0)            result = 0.0;
      if(result > maxIntensity) result = maxIntensity;
    }
  return result/(Type)maxIntensity;
}

#endif
