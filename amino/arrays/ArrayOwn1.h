// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    ____  ____    __   _  _  ___                  //
//                    /__\  (  _ \(  _ \  /__\ ( \/ )/ __)                 //
//                   /(__)\  )   / )   / /(__)\ \  / \__ \                 //
//                  (__)(__)(_)\_)(_)\_)(__)(__)(__) (___/                 //
//                                          Data Wrangling                 //
//~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// ArrayBase.h
// Written by Kristi Potter
// Arrays for VIZR
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _ARRAY_OWN_1_H
#define _ARRAY_OWN_1_H

#include "ArrayWrap1.h"

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
/// A 1D Array that OWNS its data
/// - The constructors copy the data in
/// - The destructor deletes the data
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template <class T>
class ArrayOwn1 : public ArrayWrap1<T>
{
public:
  /// Constructors - Deep Copies of data
  ArrayOwn1();
  ArrayOwn1( int s, T v ); 
  ArrayOwn1( int s, const T* v);
  ArrayOwn1( const ArrayWrap1<T>& a );
  
  ArrayOwn1( const ArrayOwn1<T>& a );
  ArrayOwn1<T>& operator=( const ArrayOwn1<T>& a );
  ~ArrayOwn1() {ArrayBase<T>::killData();}
  
  /// Transfer ownership of data ('v') to this object.
  /// - Destroys previous contents of 'this' before doing shallow copy of new data
  /// - 'killWithDelete' is true if 'v' is allocated with 'new', otherwise false ('malloc')
  void transfer( int s, T* v, bool killWithDelete );
     	
private:
  void initVal (T v);
  void initData (const T* v);
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Constructors
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn1<T>::ArrayOwn1 ()
  : ArrayWrap1<T>()
{}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn1<T>::ArrayOwn1 (int s, T v)
  : ArrayWrap1<T>(s,NULL)
{
    ArrayBase<T>::initValOwn( v );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn1<T>::ArrayOwn1 (int s, const T* v)
  : ArrayWrap1<T>(s,NULL)
{
  ArrayBase<T>::initDataOwn( v );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn1<T>::ArrayOwn1 ( const ArrayWrap1<T>& a)
  : ArrayWrap1<T>(a.size(),NULL)
{
  ArrayBase<T>::initDataOwn(a.data());
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Copy Constructor: "this" does NOT exist yet
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn1<T>::ArrayOwn1( const ArrayOwn1<T>& a ) : ArrayWrap1<T>(a) // SHALLOW copy 'a' 
{
  ArrayBase<T>::initDataOwn(a.mData);								// DEEP copy of a
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Assignment operator: "this" DOES exist
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn1<T>& ArrayOwn1<T>::operator=( const ArrayOwn1<T>& a )
{
  if( &a != this ) {
    // Kill data and allocate new memory only if sizes don't match.
    if( (this->mSize != a.size() ) || 
	(ArrayBase<T>::dim(0) != a.dim(0)) ) {
      ArrayBase<T>::killData();
      ArrayWrap1<T>::operator=(a);	// SHALLOW copy of a
      ArrayBase<T>::allocDataOwn();
    }
    ArrayBase<T>::copyDataOwn(a.mData);	// Deep copy of a.mData
  }
  return *this; 
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Transfer ownership of data ('v') to this object.
// - Destroys previous contents of 'this' before doing shallow copy of new data
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
void ArrayOwn1<T>::transfer( int s, T* v, bool killWithDelete )
{
  ArrayBase<T>::killData();
  
  int sizes[1];
  sizes[0] = s;
  set(1, sizes, v);
  
  this->mKillWithDelete = killWithDelete;
}

//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//
// Typedefs
//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//

typedef ArrayOwn1<char> 		Arrayo1c;
typedef ArrayOwn1<unsigned char> 	Arrayo1uc;
typedef ArrayOwn1<char> 		Arrayo1b;
typedef ArrayOwn1<unsigned char> 	Arrayo1ub;
typedef ArrayOwn1<short> 		Arrayo1s;
typedef ArrayOwn1<unsigned short> 	Arrayo1us;
typedef ArrayOwn1<int> 			Arrayo1i;
typedef ArrayOwn1<unsigned int>	 	Arrayo1ui;
typedef ArrayOwn1<long> 		Arrayo1l;
typedef ArrayOwn1<unsigned long> 	Arrayo1ul;
typedef ArrayOwn1<long long> 		Arrayo1ll;
typedef ArrayOwn1<unsigned long long>	Arrayo1ull;
typedef ArrayOwn1<float> 		Arrayo1f;
typedef ArrayOwn1<double> 		Arrayo1d;

typedef ArrayOwn1<Vector2ub> 	Arrayo1v2ub;
typedef ArrayOwn1<Vector2i> 	Arrayo1v2i;
typedef ArrayOwn1<Vector2ui> 	Arrayo1v2ui;
typedef ArrayOwn1<Vector2f> 	Arrayo1v2f;

typedef ArrayOwn1<Vector3ub> 	Arrayo1v3ub;
typedef ArrayOwn1<Vector3i> 	Arrayo1v3i;
typedef ArrayOwn1<Vector3ui> 	Arrayo1v3ui;
typedef ArrayOwn1<Vector3f> 	Arrayo1v3f;

typedef ArrayOwn1<Vector4ub> 	Arrayo1v4ub;
typedef ArrayOwn1<Vector4i> 	Arrayo1v4i;
typedef ArrayOwn1<Vector4ui> 	Arrayo1v4ui;
typedef ArrayOwn1<Vector4f> 	Arrayo1v4f;
/*
typedef ArrayOwn1<mat2ub> 	Arrayo1m2ub;
typedef ArrayOwn1<mat2i> 	Arrayo1m2i;
typedef ArrayOwn1<mat2ui> 	Arrayo1m2ui;
typedef ArrayOwn1<mat2f> 	Arrayo1m2f;

typedef ArrayOwn1<mat3ub> 	Arrayo1m3ub;
typedef ArrayOwn1<mat3i> 	Arrayo1m3i;
typedef ArrayOwn1<mat3ui> 	Arrayo1m3ui;
typedef ArrayOwn1<mat3f> 	Arrayo1m3f;

typedef ArrayOwn1<mat4ub> 	Arrayo1m4ub;
typedef ArrayOwn1<mat4i> 	Arrayo1m4i;
typedef ArrayOwn1<mat4ui> 	Arrayo1m4ui;
typedef ArrayOwn1<mat4f> 	Arrayo1m4f;
*/
#endif
