// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    ____  ____    __   _  _  ___                  //
//                    /__\  (  _ \(  _ \  /__\ ( \/ )/ __)                 //
//                   /(__)\  )   / )   / /(__)\ \  / \__ \                 //
//                  (__)(__)(_)\_)(_)\_)(__)(__)(__) (___/                 //
//                                          Data Wrangling                 //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// ArrayWrap5.h
// Written by Kristi Potter
// Arrays for VIZR
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _ARRAY_WRAP_5_H
#define _ARRAY_WRAP_5_H

#include "ArrayBase.h"

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
///  ArrayWrap5 	- An Array WRAPPER class.
///   		- This is a lightweight class that does NOT own its data.
///   		- Copies of "ArrayWrap5" objects are shallow.
///   		- The constructor does NOT allocate any memory.
///   		- The destructor does NOT free the memory.
///   		- Accessors (In order of increasing overhead)
///		1) The (i,j,k,m) operator is just as fast a built-in C/C++ Array.
///		2) The [] (slice) operator is next fastest 
///		   - 8% overhead for 1 level of slice
///		   - 20% overhead for 4D access via slices.
///		   - Only use slicing when you need it, otherwise use (i,j,k,m)!
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template <class T>
class ArrayWrap5 : public ArrayBase<T>
{
public:
  /// Constructors - Shallow wrap around data. No size checking!
  ArrayWrap5() : ArrayBase<T>() {}
  ArrayWrap5( int d0, int d1, int d2, int d3, int d4, T* v ) : ArrayBase<T>(d0,d1,d2,d3,d4,v) {}
  ArrayWrap5( const ArrayWrap5<T>& a ) : ArrayBase<T>(a) { mSlice = a.mSlice; }
  ArrayWrap5<T>& operator=( const ArrayWrap5<T>& a );

  virtual ~ArrayWrap5();
  
  /// Return a slice of data - Optimized by returning reference to ArrayWrap3<T> member.
  inline ArrayWrap4<T>&		operator[] (int i);
  inline const ArrayWrap4<T>& operator[] (int i) const;
  
  /// Return the element at data(i,j,k,m) - Faster than arr[i][j][k][m]
  inline T&					operator() (int i, int j, int k, int m, int n);
  inline const T&				operator() (int i, int j, int k, int m, int n) const;

protected:

  // The slice wrapper used in the [] operator. 
  // Mutable so that it can be changed by a const operator
  mutable ArrayWrap4<T>  mSlice;	
	
private:
  /// Set 'mSlice' to the ith slice of 'mData'
  inline void setSlice( int i ) const;
  /// Get the address of the data(i,j)'th element
  inline int  address(  int i, int j, int k, int m, int n) const;
  /// Reset just data pointer
  inline void setData( T* d ) { this->mData = d; }
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Assignment Op
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayWrap5<T>& ArrayWrap5<T>::operator=(const ArrayWrap5<T>& a)
{
  if( this != &a ) {
    ArrayBase<T>::operator=(a);// Call base class assign. op
    mSlice = a.mSlice;
  }
  return *this;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Destructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayWrap5<T>::~ArrayWrap5()
{}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Accessing Operators
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline ArrayWrap4<T>& ArrayWrap5<T>::operator[] (int i) 				
{ 
  setSlice(i);
  return mSlice; 
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline const ArrayWrap4<T>& ArrayWrap5<T>::operator[] (int i) const 	
{ 
  setSlice(i);
  return mSlice; 
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline T& ArrayWrap5<T>::operator() (int i, int j, int k, int m, int n)
{
  return this->mData[ address(i,j,k,m,n) ];
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline const T& ArrayWrap5<T>::operator() (int i, int j, int k, int m, int n) const
{
  return this->mData[ address(i,j,k,m,n) ];
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline int ArrayWrap5<T>::address( int i, int j, int k, int m, int n) const
{
  //return i*mAxisDim[3]*mAxisDim[2]*mAxisDim[1] + j*mAxisDim[3]*mAxisDim[2] + k*mAxisDim[3] + m;
  return i*this->mAxisStride[0] + j*this->mAxisStride[1] + k*this->mAxisStride[2] + m*this->mAxisStride[3] + n;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Set 'mSlice' to be the ith slice of 'mData'
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline void ArrayWrap5<T>::setSlice( int i ) const
{
  //int a = i * mAxisDim[1] * mAxisDim[2] * mAxisDim[3];
  int a = i * this->mAxisStride[0];
  assert( a < this->mSize );
  mSlice.setData( this->mData + a );
}

//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//
// Typedefs
//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//

typedef ArrayWrap5<char>        	Arrayw5c;
typedef ArrayWrap5<unsigned char> 	Arrayw5uc;
typedef ArrayWrap5<char>        	Arrayw5b;
typedef ArrayWrap5<unsigned char> 	Arrayw5ub;
typedef ArrayWrap5<short>       	Arrayw5s;
typedef ArrayWrap5<unsigned short> 	Arrayw5us;
typedef ArrayWrap5<int>  	 	Arrayw5i;
typedef ArrayWrap5<unsigned int> 	Arrayw5ui;
typedef ArrayWrap5<long>        	Arrayw5l;
typedef ArrayWrap5<unsigned long> 	Arrayw5ul;
typedef ArrayWrap5<long long>    	Arrayw5ll;
typedef ArrayWrap5<unsigned long long> 	Arrayw5ull;
typedef ArrayWrap5<float>       	Arrayw5f;
typedef ArrayWrap5<double>      	Arrayw5d;

typedef ArrayWrap5<Vector2ub> Arrayw5v2ub;
typedef ArrayWrap5<Vector2i>  Arrayw5v2i;
typedef ArrayWrap5<Vector2ui> Arrayw5v2ui;
typedef ArrayWrap5<Vector2f>  Arrayw5v2f;

typedef ArrayWrap5<Vector3ub> Arrayw5v3ub;
typedef ArrayWrap5<Vector3i>  Arrayw5v3i;
typedef ArrayWrap5<Vector3ui> Arrayw5v3ui;
typedef ArrayWrap5<Vector3f>  Arrayw5v3f;

typedef ArrayWrap5<Vector4ub> Arrayw5v4ub;
typedef ArrayWrap5<Vector4i>  Arrayw5v4i;
typedef ArrayWrap5<Vector4ui> Arrayw5v4ui;
typedef ArrayWrap5<Vector4f>  Arrayw5v4f;
/*
typedef ArrayWrap5<mat2ub> Arrayw5m2ub;
typedef ArrayWrap5<mat2i>  Arrayw5m2i;
typedef ArrayWrap5<mat2ui> Arrayw5m2ui;
typedef ArrayWrap5<mat2f>  Arrayw5m2f;

typedef ArrayWrap5<mat3ub> Arrayw5m3ub;
typedef ArrayWrap5<mat3i>  Arrayw5m3i;
typedef ArrayWrap5<mat3ui> Arrayw5m3ui;
typedef ArrayWrap5<mat3f>  Arrayw5m3f;

typedef ArrayWrap5<mat4ub> Arrayw5m4ub;
typedef ArrayWrap5<mat4i>  Arrayw5m4i;
typedef ArrayWrap5<mat4ui> Arrayw5m4ui;
typedef ArrayWrap5<mat4f>  Arrayw5m4f;
*/
#endif // ArrayWrap5_h
