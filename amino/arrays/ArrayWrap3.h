// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    ____  ____    __   _  _  ___                  //
//                    /__\  (  _ \(  _ \  /__\ ( \/ )/ __)                 //
//                   /(__)\  )   / )   / /(__)\ \  / \__ \                 //
//                  (__)(__)(_)\_)(_)\_)(__)(__)(__) (___/                 //
//                                          Data Wrangling                 //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// ArrayWrap3.h
// Written by Kristi Potter
// Arrays for VIZR
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _ARRAY_WRAP_3_H
#define _ARRAY_WRAP_3_H

#include "ArrayBase.h"

// Forward Decl
template <class T> class ArrayWrap4;

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
//  ArrayWrap3 	- An Array WRAPPER class.
//   		- This is a lightweight class that does NOT own its data.
//   		- Copies of "ArrayWrap3" objects are shallow.
//  		- The constructor does NOT allocate any memory.
//   		- The destructor does NOT free the memory.
//   		- Accessors (In order of increasing overhead)
//   			1) The (i,j,k) operator is just as fast a built-in C/C++ Array.
//   			2) The [] (slice) operator is next fastest 
//   			   - 10% overhead for 1 level of slice
//   			   - 15% overhead for 3D access via slices.
//   			   - Only use slicing when you need it, otherwise use (i,j,k)!
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template <class T>
class ArrayWrap3 : public ArrayBase<T>
{
public:
  friend class ArrayWrap4<T>;
  
  /// Constructors - Shallow wrap around data. No size checking!
  ArrayWrap3() : ArrayBase<T>() {}
  ArrayWrap3( int d0, int d1, int d2, T* v ) : ArrayBase<T>(d0,d1,d2,v) {}
  ArrayWrap3( const ArrayWrap3<T>& a ) : ArrayBase<T>(a) { mSlice = a.mSlice; }
  ArrayWrap3<T>& operator=( const ArrayWrap3<T>& a );
  
  virtual ~ArrayWrap3();
  
  /// Return a slice of data - Optimized by returning reference to ArrayWrap2<T> member.
  inline ArrayWrap2<T>&		operator[] (int i);
  inline const ArrayWrap2<T>& operator[] (int i) const;

  /// Return the element at data(i,j,k) - Faster than arr[i][j][k]
  inline T&		operator() (int i, int j, int k);
  inline const T&	operator() (int i, int j, int k) const;

protected:

  /// The slice wrapper used in the [] operator. 
  /// Mutable so that it can be changed by a const operator
  mutable ArrayWrap2<T>  mSlice;
		
private:
  /// Set 'mSlice' to the ith slice of 'mData'
  inline void setSlice( int i ) const;
  /// Get the address of the data(i,j)'th element
  inline int  address(  int i, int j, int k ) const;	
  /// Reset just data pointer
  inline void setData( T* d ) { this->mData = d; }	
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Assignment Op
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayWrap3<T>& ArrayWrap3<T>::operator=(const ArrayWrap3<T>& a)
{
	if( this != &a ) {
		ArrayBase<T>::operator=(a);// Call base class assign. op
		mSlice = a.mSlice;
	}
	return *this;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Destructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayWrap3<T>::~ArrayWrap3()
{}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Accessing Operators
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline ArrayWrap2<T>& ArrayWrap3<T>::operator[] (int i) 				
{ 
  setSlice(i);
  return mSlice; 
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline const ArrayWrap2<T>& ArrayWrap3<T>::operator[] (int i) const 	
{ 
  setSlice(i);
  return mSlice; 
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Set 'mSlice' to be the ith slice of 'mData'
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline void ArrayWrap3<T>::setSlice( int i ) const
{
  //int a = i * mAxisDim[1] * mAxisDim[2];
  int a = i * this->mAxisStride[0];
  assert( a < this->mSize );
  mSlice.setData( this->mData + a );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// (i,j,k)'th element Accessors
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline T& ArrayWrap3<T>::operator() (int i, int j, int k)
{
  return this->mData[ address(i,j,k) ];
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline const T& ArrayWrap3<T>::operator() (int i, int j, int k) const
{
  return this->mData[ address(i,j,k) ];
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline int ArrayWrap3<T>::address( int i, int j, int k) const
{
  //return i*mAxisDim[2]*mAxisDim[1]+ j*mAxisDim[2] + k;
  return i*this->mAxisStride[0]+ j*this->mAxisStride[1] + k;
}

//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//
// Typedefs
//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//

typedef ArrayWrap3<char>        	Arrayw3c;
typedef ArrayWrap3<unsigned char> 	Arrayw3uc;
typedef ArrayWrap3<char>        	Arrayw3b;
typedef ArrayWrap3<unsigned char> 	Arrayw3ub;
typedef ArrayWrap3<short>       	Arrayw3s;
typedef ArrayWrap3<unsigned short> 	Arrayw3us;
typedef ArrayWrap3<int>	        	Arrayw3i;
typedef ArrayWrap3<unsigned int> 	Arrayw3ui;
typedef ArrayWrap3<long>        	Arrayw3l;
typedef ArrayWrap3<unsigned long> 	Arrayw3ul;
typedef ArrayWrap3<long long>    	Arrayw3ll;
typedef ArrayWrap3<unsigned long long> Arrayw3ull;
typedef ArrayWrap3<float>       	Arrayw3f;
typedef ArrayWrap3<double>      	Arrayw3d;

typedef ArrayWrap3<Vector2ub> Arrayw3v2ub;
typedef ArrayWrap3<Vector2i>  Arrayw3v2i;
typedef ArrayWrap3<Vector2ui> Arrayw3v2ui;
typedef ArrayWrap3<Vector2f>  Arrayw3v2f;

typedef ArrayWrap3<Vector3ub> Arrayw3v3ub;
typedef ArrayWrap3<Vector3i>  Arrayw3v3i;
typedef ArrayWrap3<Vector3ui> Arrayw3v3ui;
typedef ArrayWrap3<Vector3f>  Arrayw3v3f;

typedef ArrayWrap3<Vector4ub> Arrayw3v4ub;
typedef ArrayWrap3<Vector4i>  Arrayw3v4i;
typedef ArrayWrap3<Vector4ui> Arrayw3v4ui;
typedef ArrayWrap3<Vector4f>  Arrayw3v4f;
/*
typedef ArrayWrap3<mat2ub> Arrayw3m2ub;
typedef ArrayWrap3<mat2i>  Arrayw3m2i;
typedef ArrayWrap3<mat2ui> Arrayw3m2ui;
typedef ArrayWrap3<mat2f>  Arrayw3m2f;

typedef ArrayWrap3<mat3ub> Arrayw3m3ub;
typedef ArrayWrap3<mat3i>  Arrayw3m3i;
typedef ArrayWrap3<mat3ui> Arrayw3m3ui;
typedef ArrayWrap3<mat3f>  Arrayw3m3f;

typedef ArrayWrap3<mat4ub> Arrayw3m4ub;
typedef ArrayWrap3<mat4i>  Arrayw3m4i;
typedef ArrayWrap3<mat4ui> Arrayw3m4ui;
typedef ArrayWrap3<mat4f>  Arrayw3m4f;
*/
#endif // ArrayWrap3_h
