// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    ____  ____    __   _  _  ___                  //
//                    /__\  (  _ \(  _ \  /__\ ( \/ )/ __)                 //
//                   /(__)\  )   / )   / /(__)\ \  / \__ \                 //
//                  (__)(__)(_)\_)(_)\_)(__)(__)(__) (___/                 //
//                                          Data Wrangling                 //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// ArrayOwn3.h
// Written by Kristi Potter
// Arrays for VIZR
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _ARRAY_3_OWN_H
#define _ARRAY_3_OWN_H

#include "ArrayWrap3.h"
#include <assert.h>

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
// ArrayOwn3 - A lightweight 3D Array class that OWNS its data.
//		  	 - Very little (nearly none) error checking
//		  	 - Designed to act just like a built-in 2D Array, but handle memory allocation
//		  	 - No dynamic resizing of memory. 
//		  	 - "reshape(d0, d1, d2)" only sets dimensions. It does NOT affect memory allocation.
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template <class T>
class ArrayOwn3 : public ArrayWrap3<T>
{
public:
  /// Constructors - Deep copies of input data
  ArrayOwn3 ();
  ArrayOwn3 (int d0, int d1, int d2, T v ); 
  ArrayOwn3 (int d0, int d1, int d3, const T* v);
  ArrayOwn3 ( const ArrayWrap3<T>& a );
  
  /// Copy Constructor, Assignment Operator, and Destructor
  ArrayOwn3 (const ArrayOwn3<T>& a);
  ArrayOwn3<T>& operator= (const ArrayOwn3<T>& a);
  ~ArrayOwn3() {ArrayBase<T>::killData();}

  /// Transfer ownership of data ('v') to this object.
  /// - Destroys previous contents of 'this' before doing shallow copy of new data
  /// - 'killWithDelete' is true if 'v' is allocated with 'new', otherwise false ('malloc')
  void transfer( int d0, int d1, int d3, T* v, bool killWithDelete );

private:
  void initVal (T v);
  void initData (const T* d);
  void copy (const ArrayOwn3<T>& a);
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Constructors
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn3<T>::ArrayOwn3()
  : ArrayWrap3<T>()
{}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn3<T>::ArrayOwn3 (int d0, int d1, int d2, T val)
  : ArrayWrap3<T>(d0,d1,d2,NULL)
{
  ArrayBase<T>::initValOwn( val );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn3<T>::ArrayOwn3 (int d0, int d1, int d2, const T* data)
  : ArrayWrap3<T>(d0,d1,d2,NULL)
{
  ArrayBase<T>::initDataOwn( data );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn3<T>::ArrayOwn3 ( const ArrayWrap3<T>& a ) : ArrayWrap3<T>(a)
{ 
  ArrayBase<T>::initDataOwn( a.data() );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Copy Constructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn3<T>::ArrayOwn3 (const ArrayOwn3<T>& a) : ArrayWrap3<T>(a)// Shallow copy of a
{
  ArrayBase<T>::initDataOwn(a.mData);			 // DEEP copy of a
}
    
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Assignment Operator
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn3<T>& ArrayOwn3<T>::operator= (const ArrayOwn3<T>& a) 
{ 
  if( &a != this ) {
    // Kill data and allocate new memory only if sizes don't match.
    if( (this->mSize != a.size() ) || 
	(ArrayBase<T>::dim(0) != a.dim(0)) ||
	(ArrayBase<T>::dim(1) != a.dim(1)) ||
	(ArrayBase<T>::dim(2) != a.dim(2)) ) {
      ArrayBase<T>::killData();
      ArrayWrap3<T>::operator=(a);	// SHALLOW copy of a
      ArrayBase<T>::allocDataOwn();
    }
    ArrayBase<T>::copyDataOwn(a.mData);	// Deep copy of a.mData
  }
  return *this; 
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Transfer ownership of data ('v') to this object.
// - Destroys previous contents of 'this' before doing shallow copy of new data
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
void ArrayOwn3<T>::transfer( int d0, int d1, int d2, T* v, bool killWithDelete )
{
  ArrayBase<T>::killData();
  
  int sizes[3];
  sizes[0] = d0;
  sizes[1] = d1;
  sizes[2] = d2;
  
  set(3, sizes, v);
  this->mSlice = ArrayWrap2<T>(d1,d2,v);
  this->mKillWithDelete = killWithDelete;
}

//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//
// Typedefs
//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//

typedef ArrayOwn3<char>         	Arrayo3c;
typedef ArrayOwn3<unsigned char> 	Arrayo3uc;
typedef ArrayOwn3<char>	        	Arrayo3b;
typedef ArrayOwn3<unsigned char> 	Arrayo3ub;
typedef ArrayOwn3<short>        	Arrayo3s;
typedef ArrayOwn3<unsigned short> 	Arrayo3us;
typedef ArrayOwn3<int> 	        	Arrayo3i;
typedef ArrayOwn3<unsigned int> 	Arrayo3ui;
typedef ArrayOwn3<long>         	Arrayo3l;
typedef ArrayOwn3<unsigned long> 	Arrayo3ul;
typedef ArrayOwn3<long long>     	Arrayo3ll;
typedef ArrayOwn3<unsigned long long> 	Arrayo3ull;
typedef ArrayOwn3<float>        	Arrayo3f;
typedef ArrayOwn3<double>       	Arrayo3d;

typedef ArrayOwn3<Vector2ub> 	Arrayo3v2ub;
typedef ArrayOwn3<Vector2i> 	Arrayo3v2i;
typedef ArrayOwn3<Vector2ui> 	Arrayo3v2ui;
typedef ArrayOwn3<Vector2f> 	Arrayo3v2f;

typedef ArrayOwn3<Vector3ub> 	Arrayo3v3ub;
typedef ArrayOwn3<Vector3i> 	Arrayo3v3i;
typedef ArrayOwn3<Vector3ui>	Arrayo3v3ui;
typedef ArrayOwn3<Vector3f> 	Arrayo3v3f;

typedef ArrayOwn3<Vector4ub> 	Arrayo3v4ub;
typedef ArrayOwn3<Vector4i> 	Arrayo3v4i;
typedef ArrayOwn3<Vector4ui>	Arrayo3v4ui;
typedef ArrayOwn3<Vector4f> 	Arrayo3v4f;
/*
typedef ArrayOwn3<mat2ub> 	Arrayo3m2ub;
typedef ArrayOwn3<mat2i> 	Arrayo3m2i;
typedef ArrayOwn3<mat2ui> 	Arrayo3m2ui;
typedef ArrayOwn3<mat2f> 	Arrayo3m2f;

typedef ArrayOwn3<mat3ub> 	Arrayo3m3ub;
typedef ArrayOwn3<mat3i> 	Arrayo3m3i;
typedef ArrayOwn3<mat3ui> 	Arrayo3m3ui;
typedef ArrayOwn3<mat3f> 	Arrayo3m3f;

typedef ArrayOwn3<mat4ub> 	Arrayo3m4ub;
typedef ArrayOwn3<mat4i> 	Arrayo3m4i;
typedef ArrayOwn3<mat4ui> 	Arrayo3m4ui;
typedef ArrayOwn3<mat4f> 	Arrayo3m4f;
*/
#endif // ArrayOwn3_h
