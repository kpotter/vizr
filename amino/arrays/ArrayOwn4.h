// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    ____  ____    __   _  _  ___                  //
//                    /__\  (  _ \(  _ \  /__\ ( \/ )/ __)                 //
//                   /(__)\  )   / )   / /(__)\ \  / \__ \                 //
//                  (__)(__)(_)\_)(_)\_)(__)(__)(__) (___/                 //
//                                          Data Wrangling                 //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// ArrayOwn4.h
// Written by Kristi Potter
// Arrays for VIZR
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _ARRAY_4_OWN_H
#define _ARRAY_4_OWN_H

#include "ArrayWrap4.h"
#include <assert.h>

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
/// ArrayOwn4 - A lightweight 4D Array class that OWNS its data.
///  	 - Very little (nearly none) error checking
///       - Designed to act just like a built-in 2D Array, but handle memory allocation
///	 - No dynamic resizing of memory. 
///	 - "reshape(d0, d1, d2, d3)" only sets dimensions. 
///         It does NOT affect memory allocation.
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template <class T>
class ArrayOwn4 : public ArrayWrap4<T>
{
public:
  /// Constructors - Deep copies of input data
  ArrayOwn4 ();
  ArrayOwn4 (int d0, int d1, int d2, int d3, T v ); 
  ArrayOwn4 (int d0, int d1, int d2, int d3, const T* v);
  ArrayOwn4 ( const ArrayWrap4<T>& a );

  /// Copy Constructor, Assignment Operator, and Destructor
  ArrayOwn4 (const ArrayOwn4<T>& a);
  ArrayOwn4<T>& operator= (const ArrayOwn4<T>& a);
  ~ArrayOwn4() {ArrayBase<T>::killData();}

  /// Transfer ownership of data ('v') to this object.
  /// - Destroys previous contents of 'this' before doing shallow copy of new data
  /// - 'killWithDelete' is true if 'v' is allocated with 'new', otherwise false ('malloc')
  void transfer( int d0, int d1, int d3, int d4, T* v, bool killWithDelete );

private:
  void initVal (T v);
  void initData (const T* d);
  void copy (const ArrayOwn4<T>& a);
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Constructors
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn4<T>::ArrayOwn4()
  : ArrayWrap4<T>()
{}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn4<T>::ArrayOwn4 (int d0, int d1, int d2, int d3, T val)
  : ArrayWrap4<T>(d0, d1, d2, d3, NULL)
{
  ArrayBase<T>::initValOwn( val );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn4<T>::ArrayOwn4 (int d0, int d1, int d2, int d3, const T* data)
  : ArrayWrap4<T>(d0, d1, d2, d3, NULL)
{
  ArrayBase<T>::initDataOwn( data );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn4<T>::ArrayOwn4 ( const ArrayWrap4<T>& a ) : ArrayWrap4<T>(a)
{
  ArrayBase<T>::initDataOwn( a.data() );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Copy Constructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn4<T>::ArrayOwn4 (const ArrayOwn4<T>& a) : ArrayWrap4<T>(a)
												 // Shallow copy of a
{
  ArrayBase<T>::initDataOwn(a.mData);			 // DEEP copy of a
}
    
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Assignment Operator
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn4<T>& ArrayOwn4<T>::operator= (const ArrayOwn4<T>& a) 
{ 
  if( &a != this ) {
    if( (this->mSize != a.size() ) || 
	(ArrayBase<T>::dim(0) != a.dim(0)) ||
	(ArrayBase<T>::dim(1) != a.dim(1)) ||
	(ArrayBase<T>::dim(2) != a.dim(2)) ||
	(ArrayBase<T>::dim(3) != a.dim(3)) ) {
      ArrayBase<T>::killData();	
      ArrayWrap4<T>::operator=(a);		// SHALLOW copy of a
      ArrayBase<T>::allocDataOwn();
    }
    ArrayBase<T>::copyDataOwn(a.mData);	// Deep copy of a.mData
  }
  return *this; 
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Transfer ownership of data ('v') to this object.
// - Destroys previous contents of 'this' before doing shallow copy of new data
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
void ArrayOwn4<T>::transfer( int d0, int d1, int d2, int d3, T* v, bool killDataWithDelete )
{
  ArrayBase<T>::killData();
  
  int sizes[4];
  sizes[0] = d0;
  sizes[1] = d1;
  sizes[2] = d2;
  sizes[3] = d3;
  
  set(4, sizes, v);
  this->mSlice = ArrayWrap3<T>(d1,d2,d3,v);
  this->mKillWithDelete = killDataWithDelete;
}

//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//
// Typedefs
//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//

typedef ArrayOwn4<char>         	Arrayo4c;
typedef ArrayOwn4<unsigned char> 	Arrayo4uc;
typedef ArrayOwn4<char>	        	Arrayo4b;
typedef ArrayOwn4<unsigned char> 	Arrayo4ub;
typedef ArrayOwn4<short>        	Arrayo4s;
typedef ArrayOwn4<unsigned short> 	Arrayo4us;
typedef ArrayOwn4<int>   		Arrayo4i;
typedef ArrayOwn4<unsigned int> 	Arrayo4ui;
typedef ArrayOwn4<long>         	Arrayo4l;
typedef ArrayOwn4<unsigned long> 	Arrayo4ul;
typedef ArrayOwn4<long long>    	Arrayo4ll;
typedef ArrayOwn4<unsigned long long> 	Arrayo4ull;
typedef ArrayOwn4<float>        	Arrayo4f;
typedef ArrayOwn4<double>       	Arrayo4d;

typedef ArrayOwn4<Vector2ub> 	Arrayo4v2ub;
typedef ArrayOwn4<Vector2i> 	Arrayo4v2i;
typedef ArrayOwn4<Vector2ui> 	Arrayo4v2ui;
typedef ArrayOwn4<Vector2f> 	Arrayo4v2f;

typedef ArrayOwn4<Vector3ub> 	Arrayo4v3ub;
typedef ArrayOwn4<Vector3i> 	Arrayo4v3i;
typedef ArrayOwn4<Vector3ui>	Arrayo4v3ui;
typedef ArrayOwn4<Vector3f> 	Arrayo4v3f;

typedef ArrayOwn4<Vector4ub> 	Arrayo4v4ub;
typedef ArrayOwn4<Vector4i> 	Arrayo4v4i;
typedef ArrayOwn4<Vector4ui>	Arrayo4v4ui;
typedef ArrayOwn4<Vector4f> 	Arrayo4v4f;
/*
typedef ArrayOwn4<mat2ub> 	Arrayo4m2ub;
typedef ArrayOwn4<mat2i> 	Arrayo4m2i;
typedef ArrayOwn4<mat2ui> 	Arrayo4m2ui;
typedef ArrayOwn4<mat2f> 	Arrayo4m2f;

typedef ArrayOwn4<mat3ub> 	Arrayo4m3ub;
typedef ArrayOwn4<mat3i> 	Arrayo4m3i;
typedef ArrayOwn4<mat3ui> 	Arrayo4m3ui;
typedef ArrayOwn4<mat3f> 	Arrayo4m3f;

typedef ArrayOwn4<mat4ub> 	Arrayo4m4ub;
typedef ArrayOwn4<mat4i> 	Arrayo4m4i;
typedef ArrayOwn4<mat4ui> 	Arrayo4m4ui;
typedef ArrayOwn4<mat4f> 	Arrayo4m4f;
*/
#endif // ArrayOwn4_h
