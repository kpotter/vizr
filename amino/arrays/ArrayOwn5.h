// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    ____  ____    __   _  _  ___                  //
//                    /__\  (  _ \(  _ \  /__\ ( \/ )/ __)                 //
//                   /(__)\  )   / )   / /(__)\ \  / \__ \                 //
//                  (__)(__)(_)\_)(_)\_)(__)(__)(__) (___/                 //
//                                          Data Wrangling                 //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// ArrayOwn5.h
// Written by Kristi Potter
// Arrays for VIZR
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _ARRAY_5_OWN_H
#define _ARRAY_5_OWN_H

#include "ArrayWrap5.h"

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
/// ArrayOwn5 - A lightweight 5D Array class that OWNS its data.
///  	 - Very little (nearly none) error checking
///  	 - Designed to act just like a built-in 2D Array, but handle memory allocation
/// 	 - No dynamic resizing of memory. 
///  	 - "reshape(d0, d1, d2, d2, d3)" only sets dimensions. It does NOT affect memory 
///  allocation.
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template <class T>
class ArrayOwn5 : public ArrayWrap5<T>
{
public:
 
  /// Constructors - Deep copies of input data
  ArrayOwn5 ();
  ArrayOwn5 (int d0, int d1, int d2, int d3, int d4, T v ); 
  ArrayOwn5 (int d0, int d1, int d2, int d3, int d4, const T* v);
  ArrayOwn5 ( const ArrayWrap5<T>& a );
  
  /// Copy Constructor, Assignment Operator, and Destructor
  ArrayOwn5 (const ArrayOwn5<T>& a);
  ArrayOwn5<T>& operator= (const ArrayOwn5<T>& a);
  ~ArrayOwn5() {ArrayBase<T>::killData();}
  
  /// Transfer ownership of data ('v') to this object.
  /// - Destroys previous contents of 'this' before doing shallow copy of new data
  /// - 'killWithDelete' is true if 'v' is allocated with 'new', otherwise false ('malloc')
  void transfer( int d0, int d1, int d3, int d4, int d5, T* v, bool killWithDelete );

private:
  void initVal (T v);
  void initData (const T* d);
  void copy (const ArrayOwn5<T>& a);
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Constructors
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn5<T>::ArrayOwn5()
  : ArrayWrap5<T>()
{}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn5<T>::ArrayOwn5 (int d0, int d1, int d2, int d3, int d4, T val)
  : ArrayWrap5<T>(d0, d1, d2, d3, d4, NULL)
{
  ArrayBase<T>::initValOwn( val );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn5<T>::ArrayOwn5 (int d0, int d1, int d2, int d3, int d4, const T* data)
  : ArrayWrap5<T>(d0, d1, d2, d3, d4, NULL)
{
  ArrayBase<T>::initDataOwn( data );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn5<T>::ArrayOwn5 ( const ArrayWrap5<T>& a ) : ArrayWrap5<T>(a)
{
  ArrayBase<T>::initDataOwn( a.data() );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Copy Constructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn5<T>::ArrayOwn5 (const ArrayOwn5<T>& a) : ArrayWrap5<T>(a) // Shallow copy of a
{
	ArrayBase<T>::initDataOwn(a.mData);			 // DEEP copy of a
}
    
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Assignment Operator
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn5<T>& ArrayOwn5<T>::operator= (const ArrayOwn5<T>& a) 
{ 
  if( &a != this ) {
    if( (this->mSize != a.size() ) || 
	(ArrayBase<T>::dim(0) != a.dim(0)) ||
	(ArrayBase<T>::dim(1) != a.dim(1)) ||
	(ArrayBase<T>::dim(2) != a.dim(2)) ||
	(ArrayBase<T>::dim(3) != a.dim(3)) ||
	(ArrayBase<T>::dim(4) != a.dim(4)) ) {
      ArrayBase<T>::killData();	
      ArrayWrap5<T>::operator=(a);	// SHALLOW copy of a
      ArrayBase<T>::allocDataOwn();
    }
    ArrayBase<T>::copyDataOwn(a.mData);	// Deep copy of a.mData
  }
  return *this; 
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Transfer ownership of data ('v') to this object.
// - Destroys previous contents of 'this' before doing shallow copy of new data
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
void ArrayOwn5<T>::transfer( int d0, int d1, int d2, int d3, int d4, T* v, bool killWithDelete )
{
  ArrayBase<T>::killData();
  
  int sizes[5];
  sizes[0] = d0;
  sizes[1] = d1;
  sizes[2] = d2;
  sizes[3] = d3;
  sizes[4] = d4;
  
  set(5, sizes, v);
  this->mSlice = ArrayWrap4<T>(d1,d2,d3,d4,v);
  this->mKillWithDelete = killWithDelete;
}

//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//
// Typedefs
//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//

typedef ArrayOwn5<char>         	Arrayo5c;
typedef ArrayOwn5<unsigned char> 	Arrayo5uc;
typedef ArrayOwn5<char>	         	Arrayo5b;
typedef ArrayOwn5<unsigned char> 	Arrayo5ub;
typedef ArrayOwn5<short>        	Arrayo5s;
typedef ArrayOwn5<unsigned short> 	Arrayo5us;
typedef ArrayOwn5<int> 	        	Arrayo5i;
typedef ArrayOwn5<unsigned int> 	Arrayo5ui;
typedef ArrayOwn5<long>         	Arrayo5l;
typedef ArrayOwn5<unsigned long> 	Arrayo5ul;
typedef ArrayOwn5<long long>    	Arrayo5ll;
typedef ArrayOwn5<unsigned long long> 	Arrayo5ull;
typedef ArrayOwn5<float>        	Arrayo5f;
typedef ArrayOwn5<double>         	Arrayo5d;

typedef ArrayOwn5<Vector2ub> 	Arrayo5v2ub;
typedef ArrayOwn5<Vector2i> 	Arrayo5v2i;
typedef ArrayOwn5<Vector2ui> 	Arrayo5v2ui;
typedef ArrayOwn5<Vector2f> 	Arrayo5v2f;

typedef ArrayOwn5<Vector3ub> 	Arrayo5v3ub;
typedef ArrayOwn5<Vector3i> 	Arrayo5v3i;
typedef ArrayOwn5<Vector3ui>	Arrayo5v3ui;
typedef ArrayOwn5<Vector3f> 	Arrayo5v3f;

typedef ArrayOwn5<Vector4ub> 	Arrayo5v4ub;
typedef ArrayOwn5<Vector4i> 	Arrayo5v4i;
typedef ArrayOwn5<Vector4ui>	Arrayo5v4ui;
typedef ArrayOwn5<Vector4f> 	Arrayo5v4f;
/*
typedef ArrayOwn5<mat2ub> 	Arrayo5m2ub;
typedef ArrayOwn5<mat2i> 	Arrayo5m2i;
typedef ArrayOwn5<mat2ui> 	Arrayo5m2ui;
typedef ArrayOwn5<mat2f> 	Arrayo5m2f;

typedef ArrayOwn5<mat3ub> 	Arrayo5m3ub;
typedef ArrayOwn5<mat3i> 	Arrayo5m3i;
typedef ArrayOwn5<mat3ui> 	Arrayo5m3ui;
typedef ArrayOwn5<mat3f> 	Arrayo5m3f;

typedef ArrayOwn5<mat4ub> 	Arrayo5m4ub;
typedef ArrayOwn5<mat4i> 	Arrayo5m4i;
typedef ArrayOwn5<mat4ui> 	Arrayo5m4ui;
typedef ArrayOwn5<mat4f> 	Arrayo5m4f;
*/
#endif // ArrayOwn5_h
