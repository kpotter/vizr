// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    ____  ____    __   _  _  ___                  //
//                    /__\  (  _ \(  _ \  /__\ ( \/ )/ __)                 //
//                   /(__)\  )   / )   / /(__)\ \  / \__ \                 //
//                  (__)(__)(_)\_)(_)\_)(__)(__)(__) (___/                 //
//                                          Data Wrangling                 //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// ArrayWraps.h
// Written by Kristi Potter
// Arrays for VIZR
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _ARRAY_WRAP_2_H
#define _ARRAY_WRAP_2_H

#include "ArrayBase.h"

// Forward Decl
template <class T> class ArrayWrap3;

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
//  ArrayWrap2 	- An Array WRAPPER class.
//  		- This is a lightweight class that does NOT own its data.
//  		- Copies of "ArrayWrap2" objects are shallow.
//   		- The constructor does NOT allocate any memory.
//   		- The destructor does NOT free the memory.
//   		- Accessors (In order of increasing overhead)
//		1) The (i,j) operator is just as fast a built-in C/C++ Array.
//		2) The [] (slice) operator is next fastest 
//		   - 8% overhead for 2D access via slices.
//		   - Only use slicing when you need it, otherwise use (i,j)!
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template <class T>
class ArrayWrap2 : public ArrayBase<T>
{
public:
  friend class ArrayWrap3<T>;
  
  /// Constructors - Shallow wrap around data. No size checking!
  ArrayWrap2() : ArrayBase<T>() {}
  ArrayWrap2( int d0, int d1, T* v=0 ) : ArrayBase<T>(d0,d1,v) {}
  ArrayWrap2( const ArrayWrap2<T>& a ) : ArrayBase<T>(a) {mSlice = a.mSlice;}
  ArrayWrap2<T>& operator=( const ArrayWrap2<T>& a );
  
  virtual ~ArrayWrap2();
  
  /// Return a slice of Array
  inline ArrayWrap1<T>&		operator[] (int i);
  inline const ArrayWrap1<T>& operator[] (int i) const;
  
  /// Return the element at data(i,j) - Faster than arr[i][j]
  inline T&					operator() (int i, int j);
  inline const T&				operator() (int i, int j) const;
  
protected:
  
  /// The slice wrapper used in the [] operator. 
  /// Mutable so that it can be changed by a const operator
  mutable ArrayWrap1<T>  mSlice;	
  
private:
  /// Set 'mSlice' to be the ith slice of 'mData'
  inline void setSlice( int i ) const;
  /// Get the address of the data(i,j)'th element
  inline int  address(  int i, int j ) const; 
  /// Reset just data pointer
  inline void setData( T* d ) { this->mData = d; } 
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Assignment Op
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
  ArrayWrap2<T>& ArrayWrap2<T>::operator=(const ArrayWrap2<T>& a)
  {
    if( this != &a ) {
      ArrayBase<T>::operator=(a);// Call base class assign. op
      mSlice = a.mSlice;
    }
    return *this;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Destructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayWrap2<T>::~ArrayWrap2()
{}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Accessing Operators
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline ArrayWrap1<T>& ArrayWrap2<T>::operator[] (int i) 				
{ 
  setSlice(i);
  return mSlice;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline const ArrayWrap1<T>& ArrayWrap2<T>::operator[] (int i) const 	
{ 
  setSlice(i);
  return mSlice;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Set 'mSlice' to be the ith slice of 'mData'
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline void ArrayWrap2<T>::setSlice( int i ) const
{
  int a = i * this->mAxisStride[0]; 
  assert( a < this->mSize ); 
  mSlice.setData( this->mData + a );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline T& ArrayWrap2<T>::operator() (int i, int j)
{
  return this->mData[ address(i,j) ];
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline const T& ArrayWrap2<T>::operator() (int i, int j) const
{
  return this->mData[ address(i,j) ];
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline int ArrayWrap2<T>::address( int i, int j) const 
{
  return i*this->mAxisStride[0] + j;
}


//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//
// Typedefs
//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//

typedef ArrayWrap2<char>        	Arrayw2c;
typedef ArrayWrap2<unsigned char> 	Arrayw2uc;
typedef ArrayWrap2<char>        	Arrayw2b;
typedef ArrayWrap2<unsigned char> 	Arrayw2ub;
typedef ArrayWrap2<short>       	Arrayw2s;
typedef ArrayWrap2<unsigned short> 	Arrayw2us;
typedef ArrayWrap2<int>	        	Arrayw2i;
typedef ArrayWrap2<unsigned int> 	Arrayw2ui;
typedef ArrayWrap2<long>        	Arrayw2l;
typedef ArrayWrap2<unsigned long> 	Arrayw2ul;
typedef ArrayWrap2<long long>    	Arrayw2ll;
typedef ArrayWrap2<unsigned long long>	Arrayw2ull;
typedef ArrayWrap2<float>       	Arrayw2f;
typedef ArrayWrap2<double> 	        Arrayw2d;

typedef ArrayWrap2<Vector2ub> Arrayw2v2ub;
typedef ArrayWrap2<Vector2i>  Arrayw2v2i;
typedef ArrayWrap2<Vector2ui> Arrayw2v2ui;
typedef ArrayWrap2<Vector2f>  Arrayw2v2f;

typedef ArrayWrap2<Vector3ub> Arrayw2v3ub;
typedef ArrayWrap2<Vector3i>  Arrayw2v3i;
typedef ArrayWrap2<Vector3ui> Arrayw2v3ui;
typedef ArrayWrap2<Vector3f>  Arrayw2v3f;

typedef ArrayWrap2<Vector4ub> Arrayw2v4ub;
typedef ArrayWrap2<Vector4i>  Arrayw2v4i;
typedef ArrayWrap2<Vector4ui> Arrayw2v4ui;
typedef ArrayWrap2<Vector4f>  Arrayw2v4f;
/*
typedef ArrayWrap2<mat2ub> Arrayw2m2ub;
typedef ArrayWrap2<mat2i>  Arrayw2m2i;
typedef ArrayWrap2<mat2ui> Arrayw2m2ui;
typedef ArrayWrap2<mat2f>  Arrayw2m2f;

typedef ArrayWrap2<mat3ub> Arrayw2m3ub;
typedef ArrayWrap2<mat3i>  Arrayw2m3i;
typedef ArrayWrap2<mat3ui> Arrayw2m3ui;
typedef ArrayWrap2<mat3f>  Arrayw2m3f;

typedef ArrayWrap2<mat4ub> Arrayw2m4ub;
typedef ArrayWrap2<mat4i>  Arrayw2m4i;
typedef ArrayWrap2<mat4ui> Arrayw2m4ui;
typedef ArrayWrap2<mat4f>  Arrayw2m4f;
*/
#endif // ArrayWrap2_h
