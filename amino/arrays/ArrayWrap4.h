// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    ____  ____    __   _  _  ___                  //
//                    /__\  (  _ \(  _ \  /__\ ( \/ )/ __)                 //
//                   /(__)\  )   / )   / /(__)\ \  / \__ \                 //
//                  (__)(__)(_)\_)(_)\_)(__)(__)(__) (___/                 //
//                                          Data Wrangling                 //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// ArrayWrap1.h
// Written by Kristi Potter
// Arrays for VIZR
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _ARRAY_WRAP_4_H
#define _ARRAY_WRAP_4_H

#include "ArrayBase.h"

// Forward Decl
template <class T> class ArrayWrap5;

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
//  ArrayWrap4 	- An Array WRAPPER class.
//   		- This is a lightweight class that does NOT own its data.
//   		- Copies of "ArrayWrap4" objects are shallow.
//   		- The constructor does NOT allocate any memory.
//   		- The destructor does NOT free the memory.
//   		- Accessors (In order of increasing overhead)
//		1) The (i,j,k,m) operator is just as fast a built-in C/C++ Array.
//		2) The [] (slice) operator is next fastest 
//		   - 8% overhead for 1 level of slice
//		   - 20% overhead for 4D access via slices.
//		   - Only use slicing when you need it, otherwise use (i,j,k,m)!
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template <class T>
class ArrayWrap4 : public ArrayBase<T>
{
public:
  friend class ArrayWrap5<T>;
  
  /// Constructors - Shallow wrap around data. No size checking!
  ArrayWrap4() : ArrayBase<T>() {}
  ArrayWrap4( int d0, int d1, int d2, int d3, T* v ): ArrayBase<T>(d0,d1,d2,d3,v) {}
  ArrayWrap4( const ArrayWrap4<T>& a ) : ArrayBase<T>(a) { mSlice = a.mSlice; }
  ArrayWrap4<T>& operator=( const ArrayWrap4<T>& a );
  
  virtual ~ArrayWrap4();
  
  /// Return a slice of data - Optimized by returning reference to ArrayWrap3<T> member.
  inline ArrayWrap3<T>&		operator[] (int i);
  inline const ArrayWrap3<T>& operator[] (int i) const;
  
  // Return the element at data(i,j,k,m) - Faster than arr[i][j][k][m]
  inline T&					operator() (int i, int j, int k, int m);
  inline const T&				operator() (int i, int j, int k, int m) const;

protected:

  /// The slice wrapper used in the [] operator. 
  /// Mutable so that it can be changed by a const operator
  mutable ArrayWrap3<T>  mSlice;	
	
private:
  /// Set 'mSlice' to the ith slice of 'mData'
  inline void setSlice( int i ) const;
  /// Get the address of the data(i,j)'th element
  inline int  address(  int i, int j, int k, int m ) const;
  /// Reset just data pointer
  inline void setData( T* d ) { this->mData = d; }
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Assignment Op
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayWrap4<T>& ArrayWrap4<T>::operator=(const ArrayWrap4<T>& a)
{
  if( this != &a ) {
    ArrayBase<T>::operator=(a);// Call base class assign. op
    mSlice = a.mSlice;
  }
  return *this;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Destructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayWrap4<T>::~ArrayWrap4()
{}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Accessing Operators
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline ArrayWrap3<T>& ArrayWrap4<T>::operator[] (int i) 				
{ 
  setSlice(i);
  return mSlice; 
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline const ArrayWrap3<T>& ArrayWrap4<T>::operator[] (int i) const 	
{ 
  setSlice(i);
  return mSlice; 
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline T& ArrayWrap4<T>::operator() (int i, int j, int k, int m)
{
  return this->mData[ address(i,j,k,m) ];
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline const T& ArrayWrap4<T>::operator() (int i, int j, int k, int m) const
{
  return this->mData[ address(i,j,k,m) ];
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline int ArrayWrap4<T>::address( int i, int j, int k, int m) const
{
  //return i*mAxisDim[3]*mAxisDim[2]*mAxisDim[1] + j*mAxisDim[3]*mAxisDim[2] + k*mAxisDim[3] + m;
  return i*this->mAxisStride[0] + j*this->mAxisStride[1] + k*this->mAxisStride[2] + m;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Set 'mSlice' to be the ith slice of 'mData'
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
inline void ArrayWrap4<T>::setSlice( int i ) const
{
  //int a = i * mAxisDim[1] * mAxisDim[2] * mAxisDim[3];
  int a = i * this->mAxisStride[0];
  assert( a < this->mSize );
  mSlice.setData( this->mData + a );
}


//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//
// Typedefs
//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//

typedef ArrayWrap4<char>        	Arrayw4c;
typedef ArrayWrap4<unsigned char> 	Arrayw4uc;
typedef ArrayWrap4<char>        	Arrayw4b;
typedef ArrayWrap4<unsigned char> 	Arrayw4ub;
typedef ArrayWrap4<short>        	Arrayw4s;
typedef ArrayWrap4<unsigned short> 	Arrayw4us;
typedef ArrayWrap4<int>	        	Arrayw4i;
typedef ArrayWrap4<unsigned int> 	Arrayw4ui;
typedef ArrayWrap4<long>        	Arrayw4l;
typedef ArrayWrap4<unsigned long> 	Arrayw4ul;
typedef ArrayWrap4<long long>   	Arrayw4ll;
typedef ArrayWrap4<unsigned long long> 	Arrayw4ull;
typedef ArrayWrap4<float>       	Arrayw4f;
typedef ArrayWrap4<double>       	Arrayw4d;

typedef ArrayWrap4<Vector2ub> Arrayw4v2ub;
typedef ArrayWrap4<Vector2i>  Arrayw4v2i;
typedef ArrayWrap4<Vector2ui> Arrayw4v2ui;
typedef ArrayWrap4<Vector2f>  Arrayw4v2f;

typedef ArrayWrap4<Vector3ub> Arrayw4v3ub;
typedef ArrayWrap4<Vector3i>  Arrayw4v3i;
typedef ArrayWrap4<Vector3ui> Arrayw4v3ui;
typedef ArrayWrap4<Vector3f>  Arrayw4v3f;

typedef ArrayWrap4<Vector4ub> Arrayw4v4ub;
typedef ArrayWrap4<Vector4i>  Arrayw4v4i;
typedef ArrayWrap4<Vector4ui> Arrayw4v4ui;
typedef ArrayWrap4<Vector4f>  Arrayw4v4f;
/*
typedef ArrayWrap4<mat2ub> Arrayw4m2ub;
typedef ArrayWrap4<mat2i>  Arrayw4m2i;
typedef ArrayWrap4<mat2ui> Arrayw4m2ui;
typedef ArrayWrap4<mat2f>  Arrayw4m2f;

typedef ArrayWrap4<mat3ub> Arrayw4m3ub;
typedef ArrayWrap4<mat3i>  Arrayw4m3i;
typedef ArrayWrap4<mat3ui> Arrayw4m3ui;
typedef ArrayWrap4<mat3f>  Arrayw4m3f;

typedef ArrayWrap4<mat4ub> Arrayw4m4ub;
typedef ArrayWrap4<mat4i>  Arrayw4m4i;
typedef ArrayWrap4<mat4ui> Arrayw4m4ui;
typedef ArrayWrap4<mat4f>  Arrayw4m4f;
*/
#endif // ArrayWrap4_h
