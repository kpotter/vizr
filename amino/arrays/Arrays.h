// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    ____  ____    __   _  _  ___                  //
//                    /__\  (  _ \(  _ \  /__\ ( \/ )/ __)                 //
//                   /(__)\  )   / )   / /(__)\ \  / \__ \                 //
//                  (__)(__)(_)\_)(_)\_)(__)(__)(__) (___/                 //
//                                          Data Wrangling                 //
//~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// Arrays.h
// Written by Kristi Potter
// Arrays for VIZR
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _ARRAY_H
#define _ARRAY_H

#include "ArrayBase.h"
#include "ArrayWrap1.h"
#include "ArrayWrap2.h"
#include "ArrayWrap3.h"
#include "ArrayWrap4.h"
#include "ArrayWrap5.h"
#include "ArrayOwn1.h"
#include "ArrayOwn2.h"
#include "ArrayOwn3.h"
#include "ArrayOwn4.h"
#include "ArrayOwn5.h"

#endif

