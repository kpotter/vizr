// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    ____  ____    __   _  _  ___                  //
//                    /__\  (  _ \(  _ \  /__\ ( \/ )/ __)                 //
//                   /(__)\  )   / )   / /(__)\ \  / \__ \                 //
//                  (__)(__)(_)\_)(_)\_)(__)(__)(__) (___/                 //
//                                          Data Wrangling                 //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// ArrayOwn2.h
// Written by Kristi Potter
// Arrays for VIZR
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _ARRAY_2_OWN_H_
#define _ARRAY_2_OWN_H_

#include "ArrayWrap2.h"
#include <assert.h>

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
// ArrayOwn2 - A lightweight 2D Array class that OWNS its data.
//  - Very little (nearly none) error checking
//  - Designed to act just like a built-in 2D Array, but handle memory allocation
//  - No dynamic resizing of memory. 
//  - "reshape(dim0,dim1)" only sets dimensions. It does NOT affect memory allocation.
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template <class T>
class ArrayOwn2 : public ArrayWrap2<T>
{
public:
  /// Constructors - Deep copies of input data
  ArrayOwn2 ();
  ArrayOwn2 (int d0, int d1, T v ); 
  ArrayOwn2 (int d0, int d1, const T* v);
  ArrayOwn2 ( const ArrayWrap2<T>& a );
  
  /// Copy Constructor, Assignment Operator, and Destructor
  ArrayOwn2 (const ArrayOwn2<T>& a);
  ArrayOwn2<T>& operator= (const ArrayOwn2<T>& a);
  ~ArrayOwn2() {ArrayBase<T>::killData();}
  
  /// Transfer ownership of data ('v') to this object.
  /// - Destroys previous contents of 'this' before doing shallow copy of new data
  /// - 'killWithDelete' is true if 'v' is allocated with 'new', otherwise false ('malloc')
  void transfer( int d0, int d1, T* v, bool killWithDelete );

private:
  void initVal (T v);
  void initData (const T* d);
  void copy (const ArrayOwn2<T>& a);
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Constructors
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn2<T>::ArrayOwn2()
  : ArrayWrap2<T>()
{}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn2<T>::ArrayOwn2 (int d0, int d1, T val)
  : ArrayWrap2<T>(d0,d1,NULL)
{
  ArrayBase<T>::initValOwn( val );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn2<T>::ArrayOwn2 (int d0, int d1, const T* data)
  : ArrayWrap2<T>(d0,d1,NULL)
{
  ArrayBase<T>::initDataOwn( data );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn2<T>::ArrayOwn2 ( const ArrayWrap2<T>& a ) : ArrayWrap2<T>(a)
{
  ArrayBase<T>::initDataOwn( a.data() );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Copy Constructor: "this" does NOT exist yet
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn2<T>::ArrayOwn2( const ArrayOwn2<T>& a ) : ArrayWrap2<T>(a)// SHALLOW copy 'a'
{
  ArrayBase<T>::initDataOwn(a.mData);			 // DEEP copy of a
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Assignment operator: "this" DOES exist
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayOwn2<T>& ArrayOwn2<T>::operator=( const ArrayOwn2<T>& a )
{
  if( &a != this ) {
    // Kill data and allocate new memory only if sizes don't match.
    if( (this->mSize != a.size() ) || 
	(ArrayBase<T>::dim(0) != a.dim(0)) ||
	(ArrayBase<T>::dim(1) != a.dim(1)) ) {
      ArrayBase<T>::killData();
      ArrayWrap2<T>::operator=(a);	// SHALLOW copy of a
      ArrayBase<T>::allocDataOwn();
    }
    ArrayBase<T>::copyDataOwn(a.mData);	// Deep copy of a.mData
  }
  return *this; 
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Transfer ownership of data ('v') to this object.
// - Destroys previous contents of 'this' before doing shallow copy of new data
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
void ArrayOwn2<T>::transfer( int d0, int d1, T* v, bool killWithDelete )
{
  ArrayBase<T>::killData();
	
  int sizes[2];
  sizes[0] = d0;
  sizes[1] = d1;
  
  set(2, sizes, v);
  this->mSlice = ArrayWrap1<T>(d1,v);
  this->mKillWithDelete = killWithDelete;
}

//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//
// Typedefs
//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//

typedef ArrayOwn2<char>         	Arrayo2c;
typedef ArrayOwn2<unsigned char> 	Arrayo2uc;
typedef ArrayOwn2<char>	        	Arrayo2b;
typedef ArrayOwn2<unsigned char> 	Arrayo2ub;
typedef ArrayOwn2<short>        	Arrayo2s;
typedef ArrayOwn2<unsigned short> 	Arrayo2us;
typedef ArrayOwn2<int> 	        	Arrayo2i;
typedef ArrayOwn2<unsigned int> 	Arrayo2ui;
typedef ArrayOwn2<long>         	Arrayo2l;
typedef ArrayOwn2<unsigned long> 	Arrayo2ul;
typedef ArrayOwn2<long long>    	Arrayo2ll;
typedef ArrayOwn2<unsigned long long> 	Arrayo2ull;
typedef ArrayOwn2<float>        	Arrayo2f;
typedef ArrayOwn2<double>       	Arrayo2d;

typedef ArrayOwn2<Vector2ub> Arrayo2v2ub;
typedef ArrayOwn2<Vector2i>  Arrayo2v2i; 
typedef ArrayOwn2<Vector2ui> Arrayo2v2ui;
typedef ArrayOwn2<Vector2f>  Arrayo2v2f;

typedef ArrayOwn2<Vector3ub> Arrayo2v3ub;
typedef ArrayOwn2<Vector3i>  Arrayo2v3i;
typedef ArrayOwn2<Vector3ui> Arrayo2v3ui;
typedef ArrayOwn2<Vector3f>  Arrayo2v3f;

typedef ArrayOwn2<Vector4ub> Arrayo2v4ub;
typedef ArrayOwn2<Vector4i>  Arrayo2v4i;
typedef ArrayOwn2<Vector4ui> Arrayo2v4ui;
typedef ArrayOwn2<Vector4f>  Arrayo2v4f;
/*
typedef ArrayOwn2<mat2ub> Arrayo2m2ub;
typedef ArrayOwn2<mat2i>  Arrayo2m2i;
typedef ArrayOwn2<mat2ui> Arrayo2m2ui;
typedef ArrayOwn2<mat2f>  Arrayo2m2f;

typedef ArrayOwn2<mat3ub> Arrayo2m3ub;
typedef ArrayOwn2<mat3i>  Arrayo2m3i;
typedef ArrayOwn2<mat3ui> Arrayo2m3ui;
typedef ArrayOwn2<mat3f>  Arrayo2m3f;

typedef ArrayOwn2<mat4ub> Arrayo2m4ub;
typedef ArrayOwn2<mat4i>  Arrayo2m4i;
typedef ArrayOwn2<mat4ui> Arrayo2m4ui;
typedef ArrayOwn2<mat4f>  Arrayo2m4f;
*/
#endif // ArrayOwn2_h
