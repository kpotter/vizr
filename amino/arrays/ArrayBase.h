// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    ____  ____    __   _  _  ___                  //
//                    /__\  (  _ \(  _ \  /__\ ( \/ )/ __)                 //
//                   /(__)\  )   / )   / /(__)\ \  / \__ \                 //
//                  (__)(__)(_)\_)(_)\_)(__)(__)(__) (___/                 //
//                                          Data Wrangling                 //
//~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// ArrayBase.h
// Written by Kristi Potter
// Array Base for VIZR
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _ARRAY_BASE_H
#define _ARRAY_BASE_H

#include <assert.h>

#define MAX_ARRAY_DIM 5
  
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
/// This is a base class for all arrays in vizr
///  - it knows its 
///    * dimension
///    * size of each axis
///    * the data pointer
///    * total size of the array
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template <class T>
class ArrayBase
{
public:
  
  /// Default constructor
  ArrayBase();
  
  /// Assignment constructors implicitly set the dimension
  ArrayBase(int d0, T* a);
  ArrayBase(int d0, int d1, T* a);
  ArrayBase(int d0, int d1, int d2, T* a);
  ArrayBase(int d0, int d1, int d2, int d3, T* a);
  ArrayBase(int d0, int d1, int d2, int d3, int d4, T* a);
  
  /// This assignment constructor explicitly sets the dimension
  ArrayBase(int dim, int* aSize, T* a);
  
  /// Copy constructor, creates an identical array
  ArrayBase( const ArrayBase<T> &a);

  // 10/10/02 Added by Aaron Lefohn
  // - I think that operator= should have the same 
  //   functionality as the copy ctor. If other, non-trad
  //   assignments are required, they should exist via
  //   a member function such as "transfer" or "wrap"
  // (OLD comment: "assignment operator, assumes data is WRAPPED by class,
  //	        only sets the data pointer and total size, YOU specify the 
  //		dimension and axis sizes when you construct the class")
  /// Equals operator has the same functionality as the copy contructor
  ArrayBase &operator=( const ArrayBase<T>& a );
  
  /// Return the element at data(i,j,k,m,n)
  inline T &operator() (int i, int j=0, int k=0, int m=0, int n=0);
  inline const T &operator() (int i, int j=0, int k=0, int m=0, int n=0) const;

  /// Return the dimension of the array
  inline int dim() const {return mDim;}
  
  /// Return the size of an axis
  inline int dim(const int axis) const {return mAxisDim[axis];}

  /// Get the total elements in array
  inline int size() const {return mSize;}

  /// Is the array empty?
  inline int empty() const {return mData==NULL || mSize==0;}

  /// Get the data
  inline T* data() { return mData; }
  inline const T* data() const { return mData; }

  /// Should the data be deleted with 'delete' or 'malloc'?
  // - NOTE: Data only deleted by 'arrayOwnN<T>' classes, not by arrayWrapN<T>
  inline bool killWithDelete() const {return mKillWithDelete;}
  
  /// Reshape the axes sizes, does not change dimension of array, 
  /// does not reallocate data
  inline void	reshape( int d0, int d1 = 0, int d2 = 0, int d3 = 0, int d4 = 0 );
  
protected:
  
  T*   mData;                              ///< data pointer

  int  mAxisDim[MAX_ARRAY_DIM];       ///< # of elements allong each axis
  int  mAxisStride[MAX_ARRAY_DIM];    ///< # of elements to jump for the next entry
  int  mSize;                              ///< total # of elements in array
  int  mDim;                               ///< # of axes in array
  bool mKillWithDelete;			   ///< Kill data with delete or free?
  
  inline void set( int dim, int* aSize, T* a);     ///< # of axies, # of elements per axis, data pointer
  //inline int  address(int i, int j, int k, int m, int n); ///< address of array element	
  inline const int  address(int i, int j, int k, int m, int n) const; ///< address of array element
  
  // ***************************************
  // ****   arrayOwn routines
  // ***************************************
  /// Init memory from a single value
  /// Preconditions: 1) mData MUST already be [] deleted if necessary
  ///				  2) mSize MUST be set
  inline void initValOwn(T val);
  inline void initDataOwn(const T* data); ///< Calls allocCopyOwn and copyDataOwn
  inline void copyDataOwn(const T* data); ///< Copies data: does NOT allocate memory
  inline void allocDataOwn();		  ///< Allocates data, does NOT copy memory
  inline void killData();                 ///< Called by arrayOwnN<T> destructors
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Default constructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class T>
ArrayBase<T>::ArrayBase()
:mData(0),mSize(0),mDim(0),mKillWithDelete(true) {}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class T>
ArrayBase<T>::ArrayBase(int d0, T* a)
{
  int sizes[1];
  sizes[0] = d0;
  set(1, sizes, a);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class T>
ArrayBase<T>::ArrayBase(int d0, int d1, T* a)
{
  int sizes[2];
  sizes[0] = d0;
  sizes[1] = d1;
  set(2, sizes, a);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class T>
ArrayBase<T>::ArrayBase(int d0, int d1, int d2, T* a)
{
  int sizes[3];
  sizes[0] = d0;
  sizes[1] = d1;
  sizes[2] = d2;
  set(3, sizes, a);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class T>
ArrayBase<T>::ArrayBase(int d0, int d1, int d2, int d3, T* a)
{
  int sizes[4];
  sizes[0] = d0;
  sizes[1] = d1;
  sizes[2] = d2;
  sizes[3] = d3;
  set(4, sizes, a);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class T>
ArrayBase<T>::ArrayBase(int d0, int d1, int d2, int d3, int d4, T* a)
{
  int sizes[5];
  sizes[0] = d0;
  sizes[1] = d1;
  sizes[2] = d2;
  sizes[3] = d3;
  sizes[4] = d3;
  set(5, sizes, a);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class T>
ArrayBase<T>::ArrayBase(int dim, int *aSize, T* a)
{
  set(dim, aSize, a);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//copy constructor, creates an identical array
// SHALLOW COPY!
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class T>
ArrayBase<T>::ArrayBase( const ArrayBase<T> &a)
{
  assert( MAX_ARRAY_DIM == 5 );
  int sizes[MAX_ARRAY_DIM];
  sizes[0] = a.mAxisDim[0];
  sizes[1] = a.mAxisDim[1];
  sizes[2] = a.mAxisDim[2];
  sizes[3] = a.mAxisDim[3];
  sizes[4] = a.mAxisDim[4];
  
  set(a.mDim, sizes, a.mData);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//assignment operator
// SHALLOW COPY
// 10/10/02 Added by Aaron Lefohn
// - I think that operator= should have the same 
//   functionality as the copy ctor. If other, non-trad
//   assignments are required, they should exist via
//   a member function such as "transfer" or "wrap
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class T>
ArrayBase<T>& ArrayBase<T>::operator=( const ArrayBase<T>& a )
{
  if( this != &a ) {
    assert( MAX_ARRAY_DIM == 5 );
    int sizes[MAX_ARRAY_DIM];
    sizes[0] = a.mAxisDim[0];
    sizes[1] = a.mAxisDim[1];
    sizes[2] = a.mAxisDim[2];
    sizes[3] = a.mAxisDim[3];
    sizes[4] = a.mAxisDim[4];
    
    set(a.mDim, sizes, a.mData);
  }
  return *this;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Return the element at data(i,j,k,m)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class T>
T& ArrayBase<T>::operator() (int i, int j, int k, int m, int n)
{
  return mData[address(i,j,k,m,n)];
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class T>
const T& ArrayBase<T>::operator() (int i, int j, int k, int m, int n) const
{
  return mData[address(i,j,k,m,n)];
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/// Address of array element	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class T>
const int ArrayBase<T>::address(int i, int j, int k, int m, int n) const
{
  assert(mSize < (i*mAxisStride[0] + j*mAxisStride[1] + k*mAxisStride[2] +
		  m*mAxisStride[3] + n*mAxisStride[4]));
  return i*mAxisStride[0] + j*mAxisStride[1] + k*mAxisStride[2] + m*mAxisStride[3] + n*mAxisStride[4];
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class T>
void ArrayBase<T>::set( int dim, int* aSize, T* a)
{
  assert(dim >= 0 );
  assert(dim <= MAX_ARRAY_DIM);
  
  //Zero out everything
  for(int s=0; s<MAX_ARRAY_DIM; ++s)
    {
      mAxisStride[s] = 0;
      mAxisDim[s] = 0;
    }
  
  //Set the member variables
  mDim = dim;
  int size = dim > 0 ? 1 : 0;  
  for(int i=0; i<dim; ++i)
    {
      //Axis size & total size
      mAxisDim[i] = aSize[i];
      //if(i==0) size = aSize[i];
      //else 
      size *= aSize[i];
      
      //Strides
      mAxisStride[i] = 1;
      for(int j=i+1; j<dim; ++j)
	{
	  mAxisStride[i] *= aSize[j];
	  assert(aSize[j] > 0);
	}
    }
  //Set the total size and array pointer
  mSize = size;
  mData = a;
  mKillWithDelete = true;
}
  
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template<class T>
void ArrayBase<T>::reshape( int d0, int d1, int d2, int d3, int d4)
{
  //Set the new dimensions
  mAxisDim[0] = d0;
  mAxisDim[1] = d1;
  mAxisDim[2] = d2;
  mAxisDim[3] = d3;
  mAxisDim[4] = d4;
  
  //Set the strides
  for(int i=0; i<mDim; ++i)
    {		
      mAxisStride[i] = 1;
      for(int j=i+1; j<mDim; ++j)
	{
	  mAxisStride[i] *= mAxisDim[j];
	}
    }
}
  
// ***************************************
// ****   arrayOwn routines
// ***************************************
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Init memory from a single value
// Preconditions: 1) mData MUST already be [] deleted if necessary
//				  2) mSize MUST be set
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
void ArrayBase<T>::initValOwn(T val)
{
  if( mSize > 0 ) {
    mData = new T[mSize];
    assert(mData);
    for (int i=0; i<mSize; i++) { mData[i] = val; }	
  }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Init memory from an array
// Init memory from a single value
// Preconditions: 1) mData MUST already be [] deleted if necessary
//				  2) mSize MUST be set
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
void ArrayBase<T>::initDataOwn(const T* data)
{	
  // Do DEEP copy of 'data'
  /*if( mSize > 0 ) {
    mData = new T[mSize];
    assert(mData);
    for (int i=0; i<mSize; i++) { mData[i] = data[i]; }	
    }*/
  allocDataOwn();
  copyDataOwn(data);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
void ArrayBase<T>::allocDataOwn()
{
  //cerr << "ArrayBase<T>::allocDataOwn() Called\n";
  if( mSize > 0 ) {
    mData = new T[mSize];
    assert(mData);
  }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
void ArrayBase<T>::copyDataOwn(const T* data)
{
  if( mSize > 0 && mData) {
    for (int i=0; i<mSize; i++) { mData[i] = data[i]; }	
  }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Delete old mem
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
void ArrayBase<T>::killData()
{
  //cerr << "ArrayBase<T>::killData() Called\n";
  if( mData ) {						
    if( mKillWithDelete ) {
      delete [] mData;
    }
    else {
      free(mData);
    }
    mData = NULL;
  }
}

//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//
// Typedefs
//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//
typedef ArrayBase<char>  	      Arraybc;
typedef ArrayBase<unsigned char>      Arraybuc;
typedef ArrayBase<char> 	      Arraybb;
typedef ArrayBase<unsigned char>      Arraybub;
typedef ArrayBase<short> 	      Arraybs;
typedef ArrayBase<unsigned short>     Arraybus;
typedef ArrayBase<int>	   	      Arraybi;
typedef ArrayBase<unsigned int>       Arraybui;
typedef ArrayBase<long> 	      Arraybl;
typedef ArrayBase<unsigned long>      Arraybul;
typedef ArrayBase<long long> 	      Arraybll;
typedef ArrayBase<unsigned long long> Arraybull;
typedef ArrayBase<float> 	      Arraybf;
typedef ArrayBase<double>     	      Arraybd;

typedef ArrayBase<Vector2ub> Arraybv2ub;
typedef ArrayBase<Vector2i>  Arraybv2i;
typedef ArrayBase<Vector2ui> Arraybv2ui;
typedef ArrayBase<Vector2f>  Arraybv2f;

typedef ArrayBase<Vector3ub> Arraybv3ub;
typedef ArrayBase<Vector3i>  Arraybv3i;
typedef ArrayBase<Vector3ui> Arraybv3ui;
typedef ArrayBase<Vector3f>  Arraybv3f;

typedef ArrayBase<Vector4ub> Arraybv4ub;
typedef ArrayBase<Vector4i>  Arraybv4i;
typedef ArrayBase<Vector4ui> Arraybv4ui;
typedef ArrayBase<Vector4f>  Arraybv4f;
/*
typedef ArrayBase<mat2ub> Arraybm2ub;
typedef ArrayBase<mat2i>  Arraybm2i;
typedef ArrayBase<mat2ui> Arraybm2ui;
typedef ArrayBase<mat2f>  Arraybm2f;

typedef ArrayBase<mat3ub> Arraybm3ub;
typedef ArrayBase<mat3i>  Arraybm3i;
typedef ArrayBase<mat3ui> Arraybm3ui;
typedef ArrayBase<mat3f>  Arraybm3f;

typedef ArrayBase<mat4ub> Arraybm4ub;
typedef ArrayBase<mat4i>  Arraybm4i;
typedef ArrayBase<mat4ui> Arraybm4ui;
typedef ArrayBase<mat4f>  Arraybm4f;
*/
#endif

