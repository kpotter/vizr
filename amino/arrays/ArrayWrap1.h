// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    ____  ____    __   _  _  ___                  //
//                    /__\  (  _ \(  _ \  /__\ ( \/ )/ __)                 //
//                   /(__)\  )   / )   / /(__)\ \  / \__ \                 //
//                  (__)(__)(_)\_)(_)\_)(__)(__)(__) (___/                 //
//                                          Data Wrangling                 //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// ArrayWrap1.h
// Written by Kristi Potter
// Arrays for VIZR
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _ARRAY_WRAP_1_H
#define _ARRAY_WRAP_1_H

#include "ArrayBase.h"

// Foward Decl
template <class T> class ArrayWrap2;
  
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
///  Array1 - An Array WRAPPER class.
///  	   - This is a lightweight class that does NOT own its data.
///  	   - Copies of "ArrayWrap1" objects are shallow.
///  	   - The constructor does NOT allocate any memory.
///		   - The destructor does NOT free the memory.
///		   - The (i) and [i] operators are identical for the 1D case.
///		     Both exist here for compatibility with higher-dimensional Arrays.
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template <class T>
class ArrayWrap1 : public ArrayBase<T>
{
public:

  // This is a friend so that it can access the 'setData' function.
  friend class ArrayWrap2<T>; 
  
  // Constructors - Shallow wrap around data
  // WARNING: No size checking is done here so be sure to get allocation correct!!
  ArrayWrap1() : ArrayBase<T>() {};
  ArrayWrap1( int s, T* v) : ArrayBase<T>(s,v) {}
  ArrayWrap1( const ArrayWrap1<T>& a ) : ArrayBase<T>(a) {}
  ArrayWrap1<T>& operator=( const ArrayWrap1<T>& a );
  
  ~ArrayWrap1();
  
  // Accessors - The [] and () operators are identical here, but here for consistency with
  // 			   the higher-dimensional Arrays.
  inline T&       operator[] (int i)       { assert(i < this->mSize); return this->mData[i]; }
  inline const T& operator[] (int i) const { assert(i < this->mSize); return this->mData[i]; }
  inline T&       operator() (int i)	      { assert(i < this->mSize); return this->mData[i]; }
  inline const T& operator() (int i) const { assert(i < this->mSize); return this->mData[i]; }
    
 protected:
  
 private:
    inline void     setData(T* d) {this->mData = d;}
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Assignment Op
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayWrap1<T>& ArrayWrap1<T>::operator=(const ArrayWrap1<T>& a)
{
  if( this != &a ) {
    ArrayBase<T>::operator=(a);// Call base class assign. op
  }
  return *this;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Destructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
template <class T>
ArrayWrap1<T>::~ArrayWrap1()
{}

//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//
// Typedefs
//~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#//

typedef ArrayWrap1<char>        	Arrayw1c;
typedef ArrayWrap1<unsigned char> 	Arrayw1uc;
typedef ArrayWrap1<char>        	Arrayw1b;
typedef ArrayWrap1<unsigned char> 	Arrayw1ub;
typedef ArrayWrap1<short>       	Arrayw1s;
typedef ArrayWrap1<unsigned short> 	Arrayw1us;
typedef ArrayWrap1<int>	        	Arrayw1i;
typedef ArrayWrap1<unsigned int> 	Arrayw1ui;
typedef ArrayWrap1<long>        	Arrayw1l;
typedef ArrayWrap1<unsigned long>       Arrayw1ul;
typedef ArrayWrap1<long long> 	        Arrayw1ll;
typedef ArrayWrap1<unsigned long long>	Arrayw1ull;
typedef ArrayWrap1<float>       	Arrayw1f;
typedef ArrayWrap1<double>      	Arrayw1d;

typedef ArrayWrap1<Vector2ub> Arrayw1v2ub;
typedef ArrayWrap1<Vector2i>  Arrayw1v2i;
typedef ArrayWrap1<Vector2ui> Arrayw1v2ui;
typedef ArrayWrap1<Vector2f>  Arrayw1v2f;

typedef ArrayWrap1<Vector3ub> Arrayw1v3ub;
typedef ArrayWrap1<Vector3i>  Arrayw1v3i;
typedef ArrayWrap1<Vector3ui> Arrayw1v3ui;
typedef ArrayWrap1<Vector3f>  Arrayw1v3f;

typedef ArrayWrap1<Vector4ub> Arrayw1v4ub;
typedef ArrayWrap1<Vector4i>  Arrayw1v4i;
typedef ArrayWrap1<Vector4ui> Arrayw1v4ui;
typedef ArrayWrap1<Vector4f>  Arrayw1v4f;
/*
typedef ArrayWrap1<mat2ub> Arrayw1m2ub;
typedef ArrayWrap1<mat2i>  Arrayw1m2i;
typedef ArrayWrap1<mat2ui> Arrayw1m2ui;
typedef ArrayWrap1<mat2f>  Arrayw1m2f;

typedef ArrayWrap1<mat3ub> Arrayw1m3ub;
typedef ArrayWrap1<mat3i>  Arrayw1m3i;
typedef ArrayWrap1<mat3ui> Arrayw1m3ui;
typedef ArrayWrap1<mat3f>  Arrayw1m3f;

typedef ArrayWrap1<mat4ub> Arrayw1m4ub;
typedef ArrayWrap1<mat4i>  Arrayw1m4i;
typedef ArrayWrap1<mat4ui> Arrayw1m4ui;
typedef ArrayWrap1<mat4f>  Arrayw1m4f;
*/
#endif // ArrayWrap1_h

