// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// Matrix.h                                                                //
// Written by Kristi Potter                                                //
// August 2010                                                             //
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _MATRIX_H_KP
#define _MATRIX_H_KP

#include <iostream>
#include <Maths.h>

// Forward declarations
template<typename Type> class Quaternion;
template<typename Type, int Dim> class Vector; 

// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Definition of a variable type and dimension x dimension matrix class.
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
template< typename Type, int Dim >
class Matrix
{
public:
  // public data accessor
  Type m[Dim*Dim];
  
  //  - + - + - + - + - + Constructors (up to dim 4) + - + - + - + - + - + - //
  Matrix();
  Matrix(const Matrix &mat);
  Matrix(Type &mat);
  Matrix(Type *mat);
  Matrix(const Quaternion<Type> &q);
  Matrix(const Type theta, const Vec<Type,3> &v);
  Matrix(Type m1,Type m2,Type m3,Type m4,Type m5=0,Type m6=0,Type m7=0,Type m8=0,Type m9=0,Type m10=0,Type m11=0,Type m12=0,Type m13=0,Type m14=0,Type m15=0,Type m16=0);
  virtual ~Matrix() {}

  // - + - + - + - + - + - + - + Accesssors + - + - + - + - + - + - + - + - //
  // [] gets you a element accessed like an array, (,) lets you go by row, column
  Type &operator[](int i)                   { return m[i]; }
  Type operator[](int i) const              { return m[i]; }
  const Type &operator()(int i, int j)      { return m[i*Dim + j]; }
  const Type operator()(int i, int j) const { return m[i*Dim + j]; }
  Matrix &operator=(const Matrix &mat)
  { for(int i=0; i<Dim*Dim; ++i) this->m[i] = mat.m[i]; return *this;}

  // Angle axis
  void set(const Type theta, const Vec<Type,3> &v); 

  // - + - + - + - + - + - + -  Dim/Len/Size - + - + - + - + - + - + - + - //
  int dim()  { return Dim; }
  int len()  { return Dim*Dim; }
  int size() { return Dim*Dim; }

  // - + - + - + - + - + - + - Math Operators - + - + - + - + - + - + - + -//  
  Matrix operator-    () const                // negate
  { Matrix M; for(int i=0; i<Dim*Dim; i++) M[i] = -(this->m[i]); return M; }
  Matrix operator+    (const Type& m) const   // +  scalar
  { Matrix M; for(int i=0; i<Dim*Dim; i++) M[i] = this->m[i] + m; return M; }
  Matrix operator+    (const Matrix &m) const // +  Matrix
  { Matrix  M; for(int i=0; i<Dim*Dim; i++) M[i] = this->m[i] + m.m[i]; return M; }
  Matrix &operator+=  (const Type& m)         // += scalar
  { for(int i=0; i<Dim*Dim; i++) this->m[i] += m; return *this;}
  Matrix &operator+=  (const Matrix &m)       // += Matrix
  { for(int i=0; i<Dim*Dim; i++) this->m[i] += m.m[i]; return *this;}
  Matrix operator-    (const Type& m) const   // -  scalar
  { Matrix M; for(int i=0; i<Dim*Dim; i++) M[i] = this->m[i] - m; return M; }
  Matrix operator-    (const Matrix &m) const // -  Matrix
  { Matrix M; for(int i=0; i<Dim; i++) M[i] = this->m[i] - m.m[i]; return M; }
  Matrix &operator-=  (const Type& m)         // -= scalar
  { for(int i=0; i<Dim; i++) this->m[i] -= m; return *this;}
  Matrix &operator-=  (const Matrix &m)       // -= Matrix
  { for(int i=0; i<Dim; i++) this->m[i] -= m.m[i]; return *this;}
  Matrix operator*    (const Type& m) const   // *  scalar
  { Matrix M; for(int i=0; i<Dim; i++) M[i] = this->m[i] * m; return M; }
  Matrix &operator*=  (const Type& m)         // *= scalar
  { for(int i=0; i<Dim; i++) this->m[i] *= m; return *this;}
  Matrix operator/    (const Type& m) const   // /  scalar
  { Matrix M; for(int i=0; i<Dim; i++) M[i] = this->m[i] / m; return M; }
  Matrix &operator/=  (const Type& m)        // /= scalar
  { for(int i=0; i<Dim; i++) this->m[i] /= m; return *this;}
  Matrix operator*    (const Matrix &m) const // *  Matrix
  { Matrix M; for(int i=0; i<Dim; i++) for(int j = 0; j < Dim; j++) { Type sum = 0; for(int x = 0; x < Dim; x++) sum += this->m[i*Dim+x]*m.m[x*Dim+j]; M[i*Dim+j] = sum; } return M; }
  Matrix mult(const Matrix &m)               // position-wise Matrix mult
  { Matrix M; for(int i=0; i<Dim*Dim; i++) M[i] = this->m[i]*m.m[i]; return M; }
  Matrix T()
  { Matrix M; for(int i=0; i<Dim; i++) for(int j =0; j<Dim; j++) M[i*Dim+j] = this->m[j*Dim+i]; return M; }

  // - + - + - + - + - + Comparison Operators + - + - + - + - + - + - //
  bool operator==     (const Matrix &m) const // ==
  { for(int i=0; i<Dim; i++) if(this->m[i] != m[i]) return false;  return true;  }
  bool operator!=     (const Matrix &m) const // !=
  { return !(operator==(m)); }

};

// -<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>- //
// Define some common Matrix Types  
// -<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>-<~>- //
typedef Matrix<float, 2>           Matrix2f;
typedef Matrix<float, 3>           Matrix3f;
typedef Matrix<float, 4>           Matrix4f; 
typedef Matrix<double, 2>          Matrix2d;
typedef Matrix<double, 3>          Matrix3d;
typedef Matrix<double, 4>          Matrix4d;
typedef Matrix<int, 2>             Matrix2i;
typedef Matrix<int, 3>             Matrix3i;
typedef Matrix<int, 4>             Matrix4i; 
typedef Matrix<unsigned int, 2>    Matrix2ui;
typedef Matrix<unsigned int, 3>    Matrix3ui;
typedef Matrix<unsigned int, 4>    Matrix4ui; 
typedef Matrix<short, 2>           Matrix2s;
typedef Matrix<short, 3>           Matrix3s;
typedef Matrix<short, 4>           Matrix4s; 
typedef Matrix<unsigned short, 2>  Matrix2us;
typedef Matrix<unsigned short, 3>  Matrix3us;
typedef Matrix<unsigned short, 4>  Matrix4us; 
typedef Matrix<char, 2>            Matrix2b;
typedef Matrix<char, 3>            Matrix3b;
typedef Matrix<char, 4>            Matrix4b; 
typedef Matrix<unsigned char, 2>   Matrix2ub;
typedef Matrix<unsigned char, 3>   Matrix3ub;
typedef Matrix<unsigned char, 4>   Matrix4ub; 

// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Implementation of Matrix
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
template< typename Type, int Dim > Matrix<Type, Dim>::Matrix()
{ for(int i = 0; i < Dim; i++) for(int j = 0; j < Dim; j++) { m[i*Dim +j] = 0; if (i==j) m[i*Dim+j] = 1;  }}
template< typename Type, int Dim > Matrix<Type, Dim>::Matrix(const Matrix &mat)
{ for(int i = 0; i < Dim*Dim; i++) this->m[i] = mat.m[i]; }
template< typename Type, int Dim > Matrix<Type, Dim>::Matrix(Type &mat)
{ for(int i = 0; i < Dim*Dim; i++) this->m[i] = mat; }
template< typename Type, int Dim > Matrix<Type, Dim>::Matrix(Type *mat)
{ for(int i = 0; i < Dim*Dim; i++) this->m[i] = mat[i]; }
template< typename Type, int Dim > Matrix<Type, Dim>::Matrix(Type m1, Type m2, Type m3, Type m4, Type m5, Type m6, Type m7, Type m8, Type m9, Type m10, Type m11, Type m12, Type m13, Type m14, Type m15, Type m16)
{ Type args[] = {m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16}; for(int i = 0; i<Dim*Dim; i++) this->m[i] = args[i]; }

template < typename Type, int Dim >
Matrix<Type,Dim>::Matrix(const Quaternion<Type> &q)
{ m[0] = q.w()*q.w() + q.x()*q.x() - q.y()*q.y() - q.z()*q.z();
  m[1] = 2*(q.x()*q.y() + q.w()*q.z());
  m[2] = 2*(q.x()*q.z() - q.w()*q.y());
  m[3] = 2*(q.x()*q.y() - q.w()*q.z());
  m[4] = q.w()*q.w() + q.y()*q.y() - q.x()*q.x() - q.z()*q.z();
  m[5] = 2*(q.y()*q.z() + q.w()*q.x());
  m[6] = 2*(q.x()*q.z() + q.w()*q.y());
  m[7] = 2*(q.y()*q.z() - q.w()*q.x());
  m[8] = q.w()*q.w() + q.z()*q.z() - q.x()*q.x() - q.y()*q.y(); 
}

template < typename Type, int Dim>
Matrix<Type,Dim>::Matrix(const Type theta, const Vec<Type,3> &v)
{ set(theta,v); }

template < typename Type, int Dim>
void Matrix<Type,Dim>::set(const Type theta, const Vec<Type,3> &v)
{
  double c = cos(theta);
  double s = sin(theta);
  double t = 1.0 - c;
  m[0] = T(c + v.x()*v.x()*t);
  m[4] = T(c + v.y()*v.y()*t);
  m[8] = T(c + v.z()*v.z()*t);
 
  double tmp1 = v.x()*v.y()*t;
  double tmp2 = v.z()*s;
  m[1] = T(tmp1 + tmp2);
  m[3] = T(tmp1 - tmp2);
  
  tmp1 = v.x()*v.z()*t;
  tmp2 = v.y()*s;
  m[2] = T(tmp1 - tmp2);
  m[6] = T(tmp1 + tmp2);
  tmp1 = v.y()*v.z()*t;
  tmp2 = v.x()*s;
  m[5] = T(tmp1 + tmp2);
  m[7] = T(tmp1 - tmp2);
}

// -(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)- //
// Operators external to the class
// -(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)-(-)- //
template <typename Type, int Dim>
std::ostream& operator<< (std::ostream& os, const Matrix<Type, Dim> &M)                                 // <<
{ for(int i = 0; i < Dim; i++){  os << "| "; for(int j = 0; j < Dim; j++) os << M.m[i*Dim+j] << " "; os << "|" << std::endl; } return os; }
template <typename Type, int Dim>
Vec<Type, Dim> operator*(const Matrix<Type,Dim> &m, const Vec<Type, Dim> &v)                      // mat*vec
{ Vec<Type,Dim> V; for(int i=0; i<Dim; i++) { Type sum = 0; for(int j=0; j<Dim; j++) sum += m[i*Dim+j]*v[j]; V[i] = sum; } return V; }
template<typename Type, int Dim>                                                    
Vec<Type, Dim> operator*(const Vec<Type,Dim> &v, const Matrix<Type,Dim> &m)                       // vec*mat
{ Vec<Type, Dim> V; for(int i=0; i<Dim; i++) { Type sum = 0; for(int j=0; j<Dim; j++) sum += m[j*Dim+i]*v[j]; V[i] = sum; } return V;}
template<typename Type, int Dim>                                                                        // scalar*mat
Matrix<Type,Dim> operator*(const Type &v, const Matrix<Type,Dim> &m)
{ Matrix<Type, Dim> M; for(int i=0; i<Dim*Dim; i++) M[i] = m.m[i]*v; return M; }


template <class Type>
Matrix<Type,4> getMatrix4(const Matrix<Type,3> &M, const Vec<Type,3> &v)
{
  Matrix<Type,4> m;
  m.m[ 0] = M.m[0]; m.m[ 1] = M.m[1]; m.m[ 2] = M.m[2]; m.m[ 3] = 0;
  m.m[ 4] = M.m[3]; m.m[ 5] = M.m[4]; m.m[ 6] = M.m[5]; m.m[ 7] = 0;
  m.m[ 8] = M.m[6]; m.m[ 9] = M.m[7]; m.m[10] = M.m[8]; m.m[11] = 0;
  m.m[12] = v.x(); m.m[13] = v.y(); m.m[14] = v.z(); m.m[15] = 1;
  return m;
}

// -:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:- //
// Matrix Transformation Constants
// -:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:- //
template<typename Type> Matrix<Type, 2> rot2(Type angle = 90, bool inRads= false)                    // rotation matrix
{ if(inRads) angle = rad2deg(angle); return Matrix<Type, 2>(cosD(angle), -sinD(angle), sinD(angle), cosD(angle));}
template<typename Type> Matrix<Type, 3> rot3(Type angle = 90, char axis = 'z', bool inRads= false)   // rotation matrix
{ if(inRads) angle = rad2deg(angle); 
  if(axis=='x')      return Matrix<Type,3>(1, 0, 0, 0, cosD(angle), -sinD(angle), 0, sinD(angle), cosD(angle));
  else if(axis=='y') return Matrix<Type,3>(cosD(angle), 0, sinD(angle), 0, 1, 0, -sinD(angle), 0, cosD(angle));
  else if(axis=='z') return Matrix<Type,3>(cosD(angle), -sinD(angle), 0, sinD(angle), cosD(angle), 0, 0, 0, 1);
  else { std::cerr << "Matrix<Type, 3> rot(): Unknown axis, returning identity!" << std::endl; return Matrix<Type,3>(); }
}
template<typename Type> Matrix<Type, 4> rot(Type angle = 90, char axis = 'z', bool inRads= false)   // rotation matrix
{ if(inRads) angle = rad2deg(angle);  
  if(axis=='x')      return Matrix<Type,4>(1, 0, 0, 0, 0, cosD(angle), -sinD(angle), 0, 0, sinD(angle), cosD(angle), 0, 0, 0, 0, 1);
  else if(axis=='y') return Matrix<Type,4>(cosD(angle), 0, sinD(angle), 0, 0, 1, 0, 0, -sinD(angle), 0, cosD(angle), 0, 0, 0, 0, 1);
  else if(axis=='z') return Matrix<Type,4>(cosD(angle), -sinD(angle), 0, 0, sinD(angle), cosD(angle), 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
  else { std::cerr << "Matrix<Type, 4> rot(): Unknown axis, returning identity!" << std::endl; return Matrix<Type,4>(); }
}
template<typename Type, int Dim> Matrix<Type, Dim> scale(Type x=1, Type y=1, Type z=1)             // scale matrix
{ Matrix<Type, Dim> s;  s[0] = x; s[1*Dim+1] = y; s[2*Dim+2] = z; return s; }
template<typename Type, int Dim> Matrix<Type, Dim> trans(Type x = 0, Type y = 0, Type z = 0)       // translate matrix
{ Type args[] = {x, y, z, 1}; Matrix<Type,Dim> M; for(int i=0; i<Dim; i++) M[i*Dim+Dim-1] = args[i]; return M;}
template<typename Type> Matrix<Type, 3> rot(const Matrix<Type, 4> &m)                              // rot
{ return Matrix<Type,3>(m.m[ 0], m.m[ 1], m.m[ 2],  m.m[ 4], m.m[ 5], m.m[ 6],  m.m[ 8], m.m[ 9], m.m[10]); }
template <class Type>
Vec<Type,3> getTrans(const Matrix<Type,4> &m)                                                   // getTrans
{ return Vec<Type,3>(m.m[12], m.m[13], m.m[14]); }
template <class Type>
void setTrans(Matrix<Type,4> &m, const Vec<Type,3> &v)                                          // setTrans
{ m.m[12] = v.x(); m.m[13] = v.y(); m.m[14] = v.z(); }
template <class Type>
Vec<Type,3> tpoint(const Matrix<Type,4> &m, const Vec<Type,3> &p)                            // tpoint
{ return Vec<Type,3>(m.m[ 0]*p.x() + m.m[ 4]*p.y() + m.m[ 8]*p.z() + m.m[12],
			m.m[ 1]*p.x() + m.m[ 5]*p.y() + m.m[ 9]*p.z() + m.m[13],
			m.m[ 2]*p.x() + m.m[ 6]*p.y() + m.m[10]*p.z() + m.m[14]); }
template <class Type>
Vec<Type,3> tdir(const Matrix<Type,4> &m, const Vec<Type,3> &p)                              // tdir
{ return Vec<Type,3>(m.m[ 0]*p.x() + m.m[ 4]*p.y() + m.m[ 8]*p.z(),
			m.m[ 1]*p.x() + m.m[ 5]*p.y() + m.m[ 9]*p.z(),
			m.m[ 2]*p.x() + m.m[ 6]*p.y() + m.m[10]*p.z()); }

// -:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:- //
// Matrix Constants
// -:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:- //
const Matrix4f Matrix4f_ident = Matrix4f();
const Matrix3f Matrix3f_ident = Matrix3f();

#endif
