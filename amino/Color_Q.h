// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// Color_Q.h
// Written by Kristi Potter, 2013
// Functions to use QT color stuff
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _COLOR_Q_H
#define _COLOR_Q_H

#include "Color.h"
#include <QColor>

// - - - - - - - - - - - - - - - - - - - - - - - - - - //
// Get a QColor
// - - - - - - - - - - - - - - - - - - - - - - - - - - //
template< typename Type >
QColor toQColor(Color<Type>  color) { 
  Color<int> ci = color.toInt(); 
  return QColor(ci.r(), ci.g(), ci.b()); 
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - //
// Get a QLinearGradient
// - - - - - - - - - - - - - - - - - - - - - - - - - - //
QLinearGradient * getQLinearGradient(Colormap * colormap, QPoint min, QPoint max){
  QLinearGradient * gradient = new QLinearGradient(min, max);
  gradient->setColorAt(0, toQColor<float>(colormap->getLow()));
  gradient->setColorAt(0.5, toQColor<float>(colormap->getMid()));
  gradient->setColorAt(1, toQColor<float>(colormap->getHigh()));
return gradient;
}



#endif
