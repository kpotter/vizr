// -*- C++ -*-
//<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>//
//                               GenPrimGL                                  //
//                                                                          //
//                              GenPrimGL.h                                 //
//                                                                          //
//                       Written by Kristi Potter                           //
//                             April 15, 2009                               //
//                                                                          //
//<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>//
#ifndef _GEN_PRIM_GL_H 
#define _GEN_PRIM_GL_H 

#include "GLIncludes.h"
#include <RenderableState.h> 

// Look in these files to get the typedef'd types
#include "VertexArray.h"
#include "NormalArray.h"
#include "ColorArray.h"
#include "IndexArray.h"
#include "TexCoordArray.h"
#include "EdgeFlagArray.h"
#include "RangeArray.h"

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
/// A generic geometry primitive that uses vertex arrays.
/// The template parameters are from each of the array types.
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template< class VrtArry =   VertexArray3F, 
	  class NrmArry =   NormalArrayF, 
	  class ClrArry =   ColorArray3F,
	  class TxCArry =   TexCoordArray3F,
	  class IdxArry =   IndexArrayI,
	  class RngArry =   RangeArray2UI >
class GenPrimGL : public RenderableState
{
 public:
  /// The Vertex Storage Type
  typedef typename VrtArry::VStorageType VrtStorageType; 
  typedef typename NrmArry::NStorageType NrmStorageType;
  typedef typename ClrArry::CStorageType ClrStorageType;
  typedef typename TxCArry::TStorageType TxCStorageType;
  typedef typename IdxArry::IStorageType IdxStorageType;
  typedef typename RngArry::RStorageType RngStorageType;

  /// Enumerate the drawing options
  enum DrawMethod {
    DRAW_ARRAYS = 0, // first and count
    DRAW_ARRAY_ELEMENT, // list of int i's
    DRAW_ELEMENTS, // count (from 0 to count)
    DRAW_RANGE_ELEMENTS, // start, end, count
    DRAW_DEBUG,
  };

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Default constructor takes in:
  /// the GL primitive type (GL_POINTS, LINES, LINE_STRIP, LINE_LOOP, 
  ///                        TRIANGLES, TRIANGLE_STRIP, TRIANGLE_FAN,
  ///                        QUADS, QUAD_STRIP, POLYGON)
  /// and optional arrays for verticies, normals, color, texture coordinates,
  /// an index array and edge flag array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  GenPrimGL(GLenum primType, 
	    VrtArry verts                  = VrtArry(),
	    NrmArry norms                  = NrmArry(), 
	    ClrArry colors                 = ClrArry(), 
	    std::vector<TxCArry> texCoords = std::vector<TxCArry>(0,TxCArry()),
	    IdxArry indexes                = IdxArry(), 
	    EdgeFlagArray edgeFlags        = EdgeFlagArray(), 
	    RngArry ranges                 = RngArry())
  : RenderableState(),
    _primType(primType), 
    _verts(verts),         
    _norms(norms),    
    _colors(colors),
    _texCoords(texCoords), 
    _indexes(indexes),
    _edgeFlags(edgeFlags),
    _ranges(ranges),     
    _release(true)
  { 
    // Default draw goes through all of the elements in the array
    setDrawArrays(0, _verts.getSize()); 
  }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  // Constructor takes in vectors of the storage type
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  GenPrimGL(GLenum primType, 
	    std::vector<VrtStorageType> verts,
	    std::vector<NrmStorageType> norms                  = std::vector<NrmStorageType>(0),
	    std::vector<ClrStorageType> colors                 = std::vector<ClrStorageType>(0),
	    std::vector< std::vector<TxCStorageType> >texCoords= 
	    std::vector< std::vector<TxCStorageType> > (0),
	    std::vector<IdxStorageType> indexes                = std::vector<IdxStorageType>(0),
	    const std::vector<GLboolean> &edgeFlags            = std::vector<GLboolean>(0),
	    std::vector<RngStorageType> ranges                 = std::vector<RngStorageType>(0))
    : RenderableState(),
      _primType(primType)
  {
    setVertexArray(verts);
    setNormalArray(norms);
    setColorArray(colors);
    setTexCoordArray(texCoords);
    setIndexArray(indexes);
    setEdgeFlagArray(edgeFlags);
    setRangeArray(ranges);

    // Default draw goes through all of the elements in the array
    setDrawArrays(0, _verts.getSize()); 
  }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Copy constructor - protected so it can only be 
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  GenPrimGL(const GenPrimGL &gp)
    : RenderableState(gp),
      _primType(gp._primType),
      _verts(gp._verts),         _norms(gp._norms),     _colors(gp._colors),
      _texCoords(gp._texCoords), _indexes(gp._indexes), _edgeFlags(gp._edgeFlags),
      _ranges(gp._ranges), _release(gp._release)
  {}
	
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Clone the GenPrimGL
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  GenPrimGL * clone() { return new GenPrimGL( *this ); }
  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Destructor
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual ~GenPrimGL() {} 

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Draw first+count-1 elements of the _verts array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setDrawArrays(int first, int count) 
  { setDrawType(DRAW_ARRAYS, first, count); }
  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Draw count primitives defined in the indicies array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setDrawElements(int count, std::vector< unsigned int > indicies)
  { setDrawType(DRAW_ELEMENTS, 0, count, 0, 0, 0, indicies); }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Draw a range of primitives defined in the indicies array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setDrawRangeElements(int start, int end, int count, 
			    std::vector< unsigned int > indicies)
  { setDrawType(DRAW_RANGE_ELEMENTS, 0, count, 0, start, end, indicies); }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Draw the elements in the indicies array 
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setDrawArrayElements(std::vector< unsigned int > indicies)
  { setDrawType(DRAW_ARRAY_ELEMENT, 0, 0, 0, 0, 0, indicies); }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Draw the vertex arrays using default GL calls for debug
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setDrawDebug(std::vector<unsigned int> indicies)
  { setDrawType(DRAW_DEBUG, 0, 0, 0, 0, 0, indicies); }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Set how the arrays should be drawn
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setDrawType(int drawMethod, int first = 0, int count = 0, int last = 0, 
		   int start = 0, int end = 0, 
		   std::vector<unsigned int> indicies = std::vector<unsigned int>(0))
  {
    _drawMethod = drawMethod;
    _first = first;
    _count = count;
    _last = last;
    _start = start;
    _end = end;
    _indicies = indicies;
  } 

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Draw calls the drawDef
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void drawVertexArrays() 
  { 
    // Bind all arrays
    if(!isBound())
      {
	bindDef();
	_release = true;
      }
    else
      _release = false;

    // Draw the arrays
    drawVertexDef();

    // Release all arrays
    if(_release)
      releaseDef();

  }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Print out the state of the vertex arrays
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void printState()
  {
    std::cout << "%~%~%~ GenPrimGL State: ~%~%~%" << std::endl;
    std::cout << "gl primitive type: " << _primType << std::endl;
    std::cout << "verts size: " << _verts.getArray().dim(0) << std::endl;
    std::cout << "colors size: " << _colors.getArray().dim(0) << std::endl;
    std::cout << "norms size: " << _norms.getArray().dim(0) << std::endl;
    std::cout << "idx size: " << _indexes.getArray().dim(0) << std::endl;
    std::cout << "tex coords size: " << _texCoords.size() << std::endl;
    for(unsigned int i = 0; i < _texCoords.size(); i++)
      std::cout << "tex coords[" << i << "] size: " 
		<< _texCoords[i].getArray().dim(0) << std::endl; 
    std::cout << "range size: " << _ranges.getArray().dim(0) << std::endl;
    std::cout << "%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%" << std::endl << std::endl;
  }
  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Set and get the vertex array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~// 
  void setVertexArray(VrtArry verts) { _verts = verts; }
  void setVertexArray(ArrayOwn1< VrtStorageType > verts,
		      int stride = 0, int start = 0)
  { _verts = VrtArry(verts, stride, start); }
  void setVertexArray(std::vector< VrtStorageType> verts, int stride = 0, int start = 0)
  { 
    ArrayWrap1< VrtStorageType > v = ArrayWrap1< VrtStorageType >(verts.size(), 
									      &verts[0]);
    this->setVertexArray(v, stride, start);
  }


  VrtArry getVertexArray() { return _verts;}
  VrtArry* getVertexArray() const { return &_verts; }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Set and get the normal array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setNormalArray(NrmArry norms) { _norms = norms; }
  void setNormalArray(ArrayOwn1< NrmStorageType > norms, 
		    int stride = 0, int start = 0)
  { _norms = NrmArry(norms, stride, start); }
  void setNormalArray(std::vector<NrmStorageType> norms, int stride = 0, int start = 0)
  {
    ArrayWrap1< NrmStorageType > n = ArrayWrap1< NrmStorageType >(norms.size(),
									      &norms[0]);
    this->setNormalArray(n, stride, start);
  }

  NrmArry getNormArray() { return _norms; }
  NrmArry * getNormArray() const { return &_norms; }
  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Set and get the color array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setColorArray(ClrArry colors) { _colors = colors; }
  void setColorArray(ArrayOwn1< ClrStorageType > colors, 
		     int stride = 0, int start = 0)
  { _colors = ClrArry(colors, stride, start); }
  void setColorArray(std::vector< ClrStorageType > colors, int stride = 0, int start = 0)
  {
    ArrayWrap1< ClrStorageType > c = ArrayWrap1< ClrStorageType >(colors.size(),
									      &colors[0]);
    this->setColorArray(c, stride, start);
  }

  ClrArry getColorArray() { return _colors; }
  ClrArry* const getColorArray() const { return &_colors; }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Set and get the texture array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setTexCoordArray(int tx, TxCArry texCoords) { _texCoords[tx] = texCoords; }
  void setTexCoordArray(int tx, ArrayOwn1< TxCStorageType > texCoords, 
			int stride = 0, int start = 0)
  { _texCoords[tx] = TxCArry(texCoords, stride, start); }
  void setTexCoordArray(std::vector< std::vector<TxCStorageType> > texCoords, 
			int stride = 0,	int start = 0)
  {
    for(unsigned int i = 0; i < texCoords.size(); i++)
      {
	ArrayWrap1< TxCStorageType > t = ArrayWrap1< TxCStorageType>(texCoords[i].size(),
										  &texCoords[i][0]);
	this->setTexCoordArray(i, t, stride, start);
      }
  }

  TxCArry getTexCoordArray(int tx=0) { return _texCoords[tx]; }
  TxCArry* const getTexCoordArray(int tx) const { return &_texCoords[tx]; }

  /// Get the number of texture coordinate arrays
  int getNumTCoords() const { return _texCoords.size(); }

  /// Create n more sets of texture coordinates
  void addTexCoords(int numCoords = 1)
  {
    int last = 0;
    if(!_texCoords.empty())
      {
	last = _texCoords.back().getTexCoordNum() + 1;
      }
    for(int i = 0; i < numCoords; i++)
      {
	TxCArry newTex = TxCArry();
	newTex.setTexCoordNum(last+i);
	_texCoords.push_back(newTex);
      }

  }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Set and get the index array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setIndexArray(IdxArry indexes) { _indexes = indexes; }
  void setIndexArray(ArrayOwn1< IdxStorageType > indexes, 
		     int stride = 0, int start = 0)
  { _indexes = IdxArry(indexes, stride, start); }
  void setIndexArray(std::vector< IdxStorageType > indexes,
		     int stride = 0, int start = 0)
  {
    ArrayWrap1<IdxStorageType> i = ArrayWrap1<IdxStorageType>( indexes.size(),
									   &indexes[0]);
    this->setIndexArray(i, stride, start);
  }

  IdxArry getIndexArray() { return _indexes; }
  IdxArry* const getIndexArray() const { return &_indexes; }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Set and get the edge flag array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setEdgeFlagArray(EdgeFlagArray edgeFlags) { _edgeFlags = edgeFlags; }
  void setEdgeFlagArray(ArrayOwn1< GLboolean > edgeFlags,
			int stride = 0, int start = 0)
  { _edgeFlags = EdgeFlagArray(edgeFlags, stride, start); }
  void setEdgeFlagArray(std::vector< GLboolean > edgeFlags,
			int stride = 0, int start = 0)
  {
    ArrayWrap1<GLboolean> e = ArrayWrap1<GLboolean>(edgeFlags.size(), 
								&edgeFlags[0]);
    this->setEdgeFlagArray(e, stride, start);
  }


  EdgeFlagArray getEdgeFlgArray() { return _edgeFlags; }
  EdgeFlagArray* const getEdgeFlagArray() const { return &_edgeFlags;}


  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Set and get the range array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setRangeArray(RngArry ranges) { _ranges = ranges; }
  void setRangeArray(ArrayOwn1< RngStorageType > ranges, 
		     int stride, int start)
  { _ranges =  RngArry(ranges, stride, start); }
  void setRangeArray(std::vector<RngStorageType> ranges, 
		     int stride = 0, int start = 0)
  {
    ArrayWrap1< RngStorageType > r = ArrayWrap1<RngStorageType>(ranges.size(),
									    &ranges[0]);
    this->setRangeArray(r, stride, start);
  }

  RngArry getRangeArray() { return _ranges; }
  RngArry* const getRangeArray() const { return &_edgeFlags; }

  /// number of valid ranges
  unsigned int  getNumRange() const { return _ranges.getValidSize(); }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Set the indicies to draw
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setIndicies(std::vector<unsigned int> indicies)
  { _indicies = indicies; }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Get the indicies
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  std::vector< unsigned int > getIndicies() const { return _indicies; }
  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  //$$$$$$$$$$$$$$ FIXME $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
  /// Draw a range of elements from the index array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void drawRange(unsigned int rangeIdx)
  {
    std::cout << "FIXME: GenPrimGL::drawRange(unsignded int rangeIdx)" << std::endl;
    std::cout << "I'm pretty sure the draw range is screwed up and needs to be fixed." 
	      << "To this end I wonder why the range is set as a drawAttrib?" << std::endl;
  
    int start = _ranges.getArray()[rangeIdx][0];
    drawRange(0, start);
  }

  ///  index start and finish ranges 
  void drawRange(unsigned int startIdx, unsigned int finIdx)
  {     
    std::cout << "FIXME: GenPrimGL::drawRange(unsignded int startIdx, uint finIdx)" << std::endl;

    if(!isBound()) 
      { 
	bindDef(); 
	_release = true; 
      } 
    else /// bound externally, release should be external. 
      _release = false; 
    
    // Get the number of elements to darw
    int numElts = _ranges.getArray()[startIdx][1] - _ranges.getArray()[finIdx][0];
    
    //_count ins't quite right here
    glDrawElements(_primType, numElts*_count, GL_UNSIGNED_INT,
		   &(_indicies[startIdx]) );
    if(_release) 
      releaseDef(); 
    
    glErr("drawRange()");
  }
  //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$//
  
  

  protected:
 
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Equals operator
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  GenPrimGL &operator=(const GenPrimGL &gp)
  {
    RenderableState::operator=(gp);
    _primType = gp._primType;
    _verts = gp._verts;
    _norms = gp._norms;
    _colors = gp._colors;
    _texCoords = gp._texCoords;
    _indexes = gp._indexes;
    _edgeFlags = gp._edgeFlags;
    return *this;
  }

 //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Bind all enabled arrays
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual void bindDef()
  { 
    // Enable the arrays (glArrayAttribs will check if they are active)
    _verts.enable(); 
    _norms.enable();
    _colors.enable();
    _indexes.enable();

    // Enable all of the textures
    for(unsigned int i = 0; i < _texCoords.size(); ++i) 
      {	 
	if(_texCoords[i].isActive()) 
	    _texCoords[i].enable();       
      }
  } 
  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Disable all enabled arrays
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual void releaseDef()
  {
    // Disable the arrays (glArrayAttrib will check if they are active)
    _verts.disable(); 
    _norms.disable();
    _colors.disable();
    _indexes.disable();
   
    // Disable all of the textures
    for(unsigned int i = 0; i < _texCoords.size(); ++i) 
      { 
	if(_texCoords[i].isActive()) 
	  _texCoords[i].disable(); 
      } 

    // Set that we are released
    _release = true; 
  }

  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Drawdef draws the arrays based on the draw method.
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual void drawVertexDef()
  { 


    // Based on the draw method, draw the arrays
    switch(_drawMethod)
      {
      case DRAW_ARRAYS:
	// Draw the arrays
	glDrawArrays(_primType, _first, _count);
	break;
      case DRAW_ARRAY_ELEMENT:
	// Draw the elements in the passed in array
	glBegin(_primType);
	for(unsigned int i = 0; i < _indicies.size(); i++)
	  glArrayElement(_indicies[i]);
	glEnd();
	break;
      case DRAW_ELEMENTS:
	// Draw the number of elements specified by count whose indices are stored
	// in the indicies array
	glDrawElements(_primType, _count, GL_UNSIGNED_INT, &_indicies[0]);
	break;
      case DRAW_RANGE_ELEMENTS:
	// Draw a range of array elements
	glDrawRangeElements(_primType, _start, _end, _count, GL_UNSIGNED_INT, 
			    &_indicies[0]);
	break;
      case DRAW_DEBUG:
	glColor4f(0.0, 0.0, 0.0, 1.0);
	glBegin(_primType);
	{
	  for(unsigned int i = 0; i < _indicies.size(); i++)
	    glVertex3fv(_verts.getArrayPos(_indicies[i]).v);
	}
	glEnd();
	break;
      }
  }
  
 
private:
  
  /// The GL primitive type
  GLenum _primType;

  /// The vertex array
  VrtArry _verts;

  /// The normal array
  NrmArry _norms;

  /// The color array
  ClrArry _colors;

  /// The vector of texture coords arrays
  std::vector<TxCArry> _texCoords;

  /// The index array
  IdxArry _indexes;

  /// The edge flag array
  EdgeFlagArray _edgeFlags;

  /// The range array
  RngArry _ranges;

  /// Flag to release the arrays
  bool _release;

  /// The method to draw the array 
  int _drawMethod;

  /// The first array element to draw
  int _first;

  /// The number of elements to draw
  int _count;

  /// The last element to draw
  int _last;

  /// The start of the range
  int _start;
  
  /// The end of the range
  int _end;

  /// An array of elements to draw
  std::vector< unsigned int > _indicies;

  };

#endif
