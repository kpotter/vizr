// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// ColorDefines.h
// Written by Kristi Potter, 2013
// Definitions of colors
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _COLOR_DEFINES_H
#define _COLOR_DEFINES_H
#include "Color.h"

// -:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:- //
// Color Constants
// -:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:- //
  
// RGB
const ColorF black            = ColorF(0.f, 0.f, 0.f, 1.f);
const ColorF blue             = ColorF(0.f, 0.f, 1.f, 1.f);
const ColorF green            = ColorF(0.f, 1.f, 0.f, 1.f);
const ColorF lime             = ColorF(.2f, .8f, .2f, 1.f);
const ColorF seagreen         = ColorF(.18, .75, .34, 1.f);
const ColorF red              = ColorF(1.f, 0.f, 0.f, 1.f);
const ColorF white            = ColorF(1.f, 1.f, 1.f, 1.f);
const ColorF yellow           = ColorF(1.f, .96f, 0.f, 1.f);
const ColorF olivegreen       = ColorF(.33, 0.5, 0.f, 1.f);
const ColorF cornflower       = ColorF(.33, 0.5, 1.f, 1.f);
const ColorF purple           = ColorF(.33, 0.f, .56, 1.f);
const ColorF purple2          = ColorF(.57, .17, .93, 1.f);
const ColorF dkpurple         = ColorF(.33, .1,  .55, 1.f);
const ColorF seafoam          = ColorF(.33, 1.f, .56, 1.f);
const ColorF spring           = ColorF(0.f, 1.f, .56, 1.f);
const ColorF lemon            = ColorF(1.f, 1.f, .56, 1.f);
const ColorF royalblue        = ColorF(0.f, 0.f, .56, 1.f);
const ColorF gray             = ColorF(0.5f, .5f,.5f, 1.f);
const ColorF grey             = ColorF(.05f, .05f, .02f, 1.f);
const ColorF ltgray           = ColorF(.95f, .95f,.95f, 1.f);
const ColorF ltgrey           = ColorF(.85f, .85f,.85f, 1.f);

const ColorF dkgray           = ColorF(.45f, .45f,.45f, 1.f);
const ColorF dkgrey           = ColorF(.15f, .15f, .15f, 1.f);
const ColorF dkgreengray      = ColorF(.15f, .2f,.15f, 1.f);
const ColorF dkblue           = ColorF(0.f, 0.f, 0.5, 1.f);
const ColorF ltsalmon         = ColorF(1.f, .63, .48, 1.f);
const ColorF orange           = ColorF(1.0, .65, 0.f, 1.f);
const ColorF maroon           = ColorF(69.0, .19, .38, 1.f);
const ColorF cyan             = ColorF(.87, 1.0, 1.f, 1.f);
const ColorF dkbluegray       = ColorF(.41, .41, .51, 1.f); 
const ColorF honeydew         = ColorF(.94, 1.f, .94, 1.f);
const ColorF slateblue        = ColorF(.51, .43, 1.f, 1.f);
const ColorF opaque           = ColorF(1.f, 1.f, 1.f, 1.f);
const ColorF transp           = ColorF(0.f, 0.f, 0.f, 0.f);
const ColorF charcoal         = ColorF(.20, .20, .20, 0.f);

const ColorF persianGreen     = ColorF(0.f, .64f, .51f);
const ColorF blazeOrange      = ColorF(1.f, .39, 0.f);
const ColorF blazeOrangeLt    = ColorF(1.f, 189/255.f, 152/255.f);

const ColorF flushMahogany    = ColorF(191/255.f, 58/255.f, 48/255.f);
const ColorF flushMahoganyLt  = ColorF(191/255.f, 165/255.f, 163/255.f);

const ColorF hokeyPokey       = ColorF(191/255.f, 169/255.f, 48/255.f);
const ColorF hokeyPokeyLt     = ColorF(191/255.f, 184/255.f, 144/255.f);
const ColorF minsk            = ColorF(68/255.f, 43/255.f, 131/255.f);
const ColorF minskLt          = ColorF(115/255.f, 109/255.f, 131/255.f);

// HSV colors
const ColorF green_hsv          = ColorF(125.0, 1.f, 1.f,   1.f);
const ColorF blue_hsv           = ColorF(230.0, 1.f, 1.f,   1.f);
const ColorF red_hsv            = ColorF(0.f,   1.f, 1.f,   1.f);
const ColorF purple_hsv         = ColorF(261.f, 1.f, 0.97f, 1.f);



const ColorF colorList[] = { ColorI( 81, 41, 40, 255).toFloat(),  ColorI( 0, 123, 0, 255).toFloat(), minsk, ColorI( 255, 100, 0, 255).toFloat(), ColorI( 0, 0, 0, 255).toFloat(), ColorI( 0, 0, 0, 255).toFloat()};
const ColorF colorListLight[] = { ColorI(255, 141, 135, 255).toFloat(),  ColorI( 0, 123, 0).toFloat(), minsk, ColorI( 255, 100, 0).toFloat(), ColorI( 0, 0, 0).toFloat(), ColorI( 0, 0, 0).toFloat()};

// ColorI( 58, 36, 180, 255).toFloat(),  ColorI( 250, 250, 255).toFloat()
//const ColorF colorList[] = {flushMahogany, hokeyPokey, minsk, blazeOrange, persianGreen, charcoal, slateblue};
//const ColorF colorListLight[] = {flushMahoganyLt, hokeyPokeyLt, minskLt, blazeOrangeLt, white, white, white};




#endif
