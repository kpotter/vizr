// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// ColorArray.h
// Written by Kristi Potter, April 2009           
// A color array implementation
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _COLOR_ARRAY_H
#define _COLOR_ARRAY_H

#include "GLArrayAttribs.h"

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
/// A GL color array
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template< class StorageType, int Tuple, int GLType >
class ColorArray : public GLArrayAttribs< StorageType >
{
public:
  
  /// Make the storage type of the array accessible 
  typedef StorageType CStorageType;


  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Constructor initially sets the array to active
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  ColorArray(ArrayOwn1< StorageType > array = 
	     ArrayOwn1< StorageType >(),
	     int stride = 0, int start = 0) 
    : GLArrayAttribs< StorageType >(array, stride, start) {} 

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Copy constructor
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  ColorArray(const ColorArray &va) 
    :  GLArrayAttribs< StorageType > (va) {}

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  // Equals operator
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  ColorArray&operator=(const ColorArray & va)
  {
    GLArrayAttribs< StorageType >::operator=(va);
    return *this;
  }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Destructor
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual ~ColorArray() {}

 protected:

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Enable the color array and set the pointer
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual void enableDef()
  { 
    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(Tuple, GLType, this->getStride(), this->getStartPointer());
    glErr("enableDef()");
  }
  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Disable the color array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual void disableDef()
  {
    glDisableClientState(GL_COLOR_ARRAY);
    glErr("disbleDef()");
  }

}; 

// typedef normally used color arrays
typedef ColorArray< char,        1, GL_BYTE > ColorArray1B;
typedef ColorArray< Vector2b, 2, GL_BYTE > ColorArray2B;
typedef ColorArray< Vector3b, 3, GL_BYTE > ColorArray3B;
typedef ColorArray< Vector4b, 4, GL_BYTE > ColorArray4B;

typedef ColorArray< unsigned char, 1, GL_UNSIGNED_BYTE > ColorArray1UB;
typedef ColorArray< Vector2ub , 2, GL_UNSIGNED_BYTE > ColorArray2UB;
typedef ColorArray< Vector3ub , 3, GL_UNSIGNED_BYTE > ColorArray3UB;
typedef ColorArray< Vector4ub , 4, GL_UNSIGNED_BYTE > ColorArray4UB; 

typedef ColorArray< short,       1, GL_SHORT > ColorArray1S;
typedef ColorArray< Vector2s, 2, GL_SHORT > ColorArray2S;
typedef ColorArray< Vector3s, 3, GL_SHORT > ColorArray3S;
typedef ColorArray< Vector4s, 4, GL_SHORT > ColorArray4S;

typedef ColorArray< unsigned short, 1, GL_UNSIGNED_SHORT > ColorArray1US;
typedef ColorArray< Vector2us,   2, GL_UNSIGNED_SHORT > ColorArray2US;
typedef ColorArray< Vector3us,   3, GL_UNSIGNED_SHORT > ColorArray3US;
typedef ColorArray< Vector4us,   4, GL_UNSIGNED_SHORT > ColorArray4US;

typedef ColorArray< int,         1, GL_INT > ColorArray1I;
typedef ColorArray< Vector2i, 2, GL_INT > ColorArray2I;
typedef ColorArray< Vector3i, 3, GL_INT > ColorArray3I;
typedef ColorArray< Vector4i, 4, GL_INT > ColorArray4I;

typedef ColorArray< unsigned int, 1, GL_UNSIGNED_INT > ColorArray1UI;
typedef ColorArray< Vector2ui, 2, GL_UNSIGNED_INT > ColorArray2UI;
typedef ColorArray< Vector3ui, 3, GL_UNSIGNED_INT > ColorArray3UI;
typedef ColorArray< Vector4ui, 4, GL_UNSIGNED_INT > ColorArray4UI;

typedef ColorArray< float,       1, GL_FLOAT > ColorArray1F;
typedef ColorArray< Vector2f, 2, GL_FLOAT > ColorArray2F;
typedef ColorArray< Vector3f, 3, GL_FLOAT > ColorArray3F;
typedef ColorArray< Vector4f, 4, GL_FLOAT > ColorArray4F;

typedef ColorArray< double,      1, GL_DOUBLE > ColorArray1D;
typedef ColorArray< Vector2d, 2, GL_DOUBLE > ColorArray2D;
typedef ColorArray< Vector3d, 3, GL_DOUBLE > ColorArray3D;
typedef ColorArray< Vector4d, 4, GL_DOUBLE > ColorArray4D;

#endif
