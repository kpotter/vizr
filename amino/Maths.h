// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// Maths.h
// Written by Kristi Potter, 2010
// Define useful math operations
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#ifndef _MATHS_H
#define _MATHS_H

#include <iostream>
#include <limits>
#include <math.h>
#ifndef M_PI
#define M_PI 3.14159
#endif
#include "Vector.h"

// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Definitions of some common math stuff
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ // 
template<class Type>                                               // abs()
inline const Type abs(const Type &x) { return (x > 0 ? x : -x); }
template<class TypeIn, class TypeOut>                              // affine()
inline TypeOut affine(const TypeIn &i,  const TypeIn &val, const TypeIn &I, const TypeOut &o, const TypeOut &O){
    if (i == I) return O; else return (O - o)*(val - i) / (I - i) + o; }
template<class Type>                                               // sign()
inline Type sign(const Type &x) { return (x > 0 ? 1.0 : (x < 0) ? -1.0 : 0.0); }
template<class Type>                                               // cbrt()
inline Type cbrt(const Type &x) { return pow(x, 1.0/3.0); }
template<class Type>                                               // rad2deg()
inline Type rad2deg(const Type &x) { return (180.0/M_PI*x); }
template<class Type>                                               // deg2rad()
inline Type deg2rad(const Type &x) { return (M_PI/180.0*x); }
inline float frand()  { return rand()/(float)RAND_MAX; }           // frand()
inline double drand() { return rand()/(double)RAND_MAX; }          // drand()
inline char  crand()  { return rand()/int(RAND_MAX/255); }         // crand()
template<class Type>                                               // sinD() ~ sin in degrees
inline Type sinD(const Type &x){ return sin(deg2rad(x)); }   
template<class Type>                                               // cosD() ~ cos in degrees
inline Type cosD(const Type &x){ return cos(deg2rad(x)); }   
template<class Type>                                               // tanD() ~ tan in degrees
inline Type tanD(const Type &x){ return tan(deg2rad(x)); }   
template<class Type>                                               // minOf2()
inline Type minOf(const Type &x, const Type &y) { return (x < y ? x : y); } 
template<class Type>                                               // maxOf2()
inline Type maxOf(const Type &x, const Type &y) { return (x > y ? x : y); }
template <class Type>                                              // maxIndexOf4()
inline int maxIndexOf4(const Type x, const Type y, const Type z, const Type w)
{ return (x >= y ? (x >= z ? (x >= w ? 0 : 3) :  (z >= w ? 2 : 3)) : (y >= z ? (y >= w ? 1 : 3) : (z >= w ? 2 : 3))); }
template <class Type>                                              // maxOf4()
inline Type maxOf4 (const Type x, const Type y, const Type z, const Type w)
{ return (x >= y ? (x >= z ? (x >= w ? x : w) : (z >= w ? z : w)) : (y >= z ? (y >= w ? y : w) : (z >= w ? z : w))); }

// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Calculate the normal of 3 pts
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
template <class T>
inline Vec<T, 3> calcNorm(Vec<T, 3> p1, Vec<T, 3> p2, Vec<T, 3> p3) {

    Vec<T, 3> norm;
    Vec<T, 3> U = p2 - p1;
    Vec<T, 3> V = p3 - p1;
    norm.x(U.y() * V.z() - U.z() * V.y());
    norm.y( U.z() * V.x() - U.x() * V.z());
    norm.z(U.x() * V.y() - U.y() * V.x());
    T length = norm.length();
    norm = norm/length;
    return norm;
}

// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Ray intersections
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //

// --- Ray/plane intersection. --- //
// Returns infinity if no hit.
template< class Type >
inline Type intersectRayPlane(const Vec<Type,3> &rpos, const Vec<Type,3> &rdir,
                  const Vec<Type,3> &ppos, const Vec<Type,3> &pnorm)
{ const Type rdDpn = rdir.dot(pnorm);
  if(!rdDpn) return std::numeric_limits<Type>::max();
  return ( ppos.dot(pnorm) - rpos.dot(pnorm) )/rdDpn;
}

// --- Line/plane intersection. --- //
// Returns infinity if no hit (ie. line and plane are parallel)
// Returns [0--1] if line intersects plane between endpoints (lpt0, lpt1)
// Return < 0 if line intersects before lpt0, > 1 if after lpt1
template < class Type >
inline Type linePlaneIntersect( const Vec<Type,3> &lpt0, const Vec<Type,3> &lpt1,
                const Vec<Type,3> &ppos, const Vec<Type,3> &pnorm )
{ // pnorm.dot( lpt1 - lpt0 )
  const Type rdDpn = (pnorm.x() * (lpt1.x() - lpt0.x()) + 
		      pnorm.y() * (lpt1.y() - lpt0.y()) + 
		      pnorm.z() * (lpt1.z() - lpt0.z()));
  if( ! rdDpn ) return std::numeric_limits<Type>::max();
  /// pnorm.dot( ppos - lpt0 ) / pnorm.dot( lpt1 - lpt0 )
  return ( pnorm.x() * (ppos.x() - lpt0.x()) + 
	   pnorm.y() * (ppos.y() - lpt0.y()) + 
	   pnorm.z() * (ppos.z() - lpt0.z())   ) / rdDpn;
}

// --- Ray/sphere intersection. --- //
// Returns infinity if no hit, otherwise returns a Vector2 containing the
// min and max t's for the intersection in that order
template< class Type >
inline Vec<Type,2> intersectRaySphere(const Vec<Type,3> &rpos, const Vec<Type,3> &rdir,
                    const Vec<Type,3> &cent, const Type r)
{ Vec<Type,3> x = rpos - cent;
  Type a = rdir.dot(rdir);
  Type b = rdir.dot(x) * 2;
  Type c = x.dot(x) - r*r;
  Type d = b*b - 4*a*c;
  if( d <= 0 ) return Vec<Type,2>(std::numeric_limits<Type>::max());
  return Vec<Type,2>((-b-sqrt(d))/(2*a), (-b+sqrt(d))/(2*a));
}

// --- Minimum distance between two lines. --- //
// where the lines are described by a point and a ray.
// Returns a vec3 where [0] is the min distance, [1] is the 
// t value for line 1, and [2] is the t value for line 2
template< class Type >
inline Vec<Type,3> getMinLineDistance( const Vec<Type,3> &l1pt, const Vec<Type,3> &l1v,
                      const Vec<Type,3> &l2pt, const Vec<Type,3> &l2v )
{ Vec<Type,3> w = l1pt - l2pt;
  Type a = l1v.dot( l1v );
  Type b = l1v.dot( l2v );
  Type c = l2v.dot( l2v );
  Type d = l1v.dot( w );
  Type e = l2v.dot( w );
  Type D = a*c - b*b;
  Type t1, t2;

  if ( D < Type(.00000001) ){ t1 = Type(0.0); t2 = ( b<c ? d/b : e/c ); }
  else { t1 = (b*e - c*d)/D; t2 = (a*e - b*d)/D; }
  Vec<Type,3> dl = w + (t1*l1pt) - (t2*l2pt);
  return Vec<Type,3>( dl.norm(), t1, t2 );
}
#endif
