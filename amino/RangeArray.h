// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// RangeArray.h
// Written by Kristi Potter, April 2009           
// A vertex array implementation
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _RANGE_ARRAY_H
#define _RANGE_ARRAY_H

#include "GLArrayAttribs.h"

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
/// A range array
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template< class StorageType, int Tuple, int GLType >
class RangeArray : public GLArrayAttribs< StorageType >
{
public:

  /// Make the storage type of the array accessible 
  typedef StorageType RStorageType;

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Constructor initially sets the array to active
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  RangeArray(ArrayOwn1< StorageType > array = ArrayOwn1< StorageType >(),
	     int stride = 0, int start = 0) 
    : GLArrayAttribs< StorageType >(array, stride, start) {} 

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Copy constructor
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  RangeArray(const RangeArray &va) 
    :  GLArrayAttribs< StorageType > (va) {}

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  // Equals operator
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  RangeArray &operator=(const RangeArray< StorageType, Tuple,  GLType>  &va)
  {
    GLArrayAttribs< StorageType >::operator=(va);
    return *this;
  }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Destructor
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual ~RangeArray() {}

 protected:

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Enable the range array and set the pointer
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual void enableDef()
  { 
    glErr("enableDef()");
  }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Disable the range array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual void disableDef()
  {
    glErr("disbleDef()");
  }

}; 

// typedef normally used range arrays
typedef RangeArray< unsigned int, 1, GL_UNSIGNED_INT > RangeArray1UI;
typedef RangeArray< Vector2ui, 2, GL_UNSIGNED_INT > RangeArray2UI;
typedef RangeArray< Vector3ui, 3, GL_UNSIGNED_INT > RangeArray3UI;
typedef RangeArray< Vector4ui, 4, GL_UNSIGNED_INT > RangeArray4UI;

#endif
