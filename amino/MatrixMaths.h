// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// MatrixMaths.h
// Written by Kristi Potter, 2010
// Math on matrices
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _MATRIX_MATHS_H
#define _MATRIX_MATHS_H

#include <iostream>
#include <limits>
#include <math.h>
#include <Vector.h>
#include <Matrix.h>
#include <Quaternion.h>


// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Trackball for rotations
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Helper function for the trackball
template <class Type>
Type tb_project_to_sphere(const Type r, const Type x, const Type y)
{ Type z;
  const Type d = sqrtf(x*x + y*y);
  /* Inside sphere */
  if (d < r * 0.70710678118654752440) z = sqrtf(r*r - d*d);
  /* On hyperbola */
  else { const Type t = r / 1.41421356237309504880f; z = t*t / d; }
  return z;
}
template <class Type>
Quaternion<Type>  trackball(const Vec<Type,2> & pt1, const Vec<Type,2> & pt2, const Type trackballsize)
{
  // std::cout << "TRACKBALL" << std::endl;

  //std::cout<< "pt1: " << pt1 <<  " pt2: " << pt2 << " size: " << trackballsize << std::endl;

  Vec<Type,3> a; // Axis of rotation
  Type phi;  // how much to rotate about axis
  Vec<Type,3> d;
  Type t;
  // Zero rotation
  if (pt1 == pt2) return Quatf();
  
  // First, figure out z-coordinates for projection of P1 and P2 to
  // deformed sphere
  Vec<Type,3> p1(pt1.x(),pt1.y(),tb_project_to_sphere(trackballsize,pt1.x(),pt1.y()));
  Vec<Type,3> p2(pt2.x(),pt2.y(),tb_project_to_sphere(trackballsize,pt2.x(),pt2.y()));
  
  //std::cout << "p1: " << p1 << " p2: " << p2 << std::endl;

  //  Now, we want the cross product of P1 and P2
  a = cross(p1,p2);
  
  //std::cout << "a: " <<  a << std::endl;

  //  Figure out how much to rotate around that axis.
  d = p1 - p2;
  //std::cout << "d: " << d << std::endl;
  t = d.norm() / (trackballsize);
  // std::cout << "t: " << t << std::endl;
 
  // Avoid problems with out-of-control values...
  if (t > 1)  t = 1;
  if (t < -1) t = -1;
  phi = 2 * asin(t);
  return Quaternion<Type>(phi,a);
}

// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Determinant of 2x2, 3x3 and 4x4 matrices
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
template<class Type> Type determinant(const Matrix<Type, 2> &m)
{ return (m[0]*m[3]-m[1]*m[2]); }
template<class Type> Type determinant(const Matrix<Type, 3> &m)
{ return (m[0]*m[4]*m[8] + m[3]*m[7]*m[2] + m[6]*m[1]*m[5] - m[0]*m[7]*m[5] - m[6]*m[4]*m[2] - m[3]*m[1]*m[8]); }
template<class Type> Type determinant(const Matrix<Type, 4> &m)
{ return (m[0]*m[5]*m[10]*m[15] + m[0]*m[6]*m[11]*m[13] + m[0]*m[7]*m[9]*m[14]  +
	  m[1]*m[4]*m[11]*m[14] + m[1]*m[6]*m[8]*m[15]  + m[1]*m[7]*m[10]*m[12] + 
	  m[2]*m[4]*m[9]*m[15]  + m[2]*m[5]*m[11]*m[12] + m[2]*m[7]*m[8]*m[13]  + 
	  m[3]*m[4]*m[10]*m[13] + m[3]*m[5]*m[8]*m[14]  + m[3]*m[6]*m[9]*m[12]  -
	  m[0]*m[5]*m[11]*m[14] - m[0]*m[6]*m[9]*m[15]  - m[0]*m[7]*m[10]*m[13] - 
	  m[1]*m[4]*m[10]*m[15] - m[1]*m[6]*m[11]*m[12] - m[1]*m[7]*m[8]*m[14]  - 
	  m[2]*m[4]*m[11]*m[13] - m[2]*m[5]*m[8]*m[15]  - m[2]*m[7]*m[9]*m[12]  - 
	  m[3]*m[4]*m[9]*m[14]  - m[3]*m[5]*m[10]*m[12] - m[3]*m[6]*m[8]*m[13]);}
											
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
// Inverse of 2x2, 3x3 and 4x4 matrices
// ~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~@~ //
template<class Type>
Matrix<Type, 2> inverse(const Matrix<Type, 2> &m)
{ Type det = determinant(m);
  if(det == 0) { std::cerr << "ERROR! inverse(Matrix<Type,2>): zero determinant! Returning original matrix." << det << std::endl; return m; }
  Matrix<Type, 2> inv(m[3], -m[1], -m[2], m[0]); return 1/det*inv; 
}
template<class Type>
Matrix<Type, 3> inverse(const Matrix<Type, 3> &m)
{ Type det = determinant(m);
  if(det == 0) { std::cerr << "ERROR! inverse(Matrix<Type,3>): zero determinant! Returning original matrix." << det << std::endl; return m; }
  Matrix<Type, 3>inv(m[4]*m[8]-m[5]*m[7], m[2]*m[7]-m[1]*m[8], m[1]*m[5]-m[2]*m[4],
		     m[5]*m[6]-m[3]*m[8], m[0]*m[8]-m[2]*m[6], m[2]*m[3]-m[0]*m[5],
		     m[3]*m[7]-m[4]*m[6], m[1]*m[6]-m[0]*m[7], m[0]*m[4]-m[1]*m[3]);
  return 1/det*inv;
}
template<class Type>
Matrix<Type, 4>inverse(const Matrix<Type, 4> &m)
{ Type det = determinant(m);
  if(det == 0) { std::cerr << "ERROR! inverse(Matrix<Type,4>): zero determinant! Returning original matrix." << det << std::endl; return m; }
  Type m0 =  m[6]*m[11]*m[13] - m[7]*m[10]*m[13] + m[7]*m[9]*m[14] - m[5]*m[11]*m[14] - m[6]*m[9]*m[15] + m[5]*m[10]*m[15];
  Type m1 =  m[3]*m[10]*m[13] - m[2]*m[11]*m[13] - m[3]*m[9]*m[14] + m[1]*m[11]*m[14] + m[2]*m[9]*m[15] - m[1]*m[10]*m[15];
  Type m2 =  m[2]*m[7]*m[13]  - m[3]*m[6]*m[13]  + m[3]*m[5]*m[14] - m[1]*m[7]*m[14]  - m[2]*m[5]*m[15] + m[1]*m[6]*m[15];
  Type m3 =  m[3]*m[6]*m[9]   - m[2]*m[7]*m[9]   - m[3]*m[5]*m[10] + m[1]*m[7]*m[10]  + m[2]*m[5]*m[11] - m[1]*m[6]*m[11];
  Type m4 =  m[7]*m[10]*m[12] - m[6]*m[11]*m[12] - m[7]*m[8]*m[14] + m[4]*m[11]*m[14] + m[6]*m[8]*m[15] - m[4]*m[10]*m[15];
  Type m5 =  m[2]*m[11]*m[12] - m[3]*m[10]*m[12] + m[3]*m[8]*m[14] - m[0]*m[11]*m[14] - m[2]*m[8]*m[15] + m[0]*m[10]*m[15];
  Type m6 =  m[3]*m[6]*m[12]  - m[2]*m[7]*m[12]  - m[3]*m[4]*m[14] + m[0]*m[7]*m[14]  + m[2]*m[4]*m[15] - m[0]*m[6]*m[15];
  Type m7 =  m[2]*m[7]*m[8]   - m[3]*m[6]*m[8]   + m[3]*m[4]*m[10] - m[0]*m[7]*m[10]  - m[2]*m[4]*m[11] + m[0]*m[6]*m[11];
  Type m8 =  m[5]*m[11]*m[12] - m[7]*m[9]*m[12]  + m[7]*m[8]*m[13] - m[4]*m[11]*m[13] - m[5]*m[8]*m[15] + m[4]*m[9]*m[15];
  Type m9 =  m[3]*m[9]*m[12]  - m[1]*m[11]*m[12] - m[3]*m[8]*m[13] + m[0]*m[11]*m[13] + m[1]*m[8]*m[15] - m[0]*m[9]*m[15];
  Type m10 = m[1]*m[7]*m[12]  - m[3]*m[5]*m[12]  + m[3]*m[4]*m[13] - m[0]*m[7]*m[13]  - m[1]*m[4]*m[15] + m[0]*m[5]*m[15];
  Type m11 = m[3]*m[5]*m[8]   - m[1]*m[7]*m[8]   - m[3]*m[4]*m[9]  + m[0]*m[7]*m[9]   + m[1]*m[4]*m[11] - m[0]*m[5]*m[11];
  Type m12 = m[6]*m[9]*m[12]  - m[5]*m[10]*m[12] - m[6]*m[8]*m[13] + m[4]*m[10]*m[13] + m[5]*m[8]*m[14] - m[4]*m[9]*m[14];
  Type m13 = m[1]*m[10]*m[12] - m[2]*m[9]*m[12]  + m[2]*m[8]*m[13] - m[0]*m[10]*m[13] - m[1]*m[8]*m[14] + m[0]*m[9]*m[14];
  Type m14 = m[2]*m[5]*m[12]  - m[1]*m[6]*m[12]  - m[2]*m[4]*m[13] + m[0]*m[6]*m[13]  + m[1]*m[4]*m[14] - m[0]*m[5]*m[14];
  Type m15 = m[1]*m[6]*m[8]   - m[2]*m[5]*m[8]   + m[2]*m[4]*m[9]  - m[0]*m[6]*m[9]   - m[1]*m[4]*m[10] + m[0]*m[5]*m[10];
  Matrix<Type, 4> inv(m0,m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15);
  return 1/det*inv;
}



#endif
