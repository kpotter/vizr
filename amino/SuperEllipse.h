// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// SuperEllipse.h
// Written by Kristi Potter, October 2006           
// A SuperEllipsoid Implementation
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _GL_SUPER_ELLIPSE_H
#define _GL_SUPER_ELLIPSE_H

#include <Vector>
#include "Maths.h"
#include "SmartPointer.h"
#include "GenPrimGL.h"

//%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-//
/// A typdef'ed version of a GenPrimGL (vertex array).
//%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-//
//typedef GenPrimGL< VertAttribV3D,      // The vertices are double 3's
//		   NormalAttribV3D     // The normals are a double 3's
//		   > GenSuperPrim;

//%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-//
/// Super Ellipse is an ellipsoidal implementation of super quadrics.
//%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-//
class SuperEllipse : public GenPrimGL< VertexArray3D, NormalArrayD >
{
public:
  
  //----------------------------------------------------------------------//
  /// Default constructor. 
  /// Input Parameters: 
  ///  Length of the three axes (double x, y, z)(default x = y = z = 6)
  ///  Exponents to determine shape (double e1, e2)(default e1 = e2 = 1)
  ///  The maximum number of times to sudivide (int depth) (default = 1)
  ///
  /// The constructor
  //----------------------------------------------------------------------//
  SuperEllipse(double x = 6, double y = 6, double z = 6, 
	       double e1 = 1, double e2 = 1, int depth = 2);
  
  /// constructor for compatability with GLUItems 
  SuperEllipse(int,int);

  //----------------------------------------------------------------------//
  /// setAxes changes the length of each axis and recalculates the super 
  /// ellipsoid.
  ///
  /// Input: double a1, double a2, double a3
  ///
  /// Returns: void
  //----------------------------------------------------------------------//
  void setAxes(double a1, double a2, double a3)
  { _a1 = a1; _a2 = a2; _a3 = a3; 
    _calculated = false; 
    delVariables();
    calculateApprox();
  }
  
  //----------------------------------------------------------------------//
  /// setAxes changes the exponents of the super ellipsoid
  ///
  /// Input: double e1, double e2
  ///
  /// Returns: void
  //----------------------------------------------------------------------//
  void setExponents(double e1, double e2)  
  { 
    _e1 = e1; _e2 = e2;
    _calculated = false; 
    delVariables();
    calculateApprox(); 
  }

  // Set the position of the ellipsoid
  void setPosition(Vector3f p) { _position = p; }
  
  // Get the position of the ellipsoid
  Vector3f getPosition() { return _position; }
  
  //----------------------------------------------------------------------//
  /// getPoint takes in a theta and phi, and returns the (x,y,z) point on 
  /// the super ellipsoid.
  /// 
  /// Input:  double theta, double phi
  ///
  /// Return: Vector3d
  //----------------------------------------------------------------------//
  Vector3d getPoint(double theta, double phi);

  //----------------------------------------------------------------------//
  /// getNormal takes in a theta and a phi and returns the normal of the
  ///  (x,y,z) point.
  ///
  /// Input: double theta, double phi
  ///
  /// Return: Vector3d
  //----------------------------------------------------------------------//
  Vector3d getNormal(double theta, double phi);

  //----------------------------------------------------------------------//
  /// Subdivide takes in three (theta, phi) points that define a triangle
  /// This triangle is then subdivided to the proper depth and the new points
  /// are stored in the points vector and the triangles in the triangle vector
  //----------------------------------------------------------------------//
  void subdivide(Vector3d A, Vector3d B, Vector3d C, int depth);
  
  //----------------------------------------------------------------------//
  /// Draw draw the super ellipsoid as a triangluated mesh
  //----------------------------------------------------------------------//
  void draw();
  void drawPoint(const Vector3f &pos, float rad );

  ArrayOwn1< Vector3d > getBoundingBox();

  void delVariables()
  {
    _points.clear();
    _triangles.clear();
    _normals.clear();
  }

 protected:

  // The lengths of the axes in the x, y, and z directions
  double _a1, _a2, _a3;

  // The exponents of the super ellipsoid
  double _e1, _e2;
  
  // The maximum number of times to subdivide
  int _maxSubdivide;

  // The position of the super ellipsoid
  Vector3f _position;

  // The points of the triangular mesh
  std::vector< Vector3d > _points;

  // The edges of the mesh
  std::vector< Vector3i > _triangles;

  // The normals at each point
  std::vector<Vector3d>  _normals;

  // A vertex array of points
  ArrayWrap1< Vector3d > _vertPoints;

  // A vertex array of normals
  ArrayWrap1< Vector3d > _vertNorms;

  // Flag set to false when the super ellipse needs to be calculated
  bool _calculated;

  //----------------------------------------------------------------------//
  /// Calculate the approximation of the superellipse
  //----------------------------------------------------------------------//
  void calculateApprox();
};

//%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-//
/// Typedef'd smart pointer and vector of SuperEllipse.
//%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-//
typedef SmartPointer< SuperEllipse > SuperEllipseSP;
typedef std::vector< SuperEllipseSP > SuperEllipseVec;

#endif
