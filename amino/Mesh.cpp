// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// Mesh.cpp
// Written by Kristi Potter, 2013
// Class to define a mesh
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#include "Mesh.h"
#include "Maths.h"
#include "ColorDefines.h"

// TODO: pass in bounding box to rescale and/or flag on whether to rescale

// * * * Constructor * * * //
Mesh::Mesh(std::vector<Vertex> vertices, std::vector<int> indices,
           std::vector<Vector3f> normals, std::vector<ColorF> colors)
 : _vertices(vertices), _indices(indices), _normals(normals), _colors(colors)
{
  // Find the mesh bounding box
  findBoundingBox();

  // Rescale the mesh to (-1,1) in all dims
  rescaleMesh(BoundingBox(-1, 1, -1, 1, -1, 1));

  // If we have no normals, calculate them
  if(_normals.size() == 0)
    calculateNormals();

  // If we have no colors, color with grey
  if(_colors.size() == 0){
      _colors.resize(_vertices.size());
      setColor(gray);
  }
}

// - - - Find the mesh bounding box - - - //
void Mesh::findBoundingBox(){
  
  // Find the min & max in all 3 dimensions
  float minX, maxX, minY, maxY, minZ, maxZ;
  minX = maxX = _vertices[0].x();
  minY = maxY = _vertices[0].y();
  minZ = maxZ = _vertices[0].z();
  for(unsigned int i = 0; i < _vertices.size(); i++){
      float x = _vertices[i].x();
      float y = _vertices[i].y();
      float z = _vertices[i].z();
    minX = minX < x ? minX : x;
    maxX = maxX > x ? maxX : x;
    minY = minY < y ? minY : y;
    maxY = maxY > y ? maxY : y;
    minZ = minZ < z ? minZ : z;
    maxZ = maxZ > z ? maxZ : z;
  }
  _bBox = BoundingBox(Vertex(minX, minY, minZ), Vertex(maxX, maxY, maxZ));
}

// - - - Rescale the mesh to be within a bounding box - - - //
void Mesh::rescaleMesh(BoundingBox box){
  
  for(unsigned int i = 0; i < _vertices.size(); i++){
      _vertices[i].x(affine(_bBox.min.x(), _vertices[i].x(), _bBox.max.x(), box.min.x(), box.max.x()));
      _vertices[i].y(affine(_bBox.min.y(), _vertices[i].y(), _bBox.max.y(), box.min.y(), box.max.y()));
      _vertices[i].z(affine(_bBox.min.z(), _vertices[i].z(), _bBox.max.z(), box.min.z(), box.max.z()));
  }
}

// - - - Calculate the mesh normals - - - //
void Mesh::calculateNormals(){

    _normals = std::vector<Vector3f>(_vertices.size());
    for(unsigned int i = 0; i < _normals.size(); i++)
        _normals[i] = Vector3f(0,0,0);

    // Go through each face, adding in its normal
    for(unsigned int i = 0; i < _indices.size(); i+=3){

        int x = _indices[i+0];
        int y = _indices[i+1];
        int z = _indices[i+2];

        Vector3f norm = calcNorm(_vertices[y], _vertices[x], _vertices[z]);
        _normals[x] += norm;
        _normals[y] += norm;
        _normals[z] += norm;
    }

    for(unsigned int i = 0; i < _normals.size(); i++){
        _normals[i].x(_normals[i].x() /3.0);
        _normals[i].y(_normals[i].y() /3.0);
        _normals[i].z(_normals[i].z() /3.0);
        _normals[i].normalize();
    }
}

// - - - Set the colors to a single color - - - //
void Mesh::setColor(ColorF color){
    for(unsigned int i = 0; i < _colors.size(); i++)
        _colors[i] = color;
}

