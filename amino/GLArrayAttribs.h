// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// GLArrayAttribs.h
// Written by Kristi Potter, April 2009           
// General attributes for arrays
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#ifndef _GL_ARRAY_ATTRIBS_H
#define _GL_ARRAY_ATTRIBS_H

#include "Arrays.h"
#include "SmartPointer.h"
#include "GlUtil.h"

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
/// General attributes for arrays such as vertex (texture, index, etc)
/// Templated on the type (int, float, etc), the storage type,
/// the size of the array, and the GL enumerated type
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template< class StorageType >
class GLArrayAttribs : public Counted
{
 public:

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Constructor sets activity based on the size of the input Array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  GLArrayAttribs(ArrayOwn1< StorageType > array = 
		 ArrayOwn1< StorageType >(0, StorageType(0)), 
		 int stride = 0, int start = 0, GLboolean normalized = true)
    : _array(array),
      _start(start),
      _stride(stride),
      _size(array.size()),
      _normalized(normalized)
  {
    // If the parameter array has a size, set active
    if(_size > 0)
      activate();
    else
      deactivate();
  }

  GLArrayAttribs(int size, int stride = 0, int start = 0, GLboolean normalized = true)
    : _array(size, StorageType(0)),
      _start(start),
      _stride(stride),
      _size(size),
      _normalized(normalized)
  {
    // If the parameter array has a size, set active
    if(_size > 0)
      activate();
    else
      deactivate();
  }
  

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Copy constructor
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  GLArrayAttribs(const GLArrayAttribs &gaa) 
    :  _active(gaa._active),
       _array(gaa._array), 
       _start(gaa._start),
       _stride(gaa._stride),
       _size(gaa._size),
       _normalized(gaa._normalized)
  {}

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Equals operator
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  GLArrayAttribs &operator=(const GLArrayAttribs &gaa) {
    _active = gaa._active;
    _array = gaa._array; 
    _start = gaa._start;
    _stride = gaa._stride;
    _size = gaa._size;
    _normalized = gaa._normalized;
    return *this;
  }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Destructor
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual ~GLArrayAttribs() {}

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Set the data array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setArray(const ArrayOwn1< StorageType > array) { _array = array; }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Get the data array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  ArrayOwn1< StorageType > getArray() const { return _array; }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Set a value at position i in the array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setArrayPos(int i, const StorageType value) 
  { 
    if(i < _array.size())
      _array[i] = value;
    else
      {
	std::cerr << "glArrayAttribs::ERROR INDEX OUT OF BOUNDS" << std::endl;
	exit(1);
      }
  }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Get a value at position i in the array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  StorageType getArrayPos(int i) const
  {
     if(i < _array.size())
       return _array[i];
    else
      {
	std::cerr << "glArrayAttribs::ERROR INDEX OUT OF BOUNDS" << std::endl;
	exit(1);
      }
  }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Get the size of the array
  /// This is the size of the data array (actual memory space)
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  unsigned int getSize() const { return _array.size(); }
  int getTrueSize() const { return _array.size(); }
  
  /// The array should be bigger than what you need,
  /// if it is a dynamic array, set the size to
  /// indicate how much of the array is valid,
  /// should be -1 if none of it is.
  void setValidSize(unsigned int size) { _size = size; }
  unsigned int getValidSize() const { return _size; }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Enable the active array 
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void enable() {
    if(_active)
      enableDef();
  }
  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Disable the active array 
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void disable() {
    if(_active)
      disableDef();
  }
  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Activate the array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void activate() { setActive(true); }
  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Deactivate the array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void deactivate() { setActive(false); }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Set the active state of the array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setActive(const bool onoff) { _active = onoff; }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Get the active state of the array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  bool isActive() const { return _active; }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Set the stride
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setStride(const int stride) { _stride = stride; }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Get the stride
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  int getStride() const { return _stride; }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Set the starting position in the array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  void setStart(int start) { _start = start; }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Get the starting position in the array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  int getStart() const{ return _start; }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Get the memory address of the first coordinate of the array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  const void * getStartPointer() { return &_array.data()[_start]; }

protected:
  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// PURE VIRTUAL: Actions to take on enable
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual void enableDef()
  {
    /* glEnableVertexAttribArrayARB(_attribNum);
    glVertexAttribPointerARB(_attribNum, DIM, GLTYPE, _norm, 0,_array.data());
    */
    glErr("enableDef()");
  }
  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// PURE VIRTUAL: Actions to take on disable
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual void disableDef()
  {
    //glDisableVertexAttribArrayARB(_attribNum);
    glErr("disableDef()");
  }

 private:
  
  // Flag true if this array is active
  bool _active;

  /// The array of data
  ArrayOwn1< StorageType > _array;

  /// The start of the Array 
  int _start;

  /// The stride (for interleaved Arrays)
  int _stride;

  /// The size of the data
  int _size;

  /// Normalize foxed point types?
  GLboolean _normalized;

};

#endif

