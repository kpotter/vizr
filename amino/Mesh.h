// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// Mesh.h
// Written by Kristi Potter, 2013
// Class to define a mesh
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _MESH_H
#define _MESH_H

#include "Vector.h"
#include <vector>
#include "Color.h"
#include "BoundingBox.h"

  // * * * Class to hold a geometric mesh & its data * * * //
  class Mesh {

  public:
    
    // --- Constructor --- //
    Mesh( std::vector<Vertex> vertices, std::vector<int> indices,
          std::vector<Vector3f> normals = std::vector<Vector3f>(),
          std::vector<ColorF> colors = std::vector<ColorF>());
    
    // Set the colors to a single color
    void setColor(ColorF color);

    // Set the colors
    void setColors(std::vector<ColorF> colors) { _colors = colors; }


    Vertex getPt(int i) { return _vertices[i]; }
    int getIdx(int i) { return _indices[i]; }
    Vector3f getNorm(int i){ return _normals[i]; }
    ColorF getColor(int i) { return _colors[i]; }

    int getNumIdx(){ return _indices.size(); }
    int getNumPts(){ return _vertices.size(); }

  protected:
    
    // - - - Member variables - - - //
    std::vector<Vector3f> _normals; // The normals, per-vertex
    std::vector<Vertex> _vertices; // The mesh vertices
    std::vector<int> _indices;  // The mesh indices
    std::vector<ColorF> _colors; // The colors, per-vertex
    BoundingBox _bBox; // The bounding box of the mesh
    
    // Find the mesh bounding box
    void findBoundingBox();

    // Rescale the mesh to be within the drawing window
    void rescaleMesh(BoundingBox box);
    
    // Calculate the normals
    void calculateNormals();    

  };

#endif // MESH_H
