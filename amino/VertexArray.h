// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// VertexArray.h
// Written by Kristi Potter, April 2009           
// A vertex array implementation
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef _VERTEX_ARRAY_H
#define _VERTEX_ARRAY_H

#include "GLArrayAttribs.h"

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
/// A GL vertex array
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
template< class StorageType, int Tuple, int GLType >
class VertexArray : public GLArrayAttribs< StorageType >
{
public:

  /// Make the storage type of the array accessible 
  typedef StorageType VStorageType;

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Constructor initially sets the array to active
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  VertexArray(ArrayOwn1< StorageType > array = ArrayOwn1< StorageType >(),
	      int stride = 0, int start = 0) 
    : GLArrayAttribs< StorageType >(array, stride, start) {} 

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Copy constructor
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  VertexArray(const VertexArray &va) 
    :  GLArrayAttribs< StorageType > (va) {}

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  // Equals operator
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  VertexArray &operator=(const VertexArray< StorageType, Tuple,  GLType>  &va)
  {
    GLArrayAttribs< StorageType >::operator=(va);
    return *this;
  }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Destructor
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual ~VertexArray() {}

 protected:

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Enable the vertex array and set the pointer
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual void enableDef()
  { 
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(Tuple, GLType, this->getStride(), this->getStartPointer());
    glErr("enableDef()");
  }

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  /// Disable the index array
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  virtual void disableDef()
  {
    glDisableClientState(GL_VERTEX_ARRAY);
    glErr("disbleDef()");
  }

}; 

// typedef normally used vertex arrays
typedef VertexArray< Vector2s, 2, GL_SHORT > VertexArray2S;
typedef VertexArray< Vector3s, 3, GL_SHORT > VertexArray3S;
typedef VertexArray< Vector4s, 4, GL_SHORT > VertexArray4S;

typedef VertexArray< Vector2i, 2, GL_INT > VertexArray2I;
typedef VertexArray< Vector3i, 3, GL_INT > VertexArray3I;
typedef VertexArray< Vector4i, 4, GL_INT > VertexArray4I;

typedef VertexArray< Vector2f, 2, GL_FLOAT > VertexArray2F;
typedef VertexArray< Vector3f, 3, GL_FLOAT > VertexArray3F;
typedef VertexArray< Vector4f, 4, GL_FLOAT > VertexArray4F;

typedef VertexArray< Vector2d, 2, GL_DOUBLE > VertexArray2D;
typedef VertexArray< Vector3d, 3, GL_DOUBLE > VertexArray3D;
typedef VertexArray< Vector4d, 4, GL_DOUBLE > VertexArray4D;

#endif
