// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                     __    __  __  ____  _  _  _____                     //
//                    /__\  (  \/  )(_  _)( \( )(  _  )                    //
//                   /(__)\  )    (  _)(_  )  (  )(_)(                     //
//                  (__)(__)(_/\/\_)(____)(_)\_)(_____)                    //
//                                       Building Blocks                   //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
// ColorMapFactories.h
// Written by Kristi Potter
// August 2010
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
#ifndef COLORMAPFACTORIES_H
#define COLORMAPFACTORIES_H

#include "colormap.h"

//-----------------------------------------------//
// Some predefined colormaps
//-----------------------------------------------//

// Two-color
class GreyscaleColormapFactory : public TwoColormapFactory {
public:
    GreyscaleColormapFactory(float min=0.0, float max=1.0) : TwoColormapFactory(ColorF(0.f,0.f,0.f), ColorF(1.f,1.f,1.f), min, max){ }
};

class YellowBlueColormapFactory : public TwoColormapFactory{
public:
    YellowBlueColormapFactory(float min=0.0, float max=1.0) : TwoColormapFactory(ColorF(1.f, .98, 0.f), ColorF(0.f, .3, 1.f), min, max){}
};

class BlueYellowColormapFactory : public TwoColormapFactory{
public:
    BlueYellowColormapFactory(float min=0.0, float max=1.0) : TwoColormapFactory(ColorF(0.f, .3, 1.f), ColorF(1.f, .98, 0.f), min, max){}
};

class BlackYellowColormapFactory : public TwoColormapFactory{
public:
    BlackYellowColormapFactory(float min=0.0, float max=1.0) : TwoColormapFactory(ColorF(0.f, 0.f, 0.f), ColorF(1.f, .98, 0.f), min, max){}
};

class PurpleYellowColormapFactory : public TwoColormapFactory{
public:
    PurpleYellowColormapFactory(float min=0.0, float max=1.0) : TwoColormapFactory(ColorF(.22f, .08, 0.69f), ColorF(1.f, 0.83, 0.f), min, max){}
};

class MagentaLimeColormapFactory : public TwoColormapFactory{
public:
    MagentaLimeColormapFactory(float min=0.0, float max=1.0) : TwoColormapFactory(ColorF(.68, 0.04, 0.62), ColorF(.78, .96, 0.05), min, max){}
};

class TealTangerineColormapFactory : public TwoColormapFactory{
public:
  TealTangerineColormapFactory(float min=0.0, float max=1.0) : TwoColormapFactory(ColorF(.03, 0.63, 0.55), ColorF(1.f, .44, 0.05), min, max){}
};

class RedGreenColormapFactory : public TwoColormapFactory{
public:
    RedGreenColormapFactory(float min, float max) : TwoColormapFactory(red, green, min, max){}
};

// Three-color
class RainbowColormapFactory : public ThreeColormapFactory{
public:
    RainbowColormapFactory(float min=0.0, float max=1.0) :
        ThreeColormapFactory(ColorF(0,.4,.8), ColorF(.8,.8,.0), ColorF(.82,.16,.02), min, max){}
};

class GreenYellowOrangeColormapFactory : public ThreeColormapFactory{
public:
    GreenYellowOrangeColormapFactory(float min=0.0, float max=1.0) :
        ThreeColormapFactory(persianGreen, yellow, blazeOrange, min, max){}
};

class MinskTwilightColormapFactory : public ThreeColormapFactory{
public:
    MinskTwilightColormapFactory(float min=0.0, float max=1.0) :
        ThreeColormapFactory( flushMahogany, hokeyPokey, minsk, min, max){}
};


//

/*
class YellowWhiteBlueColormapFactory : public ThreeColormapFactory{
public:
    YellowWhiteBlueColormapFactory(){ super(new String("YLWWHTBLU"), color(255,250,0,255), color(255,255,255,255), color(0,77,255,255)); }
    YellowWhiteBlueColormapFactory(float min, float max){ super(new String("YLWWHTBLU"), color(255,250,0,255), color(255,255,255,255), color(0,77,255,255), min, max); }
};

class RedWhiteGreenColormapFactory : public ThreeColormapFactory{
public:
    RedWhiteGreenColormapFactory(){ super(new String("REDWHTGRN"), color(210, 41,  5), color(255,255,255,255), color(0,204,0,255)); }
    RedWhiteGreenColormapFactory(float min, float max){ super(new String("REDWHTGRN"), color(210, 41,  5,255), color(255,255,255,255), color(0,204,0,255), min, max); }
};*/

#endif // COLORMAPFACTORIES_H
