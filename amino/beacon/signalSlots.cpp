// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                                           ___       ____         __          ___     ___    _     _                                                      //
//                                         (   ,   )    (  ___)      / O \      /    _ )  (      )  (   \(   )                                                    //
//                                          )  ,  <     ) ___)    /  (   ) \   (   (_      ) O (    )       (                                                     //
//                                         (____)  (____)  (__) (__)  \___)  (___)  (_) \_)                                                    //
//                                                                                                   Signals & Slots                                                    //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  //
// signalSlots.cpp
// By Joe Kniss, with help from Yarden Livnat 2002
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#include "signal.h"
#include "signalCall.h"
#include <algorithm>

using namespace std;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Destruct the signal tracker
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
SignalTracker::~SignalTracker()
{
   for(int i = 0; i < int(_sigs.size()); ++i)
      if(_sigs[i]) _sigs[i]->detachSlotInterface(callee);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Detach the signal
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
void SignalTracker::detachSignalInterface(SignalInterface *sig)
{
   SignalPVecIter i = _sigs.begin();
   while( (i = find(_sigs.begin(),_sigs.end(),sig)) != _sigs.end())
   {
     (*i) = 0;
      _sigs.erase(i);
   }
}

