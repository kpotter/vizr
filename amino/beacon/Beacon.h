// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                                           ___       ____         __          ___     ___    _     _                                                      //
//                                         (   ,   )    (  ___)      / O \      /    _ )  (      )  (   \(   )                                                    //
//                                          )  ,  <     ) ___)    /  (   ) \   (   (_      ) O (    )       (                                                     //
//                                         (____)  (____)  (__) (__)  \___)  (___)  (_) \_)                                                    //
//                                                                                                   Signals & Slots                                                    //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  //
// Beacon.h
// By Joe Kniss, with help from Yarden Livnat 2002  
// Class to define signals and slots
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#ifndef _BEACON_H
#define _BEACON_H

// You can quite comfortably ignore most of the classes in this framework
// if you read the documentation in this file <signal.h> for Signal.  
#include <signal.h>


////////////////////////////////////////////////////////////////////////////
/// Connect, use this to connect a Signal to a slot (member function of a
///   class that has "HAS_SLOTS" declared).
/// \code
/// s        = the signal,                             ex: a.theSignal 
///                                                        (MyClassA instance)
/// callee   = instance of the class recieving signal, ex: &b          
///                                                        (MyClassB instance)
/// fncPtr   = the "slot",                             ex: &MyClassB::theSlot
/// priority = the higher the number, the earlier it will be called
/// \endcode
/// 
/// Although this is a template function, 
/// the template parameters are resolved implicitly, so you can call "connect"
/// without specifying any template parameters, ex: 
/// \code connect(a.theSignal, &b, &myClassB::theSlot); \endcode
/// 
/// Here is another, more concrete, example using connect()
/// \code
///   #include <beacon.h>
///   #include <iostream>
///   class MySlotClass {
///   public:
///     HAS_SLOTS;
///     void someSlot(float f) { std::cerr << "someSlot(f): f = " << f; }
///   };
///
///   class MySignalClass {
///   public:
///      Signal<float>  myFloatChanged;
///      void emmitMyFloat(float f) { myFloatChanged(f); }
///   };
///   
///   ///... in some function ... (main maybe?)
///   MySignalClass sigClass;
///   MySlotClass   slotClass;
///   /// connect sigClass.myFloatChanged to slotClass's slot "someSlot(f)"
///   connect(sigClass.myFloatChanged, &slotClass, &MySlotClass::someSlot);
///   /// send a signal
///   sigClass.emmitMyFloat(3.15159265);
///   /// should print "someSlot(f): f = 3.14159265" to stdout
///   /// now disconnect the sig/slot combo
///   disconnect(sigClass.myFloatChanged, &slotClass, &MySlotClass::someSlot);
///   /// try the signal again
///   sigClass.emmitMyFloat(2.7182818);
///   /// shouldn't print anything to stdout!
/// \endcode
/// see also Signal for more documetation on this API \n 
/// see disconnect for the reverse of this function.
////////////////////////////////////////////////////////////////////////////
template<class SIG, class CE, class F>
void connect( SIG &s,          ///< the signal
              CE *callee,      ///< pointer to class with slot
              F fncPtr,        ///< member function of "callee" (the slot)
              int priority     ///< optional priority, higher = earlier called (defaults to 0)
             )
{
   s.addCall(callee, fncPtr, priority);   
}

/// disconnect, the opposite of connect
template<class SIG, class CE, class F>
void disconnect( SIG &s,       ///< the signal
                 CE *callee,   ///< pointer to class with slot
                 F fncPtr      ///< member function of "callee" (the slot)
                )
{
   s.delCall(callee, fncPtr);   
}

#endif


