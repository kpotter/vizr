// -*- C++ -*-
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//
//                                           ___       ____         __          ___     ___    _     _                                                      //
//                                         (   ,   )    (  ___)      / O \      /    _ )  (      )  (   \(   )                                                    //
//                                          )  ,  <     ) ___)    /  (   ) \   (   (_      ) O (    )       (                                                     //
//                                         (____)  (____)  (__) (__)  \___)  (___)  (_) \_)                                                    //
//                                                                                                   Signals & Slots                                                    //
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  //
// signalIF.h 
// Written by Joe Kniss, with help from Yarden Livnat 2002 
// <@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@><@>//

#ifndef _SIGNAL_INTERFACE_H
#define _SIGNAL_INTERFACE_H

//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
/// Signal interface, see Signal, NOT FOR GENERAL USE. (below). 
///   Users can ignore this class
//~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-//
class SignalInterface {
public:
  SignalInterface() {}
  virtual ~SignalInterface() {} 
  
  /// detatch a slot, all references to it!!!
  /// The Signal<> class implements this
  /// and deletes all calls to pointers that match. 
  /// Called by the "SignalTracker" object when
  /// someone declaring "HAS_SLOTS" destructs. 
  virtual void detachSlotInterface(void const *callee) = 0;
};

#endif

